/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.width = '500';

	config.filebrowserBrowseUrl = '/admin/mod_elfinder/elfinder_init/ckeditor_view';
	config.filebrowserImageBrowseUrl = '/admin/mod_elfinder/elfinder_init/ckeditor_view';
	config.filebrowserFlashBrowseUrl = '/admin/mod_elfinder/elfinder_init/ckeditor_view';
	config.filebrowserUploadUrl = '/admin/mod_elfinder/elfinder_init/ckeditor_view';
	config.filebrowserImageUploadUrl = '/admin/mod_elfinder/elfinder_init/ckeditor_view';
	config.filebrowserFlashUploadUrl = '/admin/mod_elfinder/elfinder_init/ckeditor_view';
	config.filebrowserWindowWidth = '1000';
    config.filebrowserWindowHeight = '660';
	
	config.skin = 'bootstrapck';
	config.allowedContent = true;
	config.title = false;
    config.extraPlugins = "codemirror,magicline,lineutils,oembed,locationmap,texttransform,nbsp";
    config.locationMapPath = '/';
	
	config.toolbar_Full =
	[
	    ['Source','-','Save'],
	    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
	    ['Undo','Redo','-','SelectAll','RemoveFormat'],
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','Outdent','Indent','Blockquote'],
	    ['NumberedList','BulletedList'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	    ['Link','Unlink','Anchor'],
	    ['Image','oembed','Flash'/*,'LocationMap'*/,'-','Table','CreateDiv','HorizontalRule','SpecialChar'],
	    ['Format','Font','FontSize','-','TransformTextToUppercase', 'TransformTextToLowercase', 'TransformTextCapitalize', 'TransformTextSwitcher'],
	    ['TextColor','BGColor'],
	    ['Maximize', 'ShowBlocks']
	];

	config.toolbar_Basic =
	[
	    ['Source','-','Save'],
	    ['Cut','Copy','Paste','PasteText','PasteFromWord'],['Bold','Italic','Underline','Strike'],
	    ['Undo','Redo','-','SelectAll','RemoveFormat'],
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','Outdent','Indent','Blockquote'],
	    ['NumberedList','BulletedList','-'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	    ['Link','Unlink'],
	    ['Image','Flash','-','HorizontalRule','SpecialChar'], //,'LocationMap'
	    ['TextColor','BGColor'],
	    ['Maximize', 'ShowBlocks']
	];

	config.toolbar_Simple =
	[
	    ['Source','-','Save'],['Cut','Copy','Paste'],
		['Bold','Italic','Underline','Strike'],
	    ['Undo','Redo'],['NumberedList','BulletedList'],
		['TextColor','BGColor'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	    ['Link','Unlink','Image'],
	    ['Format','FontSize']
	];

	config.toolbar_Newsletter =
	[
	    ['Source'],
	    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
	    ['Undo','Redo','-','SelectAll','RemoveFormat'],
	    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','Outdent','Indent','Blockquote'],
	    ['NumberedList','BulletedList'],
	    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	    ['Link','Unlink'],
	    ['Image','Table','HorizontalRule','SpecialChar'],
	    ['Format','Font','FontSize'],
	    ['TextColor','BGColor'],
	    ['Maximize', 'ShowBlocks']
	];

	config.codemirror = {
	    // Whether or not to show the search Code button on the toolbar
	    showSearchButton: false,
	    // Whether or not to show the format button on the toolbar
	    showFormatButton: false,
	    // Whether or not to show the comment button on the toolbar
	    showCommentButton: false,
	    // Whether or not to show the uncomment button on the toolbar
	    showUncommentButton: false,
        // Whether or not to show the showAutoCompleteButton button on the toolbar
        showAutoCompleteButton: false
	};

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;h4;h5;pre;div';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:Upload';
};
