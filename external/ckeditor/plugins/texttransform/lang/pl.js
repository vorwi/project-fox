/**
 * @author: Önder Ceylan <onderceylan@gmail.com>
 * @copyright Copyright (c) 2013 - Önder Ceylan. All rights reserved.
 * @license Licensed under the terms of GPL, LGPL and MPL licenses.
 * @version 1.0
 *
 * Date: 5/10/13
 * Time: 9:45 AM
 */

// set CKeditor lang
CKEDITOR.plugins.setLang( 'texttransform', 'pl', {
    transformTextSwitchLabel: 'Przełącz małe/WIELKIE litery',
    transformTextToLowercaseLabel: 'małe litery',
    transformTextToUppercaseLabel: 'WIELKIE litery',
    transformTextCapitalizeLabel: 'Pierwsza wielka litera'
});
