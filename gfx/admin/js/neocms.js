$(document).ready(function() {
    //obsługa formularzy dodawania nowych obiektów
    (function() {
        var $mainModal = $('#main_modal');

        $('.add-entity .box-header').click(function(e) {
            var $box = $(this).parents('.box:first'),
                $mTitle = $mainModal.find('.modal-title'),
                $mContent = $mainModal.find('.modal-body'),
                $form;

            $mTitle.html($box.find('.box-title').text());
            $box.find('.box-body').detach().appendTo($mContent);
            $form = $mContent.find('form');

            if (!$form.data('onsubmit-catched')) {
                $form.data('onsubmit-catched', 1);
                $form.on('submit', function(e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    $mContent.isLoading({
                        text: "",
                        position: "overlay"
                    });

                    var formData = new FormData();
                    var formParams = $form.serializeArray();

                    $.each($form.find('input[type="file"]'), function(i, tag) {
                        $.each($(tag)[0].files, function(i, file) {
                            formData.append(tag.name, file);
                        });
                    });

                    $.each(formParams, function(i, val) {
                        formData.append(val.name, val.value);
                    });

                    $.ajax({
                        url: $form.attr('action'),
                        method: $form.attr('method') || 'post',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(response, status) {
                            console.log("ajax.success");
                            if (response.redirect) {
                                setTimeout(function() {
                                    document.location.assign(response.redirect);
                                }, 100);
                            }
                        }
                    }).always(function(response, status, error) {
                        if (error.message) {
                            toastr.error(error.message);
                        }
                        if (typeof error === 'string') {
                            toastr.error(error);
                        }
                        if (response.messages) {
                            _CMS.showMessages(response.messages);
                        }
                        $mContent.isLoading('hide');
                    });
                });
            }

            $mainModal.modal('show');
            $mainModal.one('hidden.bs.modal', function(e) {
                $mContent.find('.box-body').detach().appendTo($box);
            });
        });
    })();

    $('.info-tooltip').tooltip();

    //obsługa odnośników wysyłających formularz nadrzędny po ich kliknięciu
    $(document).on('click', '.click-submit', function(e) {
        e.preventDefault();
        var $parentForm = $(this).parents('form').first(),
            actionUrl = $(this).data('action'),
            confirmTxt = $(this).data('confirm');
        if (!confirmTxt || confirm(confirmTxt)) {
            if (typeof actionUrl !== 'undefined') {
                $parentForm.attr('action', actionUrl);
            }
            $parentForm.submit();
        }
    });
    //obsługa odnośników, których działanie jest poprzedzone komunikatem potwierdzającym
    $(document).on('click', '.click-confirm', function(e) {
        var href = $(this).data('href'),
            confirmTxt = $(this).data('confirm') || 'Czy na pewno?';
        if (!confirm(confirmTxt)) {
            e.preventDefault();
        } else if (typeof href !== 'undefined') {
            $(this).attr('href', href);
        }
    });
    //zaznaczanie zagnieżdżonych checkboxów po kliknięciu w rodzica
    $(document).on('click', '.click-check', function(e) {
        var $checkbox = $(this).find('input[type="checkbox"]'),
            isChecked = $checkbox.is(':checked');
        if (e.target == $checkbox[0]) return;
        $checkbox.attr('checked', !isChecked);
    });

    //okienka modalne dla zdjęć, filmów i innych treści
    if ($.fn.lightGallery) {
        $('.modal-box').lightGallery({
            selector: 'this'
        });
        $('.modal-gallery').lightGallery({
            selector: 'a:has(img)'
        });
    }

    if ($.fn.sortable) {
        // Return a helper with preserved width of cells  
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };
        $(".sortable").sortable({
            helper: fixHelper,
            cursor: "move",
            handle: ".fa-arrows",
            stop: function() {
                var inputs = $(this).find('input.currentposition');
                inputs.each(function(idx) {
                    $(this).val(idx);
                });
            }
        });
    }
    if ($.fn.selectize) {
        $('.select_with_rm, .select-with-rm').selectize({
            plugins: ['remove_button']
        });
        $('.select_with_rm_and_cols, .select-with-rm-and-cols').selectize({
            plugins: ['remove_button', 'optgroup_columns'],
            copyClassesToDropdown: true
        });
        $('.select_simple, .select-simple').selectize();
    }

    if ($.fn.nestedSortable) {
        var ns = $('ol.nestable-sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: '.ns-handle',
            helper: 'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            isTree: true,
            expandOnHover: 700,
            startCollapsed: false
        });
        $('.ns .disclose').on('click', function() {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
        });
        $('.ns .ns-menu button').on('click', function(e) {
            var action = $(this).data('action');
            if (action === 'expand-all') {
                $(this).parents('.ns').find('li.mjs-nestedSortable-collapsed').removeClass('mjs-nestedSortable-collapsed').addClass('mjs-nestedSortable-expanded');
                $(this).parents('.ns').find('.ui-icon-plusthick').removeClass('ui-icon-plusthick').addClass('ui-icon-minusthick');
            }
            if (action === 'collapse-all') {
                $(this).parents('.ns').find('li.mjs-nestedSortable-expanded').removeClass('mjs-nestedSortable-expanded').addClass('mjs-nestedSortable-collapsed');
                $(this).parents('.ns').find('.ui-icon-minusthick').removeClass('ui-icon-minusthick').addClass('ui-icon-plusthick');
            }
        });

        $('.ns-save').click(function() {
            var serialized = $(this).parents('.ns').find('.nestable-sortable').nestedSortable('toArray', { startDepthCount: 0 });
            ns.isLoading({
                text: "",
                position: "overlay"
            });
            $.ajax({
                data: {
                    'csrf_neocms': $('input[name=csrf_neocms]').val(),
                    'items': serialized
                },
                type: 'POST',
                url: $(this).attr('data-url')
            }).always(function(response, status, error) {
                if (error.message) {
                    toastr.error('Wystąpił błąd poczas zapisywania: ' + error.message);
                }
                if (response.messages) {
                    _CMS.showMessages(response.messages);
                }
                ns.isLoading('hide');
            });
        });
    }

    $('#add_category').click(function(e) {
        e.preventDefault();
        new_cat = $('#categories_stub').clone();
        $(new_cat).show();
        $(this).before(new_cat);
    });

    $('.lang_menu a').click(function(e) {
        e.preventDefault();
        var container = $(this).parents('fieldset');
        if (!$(this).parents('li').hasClass('active')) {
            var rel = $(this).data('rel');
            $(container).find('.lang_box, .lang_menu li').removeClass('active');
            $(rel).addClass('active');
            $(this).parents('li').addClass('active');
        }
    });

    $('input.checkall').change(function() {
        if ($(this).attr('checked')) {
            $('input.cat' + $(this).val()).attr('checked', 1);
        } else {
            $('input.cat' + $(this).val()).removeAttr('checked');
        }
    });

    $(".checkall").click(function() {
        var checked_status = this.checked,
            inputs = $(this).parents('form').find("input[name^=check], input.check");
        $(inputs).each(function() {
            this.checked = checked_status;
        });
    });

    $('.checkgroup').click(function() {

        var content = $(this).parents('table').find('tbody');
        var group_id = $(this).attr('data-rel-group-id');
        var checked_status = this.checked;
        var inputs = $(content).find("input[name^=id_group]");
        $(inputs).each(function() {
            if ($(this).val() == group_id) this.checked = checked_status;
        });
    });

    if ($('.datepicker').length > 0) {
        $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
    }
    if ($('.datetimepicker').length > 0) {
        $('.datetimepicker').datetimepicker({
            showSecond: false,
            firstDay: 1,
            timeFormat: 'HH:mm:ss',
            dateFormat: 'yy-mm-dd',
            stepHour: 1,
            stepMinute: 5
        });
    }




    $('.choice input[type="radio"]').change(function() {
        var hid = $(this).parents('.choice').find('.hidden'),
            target = $(this).parents('.choice').data('target');
        if (hid.length == 0 && typeof target != 'undefined') hid = $('#' + target);

        if ($(this).val() == 1) {
            $(hid).show();
        } else {
            $(hid).hide();
        }
    });

    //obsługa przełączania pól językowych
    (function langSwitcher() {
        var $ls = $('.lang-switcher'),
            $form = $ls.closest('form'),
            actLang, $langButtons;
        if ($ls.length > 0) {
            $form.addClass('lang-initialized');
            $ls.find('li').on('click', function(e) {
                e.preventDefault();
                $ls.find('li.active').removeClass('active');
                $(this).addClass('active');
                actLang = $(this).attr('data-lang');
                $langButtons = $form.find('.lang-fields > .nav-tabs > li > a');
                $langButtons.filter('.lang-button-' + actLang).tab('show');
            });
            $ls.find('li:first').trigger('click');
        }
    })();
});

var _CMS = (function() {
    if (toastr) {
        toastr.options = {
            closeButton: true,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: true,
            showDuration: 300,
            hideDuration: 1000,
            timeOut: 10000,
            extendedTimeOut: 2000,
            iconClasses: {
                error: 'alert alert-danger',
                info: 'alert alert-info',
                success: 'alert alert-success',
                warning: 'alert alert-warning'
            },
        };
    }

    return {
        showMessages: function(messages) {
            var types = ['success', 'warning', 'error', 'info'],
                toasterFn;
            if (typeof messages == 'object') {
                $.each(messages, function(i, m) {
                    if (m[0] && typeof m[1] !== 'undefined') {
                        toasterFn = types[m[1]] || 'info';
                        toastr[toasterFn](m[0]);
                    }
                });
            }
        }
    };
})();