$(document).ready(function(){
	var newsletter_preview_html = $('#newsletter_preview_html').val();
	$('#newsletter_preview').attr('srcdoc','<html><head><title></title></head><body>'+newsletter_preview_html)+'</body></html>';
	
	if(CKEDITOR.instances['newsletter'] && $('#newsletter_preview').length>0) {
		CKEDITOR.instances['newsletter'].on('blur', function() { 
			
			var new_inner_content = CKEDITOR.instances['newsletter'].getData();
			//$(newsletter_preview_html).find($('#content_td')).html(new_inner_content);
			
			var replaced_preview_html = newsletter_preview_html.replace(/\r?\n|\r/g,'').replace(/\s+/g,' ').replace(/>\s</g,'><');			
			replaced_preview_html = replaced_preview_html.replace(/<td id="content_td">.*<\/td><\/tr>/,'<td id="content_td">'+new_inner_content+'</td></tr>');
			console.log(replaced_preview_html);
			
			$('#newsletter_preview').attr('srcdoc','<html><head><title></title></head><body style="font: normal 13px Arial">'+replaced_preview_html+'</body></html>');
			$('#newsletter_preview_html').html(replaced_preview_html);
		});
	}
});