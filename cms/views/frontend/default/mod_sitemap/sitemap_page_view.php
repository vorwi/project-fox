<div>
	<ul class="sitemap">
	<?
		if(!empty($sitemap['articles'])){
			foreach($sitemap['articles'] as $row){

				$url = article_url($row);

				echo '<li><a href="'.$url.'" title="'.strip_tags($row->title).'">'.$row->title.'</a>';
				if(isset($row->children)){
					echo '<ul>';
					foreach($row->children as $sub){

						$sub_url = article_url($sub);

						echo '<li><a href="'.$sub_url.'" title="'.strip_tags($sub->title).'">'.$sub->title.'</a>';
							if(isset($sub->children)){
								echo '<ul>';
									foreach($sub->children as $subsub){
										$subsub_url = article_url($subsub);

										echo '<li><a href="'.$subsub_url.'" title="'.strip_tags($subsub->title).'">'.$subsub->title.'</a></li>';
									}
								echo '</ul>';
							}
						echo '</li>';
					}
					echo '</ul>';
				}
				echo '</li>';
			}
		}
	?>
	</ul>
</div>