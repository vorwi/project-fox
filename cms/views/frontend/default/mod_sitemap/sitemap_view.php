<?='<?xml version="1.0" encoding="UTF-8"?>';?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?
	if(!empty($articles)){
		$urls = array();
		foreach($articles as $row){

			$url = article_url($row);
			if(!preg_match('#http(s)?:\/\/'.$_SERVER['HTTP_HOST'].'#i', $url)) continue;

			$urls[] = $url;

			if(isset($row->children)) {
				foreach($row->children as $sub){

					$sub_url = article_url($sub);
					if(!preg_match('#http(s)?:\/\/'.$_SERVER['HTTP_HOST'].'#i', $sub_url)) continue;

					$urls[] = $sub_url;

					if(isset($sub->children)) {
						foreach($sub->children as $subsub){

							$subsub_url = article_url($subsub);
							if(!preg_match('#http(s)?:\/\/'.$_SERVER['HTTP_HOST'].'#i', $subsub_url)) continue;

							echo "<url>\n";
							echo "\t<loc>{$subsub_url}</loc>\n";
							echo "</url>\n";
						}
					}
				}
			}
		}
		$urls = array_unique($urls);
		foreach($urls as $url) {
			echo "<url>\n";
				echo "\t<loc>{$url}</loc>\n";
			echo "</url>\n";
		}
	}
?>
</urlset>