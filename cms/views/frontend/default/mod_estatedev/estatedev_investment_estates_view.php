<div class="clearfix spacer"></div>
<div class="content">
	<div class="content-body">
		<div class="row">
			<? if (isset($selected_buliding) && is_array($selected_buliding)): ?>
				<div class="col-md-6 col-lg-6">
					<div class="estate-tooltip">
						<div class="tip"></div>
						<div class="inner"></div>
						<div class="preloader"></div>
					</div>
					<?
					foreach ($selected_buliding as $building):
						if ($building->face_image):
							?>
							<div class="">
								<div class="image-map floor-sketch">
									<img class="image-top" src="<?= $building->face_image ?>" alt="<?= $building->name ?>" usemap="#facemap<?= $building->id ?>" />
									<map name="facemap<?= $building->id ?>">
										<?
										if (isset($building->face_maps) && is_array($building->face_maps)):
											foreach ($building->face_maps as $face_map):
												?>
												<area shape="poly" data-floor="<?= $face_map->id ?>" coords="<?= $face_map->coords ?>" alt="Budynek <?= $building->name ?> Piętro <?= $face_map->floor ?>">
												<?
											endforeach;
										endif;
										?>
									</map>
									<canvas class="canvas-top"></canvas>
									<canvas class="canvas-bottom"></canvas>
									<img class="image-bottom" src="<?= $building->face_image ?>" />
								</div>
								<?
								if (isset($building->face_maps) && is_array($building->face_maps)):
									foreach ($building->face_maps as $face_map):
										if (isset($face_map->floor_skeches) && is_array($face_map->floor_skeches)):
											foreach ($face_map->floor_skeches as $floor_skech):
												if ($floor_skech->image):
													?>
													<div data-floor="<?= $face_map->id ?>" class="image-map floor-sketch-map">
														<img class="image-top" src="<?= $floor_skech->image ?>" alt="<?= $building->name ?>" usemap="#floormap<?= $floor_skech->id ?>" />
														<map name="floormap<?= $floor_skech->id ?>">
															<?
															if (isset($floor_skech->floor_skeches_maps) && is_array($floor_skech->floor_skeches_maps)):
																foreach ($floor_skech->floor_skeches_maps as $floor_skech_map):
																	?>
																	<area data-floor-sketch="<?= $floor_skech_map->id ?>" data-floor-estate="<?= $floor_skech_map->id_ede ?>" shape="poly" coords="<?= $floor_skech_map->coords ?>" alt="Budynek <?= $building->name ?> Piętro <?= $face_map->floor ?>">
																	<?
																endforeach;
															endif;
															?>
														</map>
														<canvas class="canvas-top"></canvas>
														<canvas class="canvas-bottom"></canvas>
														<img class="image-bottom" src="<?= $floor_skech->image ?>" />
													</div>
													<?
												endif;
											endforeach;
										endif;
									endforeach;
								endif;
								?>
							</div>
							<?
						endif;
					endforeach;
					?>
				</div>
				<script src="js/imageMapResizer.min.js" type="text/javascript"></script>
				<script>
					var tooltipTimeout;
					var resizeTimoeut;

					$(window).resize(function () {
						clearTimeout(resizeTimoeut);
						resizeTimoeut = setTimeout(function () {
							$('.masbud tr').removeClass('marked');
							$('.image-map').each(function (idx, el) {
								var el = $(el);

								var canvas = el.find('canvas.canvas-bottom');
								canvas.attr('height', el.find('img.image-bottom').height()).attr('width', el.find('img.image-bottom').width());
								var draw_canvas = canvas[0].getContext('2d');
								draw_canvas.lineWidth = 3;

								var canvas_selected = el.find('canvas.canvas-top');
								canvas_selected.attr('height', el.find('img.image-bottom').height()).attr('width', el.find('img.image-bottom').width());

								el.find('area').each(function (idx, el) {
									var el = $(el);
									var points = el.attr('coords').split(',');

									draw_canvas.beginPath();
									draw_canvas.moveTo(points[0], points[1]);
									for (var i = 2; i < points.length; i += 2) {
										draw_canvas.lineTo(points[i], points[i + 1]);
									}
									draw_canvas.closePath();
									draw_canvas.fillStyle = 'rgba(255,0,0,0.2)';
									draw_canvas.fill();
									draw_canvas.strokeStyle = 'rgba(255,0,0,0.5)';
									draw_canvas.stroke();
								});
							});
						}, 500);
					});

					$(document).ready(function () {
						$('map').imageMapResize();
					});
					$(window).load(function () {
						$('.image-map').each(function (idx, el) {
							var el = $(el);

							var canvas = el.find('canvas.canvas-bottom');
							canvas.attr('height', el.find('img.image-bottom').height()).attr('width', el.find('img.image-bottom').width());
							var draw_canvas = canvas[0].getContext('2d');
							draw_canvas.lineWidth = 3;

							var canvas_selected = el.find('canvas.canvas-top');
							canvas_selected.attr('height', el.find('img.image-bottom').height()).attr('width', el.find('img.image-bottom').width());

							el.find('area').each(function (idx, el) {
								var el = $(el);
								var points = el.attr('coords').split(',');

								draw_canvas.beginPath();
								draw_canvas.moveTo(points[0], points[1]);
								for (var i = 2; i < points.length; i += 2) {
									draw_canvas.lineTo(points[i], points[i + 1]);
								}
								draw_canvas.closePath();
								draw_canvas.fillStyle = 'rgba(255,0,0,0.2)';
								draw_canvas.fill();
								draw_canvas.strokeStyle = 'rgba(255,0,0,0.5)';
								draw_canvas.stroke();
							});
						});

						$('area[data-floor]').on('click', function (e) {
							e.preventDefault();

							$('.floor-sketch').find('canvas.canvas-top').each(function (idx, el) {
								var canvas = $(el);
								var draw_canvas = canvas[0].getContext('2d');
								draw_canvas.clearRect(0, 0, canvas.attr('width'), canvas.attr('height'));
							});

							var canvas = $(this).parent().parent().find('canvas.canvas-top');
							var draw_canvas = canvas[0].getContext('2d');
							draw_canvas.lineWidth = 3;
							var points = $(this).attr('coords').split(',');
							draw_canvas.beginPath();
							draw_canvas.moveTo(points[0], points[1]);
							for (var i = 2; i < points.length; i += 2) {
								draw_canvas.lineTo(points[i], points[i + 1]);
							}
							draw_canvas.closePath();
							draw_canvas.fillStyle = 'rgba(0,255,0,0.2)';
							draw_canvas.fill();
							draw_canvas.strokeStyle = 'rgba(0,255,0,0.5)';
							draw_canvas.stroke();

							markEstateInFloor($(this).data('floor'));
						});

						$('area[data-floor-sketch]').on('click', function (e) {
							e.preventDefault();

							$('.floor-sketch-map').find('canvas.canvas-top').each(function (idx, el) {
								var canvas = $(el);
								var draw_canvas = canvas[0].getContext('2d');
								draw_canvas.clearRect(0, 0, canvas.attr('width'), canvas.attr('height'));
							});

							var canvas = $(this).parent().parent().find('canvas.canvas-top');
							var draw_canvas = canvas[0].getContext('2d');
							draw_canvas.lineWidth = 2;
							var points = $(this).attr('coords').split(',');
							draw_canvas.beginPath();
							draw_canvas.moveTo(points[0], points[1]);
							var area_boundry = new Object();
							area_boundry.left = 0;
							area_boundry.top = 0;
							area_boundry.bottom = canvas.attr('height');
							for (var i = 2; i < points.length; i += 2) {
								draw_canvas.lineTo(points[i], points[i + 1]);
								if (parseInt(points[i]) > area_boundry.left) {
									area_boundry.left = parseInt(points[i]);
								}
								if (parseInt(points[i + 1]) > area_boundry.top) {
									area_boundry.top = parseInt(points[i + 1]);
								}
								if (parseInt(points[i + 1]) < area_boundry.bottom) {
									area_boundry.bottom = parseInt(points[i + 1]);
								}
							}
							draw_canvas.closePath();
							draw_canvas.fillStyle = 'rgba(0,255,0,0.2)';
							draw_canvas.fill();
							draw_canvas.strokeStyle = 'rgba(0,255,0,5)';
							draw_canvas.stroke();

							markEstateInFloorSketch($(this).data('floor-sketch'));

							if ($(this).data('floor-estate')) {
								area_boundry.top = area_boundry.top + ((area_boundry.bottom - area_boundry.top));
								var pos = $(this).parent().parent().position();
								$('.estate-tooltip').css({top: pos.top + area_boundry.top - 15, left: pos.left + area_boundry.left});
								clearTimeout(tooltipTimeout);
								$('.estate-tooltip').fadeIn(100);
								$('.estate-tooltip .inner').html('');
								$('.estate-tooltip .preloader').fadeIn(250);
								$.ajax({
									dataType: 'html',
									url: '<?= site_url('estate-ajax') ?>/' + $(this).data('floor-estate'),
									success: function (content) {
										clearTimeout(tooltipTimeout);
										tooltipTimeout = setTimeout(function () {
											$('.estate-tooltip .preloader').fadeOut(250);
											$('.estate-tooltip .inner').html(content);
										}, 1000);
									}
								});
							}
						});
						$('area[data-floor-sketch]').on('mouseleave', function () {
							clearTimeout(tooltipTimeout);
							tooltipTimeout = setTimeout(function () {
								$('.estate-tooltip').fadeOut(250);
							}, 500);
						});
						$('.estate-tooltip').on('mouseenter', function () {
							clearTimeout(tooltipTimeout);
						});
						$('.estate-tooltip').on('mouseleave', function () {
							clearTimeout(tooltipTimeout);
							tooltipTimeout = setTimeout(function () {
								$('.estate-tooltip').fadeOut(250);
							}, 500);
						});

						function markEstateInFloor(floor) {
							$('div.floor-sketch-map').not('[data-floor=' + floor + ']').hide();
							$('div.floor-sketch-map[data-floor=' + floor + ']').show();

							$('.masbud').addClass('show-selected');
							$('.masbud tr').not('[data-floor=' + floor + ']').removeClass('marked');
							$('.masbud tr[data-floor=' + floor + ']').addClass('marked');
							
							if($('.masbud tr[data-floor=' + floor + ']').length > 0){
								$('.masbud-no-data').hide();
							} else {
								$('.masbud-no-data').show();
							}
						}
						function markEstateInFloorSketch(floor_sketch) {
							$('.masbud tr').not('[data-floor-skech=' + floor_sketch + ']').removeClass('marked');

							$('.masbud').addClass('show-selected');
							$('.masbud tr[data-floor-skech=' + floor_sketch + ']').addClass('marked');
							if($('.masbud tr[data-floor-skech=' + floor_sketch + ']').length > 0){
								$('.masbud-no-data').hide();
							} else {
								$('.masbud-no-data').show();
							}

						}
						$('div.floor-sketch-map').hide();
					});
				</script>
			<? endif; ?>


			<div class="<? if (isset($selected_buliding) && is_array($selected_buliding)): ?>col-md-6 col-lg-6<? else: ?>col-lg-12<? endif; ?>">
				<div class="table-responsive"> 
					<? if (isset($estates) && is_array($estates) && count($estates) > 0): ?>
						<table data-paging="false" class="table masbud apartmentsearch<? if (isset($selected_buliding) && is_array($selected_buliding)): ?> narrow<? endif; ?>">
							<thead>
								<tr>
									<?
									if (is_array($table_parameters)):
										$i = 0;
										foreach ($table_parameters as $param):
											if ($i > 0):
												?>
												<th><?= $param->name ?></th>
												<?
											endif;
											$i++;
										endforeach;
									endif;
									?>
								</tr>
							</thead>
							<tbody>
								<? foreach ($estates as $estate): ?>
									<tr data-floor="<?= $estate->in_building_floor ?>" data-floor-skech="<?= $estate->in_floor_position ?>">
										<?
										if (is_array($table_parameters)):
											$i = 0;
											foreach ($table_parameters as $param):
												if ($i > 0):
													?>
													<td>
														<?
														// class="apartment-free"
														// class="apartment-sold"
														if (isset($estate->params[$param->position])):
															$estate_param = $estate->params[$param->position];
															if (isset($estate_param->type) && $estate_param->type === 'file'):
																// parametr plikowy (np. schemat)
																if ($estate_param->text_value):
																	?>
																	<a href="<?= $estate_param->text_value ?>" target="_blank" class="apartment-view"></a>
																	<a href="<?= $estate_param->text_value ?>" target="_blank" class="pdf"><img src="images/pdf.png" alt="pdf"/></a>
																	<?
																endif;
															elseif (isset($estate_param->type) && $estate_param->type === 'text'):
																// parametr tekstowy (np. cena)
																?>
																<a href="<?= $estate->url ?>" class="apartment-view"></a>
																<? if (!empty($estate_param->text_value)): ?>
																	<?= $estate_param->text_value ?> <?= $estate_param->unit ?>
																	<?
																endif;
															elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter'):
																?>
																<a href="<?= $estate->url ?>" class="apartment-view"></a>
																<?= $estate_param->value ?>
																<?
															else:
																// parametr predefiniowany (np. status mieszkania)
																?>
																<a href="<?= $estate->url ?>" class="apartment-view"></a>
																<?
																foreach ($estate_param as $estate_param_value):
																	?>
																	<?= $estate_param_value->value ?>
																	<?
																endforeach;
															endif;
														else:
															?>
															<a href="<?= $estate->url ?>" class="apartment-view"></a>
															&nbsp;
														<?
														endif;
														?>
													</td>
													<?
												endif;
												$i++;
											endforeach;
										endif;
										?>
									</tr>
								<? endforeach; ?>
							</tbody>
						</table>

					<? else: ?>
						<?= lang('ed_estates_no_search_results') ?>
					<? endif; ?>
				</div>
				<div class="masbud-no-data">
					<?= lang('ed_estates_no_search_results') ?>
				</div>
			</div>
		</div>
	</div>
</div>