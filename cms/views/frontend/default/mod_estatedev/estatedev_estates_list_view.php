<div class="red-top"></div>
<div class="content subpage">
	<?= $breadcrumbs ?>
</div>
<div class="content">
	<div class="main-search subpage">
		<h3 class="main-header left"><img src="images/icon-main-search.png" alt="icon-main-search"/><?= sprintf(lang('ed_search_for_type'), $estate_type->name) ?></h3>
		<?= form_open($search_link, array('class' => 'main-search-content clearfix')) ?>
		<div class="search-tooltip" data-toggle="tooltip" title="<?= lang('ed_search_tooltip') ?>"></div>
		<?
		if (isset($search_parameters) && is_array($search_parameters)):
			foreach ($search_parameters as $param):
				?>
				<div class="main-search-item">
					<div class="main-search-header">
						<? if ($param->icon): ?>
							<img src="<?= $param->icon ?>" alt="search-surface"/>
						<? endif; ?>
						<p><?= $param->name ?></p>
					</div>
					<div class="main-search-body">
						<div class="dropdown-search-list">
							<span data-default="<?= lang('ed_choose') ?>">
								<? if (isset($selected_search_parameters[$param->url]['min']) && isset($selected_search_parameters[$param->url]['max'])): ?>
									<?= $selected_search_parameters[$param->url]['min'] . ' ' . $param->unit . ' ' . $selected_search_parameters[$param->url]['max'] . ' ' . $param->unit ?>
								<? else: ?>
									<?= lang('ed_choose') ?>
								<? endif; ?>

							</span>
							<div class="search-list">
								<? if ($param->type === 'text'): ?>
									<div class="search-list-item clearfix param<?= $param->id ?>">
										<div class="search-from-to">
											<div class="search-from">
												<label><?= lang('ed_search_from') ?></label>
												<input name="param[<?= $param->id ?>][from]" type="text" value="<?
												if (isset($selected_search_parameters[$param->url]['min'])): echo $selected_search_parameters[$param->url]['min'];
												endif;
												?>" class="input-search-from">
												<span class="text-top-right"><?= $param->unit ?></span>
											</div>
											<div class="search-to">
												<label><?= lang('ed_search_to') ?></label>
												<input name="param[<?= $param->id ?>][to]" type="text" value="<?
												if (isset($selected_search_parameters[$param->url]['max'])): echo $selected_search_parameters[$param->url]['max'];
												endif;
												?>" class="input-search-to">
												<span class="text-top-right"><?= $param->unit ?></span>
											</div>
										</div>
										<div class="search-slider"></div>
										<div class="slider-background"></div>
									</div>
									<script>
										var $range = $(".param<?= $param->id ?> .search-slider"),
												$from<?= $param->id ?> = $(".param<?= $param->id ?> .input-search-from"),
												$to<?= $param->id ?> = $(".param<?= $param->id ?> .input-search-to"),
												range<?= $param->id ?>,
												min<?= $param->id ?> = <?= $param->min ?>,
												max<?= $param->id ?> = <?= $param->max ?>,
												from<?= $param->id ?>,
												to<?= $param->id ?>;
										var updateValues<?= $param->id ?> = function () {
											$from<?= $param->id ?>.prop("value", from<?= $param->id ?>);
											$to<?= $param->id ?>.prop("value", to<?= $param->id ?>);
											$from<?= $param->id ?>.closest('.dropdown-search-list').find('> span').html('<?= lang('ed_search_from') ?> ' + from<?= $param->id ?> + '<?= $param->unit ?> <?= lang('ed_search_to') ?> ' + to<?= $param->id ?> + '<?= $param->unit ?>');
										};
										$range.ionRangeSlider({
											type: "double",
											min: min<?= $param->id ?>,
											max: max<?= $param->id ?>,
											prettify_enabled: false,
											grid: true,
											hide_min_max: true,
											hide_from_to: true,
											grid_num: 5,
											onChange: function (data) {
												from<?= $param->id ?> = data.from;
												to<?= $param->id ?> = data.to;
												updateValues<?= $param->id ?>();
											}
										});
										range<?= $param->id ?> = $range.data("ionRangeSlider");
										var updateRange<?= $param->id ?> = function () {
											range<?= $param->id ?>.update({
												from: from<?= $param->id ?>,
												to: to<?= $param->id ?>
											});
										};
										$from<?= $param->id ?>.on("change", function () {
											from<?= $param->id ?> = +$(this).prop("value");
											if (from<?= $param->id ?> < min<?= $param->id ?>) {
												from<?= $param->id ?> = min<?= $param->id ?>;
											}
											if (from<?= $param->id ?> > to<?= $param->id ?>) {
												from<?= $param->id ?> = to<?= $param->id ?>;
											}
											updateValues<?= $param->id ?>();
											updateRange<?= $param->id ?>();
										});
										$to<?= $param->id ?>.on("change", function () {
											to<?= $param->id ?> = +$(this).prop("value");
											if (to<?= $param->id ?> > max<?= $param->id ?>) {
												to<?= $param->id ?> = max<?= $param->id ?>;
											}
											if (to<?= $param->id ?> < from<?= $param->id ?>) {
												to<?= $param->id ?> = from<?= $param->id ?>;
											}
											updateValues<?= $param->id ?>();
											updateRange<?= $param->id ?>();
										});
									</script>
									<?
								else:
									if (isset($param->values) && is_array($param->values)):
										foreach ($param->values as $value):
											?>
											<div class="search-list-item clearfix">
												<input<? if (isset($selected_search_parameters[$param->url]) && in_array($value->value, $selected_search_parameters[$param->url])): ?> checked="checked"<? endif; ?> id="param<?= $value->id ?>" name="param[<?= $param->id ?>][<?= $value->id ?>]" value="<?= $value->value ?>" type="checkbox">
												<label for="param<?= $value->id ?>">
													<span><?= $value->label ?></span>
												</label>
											</div>
											<?
										endforeach;
									endif;

								endif;
								?>
								<div class="search-list-close"></div>
							</div>
						</div>
					</div>
				</div>
				<?
			endforeach;
		endif;
		?>
		<div class="main-search-item">
			<div class="main-search-header">
				<img src="images/search-magnifier.png" alt="search-magnifier"/>
				<p><?= lang('ed_confirm') ?></p>
			</div>
			<div class="main-search-body">
				<button type="submit" class="masbud-button"><?= lang('ed_search') ?></button>
			</div>
		</div>
		<?= form_close() ?>
	</div>

	<div class="content-body">
		<h3 class="main-header left"><img src="images/icon-main-search.png" alt="icon-main-search"/><?= lang('ed_search_results') ?></h3>
		<div class="table-responsive"> 
			<? if (isset($estates) && is_array($estates) && count($estates) > 0): ?>
				<table class="table masbud apartmentsearch">
					<thead>
						<tr>
							<?
							if (is_array($table_parameters)):
								foreach ($table_parameters as $param):
									?>
									<th><?= $param->name ?></th>
									<?
								endforeach;
							endif;
							?>
						</tr>
					</thead>
					<tbody>
						<? foreach ($estates as $estate): ?>
							<tr>
								<?
								if (is_array($table_parameters)):
									foreach ($table_parameters as $param):
										?>
										<td>
											<?
											// class="apartment-free"
											// class="apartment-sold"
											if (isset($estate->params[$param->position])):
												$estate_param = $estate->params[$param->position];
												if (isset($estate_param->type) && $estate_param->type === 'file'):
													// parametr plikowy (np. schemat)
													if ($estate_param->text_value):
														?>
														<a href="<?= $estate_param->text_value ?>" target="_blank" class="apartment-view"></a>
														<a href="<?= $estate_param->text_value ?>" target="_blank" class="pdf"><img src="images/pdf.png" alt="pdf"/></a>
														<?
													endif;
												elseif (isset($estate_param->type) && $estate_param->type === 'text'):
													// parametr tekstowy (np. cena)
													?>
													<a href="<?= $estate->url ?>" class="apartment-view"></a>
													<? if (!empty($estate_param->text_value)): ?>
														<?= $estate_param->text_value ?> <?= $estate_param->unit ?>
														<?
													endif;
												elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter'):
													?>
													<a href="<?= $estate->url ?>" class="apartment-view"></a>
													<?= $estate_param->value ?>
													<?
												else:
													// parametr predefiniowany (np. status mieszkania)
													?>
													<a href="<?= $estate->url ?>" class="apartment-view"></a>
													<?
													foreach ($estate_param as $estate_param_value):
														?>
														<?= $estate_param_value->value ?>
														<?
													endforeach;
												endif;
											else:
												?>
												<a href="<?= $estate->url ?>" class="apartment-view"></a>
												&nbsp;
											<?
											endif;
											?>
										</td>
										<?
									endforeach;
								endif;
								?>
							</tr>
						<? endforeach; ?>
					</tbody>
				</table>
			<? else: ?>
				<?= lang('ed_estates_no_search_results') ?>
			<? endif; ?>
		</div>
	</div>
</div>
<div class="investments-slider-wrapper">
	<div class="content">
		<div class="content-body">
			<? if (isset($investments) && is_array($investments)): ?>
				<div class="subpage-line"></div>
				<h3 class="main-header left"><img src="images/icon-investments.png" alt="icon-investments"/><?= lang('ed_investments') ?></h3>
				<div class="investments-slider">
					<? foreach ($investments as $investment): ?>
						<div class="investments-slider-item">
							<div class="investments-slider-header" style="background-image: url('<?= $investment->image ?>')"></div>
							<div class="investments-slider-body">
								<img src="images/investments-arrow-up.png" alt="investments-arrow-up"/>
								<p class="settlement"><?= $investment->boxname->l1 ?></p>
								<h3 class="street"><?= $investment->boxname->l2 ?></h3>
								<p class="apartment-number"><?= $investment->boxname->l3 ?></p>
								<div class="buttons">
									<a href="<?= $investment->url ?>" class="investments-more"><?= lang('ed_investment_desc') ?><img src="images/investments-arrow-more.png" alt="" class="off"><img src="images/investments-arrow-more-white.png" alt="" class="on"></a>
									<? if (isset($investment->url_estates) && !empty($investment->url_estates)): ?>
										<a href="<?= $investment->url_estates ?>" class="apartments-more"><?= lang('ed_investment_estates') ?><img src="images/investments-arrow-more.png" alt="" class="off"><img src="images/investments-arrow-more-white.png" alt="" class="on"></a>
									<? endif; ?>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			<? endif; ?>
		</div>
	</div>
</div>
