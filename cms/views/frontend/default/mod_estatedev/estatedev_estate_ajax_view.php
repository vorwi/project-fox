<? if (isset($error)): ?>
	<?= $error ?>
<? else: ?>
	<h3 class="main-header left"><?= $estate_type->name . ' ' . $estate->name ?></h3>
	<div class="estate-data">
		<table class="estate-parameters-table">
			<?
			if (isset($estate->params)):
				foreach ($estate->params as $estate_param):
					if (is_object($estate_param)):
						$presentation = $estate_param->presentation;
					else:
						$presentation = $estate_param[0]->presentation;
					endif;
					if ($presentation === '1'):
						?>
						<tr>
							<?
							if (isset($estate_param->type) && $estate_param->type === 'file'):
								// parametr plikowy (np. schemat)
								if ($estate_param->text_value):
									?>
									<td>
										<?= $estate_param->name ?>:
									</td>
									<td>
										<a href="<?= $estate_param->text_value ?>" target="_blank" class="pdf"><img src="images/pdf.png" alt="pdf"/></a>
									</td>
									<?
								endif;
							elseif (isset($estate_param->type) && $estate_param->type === 'text'):
								// parametr tekstowy (np. cena)
								if (!empty($estate_param->text_value)):
									?>
									<td>
										<?= $estate_param->name ?>:
									</td>
									<td>
										<?= $estate_param->text_value ?> <?= $estate_param->unit ?>
									</td>
									<?
								endif;
							elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter'):
								?>
								<td>
									<?= $estate_param->name ?>:
								</td>
								<td>
									<?= $estate_param->value ?>
								</td>
								<?
							else:
								// parametr predefiniowany (np. status mieszkania)
								?>
								<td><?= $estate_param[0]->name ?>:</td>
								<td>
									<?
									foreach ($estate_param as $estate_param_value):
										echo $estate_param_value->value;
									endforeach;
									?>
								</td>
							<?
							endif;
							?>
						</tr>
						<?
					endif;
				endforeach;
			endif;
			?>
		</table>

		<?
		// parametry jako przyciski pod tabelą (np. schemat)
		if (isset($estate->params)):
			foreach ($estate->params as $estate_param):
				if (is_object($estate_param)):
					$presentation = $estate_param->presentation;
				else:
					$presentation = $estate_param[0]->presentation;
				endif;
				if ($presentation === '2'):
					if (isset($estate_param->type) && $estate_param->type === 'file'):
						// parametr plikowy (np. schemat)
						if ($estate_param->text_value):
							?>
							<a href="<?= $estate_param->text_value ?>" target="_blank" class="estate-parameters-button">
								<?= sprintf(lang('ed_estate_see'), strtolower($estate_param->name)) ?>
							</a>
							<?
						endif;
					elseif (isset($estate_param->type) && $estate_param->type === 'text'):
						// parametr tekstowy (np. cena)
						if (!empty($estate_param->text_value)):
							?>
							<a href="#<?= $estate_param->url ?>" target="_blank" class="estate-parameters-button">
								<?= $estate_param->name ?>: <?= $estate_param->text_value ?> <?= $estate_param->unit ?>
							</a>
							<?
						endif;
					elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter'):
						?>
						<a href="#<?= $estate_param->url ?>" target="_blank" class="estate-parameters-button">
							<?= $estate_param->name ?>: <?= $estate_param->value ?>
						</a>
						<?
					else:
						// parametr predefiniowany (np. status mieszkania)
						?>
						<a href="#<?= $estate_param->url ?>" target="_blank" class="estate-parameters-button">
							<?= $estate_param[0]->name ?>: 
							<?
							foreach ($estate_param as $estate_param_value):
								echo $estate_param_value->value;
							endforeach;
							?>
						</a>
					<?
					endif;
				endif;
			endforeach;
		endif;
		?>
		<a href="<?= $estate->url ?>" target="_blank" class="estate-parameters-button">
			<?= sprintf(lang('ed_estate_tooltip_see'), $estate_type->name) ?>
		</a>

	</div>
<? endif; ?>