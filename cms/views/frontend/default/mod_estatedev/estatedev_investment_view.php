<div class="investment-bar-wrapper">
	<div class="content">
		<div class="content-body">
			<div class="row">
				<div class="select">
					<div class="title">
						<?= lang('ed_investment') ?>:
					</div>
					<div class="selected">
						<?= $investment->name ?>
					</div>
					<div class="items-list">
						<? foreach ($investments as $item): ?>
							<a href="<?= $item->url ?>" class="item<? if ($item->id === $investment->id): ?> active<? endif; ?>">
								<?= $item->name ?>
							</a>
						<? endforeach; ?>
					</div>
				</div>
				<? if (isset($this->params) && $this->params !== false): ?>
					<div class="select-building">
						<div class="title">
							<?= lang('ed_building') ?>:
						</div>
						<div class="selected">
							<? if (isset($building)): ?>
								<?= $building->name ?>
							<? else: ?>
								<?= lang('ed_building_all') ?>
							<? endif; ?>
						</div>
						<div class="items-list">
							<a href="<?= site_url($bulding_url) ?>" class="item<? if (!isset($this->params[$building_param->url]) || !is_array($this->params[$building_param->url])): ?> active<? endif; ?>">
								<?= lang('ed_building_all') ?>
							</a>
							<?
							if (isset($bulidings) && is_array($bulidings)):
								foreach ($bulidings as $item):
									?>
									<a href="<?= site_url($bulding_url . '__' . $building_param->url . '_' . $item->id) ?>" class="item<? if (isset($this->params[$building_param->url]) && is_array($this->params[$building_param->url]) && in_array($item->id, $this->params[$building_param->url])): ?> active<? endif; ?>">
										<?= $item->name ?>
									</a>
									<?
								endforeach;
							endif;
							?>
						</div>
					</div>
				<? endif; ?>


				<div class="tabs">
					<a<? if (!isset($this->tab['filters']) || empty($this->tab['filters'])): ?> class="active"<? endif; ?> href="<?= site_url($investment->url) ?>">
						<img src="../images/icon-investments.png" alt="" class="off"/>
						<img src="../images/icon-investments-white.png" alt="" class="on"/>
						<?= lang('ed_investment_desc_upper') ?>
					</a>
					<? if (isset($types) && is_array($types)): ?>
						<? foreach ($types as $type): ?>
							<a<? if (isset($this->params[$type->url]) && in_array($type->id, $this->params[$type->url])): ?> class="active"<? endif; ?> href="<?= site_url($investment->url . '/' . $type->url . '_' . $type->id) ?>">
								<img src="<?= $type->icon_inactive ?>" alt="" class="off"/>
								<img src="<?= $type->icon_active ?>" alt="" class="on"/>
								<?= $type->name_plural ?>
							</a>
						<? endforeach; ?>
					<? endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?= $content ?>