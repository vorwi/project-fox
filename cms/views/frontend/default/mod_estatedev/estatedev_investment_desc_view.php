<? if (isset($images) && is_array($images)): ?>
	<div class="investment-images-wrapper">
		<div class="investment-images">
			<?
			foreach ($images as $img):
				?>
				<div class="slide">
					<img src="<?= $img->image ?>" />
				</div>
				<?
			endforeach;
			?>
		</div>
		<div class="investment-images-next"></div>
		<div class="investment-images-prev"></div>
	</div>
<? else: ?>

<? endif; ?>
<div class="content">
	<div class="content-body">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="main-header left red"><?= $investment->name ?></h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 investment-description">
				<?= $investment->description ?>
			</div>
		</div>
		<div class="row">
			<div class="investment-buttons col-lg-12">
				<?
				if (isset($types) && is_array($types)):
					$i = 0;
					foreach ($types as $type):
						?>
						<a class="investment-button<? if ($i === 0): ?> active<? endif; ?>" href="<?= site_url($investment->url . '/' . $type->url . '_' . $type->id) ?>">
							<?= sprintf(lang('ed_estate_see'), strtolower($type->name_plural)) ?>
							<img src="<?= $type->icon_active ?>" alt="" class="on"/>
						</a>
						<?
						$i++;
					endforeach;
				endif;
				?>
			</div>
		</div>
	</div>
</div>
<? if (!empty($investment->map_coords)): ?>
	<div class="content">
		<div class="content-body">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="main-header left"><img src="images/investment-localization.png" alt=""/> Lokalizacja</h3>
				</div>
			</div>
		</div>
	</div>
	<div id="map" class="investment-localization-map"></div>
	<script>
		var map;
		function initMap() {
			var position = new google.maps.LatLng<?= $investment->map_coords ?>;

			map = new google.maps.Map(document.getElementById('map'), {
				center: position,
				zoom: 14,
				scrollwheel: false
			});
			var marker = new google.maps.Marker({
				position: position,
				map: map,
				draggable: false,
				title: "<?= $investment->address ?>"
			});
		}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>
<? endif; ?>

<div class="investments-slider-wrapper">
	<div class="content">
		<div class="content-body">
			<? if (isset($investments) && is_array($investments)): ?>
				<div class="subpage-line"></div>
				<h3 class="main-header left"><img src="images/icon-investments.png" alt="icon-investments"/><?= lang('ed_investments') ?></h3>
				<div class="investments-slider">
					<? foreach ($investments as $invest): ?>
						<div class="investments-slider-item<? if ($invest->id === $investment->id): ?> active<? endif; ?>">
							<div class="investments-slider-header" style="background-image: url('<?= $invest->image ?>')"></div>
							<div class="investments-slider-body">
								<img src="images/investments-arrow-up.png" alt="investments-arrow-up" class="off"/>
								<img src="images/investments-arrow-up-white.png" alt="investments-arrow-up" class="on"/>
								<p class="settlement"><?= $invest->boxname->l1 ?></p>
								<h3 class="street"><?= $invest->boxname->l2 ?></h3>
								<p class="apartment-number"><?= $invest->boxname->l3 ?></p>
								<div class="buttons">
									<a href="<?= $invest->url ?>" class="investments-more"><?= lang('ed_investment_desc') ?><img src="images/investments-arrow-more.png" alt="" class="off"><img src="images/investments-arrow-more-white.png" alt="" class="on"></a>
									<? if (isset($invest->url_estates) && !empty($invest->url_estates)): ?>
										<a href="<?= $invest->url_estates ?>" class="apartments-more"><?= lang('ed_investment_estates') ?><img src="images/investments-arrow-more.png" alt="" class="off"><img src="images/investments-arrow-more-white.png" alt="" class="on"></a>
									<? endif; ?>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			<? endif; ?>
		</div>
	</div>
</div>