<div class="red-top"></div>
<div class="content subpage">

	<?= $breadcrumbs ?>
	<div class="row">
		<div class="col-lg-12">
			<h3 class="main-header left"><img src="images/icon-investments.png" alt=""/><?= $estate_type->name . ' ' . $estate->name ?></h3>
		</div>
	</div>
	<div class="row estate-data">
		<div class="col-md-6 col-lg-6">
			<table class="estate-parameters-table">
				<?
				if (isset($estate->params)):
					foreach ($estate->params as $estate_param):
						if (is_object($estate_param)):
							$presentation = $estate_param->presentation;
						else:
							$presentation = $estate_param[0]->presentation;
						endif;
						if ($presentation === '1'):
							?>
							<tr>
								<?
								if (isset($estate_param->type) && $estate_param->type === 'file'):
									// parametr plikowy (np. schemat)
									if ($estate_param->text_value):
										?>
										<td>
											<?= $estate_param->name ?>:
										</td>
										<td>
											<a href="<?= $estate_param->text_value ?>" target="_blank" class="pdf"><img src="images/pdf.png" alt="pdf"/></a>
										</td>
										<?
									endif;
								elseif (isset($estate_param->type) && $estate_param->type === 'text'):
									// parametr tekstowy (np. cena)
									if (!empty($estate_param->text_value)):
										?>
										<td>
											<?= $estate_param->name ?>:
										</td>
										<td>
											<?= $estate_param->text_value ?> <?= $estate_param->unit ?>
										</td>
										<?
									endif;
								elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter'):
									?>
									<td>
										<?= $estate_param->name ?>:
									</td>
									<td>
										<?= $estate_param->value ?>
									</td>
									<?
								else:
									// parametr predefiniowany (np. status mieszkania)
									?>
									<td><?= $estate_param[0]->name ?>:</td>
									<td>
										<?
										foreach ($estate_param as $estate_param_value):
											echo $estate_param_value->value;
										endforeach;
										?>
									</td>
								<?
								endif;
								?>
							</tr>
							<?
						endif;
					endforeach;
				endif;
				?>
			</table>

			<?
			// parametry jako przyciski pod tabelą (np. schemat)
			if (isset($estate->params)):
				foreach ($estate->params as $estate_param):
					if (is_object($estate_param)):
						$presentation = $estate_param->presentation;
					else:
						$presentation = $estate_param[0]->presentation;
					endif;
					if ($presentation === '2'):
						if (isset($estate_param->type) && $estate_param->type === 'file'):
							// parametr plikowy (np. schemat)
							if ($estate_param->text_value):
								?>
								<a href="<?= $estate_param->text_value ?>" target="_blank" class="estate-parameters-button">
									<?= sprintf(lang('ed_estate_see'), strtolower($estate_param->name)) ?>
									<? if ($estate_param->icon_button): ?>
										<img src="<?= $estate_param->icon_button ?>" alt="<?= $estate_param->name ?>" />
									<? endif; ?>
								</a>
								<?
							endif;
						elseif (isset($estate_param->type) && $estate_param->type === 'text'):
							// parametr tekstowy (np. cena)
							if (!empty($estate_param->text_value)):
								?>
								<a href="#<?= $estate_param->url ?>" target="_blank" class="estate-parameters-button">
									<?= $estate_param->name ?>: <?= $estate_param->text_value ?> <?= $estate_param->unit ?>
									<? if ($estate_param->icon_button): ?>
										<img src="<?= $estate_param->icon_button ?>" alt="<?= $estate_param->name ?>" />
									<? endif; ?>
								</a>
								<?
							endif;
						elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter'):
							?>
							<a href="#<?= $estate_param->url ?>" target="_blank" class="estate-parameters-button">
								<?= $estate_param->name ?>: <?= $estate_param->value ?>
								<? if ($estate_param->icon_button): ?>
									<img src="<?= $estate_param->icon_button ?>" alt="<?= $estate_param->name ?>" />
								<? endif; ?>
							</a>
							<?
						else:
							// parametr predefiniowany (np. status mieszkania)
							?>
							<a href="#<?= $estate_param->url ?>" target="_blank" class="estate-parameters-button">
								<?= $estate_param[0]->name ?>: 
								<?
								foreach ($estate_param as $estate_param_value):
									echo $estate_param_value->value;
								endforeach;
								?>
								<? if ($estate_param[0]->icon_button): ?>
									<img src="<?= $estate_param[0]->icon_button ?>" alt="<?= $estate_param[0]->name ?>" />
								<? endif; ?>
							</a>
						<?
						endif;
					endif;
				endforeach;
			endif;
			?>
			<a href="" onclick="return false;" target="_blank" class="estate-parameters-button">
				<?= lang('ed_estate_question') ?>
				<img src="images/estate-icon-email.png" alt="<?= lang('ed_estate_question') ?>" />
			</a>
			<a href="" onclick="return false;" target="_blank" class="estate-parameters-button">
				<?= lang('ed_estate_print') ?>
				<img src="images/estate-icon-print.png" alt="<?= lang('ed_estate_print') ?>" />
			</a>
		</div>
		<div class="col-md-6 col-lg-6">
			<?
			// parametry w prawej kolumnie (np. rzut)
			// pomijamy te, które nie są plikiem
			if (isset($estate->params)):
				foreach ($estate->params as $estate_param):
					if (is_object($estate_param)):
						$presentation = $estate_param->presentation;
					else:
						$presentation = $estate_param[0]->presentation;
					endif;
					if ($presentation === '3'):
						if (isset($estate_param->type) && $estate_param->type === 'file'):
							// parametr plikowy (np. schemat)
							if ($estate_param->text_value):
								?>
								<img src="<?= $estate_param->text_value ?>" alt="<?= $estate_param->name ?>" class="estate-image" />
								<?
							endif;
						elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter'):
							?>
							<img src="<?= $estate_param->value ?>" alt="<?= $estate_param->name ?>" class="estate-image" />
							<?
						endif;
					endif;
				endforeach;
			endif;
			?>
		</div>
	</div>
	<? if (!empty($estate->admission)): ?>
		<div class="row estate-admission">
			<div class="col-lg-12">
				<?= $estate->admission ?>
			</div>
		</div>
	<? endif; ?>
	<? if (!empty($estate->description)): ?>
		<div class="row estate-description">
			<div class="col-lg-12">
				<?= $estate->description ?>
			</div>
		</div>
	<? endif; ?>
</div>

<div class="estate-contact-wrapper">
	<div class="content">
		<div class="content-body">
			<? if (!empty($contact)): ?>
				<div class="row estate-contact">
					<div class="col-sm-6 col-md-6 col-lg-6">
						<h3 class="estate-contact-header"><?= lang('ed_estate_contact') ?></h3>
						<? foreach ($contact as $con): ?>
							<div class="estate-contact-row">
								<? if ($con->image): ?>
									<img class="estate-contact-image" src="<?= $con->image ?>" alt="<?= $con->name ?> <?= $con->surname ?>"/>
								<? endif; ?>
								<div class="estate-contact-name"><?= $con->name ?> <?= $con->surname ?></div>
								<div class="estate-contact-button"><img src="images/estate-contact-phone.png" /> <?= $con->phone ?></div>
								<a href="mailto:<?= $con->email ?>" class="estate-contact-button"><img src="images/estate-contact-email.png" /> <?= $con->email ?></a>
							</div>
						<? endforeach; ?>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-6">
						<?= $estate->investment_admission ?>
						<a class="estate-contact-more" href="<?= $estate->investment_url ?>">więcej <img src="images/investments-arrow-more.png" /></a>
					</div>
				</div>
			<? else: ?>
				<div class="col-lg-12">
					<?= $estate->investment_admission ?>
					<a class="estate-contact-more" href="<?= $estate->investment_url ?>">więcej <img src="images/investments-arrow-more.png" /></a>
				</div>
			<? endif; ?>
		</div>
	</div>
</div>

<div class="investments-slider-wrapper no-gradient">
	<div class="content">
		<div class="content-body">
			<? if (isset($investments) && is_array($investments)): ?>
				<div class="subpage-line"></div>
				<h3 class="main-header left"><img src="images/icon-investments.png" alt="icon-investments"/><?= lang('ed_investments') ?></h3>
				<div class="investments-slider">
					<? foreach ($investments as $investment): ?>
						<div class="investments-slider-item">
							<div class="investments-slider-header" style="background-image: url('<?= $investment->image ?>')"></div>
							<div class="investments-slider-body">
								<img src="images/investments-arrow-up.png" alt="investments-arrow-up"/>
								<p class="settlement"><?= $investment->boxname->l1 ?></p>
								<h3 class="street"><?= $investment->boxname->l2 ?></h3>
								<p class="apartment-number"><?= $investment->boxname->l3 ?></p>
								<div class="buttons">
									<a href="<?= $investment->url ?>" class="investments-more"><?= lang('ed_investment_desc') ?><img src="images/investments-arrow-more-white.png" alt="" class="on"><img src="images/investments-arrow-more.png" alt="" class="off"></a>
									<? if (isset($investment->url_estates) && !empty($investment->url_estates)): ?>
										<a href="<?= $investment->url_estates ?>" class="apartments-more"><?= lang('ed_investment_estates') ?><img src="images/investments-arrow-more-white.png" alt="" class="on"><img src="images/investments-arrow-more.png" alt="" class="off"></a>
									<? endif; ?>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			<? endif; ?>
		</div>
	</div>
</div>