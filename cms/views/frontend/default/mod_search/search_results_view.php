<?=form_open(config_item('px_search'), array('method' => 'post', 'class' => 'search')) ?>
	<input type="text" name="query" value="<?=set_value('query')?>" />
	<input type="submit" name="szukaj" value="<?=lang('szukaj')?>" />
<?=form_close()?>
<?php if(empty($search_string)) {?> 
<p><?=lang('Prosimy wpisać szukane słowo lub frazę w pole wyszukiwarki.')?></p>
<?php } else {?>
<p>Znaleziono <b><?=$count?></b> wyników dla frazy <em><?=$search_string?></em></p>
<?php if(!empty($results)) {?>
<?php foreach($results as $result) {?>
<div style="border-bottom: 1px solid #E6E5EA;">
	<h3><a href="<?=$result->url?>"><?=$result->name?></a></h3>
	<p><?=substr_full_word(strip_tags($result->content), 330, '...')?></p>
	<!-- <p class="wiecej"><a href="<?=$url?>">&gt; <?=lang('czytaj więcej')?></a></p> -->
</div>
<?php }?>
<div class="clr"></div>
<?=$this->pagination->create_links()?>
<?php } ?>
<?php }?>