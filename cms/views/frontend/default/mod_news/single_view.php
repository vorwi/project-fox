<?
if(!empty($news)) {
	echo '<div class="news">';
	
	$public_dir = config_item('upload_path').(!isset($this->img_dir) ? $img_dir : $this->img_dir);
	$dir = './'.config_item('site_path').$public_dir;
	$tab = explode('.',$news->img);
	
	if(isset($tab[1])) {
		echo '<div class="img">';
		$img_thumb = $dir.$tab[0].'_thumb.'.$tab[1];
		if(file_exists($img_thumb)) echo '<a href="'.$public_dir.$news->img.'" rel="clearbox"><img src="'.$public_dir.$tab[0].'_thumb.'.$tab[1].'" alt="" /></a>';
		echo '</div>';
	}
	//echo '<p class="news_date">'.date('d.m.Y', strtotime($news->date_add)).'</p>';
	
	echo $news->content;
	
	echo '</div>';
}
?>