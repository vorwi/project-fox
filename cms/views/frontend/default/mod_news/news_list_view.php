<? if(!empty($news)) { ?>

<?
	foreach($news as $row) {
		echo '<div class="news">';
	
		$public_dir = config_item('upload_path').(!isset($this->img_dir) ? $img_dir : $this->img_dir);
		$dir = './'.config_item('site_path').$public_dir;
		$tab = explode('.',$row->img);
		
		if(!empty($row->target)){
			$target = ' target="'.$row->target.'"';
		}else{
			$target = '';
		}
		
		echo '<h2><a href="'.news_url($row).'">'.$row->title.'</a></h2>';
		if(isset($tab[1])) {
			echo '<div class="img">';
			$img_thumb = $dir.$tab[0].'_thumb.'.$tab[1];
			if(file_exists($img_thumb)) echo '<a href="'.$public_dir.$row->img.'" rel="clearbox"><img src="'.$public_dir.$tab[0].'_thumb.'.$tab[1].'" alt="" /></a>';
			echo '</div>';
		}
		//echo '<p class="news_date">'.date('d.m.Y', strtotime($row->date_add)).'</p>';
		
		echo $row->admission;
		
		echo '<p class="more"><a'.$target.' href="'.news_url($row).'" class="button_white">'.lang('nw_read_more').'</a></p>';
		echo '</div>';
	}
?> 

<? if(isset($echo_pag) && $echo_pag) { ?>
<?=$pagination;?>
<? } ?>
<? } ?> 