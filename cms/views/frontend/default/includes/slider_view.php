<? if(is_array($slides)){ ?>
		<div id="slider">
		<? foreach($slides as $s){ ?>
			<ul class="slider_show">
			<? foreach($s->img as $img){ ?>
				<li>
					<?
						if(!empty($img->desc)) {
							echo '<div class="text">';
								echo '<div class="desc"><p>'.nl2br(str_replace('|', "\n", $img->desc)).'</p></div>';
							echo '</div>';
						}
					?>
					<img src="<?=$img->img_path?>" alt="" />
				</li>
			<? } ?>
			</ul>
		<? } ?>
		</div>
<? } ?>
