<footer>
    <div class="uk-container-expand footer-wrapper ">
        <div class="uk-container wrapper">
          <div class="uk-flex ">
              <div class="copyright ">
                  <p>Copyright © 2018 Projekt Fox</p>
              </div>
              <div class="contact-footer">
                  <p>Biuro Zarządzania Projektami Rozwojowymi PROJEKT FOX</p>
                  <p>e-mail: <a href="mailto:<?=$this->settings->footer_email?>"><?=$this->settings->footer_email?></a></p>
                  <p>tel. kont.: <a href="tel:<?=str_replace(' ','',$this->settings->footer_phone)?>"><?=$this->settings->footer_phone?></a></p>
              </div>
              <div class="author">
                  <p>Projekt i wykonanie <a title="Strony internetowe Olsztyn" href="http://www.artneo.pl">Artneo.pl</a></p>
              </div>
          </div>
        </div>
    </div>
</footer>