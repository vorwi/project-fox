<ul id="menu_left">
	<?
	foreach($menu as $row){
		if(in_array($row->id, array($this->id, $this->id_parent, $this->id_grandparent))) {
			$add = 'class="active" ';
		}elseif((!empty($row->url) && $row->url==uri_string()) || (!empty($row->user_url) && $row->user_url==uri_string())) {
			$add = 'class="active" ';
		}else {
			$add = '';
		}
		
		if(!empty($row->target)){
			$target = 'target="'.$row->target.'"';
		}else{
			$target = '';
		}
		
		$children = '';
		if(!empty($row->childrens)) {
			$children .=  '<ul class="sub">';
			foreach($row->childrens as $sub){
				$sub_add = '';
				
				if(in_array($sub->id, array($this->id, $this->id_parent, $this->id_grandparent))) {
					$add = 'class="active"';
					$sub_add = 'class="active"';
				}elseif((!empty($sub->url) && $sub->url==uri_string()) || (!empty($sub->user_url) && $sub->user_url==uri_string())) {
					$add = 'class="active"';
					$sub_add = 'class="active"';
				}else {
					$sub_add = '';
				}
				
				if(!empty($sub->target)){
					$target_children = 'target="'.$sub->target.'"';
				}else{
					$target_children = '';
				}
				
				$grandchildren = '';
				if(!empty($sub->childrens)) {
					$grandchildren .=  '<ul class="sub">';
					foreach($sub->childrens as $grand){
						$grand_add = '';
						
						if(in_array($grand->id, array($this->id, $this->id_parent, $this->id_grandparent))) {
							$add = 'class="active"';
							$sub_add = 'class="active"';
							$grand_add = 'class="active"';
						}elseif((!empty($grand->url) && $grand->url==uri_string()) || (!empty($grand->user_url) && $grand->user_url==uri_string())) {
							$add = 'class="active"';
							$sub_add = 'class="active"';
							$grand_add = 'class="active"';
						}else {
							$grand_add = '';
						}
						
						if(!empty($grand->target)){
							$target_grandchildren = 'target="'.$grand->target.'"';
						}else{
							$target_grandchildren = '';
						}
						
						$grandchildren .= '<li '.$grand_add.' id="art'.$grand->id.'"><a '.$target_grandchildren.' href="'.article_url($grand).'" title="'.strip_tags($grand->title).'">'.(!empty($grand->short_title) ? $grand->short_title : $grand->title).'</a></li>';
					}
					$grandchildren .=  '</ul>';
				}
				
				$children .= '<li '.$sub_add.' id="art'.$sub->id.'"><a '.$target_children.' href="'.article_url($sub).'" title="'.strip_tags($sub->title).'">'.(!empty($sub->short_title) ? $sub->short_title : $sub->title).'</a>'.$grandchildren.'</li>';
			}
			$children .=  '</ul>';
		}

		echo '<li '.$add.' id="art'.$row->id.'"><a '.$target.' href="'.article_url($row).'" title="'.strip_tags($row->title).'">'.(!empty($row->short_title) ? $row->short_title : $row->title).'</a>'.$children.'</li>';
	}
	?>
</ul>