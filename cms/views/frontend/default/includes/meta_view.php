<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?=@$meta['meta_title']?></title>

<meta name="theme-color" content="#4a4545" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="<?=@$meta['meta_keywords']?>" lang="pl" />
<meta name="description" content="<?=@$meta['meta_description']?>" lang="pl" />

<meta property="og:title" content="<?=@$meta['og_title']?>" />
<meta property="og:description" content="<?=@$meta['og_description']?>" />
<meta property="og:image" content="<?=@$meta['og_image']?>" />
<meta property="og:url" content="<?=@$meta['og_url']?>" />
<meta property="og:site_name" content="<?=@$meta['og_site_name']?>" />

<meta name="author" content="Artneo.pl" />
<? if($this->settings->meta_robots_index == TRUE && $this->settings->meta_robots_follow): ?>
<meta name="Robots" content="all" />
<? else: ?>
<meta name="Robots" content="<?=($this->settings->meta_robots_index == TRUE) ? 'index' : 'noindex' ?>, <?=($this->settings->meta_robots_follow == TRUE) ? 'follow' : 'nofollow' ?> " />
<? endif; ?>
<meta name="Revisit-after" content="14 days" />

<meta name="apple-mobile-web-app-status-bar-style" content="#f9f621">

<link rel="shortcut icon" type="image/ico" href="/favicon.ico"/>

<!-- Include only for HomePage -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->