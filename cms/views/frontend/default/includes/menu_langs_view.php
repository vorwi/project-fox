<?php if(count($this->languages)>1) {?>
<ul class="nav langs">
	<?php foreach($this->languages as $row) {?>
    <li<?=$row->short == $_SESSION['lang'] ? ' class="active"' : ''?>>
    	<a href="<?=lang_url($row->short)?>"><?=strtoupper($row->short)?></a>
   	</li>
    <?php }?>
</ul>
<?php }?>