<? if(!isset($_COOKIE['cookie-accept'])){ ?>
	<script type="text/javascript">
	function start_cookie_alert(){
		<? if(!$settings->cookie_alert_std){ ?>
			var div = document.createElement('div');
			div.id = 'cookie-alert';
			var html = <?=json_encode($settings->cookie_alert_html)?>;
			if(document.body.firstChild) document.body.insertBefore(div, document.body.firstChild);
			else document.body.appendChild(div);
			div.innerHTML=html;
		<? }else{ ?>
			div = document.getElementById('cookie-alert');
			if(div != null) {
				div.style.display = 'block';
			}
		<? } ?>
	}
	
	function accept_cookie(){
		document.getElementById('cookie-alert').style.display="none";
		var exdays = 5000;
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape("yes") + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie="cookie-accept" + "=" + c_value + ";path=/";
	}
	
	function info_cookie(){
		if(typeof window.CB_Open == 'function'){
			CB_Open('href=<?=site_url('polityka-plikow-cookies')?>,,width=500,,height=450'); 
		} else {
			window.open("<?=site_url('polityka-plikow-cookies')?>",'<?=lang('Polityka Plików Cookies')?>','height=500,width=450');
		}
	}
	
	if (window.addEventListener) {
		window.addEventListener('load', start_cookie_alert, false);
	} else if (window.attachEvent) {
		window.attachEvent('onload', start_cookie_alert);
	}
	</script>
<? } ?>