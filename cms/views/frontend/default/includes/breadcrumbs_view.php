<ul class="breadcrumbs">
	<li><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
	<a href="<?=base_url();?>" class="first" itemprop="url">
		<span itemprop="title"><?=lang('Strona główna')?></span>
	</a>
	</span></li>
<?
if(!empty($breadcrumbs)) {
	$bc_count = count($breadcrumbs);
	$bci = 1;
	foreach($breadcrumbs as $row){
		echo '<li><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		if(!empty($row->url)) {
			echo '<a href="'.$row->url.'" itemprop="url"><span itemprop="title">'.$row->title.'</span></a>';
		} elseif($bc_count == $bci) {
			echo '<a href="'.current_url().'" itemprop="url"><span itemprop="title">'.$row->title.'</span></a>';
		} else {
			echo '<span itemprop="title">'.$row->title.'</span>';
		}
		echo '</span></li>';
		$bci++;
	}
}
?>
</ul>