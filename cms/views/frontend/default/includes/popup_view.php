<?php if($popup_image!='') {?>


<?php if(!isset($_SESSION['popup_viewed']) && ($popup_date_start == FALSE || time() >= $popup_date_start) && ($popup_date_end == FALSE || time() <= $popup_date_end)) {?>

<script type="text/javascript">
	$(window).load(function() {
    	$('#popup').delay(600).fadeIn(600);
        $('#popup').click(function(){
            $('#popup').fadeOut(200).remove();
        });
	});
</script>
<div style="display: none; left: 0; margin-top: -200px; position: fixed; top: 30%; width: 100%; z-index: 990;" id="popup">
	<div style="width: <?=$popup_width?>px; margin: 0 auto; position: relative;">
    	<?php if($popup_link!='') {?>
        <a href="<?=preg_match('#^http(s)?:\/\/#i', $popup_link) ? $popup_link : site_url($popup_link)?>">
        <?php }?>
        <img src="<?=$popup_image?>" alt="" title="kliknij, aby zamknąć" />
        <?php if($popup_link!='') {?>
        </a>
        <?php }?>
    </div>
</div>

<?php $_SESSION['popup_viewed'] = TRUE; ?>
<?php }?>
<?php }?>
