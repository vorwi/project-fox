<?/*
<ul id="menu_top" class="clr">
    <?
    foreach($menu as $row){
        if(in_array($row->id, array($this->id, $this->id_parent, $this->id_grandparent))) {
            $add = 'class="active" ';
        }elseif((!empty($row->url) && $row->url==uri_string()) || (!empty($row->user_url) && $row->user_url==uri_string())) {
            $add = 'class="active" ';
        }else {
            $add = '';
        }
        
        if(!empty($row->target)){
            $target = 'target="'.$row->target.'"';
        }else{
            $target = '';
        }

        echo '<li '.$add.'><a '.$target.' href="'.article_url($row).'" title="'.strip_tags($row->title).'">'.(!empty($row->short_title) ? $row->short_title : $row->title).'</a></li>';
    }
    ?>	
</ul>
*/?>
<nav id="navigation">
    <a href="<?=site_url()?>" class="logo"><img src="./img/logo.svg" alt=""></a>
    <a aria-label="mobile menu" class="nav-toggle">
        <span></span>
        <span></span>
        <span></span>
    </a>
    <ul class="menu-left">
        <?/*<li><a href="<?=site_url()?>" >strona główna</a></li>
        <li><a href="<?=site_url()?>#offer" uk-scroll>oferta</a></li>
        <li><a href="<?=site_url()?>#skills" uk-scroll>co nas wyróżnia</a></li>*/?>
        <?
        $is_mainpage = $this->id == 1 && $this->mod_name == "articles";
        $is_mainpage = false; // wyłączenie uk-scroll b.olender 03.12.2018
        foreach($menu as $row){
            if(in_array($row->id, array($this->id, $this->id_parent, $this->id_grandparent))) {
                $add = 'class="active" ';
            }elseif((!empty($row->url) && $row->url==uri_string()) || (!empty($row->user_url) && $row->user_url==uri_string())) {
                $add = 'class="active" ';
            }else {
                $add = '';
            }
            
            if(!empty($row->target)){
                $target = 'target="'.$row->target.'"';
            }else{
                $target = '';
            }

            if ($row->pub_menu == 1){
                echo '<li '.$add.'><a '.$target.' href="'.article_url($row).'" title="'.strip_tags($row->title).'">'.(!empty($row->short_title) ? $row->short_title : $row->title).'</a></li>';
            }

            if ($row->id == 1){
                foreach($row->childrens as $child){
                    if ($child->pub_menu == 0 || $child->id == 4 || $child->id == 10){
                        continue;
                    }
                    if(in_array($child->id, array($this->id, $this->id_parent, $this->id_grandparent))) {
                        $add = 'class="active" ';
                    }elseif((!empty($child->url) && $child->url==uri_string()) || (!empty($child->user_url) && $child->user_url==uri_string())) {
                        $add = 'class="active" ';
                    }else {
                        $add = '';
                    }
                    
                    if(!empty($child->target)){
                        $target = 'target="'.$child->target.'"';
                    }else{
                        $target = '';
                    }
        
                    echo '<li '.$add.'><a '.$target.' href="'.article_url($child).'" title="'.strip_tags($child->title).'"'.($is_mainpage?'uk-scroll':'').'>'.(!empty($child->short_title) ? $child->short_title : $child->title).'</a></li>';
                }
            }
        }
        ?>	
        <?/*
        <!-- <li><a href="doswiadczenie.html">doświadczenie</a></li> -->
        */?>
        <li><a href="<?=site_url()?>#contact" <?=($is_mainpage)?'uk-scroll':''?>>kontakt</a></li>
    </ul>
</nav>