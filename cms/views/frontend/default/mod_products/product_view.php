<? if(isset($product)){ ?>
	<div class="product-view clearfix">
		<div class="right-side">
			<h1><?=$product->title?></h1>
			<? if(!empty($product->price)){ ?>
				<h3>Cena <?=$product->price?> PLN</h3>
			<? } ?>
			<?=$product->content?>
		</div>
		<? if(isset($product->images)){ ?>
			<div class="left-side">
				<div class="main-slider" id="main-gallery">
					<? foreach ($product->images as $image){ ?>
						<div class="main-slider-item">
							<a href=""><img src="<?=$image->img_path ?>" alt=""/></a>
						</div>
					<? } ?>
				</div>
				<div class="nav-slider">
					<? foreach ($product->images as $image){ ?>
						<div class="nav-slider-item">
							<img src="<?=$image->thumb_list_path ?>" alt=""/>
						</div>
					<? } ?>
				</div>
			</div>
		<? } ?>
	</div>
<? } ?>