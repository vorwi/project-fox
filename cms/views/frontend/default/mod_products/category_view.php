<? if(is_array($subcategories)): ?>
	<div class="offert-slider">
		<? foreach ($subcategories as $subcat): ?>
			<?
			if(!empty($subcat->target)){
				$target = 'target="'.$subcat->target.'"';
			}else{
				$target = '';
			}
			$bg_style = '';
			if(isset($subcat->image) && !empty($subcat->image) && file_exists('./'.config_item('site_path').$subcat->image)){ 
				$bg_style = ' style="background-image: url('.$subcat->image.');"';
			}
			?>
			<div class="offert-item">
				<div class="offert-image"<?=$bg_style ?>>
					<span class="hover"><?=(!empty($subcat->short_title) ? $subcat->short_title : $subcat->title) ?></span>
				</div>
				<h3><?=(!empty($subcat->short_title) ? $subcat->short_title : $subcat->title) ?></h3>
				<a <?=$target?> href="<?=products_category_url($subcat)?>"></a>
			</div>
		<? endforeach; ?>
	</div>
<? endif; ?>

<? if(is_array($products)): ?>
	<div class="product-list clearfix">
		<? foreach ($products as $prod): ?>
			<? 
			$bg_style = '';
			if(isset($prod->main_image->thumb_list_path)){
				$bg_style = ' style="background-image: url('.$prod->main_image->thumb_list_path.');"';
			}
			?>
			<div class="product-list-box">
				<div class="image"<?=$bg_style ?>></div>
				<h3><?=(!empty($prod->short_title) ? $prod->short_title : $prod->title) ?></h3>
				<a href="<?=product_url($prod)?>"></a>
			</div>
		<? endforeach; ?>
	</div>
<? endif; ?>