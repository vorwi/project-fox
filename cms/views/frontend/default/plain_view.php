<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<base href="<?=base_url();?>" />
	
		<?=$meta?>
		<style type="text/css">
			body { margin: 0; padding: 10px; color: #000; font-family: Arial, 'Arial CE', sans-serif; font-size: 0.8em; background-color: #fff; }
		</style>
	</head>
<body class="plain">
	<?=$header?>
	<?=$content?>
</body>
</html>