<?
if(isset($addons)) {
	if($addons==1){
		echo '<h3><a href="'.site_url(config_item('px_gallery').'/'.$id_gal.'/'.iso_clear($gallery->name)).'">'.$gallery->name.'</a></h3>';
	} else {
		echo '<h1 class="title">'.$gallery->name.'</h1>';
	}
}

if(!empty($gallery->desc)) echo '<p>'.$gallery->desc.'</p>';
?>

<div class="img_row clr">
	<?
	if(!empty($gallery->images)){
		$i=0;
		foreach($gallery->images as $img){
			if(isset($addons) && $addons==1){
				$link = site_url(config_item('px_gallery').'/'.$id_gal.'/'.iso_clear($gallery->name));
				$add = '';
			} else {
				$link = $img->img_path;
			}
			
			echo '<a href="'.$link.'">';
			echo '<img alt="'.htmlspecialchars($img->desc).'" src="'.$img->thumb_path.'" />';
			echo '</a>';
			
			if($i==3){ echo "</div>\n<div class=\"img_row clr\">\n"; $i=0;} else $i++;
		}
	}
?>
</div>
