<div class="uk-container-expand background-wrapper">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-1 uk-width-1-2@m">
                <div class="workspace-wrapper" uk-scrollspy="cls:uk-animation-slide-left; delay:333;">
                    <h1><?=$article->title?></h1>
                    <?=$article->admission?>

                </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-2@m list-wrapper">
                <?=$article->content?>
            </div>
            <div class="uk-width-1-1 moveArrow-wrapper ">
                <a href="#logo-scroll" uk-scroll="delay:1333;"><img src="./img/moveArrow.png" alt="" class="animated bounce"></a>
            </div>
        </div>
    </div>
    <div class="uk-container bottom-section" id="logo-scroll">
        <div uk-grid>
            <div class="uk-width-1-1 logo-dest">
            	<?php if(setting('above_clients_field_1')) {?>
                <h5><?=setting('above_clients_field_1')?></h5>
                <?php }?>
                <?php if(setting('above_clients_field_2')) {?>
                <p><?=setting('above_clients_field_2')?></p>
                <?php }?>
            </div>
        </div>
        <div class="uk-flex main-logo-wrapper uk-flex-auto uk-flex-between uk-flex-wrap" uk-scrollspy="target: .logo-wrapper; cls: uk-animation-scale-up; delay: 333;">
            <? if($clients) {foreach($clients as $client) {?>
            <div class="logo-wrapper ">
                <div class="top-img-wrapper">
                    <? if (isset($client->img) && file_exists('./'.config_item('site_path').config_item('upload_path').'/suppliers/'.$client->img)): ?>
                    <? 
                    $tab = explode('.', $client->img);
                    $img_thumb = $tab[0] . '_thumb.' . $tab[1];
                    ?>
                    <a href="<?=site_url($client->project)?>">
                        <img src="<?=config_item('upload_path').'suppliers/'.$img_thumb?>" alt="<?=$client->name?>">
                    </a>
                    <? else: ?>
                        <div style="text-align: center;">
                            <h4 style="margin:0px;"><?=$client->name?></h4>
                            <?php if(!empty($client->offer)) {?>
                            <p style="margin:0px;"><?=$client->offer?></p>
                            <?php }?>
                            <?php if(!empty($client->url)) {?>
                            <p style="margin:0px;"><a href="<?=prep_url($client->url)?>" target="_blank" rel="nofollo"><?=$client->url?></a></p>
                            <?php }?>
                        </div>
                    <? endif; ?>
                </div>
                <div class="bottom-text-wrapper">
                    <? if ($client->project): ?>
                    <p><a href="<?=site_url($client->project)?>">zobacz projekty<span><img src="./img/arrow-left.png" alt=""></span></a>
                    </p>
                    <? endif; ?>
                </div>
            </div>
            <?} }?>
        </div>
    </div>
</div>