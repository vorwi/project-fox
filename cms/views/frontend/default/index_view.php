<!DOCTYPE html>
<html>
<head>
    <base href="<?=base_url();?>" />
    
    <?=$meta?>
    
    <?=put_headers('frontend')?>
    
    <?=$cookie_alert?>
    <?=$stats_script?>
</head>

<body>
    <div class="main">
        <?=$popup?>
        <?=$menu_langs;?>
        <div class="content">
            <header>
                <div class="container">
                    <?=$menu_top;?>
                </div>
            </header>
        </div>
        <?=$content?>
    </div>
    <?=$footer?>
    <?=put_footer_scripts()?>
</body>
</html>