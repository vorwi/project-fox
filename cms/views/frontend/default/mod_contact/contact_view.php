<? 
$attributes = array('enctype' => 'multipart/form-data');
echo form_open_multipart(current_url().'/#contact',$attributes);

	if(count($forms) == 1) {
		$row = current($forms);
		// echo '<p>'.lang('Do').': '.(!empty($row->name) ? $row->name : email_safe_show($row->email)).'</p>';
		echo '<div style="display: none;"><input name="id" type="hidden" value="'.$row->id.'"></div>';
	} else {
		echo lang('Do').': <select name="id">';
		foreach($forms as $row) {
			echo '<option value="'.$row->id.'">'.(!empty($row->name) ? $row->name : email_safe_show($row->email, TRUE)).'</option>';
		}
		echo '</select>';
	}
	if($row->id_art==11){
?>
	<div uk-grid>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="name" name="name" type="text" value="<?=set_value('name')?>" class="validate">
				<label for="name">Imię</label>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="surname" name="surname" type="text" value="<?=set_value('surname')?>" class="validate">
				<label for="surname">Nazwisko</label>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="email" name="email" type="email" value="<?=set_value('email')?>" class="validate">
				<label for="email">E-mail</label>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="phone" name="tel" type="tel" value="<?=set_value('tel')?>" class="validate">
				<label for="phone">Telefon</label>
			</div>
		</div>
		<div class="uk-width-1-1">
			<div class="input-field">
				<textarea id="textarea1" name="country" value="<?=set_value('country')?>" class="materialize-textarea"></textarea>
				<label for="textarea1">Kraj</label>
			</div>
		</div>
		<div class="uk-width-1-1">
			<div class="input-field">
				<textarea id="textarea2" name="city" value="<?=set_value('city')?>" class="materialize-textarea"></textarea>
				<label for="textarea2">Miejscowość</label>
			</div>
		</div>
		<div class="uk-width-1-1">
			<div class="input-field">
				<input id="cv" name="cv" type="file">
			</div>
		</div>
		<div class="uk-width-1-1">
			<div class="input-field">
				<input id="lm" name="lm" type="file">
			</div>
		</div>
	</div>
	<div class="uk-width-1-1 right-align">
		<input name="send" type="submit" class="more" value="<?=lang('Wyślij')?>" />
	</div>
<?
	}
	else{
?>

	<div uk-grid>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="name" name="name" type="text" value="<?=set_value('name')?>" class="validate">
				<label for="name">Imię</label>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="surname" name="surname" type="text" value="<?=set_value('surname')?>" class="validate">
				<label for="surname">Nazwisko</label>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="email" name="email" type="email" value="<?=set_value('email')?>" class="validate">
				<label for="email">E-mail</label>
			</div>
		</div>
		<div class="uk-width-1-1 uk-width-1-2@m">
			<div class="input-field">
				<input id="phone" name="tel" type="tel" value="<?=set_value('tel')?>" class="validate">
				<label for="phone">Telefon</label>
			</div>
		</div>
		<div class="uk-width-1-1">
			<div class="input-field">
				<textarea id="textarea1" name="message" value="<?=set_value('message')?>" class="materialize-textarea"></textarea>
				<label for="textarea1">Wiadomość</label>
			</div>
		</div>
	</div>
	<div class="uk-width-1-1 right-align">
		<input name="send" type="submit" class="more" value="<?=lang('Wyślij')?>" />
	</div>
<?}?>
<?=$security_markers?>
<input type="hidden" name="contact-form-send" value="1" />
<?=form_close()?>