<!DOCTYPE html>
<html>
<head>
	<base href="<?=base_url();?>" />
	
	<?=$meta?>
	
	<?=put_headers('frontend')?>
	
	<?=$cookie_alert?>
	<?=$stats_script?>
</head>

<body>
<div class="main">
    <?=$popup?>
    <?=$menu_langs;?>
    <div class="content">
        <header>
            <div class="container">
                <?=$menu_top;?>
            </div>
        </header>
    </div>
    <?/*
    <?=(isset($slider) ? $slider : '')?>
    <?=$content?>
    */?>
    </div>
    <?if($main_section) {?>
    <div class="uk-container-expand home-slider-wrapper" id="about">
        <div class="hero">
            <div class="  uk-light" uk-slider  <?/*style='background: url("./img/dos-bg.png"); background-size: contain;background-position:70%;background-repeat: no-repeat;'*/?>>
                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin" style="visibility: hidden;"></ul>
                <ul class="uk-slider-items uk-grid">
                    <li class="uk-width-1-1" style='background: url("./img/slider-bg.jpg");background-position:right;background-repeat: no-repeat;background-size: cover;'>
                        <div class="uk-panel">

                            <div class="uk-position-left  text-wrapper">
                                <h2><?=$main_section->main_header?></h2>
                                <p><?=$main_section->main_subtitle?></p>
                                <a href="<?=$main_section->main_link?>" class="more">więcej</a>

                            </div>
                        </div>
                    </li>
                    <!-- <li class="uk-width-1-1" style='background: url("./img/slider-bg.jpg");background-position:right;background-repeat: no-repeat;'>
                        <div class="uk-panel"> -->
                            <!--<img src="../img/slider-bg.jpg" alt="">-->
                            <!-- <div class="uk-position-left  text-wrapper ">
                                <h2>Przekształć pomysł w możliwości
                                </h2>
                                <p>Pozyskaj dotacje Unii Europejskiej dla MŚP</p>
                                <a href="" class="more">więcej</a>

                            </div>
                        </div>
                    </li> -->
                    <!-- <li class="uk-width-1-1" style='background: url("./img/slider-bg.jpg");background-position:right;background-repeat: no-repeat;'>
                        <div class="uk-panel">

                            <div class="uk-position-left  text-wrapper ">
                                <h2>Przekształć pomysł w możliwości
                                </h2>
                                <p>Pozyskaj dotacje Unii Europejskiej dla MŚP</p>
                                <a href="" class="more">więcej</a>

                            </div>
                        </div>
                    </li> -->
                </ul>
            </div>
            <div class=" moveArrow-wrapper ">
                <a href="#aboutUsScroll" uk-scroll="delay:1333;"><img src="./img/moveArrow.png" alt="" class="animated bounce"></a>
            </div>

        </div>

    </div>
    <? } ?>

    <?if($about) {?>
    <div class="uk-container-expand">
        <div uk-grid class="aboutUs" id="aboutUsScroll">
            <div class="uk-width-1-1@m uk-width-1-2@l img-wrapper" style="background:url('../img/aboutUs.jpg'); background-repeat: no-repeat; background-size:cover;">
                <img src="./img/aboutUs.jpg" alt="" uk-scrollspy="cls:uk-animation-fade;delay:333;">
            </div>
            <div class=" uk-width-1-1@m uk-width-1-2@l">
                <div class="aboutUs-wrapper" uk-scrollspy="cls:uk-animation-slide-right;delay:378;">
                    <h3><?=$about->about_header?></h3>
                    
                    <blockquote><?=$about->about_admission?></blockquote>

                    <?=$about->about_content?>
                </div>

            </div>

        </div>
    </div>
    <? } ?>

    <?if($clients) {?>
    <div class="uk-container-expand counter-wrapper" id="aboutUs">
        <div class="uk-container">
            <h3><?=$clients->clients_header?></h3>
            <div class="count-wrapper uk-flex " uk-scrollspy=" cls:uk-animation-scale-down; delay:333;target:.main-counter-wrapper;">
                <?/* for ($i=1; $i <= 7; $i++) { 
                    $top    = 'clients_number_'.$i.'_top';
                    $number = 'clients_number_'.$i;
                    $bottom = 'clients_number_'.$i.'_bottom';
                ?>
                <div class="main-counter-wrapper">
                    <div class="top-wrapper">
                        <p><?=$clients->$top?></p>
                    </div>
                    <div class="counter" data-count="<?=$clients->$number?>">0</div>
                    <div class="bottom-wrapper">
                        <p><?=$clients->$bottom?></p>
                    </div>
                </div>
                <? } */?>

                <?if ($numbers) {
                    foreach ($numbers as $number) { ?>
                <div class="main-counter-wrapper">
                    <div class="top-wrapper">
                        <p><?=$number->top?></p>
                    </div>
                    <div class="counter" data-count="<?=$number->number?>">0</div>
                    <div class="bottom-wrapper">
                        <p><?=$number->bottom?></p>
                    </div>
                </div>
                <?  } 
                } ?>
            </div>
            <div class="more-container">
                <p><?=$clients->clients_link_label?></p>
                <a href="<?=$clients->clients_link?>" class="more">więcej</a>
            </div>
        </div>
    </div>
    <? } ?>

    <?if($offer) {?>
    <div class="uk-container-expand">
        <div uk-grid class="aboutUs" id="offer">
            <div class="uk-width-1-1@m uk-width-1-2@l offer" style=" background-repeat: no-repeat; background-size:cover;" uk-scrollspy="cls:uk-animation-fade">
                <div class="aboutUs-wrapper " uk-scrollspy="cls:uk-animation-slide-right-small">
                    <h3><?=$offer->offer_header?></h3>
                    <blockquote><?=$offer->offer_admission?></blockquote>
                    
                    <?=$offer->offer_content?>
                    <div class=" moveArrow-wrapper ">
                        <a href="#workWithClients" uk-scroll="delay:1333;"><img src="./img/singlearrow.png" alt="" class="animated bounce"></a>
                    </div>

                </div>
            </div>
            <div class=" uk-width-1-1@m uk-width-1-2@l img-wrapper" style="background:url('../img/puzzle.jpg');background-repeat: no-repeat; background-size:cover;">

                <!-- <img src="./img/puzzle.jpg" alt=""> -->
            </div>
        </div>
    </div>
    <? } ?>

    <?if($collaboration) {?>
    <div class="uk-container-expand" id="workWithClients">
        <div class="workspace">
            <h4><?=$collaboration->collaboration_header?></h4>
            <div class="uk-container">
                <div uk-grid>

                    <div class="uk-width-1-1 uk-width-1-2@m">
                        <ol class="customList" uk-scrollspy="target: li; cls: uk-animation-fade; delay: 333;">
                        <?if ($offers){
                            foreach($offers as $key => $offer) { 
                                if ($key%2 == 0){
                        ?>
                                <li class="uk-scrollspy-inview uk-animation-fade"><?=$offer->text?></li>
                        <? 
                                }
                            }
                        }?>
                        </ol>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-2@m">
                        <ol class="customList" uk-scrollspy="target: li; cls: uk-animation-fade; delay: 333;">
                        <?if ($offers){
                            foreach($offers as $key => $offer) { 
                                if ($key%2 == 1){
                        ?>
                                <li class="uk-scrollspy-inview uk-animation-fade"><?=$offer->text?></li>
                        <? 
                                }
                            }
                        }?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? } ?>

    <?if($distinguish) {?>
    <div class="uk-container-expand" style="background: url('./img/differences-bg.jpg'); background-repeat: no-repeat;background-size: cover; background-position:bottom;" id="skills">
        <div class="uk-container ">
            <div class=" content-heading" uk-scrollspy="cls:uk-animation-fade">
                <p><?=$distinguish->distinguish_header?></p>
            </div>
            <div class="middle-wrapper uk-container offer home" uk-scrollspy="target: .offer-wrapper; cls: uk-animation-scale-up; delay: 333;">
                <div uk-grid class=" uk-child-width-1-3@s uk-child-width-1-6@l">
                    <?if ($skills){
                        foreach($skills as $key => $skill) { 
                    ?>
                            <div class="offer-wrapper">
                                <div class="img-wrapper">
                                    <i class="<?=$skill->icon?>"></i>
                                </div>
                                <div class="des-wrapper">
                                    <p><?=$skill->text?></p>

                                </div>
                            </div>
                    <? 
                        }
                    }?>
                </div>



            </div>
            <div class="more-container">
                <p><?=$distinguish->distinguish_link_label?></p>
                <a href="<?=$distinguish->distinguish_link?>" class="more">więcej</a>
            </div>
        </div>
    </div>
    <? } ?>

    <?if($contact) {?>
    <div class="uk-container contact-wrapper main-wrapper" id="contact">
        <? if((validation_errors()!='' && !$this->input->post('footform')) || ($this->session->flashdata('msg')!='')): ?>
            <div class="errors">
                <? if(validation_errors()!='' && !$this->input->post('footform')):?>
                <?=msg(validation_errors(),2,1,1)?>
                <? endif ?>
                <? if($this->session->flashdata('msg')!=''):?>
                    <?
                        foreach($this->session->flashdata('msg') as $row){
                            $msg = $this->session->flashdata('msg');
                            echo msg($row[0], $row[1]);
                        }
                    ?>
                <? endif ?>
            </div>
        <? endif ?>
        <h6>Kontakt</h6>
        <div uk-grid class="contact-wrapper" uk-scrollspy=" cls: uk-animation-fade; delay: 393;">

            <div class="uk-width-1-1 uk-width-1-2@m contact-detalils">
                <?=$contact->contact_content?>
                <div class="details">
                    <p><?=$this->settings->main_contact_nip?></p>
                    <p>|</p>
                    <p><?=$this->settings->main_contact_regon?></p>
                </div>
                <?=$this->settings->main_contact_email?>
                <p>tel. kont.: <a href="tel:<?=str_replace(" ","",$this->settings->main_contact_phonenumber)?>"><?=$this->settings->main_contact_phonenumber?></a></p>
            </div>
            <div class="uk-width-1-1 uk-width-1-2@m contact-form">
                <p>Formularz kontaktowy</p>
                <? /*
                <form>
                    <div uk-grid>
                        <div class="uk-width-1-1 uk-width-1-2@m">
                            <div class="input-field">
                                <input id="name" type="text" class="validate">
                                <label for="name">Imię</label>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-2@m">
                            <div class="input-field">
                                <input id="surname" type="text" class="validate">
                                <label for="surname">Nazwisko</label>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-2@m">
                            <div class="input-field">
                                <input id="email" type="email" class="validate">
                                <label for="email">E-mail</label>
                            </div>
                        </div>
                        <div class="uk-width-1-1 uk-width-1-2@m">
                            <div class="input-field">
                                <input id="phone" type="tel" class="validate">
                                <label for="phone">Telefon</label>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <div class="input-field">
                                <textarea id="textarea1" class="materialize-textarea"></textarea>
                                <label for="textarea1">Wiadomość</label>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1 right-align">
                        <button type= "submit" class="more">Wyślij</button>
                    </div>
                </form>
                */?>
                <? //form na głownej
                echo $addons['top'];
                ?>
            </div>
        </div>
        <div class="drive-wrapper" uk-scrollspy=" cls: uk-animation-scale-down; delay: 393;">
            <p>Dojazd</p>
            <div uk-grid class="drive-details">
                <div class="uk-width-1-1 uk-width-1-2@m">
                <img src="<?=$this->settings->contact_image?>" alt="">
                </div>
                <div class="uk-width-1-1 uk-width-1-2@m desc">
                    <?=$contact->contact_directions?>
                </div>
            </div>
        </div>
    </div>
    <? } ?>
</div>

<div id ="map" class="map-container">

</div>

<?=$footer?>
<?=put_footer_scripts()?>

</body>
</html>