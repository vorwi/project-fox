<? 
$attributes = array('class' => 'simple_form');
echo form_open_multipart(current_url(),$attributes);

	if(count($forms) == 1) {
		$row = current($forms);
		echo '<p>'.lang('cf_adressee').' '.(!empty($row->name) ? $row->name : email_safe_show($row->email)).'</p>';
		echo '<div style="display: none;"><input name="id" type="hidden" value="'.$row->id.'"></div>';
	} else {
		echo lang('cf_adressee').' <select name="id">';
		foreach($forms as $row) {
			echo '<option value="'.$row->id.'">'.(!empty($row->name) ? $row->name : email_safe_show($row->email, TRUE)).'</option>';
		}
		echo '</select>';
	}
?>

	<input name="email" class="val_repl" type="text" value="<?=set_value('email', lang('cf_email'))?>" />
	<input name="tytul" class="val_repl" type="text" value="<?=set_value('tytul', lang('cf_title'))?>" />
	<p><?=lang('cf_content')?></p>
	<textarea name="tresc" rows="8" cols="40"><?=set_value('tresc')?></textarea>
	<p><?=lang('cf_rewrite_code')?></p>
	<input name="captcha" class="cf_captcha_code" value="<?=lang('cf_enter_code')?>" onfocus="this.value=''" onblur="if(this.value=='') this.value='<?=lang('cf_enter_code')?>'" type="text" />
	<?='<img class="cf_captcha_img" src="temp/captcha/'.$cap['time'].'.jpg" alt="" />';?>
	<input name="send" type="submit" value="<?=lang('cf_send')?>" />
<?=form_close()?>



