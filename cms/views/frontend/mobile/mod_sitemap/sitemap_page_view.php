<?//=parray($sitemap['menu'])?>
<div>
	<ul class="sitemap">
	<?
		if(!empty($sitemap['articles'])){
			foreach($sitemap['articles'] as $row){
				
				if(!empty($row->url)) $url = preg_match('#^http(s)?:\/\/#i', $row->url) ? $row->url : site_url($row->url);
				elseif(!empty($row->user_url)) $url = site_url($row->user_url);
				else $url = site_url('page/'.$row->id.'/'.iso_clear($row->title));
			
				echo '<li><a href="'.$url.'" title="'.strip_tags($row->title).'">'.$row->title.'</a>'; 
				if(isset($row->children)){
					echo '<ul>';
					foreach($row->children as $sub){
						
						if(!empty($sub->url)) $sub_url = preg_match('#^http(s)?:\/\/#i', $sub->url) ? $sub->url : site_url($sub->url);
						elseif(!empty($sub->user_url)) $sub_url = site_url($sub->user_url);
						else $sub_url = site_url('page/'.$sub->id.'/'.iso_clear($sub->title));
					
						echo '<li><a href="'.$sub_url.'" title="'.strip_tags($sub->title).'">'.$sub->title.'</a></li>';
					}
					echo '</ul>';
				}
				echo '</li>';
			}
		}
	?>
	</ul>
</div>
