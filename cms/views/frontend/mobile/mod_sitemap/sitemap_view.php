<?='<?xml version="1.0" encoding="UTF-8"?>';?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?
	if(!empty($articles)){
		foreach($articles as $row){
			
			if(!empty($row->url)) $url = preg_match('#^http(s)?:\/\/#i', $row->url) ? $row->url : site_url($row->url);
			elseif(!empty($row->user_url)) $url = site_url($row->user_url);
			else $url = site_url('page/'.$row->id.'/'.iso_clear($row->title));
		
			echo "<url>\n";
				echo "\t<loc>{$url}</loc>\n";
			echo "</url>\n";
			
			if(isset($row->children)) {
				foreach($row->children as $sub){
				
					if(!empty($sub->url)) $sub_url = preg_match('#^http(s)?:\/\/#i', $sub->url) ? $sub->url : site_url($sub->url);
					elseif(!empty($sub->user_url)) $sub_url = site_url($sub->user_url);
					else $sub_url = site_url('page/'.$sub->id.'/'.iso_clear($sub->title));
					
					echo "<url>\n";
						echo "\t<loc>{$sub_url}</loc>\n";
					echo "</url>\n";
				}
			}
		}
	}
?>	
</urlset> 