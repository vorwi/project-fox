<? if(!empty($news)) { ?>
	
<? 
foreach($news as $row) {
	echo '<div class="news cf">';
	
	$public_dir = config_item('upload_path').(!isset($this->img_dir) ? $img_dir : $this->img_dir);
	$dir = './'.config_item('site_path').$public_dir;
	$tab = explode('.',$row->img);
	echo '<h2><a href="news/'.$row->id.'/'.iso_clear($row->title).'">'.$row->title.'</a></h2>';
	if(isset($tab[1])) {
		echo '<div class="img">';
		$img_thumb = $dir.$tab[0].'_thumb.'.$tab[1];
		if(file_exists($img_thumb)) echo '<a href="'.$public_dir.$row->img.'" rel="clearbox"><img src="'.$public_dir.$tab[0].'_thumb.'.$tab[1].'" alt="" /></a>';
		echo '</div>';
	}
	//echo '<p class="news_date">'.date('d.m.Y', strtotime($news->date_add)).'</p>';
	
	if(!empty($row->admission) && $read==0){ echo $row->admission; }
	else if($read==1) echo $row->content;
	
	echo '<p class="more"><a href="news/'.$row->id.'/'.iso_clear($row->title).'" class="button_white">'.lang('nw_read_more').'</a></p>';
	echo '</div>';
}
?>

<? } ?> 
<p class="pagination"><?=$this->pagination->create_links();?></p>