<ul id="menu_left">
	<?
	$ile = count($menu);
	$i=1;
	foreach($menu as $row){
	
		if(!empty($row->url)) $link = $row->url;
		elseif(!empty($row->user_url)) $link = site_url($row->user_url);
		else $link = site_url('page/'.$row->id.'/'.iso_clear($row->title));
		
		if($this->id==$row->id && $this->id!=config_item('main_id')) $add = 'class="rollup"';
		else $add = '';
		
		$children = '';
		if(!empty($row->children)) {
			$children .=  '<ul class="sub">';
			foreach($row->children as $sub){
				$sub_add = '';
				
				if(!empty($sub->url)) $sublink = $sub->url;
				elseif(!empty($sub->user_url)) $sublink = site_url($sub->user_url);
				else $sublink = site_url('page/'.$sub->id.'/'.iso_clear($sub->title));
				
				if($this->id == $sub->id) {
					$add = 'class="rollup"';
					$sub_add = 'class="active"';
				}
				
				$grandchildren = '';
				if(!empty($sub->children)) {
					$grandchildren .=  '<ul class="sub">';
					foreach($sub->children as $grand){
						$grand_add = '';
						
						if(!empty($grand->url)) $grandlink = $grand->url;
						elseif(!empty($grand->user_url)) $grandlink = site_url($grand->user_url);
						else $grandlink = site_url('page/'.$grand->id.'/'.iso_clear($grand->title));
						
						if($this->id == $grand->id) {
							$add = 'class="rollup"';
							$sub_add = 'class="active"';
							$grand_add = 'class="active"';
						}
						
						$grandchildren .= '<li '.$grand_add.' id="art'.$grand->id.'"><a href="'.$grandlink.'" title="'.strip_tags($grand->title).'">'.(!empty($grand->short_title) ? $grand->short_title : $grand->title).'</a></li>';
					}
					$grandchildren .=  '</ul>';
				}
				
				$children .= '<li '.$sub_add.' id="art'.$sub->id.'"><a href="'.$sublink.'" title="'.strip_tags($sub->title).'">'.(!empty($sub->short_title) ? $sub->short_title : $sub->title).'</a>'.$grandchildren.'</li>';
			}
			$children .=  '</ul>';
		}

		echo '<li '.$add.' id="art'.$row->id.'"><a href="'.$link.'" title="'.strip_tags($row->title).'">'.(!empty($row->short_title) ? $row->short_title : $row->title).'</a>'.$children.'</li>';
		
		$i++;
	}
	?>
</ul>

