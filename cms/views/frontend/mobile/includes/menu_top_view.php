<ul class="topmenu">
	<li class="close">
		<a href="#close_menu"><?= lang('menu_close') ?></a>
	</li>
	<?
	$ile = count($menu);
	$i = 1;
	foreach ($menu as $row) {

		if (!empty($row->url))
			$link = $row->url;
		elseif (!empty($row->user_url))
			$link = site_url($row->user_url);
		else
			$link = site_url('page/' . $row->id . '/' . iso_clear($row->title));

		if (in_array($row->id, array($this->id, $this->id_parent, $this->id_grandparent)))
			$add = 'class="active" ';
		else
			$add = '';

		$children = '';
		if (!empty($row->children)) {
			// && $this->id != config_item('main_art')
			$children .= '<ul class="sub">';
			foreach ($row->children as $sub) {

				if (!empty($sub->url))
					$sublink = $sub->url;
				elseif (!empty($sub->user_url))
					$sublink = site_url($sub->user_url);
				else
					$sublink = site_url('page/' . $sub->id . '/' . iso_clear($sub->title));

				if ($this->id == $sub->id){
					$add = 'class="active"';
				}
				if ($this->id == $sub->id){
					$add2 = 'class="active"';
				} else {
					$add2 = '';
				}

				$children .= '<li '.$add2.'><a href="' . $sublink . '" title="' . strip_tags($sub->title) . '">' . $sub->title . '</a></li>';
			}
			$children .= '</ul>';
		}

		echo '<li ' . $add . '><a ' . $add . 'href="' . $link . '" title="' . strip_tags($row->title) . '">' . (!empty($row->short_title) ? $row->short_title : $row->title) . '</a>' . $children . '</li>';

		$i++;
	}
	?>
	<li id="slider_pages_marker"></li>
</ul>
