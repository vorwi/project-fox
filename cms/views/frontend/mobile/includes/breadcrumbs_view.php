<ul class="breadcrumbs">
    <li><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
	<a href="<?=base_url();?>" class="first" itemprop="url">
	    <span itemprop="title">Home</span>
	</a>
    </span></li>
<?
if(!empty($breadcrumbs) && $this->id != config_item('main_art')) {
    foreach($breadcrumbs as $row){
		if((isset($row['rodzaj']) && $row['rodzaj'] == 1) || isset($row['url'])) {
		    echo '<li><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		    echo '<a href="'.(!preg_match('!^\w+://! i', $row['url']) ? site_url($row['url']) : $row['url']).'" itemprop="url"><span itemprop="title">'.$row['title'].'</span></a>';
		    echo '</span></li>';
		}
		else {
		    echo '<li><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
		    echo '<a href="'.current_url().'" itemprop="url"><span itemprop="title">'.$row['title'].'</span></a>';
		    echo '</span></li>';
		}
    }
}
?>
</ul>