<!DOCTYPE html>
<html>
	<head>
		<base href="<?= base_url(); ?>" />
		<?= $meta ?>
        <link href="css/mobile.css" rel="stylesheet" type="text/css" />
        <link href="<?= config_item('gfx_f'); ?>css/cms.css" rel="stylesheet" type="text/css" />

        <script src="<?= config_item('external_path'); ?>clearbox/clearbox.js" type="text/javascript"></script> 
		<?= $ga ?>
        <script src="js/mobile.js" type="text/javascript"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>

	<body<? if ($this->id != config_item('main_art') || $is_news === true): ?> class="subpage"<? endif; ?>>
		<div id="top">
			<div class="logoblock">
				<a href="<?= site_url() ?>" class="logo">
					<img src="images/mobile/top_logo.jpg" alt="" />
				</a>
				<a href="#open_menu" class="open"><?= lang('menu_open') ?></a>

				<div class="clear"></div>
			</div>


		</div>
		<?= $menu['top']; ?>
		<div class="clear"></div>
		<div id="content">
			<?= $header ?>
			<div class="inner">
				<?= $content ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="foot">
			<div class="inner">
				<div class="left">
					Copyright by
				</div>
				<div class="right">
					Projekt i wykonanie: <a target="_blank" title="Strony internetowe Olsztyn" href="http://www.artneo.pl/">Artneo.pl</a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="clear"></div>
    </body>
</body>
</html>
