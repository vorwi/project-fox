<? if($search_string === FALSE){ ?> 
	<p><?=lang('Prosimy wpisać szukane słowo lub frazę w pole wyszukiwarki.')?></p>
<? }else{ ?>
	<?=form_open(config_item('px_search'), array('method' => 'post', 'class' => 'search')) ?>
		<input type="text" name="query" />
		<input type="submit" name="szukaj" value="<?=lang('szukaj')?>" />
	<?=form_close()?>

	<? if($search_string !== FALSE && !empty($search_string)){ ?> 
		<p>Znaleziono <b><?=$count?></b> wyników  dla frazy <em><?=$search_string?></em></p>
	<? } ?>

	<? if($results !== FALSE){ ?>
		<? 
		if(!empty($results)){
			foreach($results as $result){ ?>
				<div style="border-bottom: 1px solid #E6E5EA;">
					<h3><a href="<?=$result->url?>"><?=$result->name?></a></h3>
					<p><?=substr_full_word(strip_tags($result->content), 330, '...')?></p>
					<!-- <p class="wiecej"><a href="<?=$url?>">&gt; <?=lang('czytaj więcej')?></a></p> -->
				</div>
			<? 
			}
		}
		?>
	<? } ?>
	<div class="clr"></div>
	<?=$this->pagination->create_links()?>
<? } ?>