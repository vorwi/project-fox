<?
if(isset($addons) && $addons==1){
	echo '<h3><a href="'.site_url('gallery/'.$id_gal.'/'.iso_clear($gallery['name'])).'">'.$gallery['name'].'</a></h3>';
} else {
	echo '<h1 class="title">'.$gallery['name'].'</h1>';
}
?>
<p><?=$gallery['desc']?></p>

<div class="img_row clr">
	<?
	if(!empty($gallery['img'])){
		$i=0;
		foreach($gallery['img'] as $img){
			//echo parray($gallery['img']);
			$tab = explode('.',$img->img);
			if(isset($addons) && $addons==1){
				$link = site_url('gallery/'.$id_gal.'/'.iso_clear($gallery['name']));
				$add = '';
			} else {
				if(!empty($img->desc)) $comment = ',,comment='.htmlspecialchars($img->desc);
				else $comment = '';
				$link = $gallery['dir'].$img->img;
				$add = 'rel="clearbox[gallery='.$gallery['name'].$comment.']"';
			}
			if(isset($tab[1])) {
				echo '<a '.$add.' href="'.$link.'">';
				echo '<img alt="" src="'.$gallery['dir'].$tab[0].'_thumb.'.$tab[1].'" />';
				echo '</a>';
			}

			if($i==3){ echo "</div>\n<div class=\"img_row clr\">\n"; $i=0;} else $i++;
		}
	}
?>
</div>
