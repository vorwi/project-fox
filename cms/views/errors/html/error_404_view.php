<div class="uk-container-expand background-wrapper">
	<div class="uk-container">
		<div uk-grid>
			<div class="uk-width-1-1 ">
				<div class="customPage" uk-scrollspy="cls:uk-animation-slide-left; delay:333;">
					<h1>Błąd 404</h1>			
						<p><?=sprintf(lang('Strona <b>%s</b> nie istnieje lub jest niedostępna.'), current_url())?></p>

						<a href="<?=site_url('/');?>" class="more">powrót do strony głównej</a>
						<?
						if(ENVIRONMENT == 'development') {
							$e = new Exception();
							$trace = explode("\n", $e->getTraceAsString());
							$trace = array_reverse($trace);
							array_shift($trace);
							array_pop($trace);

							echo '<h3>'.lang('Ścieżka błędu').'</h3>';
							echo '<ol>';
							foreach ($trace as $item){
								echo '<li>'.substr($item, strpos($item, ' ')).'</li>';
							}
							echo '</ol>';
							
						}
						?>
				</div>
			</div>
		</div>
	</div>
</div>