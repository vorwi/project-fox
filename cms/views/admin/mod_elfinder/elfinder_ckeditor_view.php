<script type="text/javascript" charset="utf-8">
	function getUrlParam(paramName) {
        var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
        var match = window.location.search.match(reParam) ;
        return (match && match.length > 1) ? match[1] : '' ;
	}

	$().ready(function() {
        var funcNum = getUrlParam('CKEditorFuncNum');

		var elf = $('#elfinder').elfinder({
			url : '/<?=$this->mod_url?>/index',
			lang: 'pl',
			height: 552,
			getFileCallback : function(file) {
                window.opener.CKEDITOR.tools.callFunction(funcNum, file.path);
                window.close();
            }
		}).elfinder('instance');
	});
</script>
<div id="elfinder"></div>