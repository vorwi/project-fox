<p class="callout callout-info">
	<b><?=lang('Witamy!')?></b>
	<br><?=lang('Znajdujesz się w panelu administracyjnym strony ').anchor(base_url(), $_SERVER['HTTP_HOST'], array('target' => '_blank'))?>
	<br><?=lang('W razie problemów z obsługą, prosimy o kontakt z naszym Biurem Obsługi Klienta: ').mailto('bok@artneo.pl')?>
</p>

<div class="row" style="font-size: 0.9em;">
	<? if(!empty($last_logins)) { ?>
	<div class="col-md-6">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title"><?=lang('Ostatnie logowania')?>:</h3>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th style="width: 180px;"><?=lang('Data')?></th>
							<? if($this->user->main) { ?>
							<th><?=lang('Login')?></th>
							<? } ?>
							<th><?=lang('IP')?></th>
							<th><?=lang('Przeglądarka')?></th>
						</tr>
					<? foreach($last_logins as $row) { ?>
						<tr>
							<td class="text-center"><?=dateV('j f Y, H:i', $row->date_try)?></td>
							<? if($this->user->main) { ?>
							<td><?=$row->login?></td>
							<? } ?>
							<td><?=$row->ip?></td>
							<td><?=substr($row->browser, 0, 45)?></td>
						</tr>
					<? } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<? } ?>
	<? if(!empty($last_articles)) { ?>
	<div class="col-md-6">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title"><?=lang('Zmiany w podstronach')?>:</h3>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th style="width: 180px;"><?=lang('Data modyfikacji')?></th>
							<th style="width: 40px;"><?=lang('Język')?></th>
							<th><?=lang('Tytuł')?></th>
							<th><?=lang('Autor')?></th>
						</tr>
					<? foreach($last_articles as $row) { ?>
						<tr>
							<td class="text-center"><?=dateV('j f Y, H:i', strtotime($row->date_modified))?></td>
							<td class="text-center"><?=lang_flag($row->lang)?></td>
							<td><?=anchor('admin/mod_articles/articles/edit/lang/pl/id/'.$row->id_art, $row->title)?></td>
							<td><?=$row->author_name?></td>
						</tr>
					<? } ?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<? } ?>
</div>