<?=form_open($this->mod_url.'/index/check/1', array('class' => 'row'));?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				
				<div class="form-group">
					<label><?=lang('Imię i nazwisko') ?></label>
					<input type="text" name="name" class="form-control" value="<?=set_value('name', $user->name)?>">
				</div>
				<div class="form-group">
					<label><?=lang('Dane kontaktowe') ?></label>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Email') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="email" value="<?=set_value('email', $user->email)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Telefon') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="phone" value="<?=set_value('phone', $user->phone)?>" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><?=lang('Dane adresowe') ?></label>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Ulica i nr budynku') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="address" value="<?=set_value('address', $user->address)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Kod pocztowy') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="post_code" value="<?=set_value('post_code', $user->post_code)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Miasto') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="city" value="<?=set_value('city', $user->city)?>" class="form-control">
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?=form_close()?>