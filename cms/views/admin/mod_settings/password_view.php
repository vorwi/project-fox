<?=form_open($this->mod_url.'/index/change/1', array('clas' => 'row'));?>
	<div class="col-md-4">
		<div class="box box-primary">
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Obecne (stare) hasło') ?></label>
					<input type="password" name="old_pass" class="form-control" value="">
				</div>
				<div class="form-group">
					<label><?=lang('Nowe hasło') ?></label>
					<input type="password" name="new_pass1" class="form-control" value="">
				</div>
				<div class="form-group">
					<label><?=lang('Powtórz nowe hasło') ?></label>
					<input type="password" name="new_pass2" class="form-control" value="">
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zmień')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?=form_close()?>