<div class="form-group form-inline"<?=(count($lang) > 1 ? '' : ' style="display: none"')?>>
	<?=lang('Wybierz język:') ?>
	<select name="lang" onchange="location.href='<?=site_url($this->mod_url.'/index/lang/')?>'+this.value" class="form-control">
	<? foreach($lang as $row){
		echo '<option value="'.$row->short.'" '.set_select('lang', $row->short, ($curr_lang == $row->short)).'>'.$row->lang.'</option>';
	} ?>
	</select>
</div>

<? if($this->session->userdata('main') == 1) { ?>
<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj kategorię') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
    	<?=form_open($this->mod_url.'/add_category', array('id' => 'add_settings_category'));?>
	    	<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Pozycja') ?></label>
				<input type="text" name="position" value="<?=set_value('position')?>" class="form-control">
			</div>
    		<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
    	<?=form_close()?>
	</div>
</div>

<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj ustawienie') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
    	<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
	    	<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Wartość wstępna') ?></label>
				<input type="text" name="value" value="<?=set_value('value')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Opis') ?></label>
				<textarea name="description" style="height: 35px;" class="form-control"><?=set_value('description')?></textarea>
			</div>
			<div class="form-group">
				<label><?=lang('Rodzaj') ?></label>
				<select name="type" class="select_simple form-control">
				<? foreach(array('text', 'textarea', 'ckeditor', 'boolean', 'file') as $t): ?>
			    	<option value="<?=$t?>" <?=set_select('type')?>><?=$t?></option>
			    <? endforeach; ?>
			    </select>
			</div>
			<div class="form-group">
				<label><?=lang('Grupa') ?></label>
				<select name="group" class="select_simple form-control">
		    	<? foreach($categories as $cat): ?>
			    	<option value="<?=$cat->id?>" <?=set_select('group')?>><?=$cat->name?></option>
			    <? endforeach; ?>
			    </select>
			</div>
			<div class="form-group">
				<label><?=lang('Pozycja') ?></label>
				<input type="text" name="position" value="<?=set_value('position')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Zależne od języka?') ?></label>
				<div class="radio-inline">
					<label><input type="radio" name="lang_related" <?=set_radio('lang_related', 1, TRUE)?> value="1"> tak</label>
				</div>
				<div class="radio-inline">
					<label><input type="radio" name="lang_related" <?=set_radio('lang_related', 0)?> value="0"> nie</label>
				</div>
			</div>
		    <input type="hidden" value="<?=$curr_lang?>" name="lang" />
    		<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
    	<?=form_close()?>
	</div>
</div>
<? } ?>

<script>
	$(function() {
		$("#add").validate({
			<?=jqueryValidateCommonVars()?>
			rules: {
				name: {
					pattern: /^[a-z0-9_]+$/i
				}
			},
			messages: {
				name: "<?=lang('Nazwa może składać się tylko ze znaków alfanumerycznych.') ?>",
			}
		});
		
		var $tabHash = $('input[name="tab_hash"]'),
			tabs = $( "#tabs" ).tabs({
				activate: function(event, ui) {
					$tabHash.val('#' + ui.newPanel.attr('id'));
				}
			});
		
		<? if($this->session->userdata('main') == 1): ?>
		// close icon: removing the tab on click
		tabs.delegate( "span.ui-icon-close", "click", function() {
			if(confirm('<?=lang('Czy na pewno chcesz usunąć kategorię ustawień?')?>\n<?=lang('Spowoduje to też usunięcie wszystkich ustawień przypisanych do tej kategorii.')?>')){
				var panelLi = $( this ).parent(), split = panelLi.find('a').attr('href').split("#"), panelId = split[1];
				panelLi.remove();
				$( "#" + panelId ).remove();
				$( "." + panelId ).val(true);
			}
		});

		<? endif; ?>
	});
</script>

<?=form_open_multipart($this->mod_url.'/index/lang/'.$curr_lang.'/change/1')?>
<input type="hidden" value="" name="tab_hash" />
<div id="tabs">
<? 
if($this->session->userdata('main') == 1):
foreach($settings_tree as $cat): 
?>
	<input class="tabs-<?=$cat->id?>" type="hidden" name="rem_cat[<?=$cat->id?>]" value="false" />
<? 
endforeach; 
endif;
?>
	<ul>
	<? foreach($settings_tree as $cat) { ?>
		<li>
			<a href="<?=current_url()?>#tabs-<?=$cat->id?>"><?=$cat->name?></a>
			<? if($this->session->userdata('main') == 1) { ?><span class="ui-icon ui-icon-close" role="presentation"><?=lang('Usuń kategorię') ?></span><? } ?>
		</li>
	<? } ?>
	</ul>
	
<? foreach($settings_tree as $cat): ?>
	<div id="tabs-<?=$cat->id?>" class="table-responsive">
		<? if(!empty($cat->settings)): ?>
		<table class="table table-bordered" style="min-width: 600px">
			<thead>
				<tr>
				    <th style="width:25px;"><?=lang('Nr')?></th>
					<th style="width: 25%"><?=lang('Opis')?></th>
					<th><?=lang('Dane')?></th>
					<th style="width:40px;"><?=lang('Kolejność')?></th>
					<? if($this->session->userdata('main') == 1){ ?>
					<th style="width:150px;"><?=lang('Działania')?></th>
					<? } ?>
				</tr>
			</thead>
			<tbody class="sortable">
			<?
			$i=1;
			foreach($cat->settings as $row){
		
				$details = "nazwa: {$row->name}, grupa: {$row->group}, poz: $row->position";
		
				echo '
				<tr>
				<td title="'.$details.'">'.$i.'</td>
				<td>'.$row->description.'</td>
				<td>';
		
				if($row->type == 'text') {
					echo '<input type="text" value="'.$row->value.'" name="'.$row->id.'" class="form-control">';
				}
				else if($row->type == 'textarea') {
					echo '<textarea name="'.$row->id.'" style="height: 75px;" class="form-control">'.$row->value.'</textarea>';
				}
				else if($row->type=='ckeditor') {
					echo ckeditor($row->id, $row->value, '100%', 'Simple', 120);
				}
				else if($row->type=='boolean') {
					echo '<label class="radio-inline"><input value="1" name="'.$row->id.'" type="radio" '.($row->value == 1 ? 'checked' : '').' /> '.lang('Tak').' </label>&#x2001;';
					echo '<label class="radio-inline"><input value="0" name="'.$row->id.'" type="radio" '.($row->value == 0 ? 'checked' : '').' /> '.lang('Nie').' </label>';
				} else if($row->type=='file') {
					echo '<div class="form-group">';
					echo form_fileinput($row->id, $row->value);
					echo '</div>';
				}
				echo '</td>';
				
				echo '<td class="text-center"><span class="fa-arrows"></span><input class="currentposition" type="hidden" style="width:30px;" name="position['.$row->id.']" value="'.$row->position.'" /></td>';
				
				if($this->session->userdata('main') == 1){
					echo '<td class="text-center nc-options">';
						if($row->pub==0) {
							echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/1", lang('Publikuj')); 
						} else { 
							echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/0", lang('Odpublikuj'));
						}
						echo '<a class="click-confirm" data-href="'.site_url("{$this->mod_url}/delete/del/{$row->id}").'" data-confirm="'.lang('Czy na pewno chcesz usunąć?').'">'.lang('Usuń').'</a>';
					echo '</td>';
				}
				echo '</tr>';
				$i++;
			}
			
			?>
			<tr>
				<td colspan="4" style="text-align: right;">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>" >
				</td>
				<td></td>
			</tr>
			</tbody>
		</table>
		<? endif; ?>
	</div>
<? endforeach; ?>
</div>

<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary" style="width: 150px;" />
<?=form_close()?>