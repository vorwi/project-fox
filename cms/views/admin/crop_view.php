<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title><?=ifset($meta_title, 'NeoCMS')?></title>
		<base href="<?=base_url();?>" />
		<meta name="author" content="Artneo.pl" />
		<meta name="copyright" content="<?=lang('Wszelkie prawa zatrzeżone')?> Artneo.pl" />
		<meta name="Robots" content="noindex, nofollow" />
			
		<? add_js(config_item('gfx_a').'js/jquery.imgareaselect.min.js', 'admin'); ?>
		<? add_css(config_item('gfx_a').'css/imgareaselect-default.css', 'admin'); ?>
		<?=put_headers('admin')?>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="iframe">
		<section class="content">
		<? 
			if(is_array($this->session->flashdata('msg_files'))) {
			 	foreach($this->session->flashdata('msg_files') as $row){
					$msg = $this->session->flashdata('msg_files');
					echo msg($row[0], $row[1]);
			 	}
			} elseif(!isset($img)) {
				echo msg(lang('Wybrane zdjęcie nie istnieje lub odnośnik jest nieprawidłowy.'), 1);
			} else {
		?>
			<?=form_open($form_url, array('class' => 'box box-borders', 'id' => 'img-crop'))?>
				<div class="box-header with-border">
					<h3 class="box-title"><?=lang('Kadrowanie zdjęcia')." #{$id}"?></h3>
					<div class="box-tools pull-right">
						<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary btn-sm">
					</div>
				</div>
				<div class="box-body">
					<p class="text-center">
						<img src="<?=$path.$img;?>" alt="" id="cropped-img">
					</p>
					
					<?=form_hidden('x_axis', '');?>
					<?=form_hidden('y_axis', '');?>
					<?=form_hidden('width', '');?>
					<?=form_hidden('height', '');?>
					<?=form_hidden('crop', 'true');?>
				</div>
				<div class="box-footer text-center">
					<input type="submit" value="<?=lang('Kadruj')?>" class="btn btn-primary" style="width: 150px;">
				</div>
			<?=form_close()?>
		
			<script type="text/javascript">
				var $image = $('#cropped-img'),
					values = {},
					minImgWidth = '<?=$width?>',
					minImgHeight = '<?=$height?>',
					imgAS,
					resizeTs,
					imgRealDim = {};
					
				$("<img/>").load(function() {
			        imgRealDim.width = this.width;
			        imgRealDim.height = this.height;
			    }).attr("src", $image.attr("src"));
				
				imgAS = $image.imgAreaSelect({
					minWidth: minImgWidth,
					minHeight: minImgHeight,
					aspectRatio: '<?=$width.':'.$height?>',
					handles: true,
					instance: true,
		            onSelectEnd: function (img, selection) {
		            	var scale = imgRealDim.width / img.width;
		            	
		                values.x_axis = selection.x1 * scale;
						values.y_axis = selection.y1 * scale;
						values.width = selection.width * scale;
						values.height = selection.height * scale;
						
						$.each(values, function(idx, value){
							$('input[name="'+ idx +'"]').val(value);
						});
					}
		        });
		        
		        $(window).on('load, resize', function(){
		        	clearTimeout(resizeTs);
		        	resizeTs = setTimeout(function(){
		        		var scale = imgRealDim.width / $image.width();
		        		imgAS.setOptions({
		        			minWidth: minImgWidth / scale,
							minHeight: minImgHeight / scale
		        		});
		        	}, 100);
		        });
		        
		        $('.imgareaselect-outer').click(function() {
		            $.each(values, function(idx, value){
						$('input[name="'+ idx +'"]').val('');
					});
		        });
		        
				$('#img-crop').submit(function(e) {
					e.preventDefault();
					
					if ($('input[name=x_axis]').val() == '') { 
						alert('<?=lang('Najpierw zaznacz obszar zdjęcia do wykadrowania.')?>');
					} else {
						$.ajax({
							type: "POST",
							url: '<?=$form_url?>',
							data: $(this).serialize(),
							success: function(){
								parent.location.reload();
							}
						});
					}
				});
			</script>

		<? } ?>
		</section>
	</body>	
</html>