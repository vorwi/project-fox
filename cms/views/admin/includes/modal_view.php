<div class="modal fade" id="main_modal" tabindex="-1" role="dialog" aria-labelledby="main_modal_label">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="<?=lang('Zamknij')?>">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="main_modal_label"></h4>
			</div>
			<div class="modal-body">
				
			</div>
		</div>
	</div>
</div>
