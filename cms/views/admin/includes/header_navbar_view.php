<nav class="navbar navbar-static-top">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only"><?=lang('Pokaż/ukryj menu')?></span> <span class="icon-bar"></span>
		<span class="icon-bar"></span> <span class="icon-bar"></span>
	</a>
	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-gears"></i> <?=lang('Narzędzia')?>
				</a>
				<ul class="dropdown-menu control-sidebar-menu">
					<li>
						<a href="javascript:void(0)" onclick="javascript:window.open('/admin/mod_elfinder/elfinder_init/standalone_view', '<?=lang('Menadżer plików')?>','width=960,height=590')">
							<i class="menu-icon fa fa-files-o bg-blue"></i>
							<div class="menu-info">
								<h4 class="control-sidebar-subheading"><?=lang('Menadżer plików')?></h4>
								<p><?=lang('Zarządzaj plikami na serwerze')?></p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?=site_url()?>" target="_blank">
							<i class="menu-icon fa fa-share bg-yellow"></i>
							<div class="menu-info">
								<h4 class="control-sidebar-subheading"><?=lang('Przejdź do strony')?></h4>
								<p><?=lang('Otwórz')?> <b><?=$_SERVER['HTTP_HOST']?></b></p>
							</div>
						</a>
					</li>
					<? if($this->user !== FALSE) { ?>
					<li>
						<a href="<?=site_url('admin/mod_settings/profile/index')?>">
							<i class="menu-icon fa fa-user bg-green"></i>
							<div class="menu-info">
								<h4 class="control-sidebar-subheading"><?=lang('Mój profil')?></h4>
								<p><?=lang('Aktualizacja danych konta')?></p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?=site_url('admin/mod_settings/password/index')?>">
							<i class="menu-icon fa fa-unlock-alt bg-red"></i>
							<div class="menu-info">
								<h4 class="control-sidebar-subheading"><?=lang('Zmiana hasła')?></h4>
								<p><?=lang('Ustaw nowe hasło do konta')?></p>
							</div>
						</a>
					</li>
					<? } ?>
				</ul>
			</li>
			<li>
				<a href="<?=site_url('admin/mod_help/help/index')?>">
					<i class="fa fa-question"></i> <?=lang('Pomoc')?>
				</a>
			</li>
			<? if($this->user !== FALSE) { ?>
			<li>
				<a href="<?=site_url('admin/logout')?>">
					<i class="fa fa-sign-out"></i> <?=lang('Wyloguj')?>
				</a>
			</li>
			<? } ?>
		</ul>
	</div>
	<? if($this->user !== FALSE) { ?>
	<div class="user-info pull-right">
		<i class="fa fa-user"></i>
		<?=$this->user->name?>
	</div>
	<? } ?>
</nav>