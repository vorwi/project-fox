<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj budynek') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?= form_open($this->mod_url . '/buildings/id/' . $this->params->id . '/action/add'); ?>
		<div class="form-group">
			<?=form_label(lang('Nazwa budynku')) ?>
			<?=form_input('name', set_value('name'), 'class="form-control"') ?>
		</div>
		<div class="form-group">
			<?=form_label(lang('Rzut boku budynku')) ?>
			<?=form_fileinput('face_image', set_value('face_image'))?>
		</div>
		<?=form_submit('', lang('Dodaj'), 'class="btn btn-primary"') ?>
		<?= form_close() ?>
	</div>
</div>
<? if ($items !== FALSE){ ?>
	<?=form_open();?>
	<table class="table table-bordered table-hover sortable">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th style=""><?=lang('Nazwa') ?></th>
				<th style="width:70px;"><?=lang('Kolejność') ?></th>
				<th style="width:180px;"><?=lang('Data modyfikacji') ?></th>
				<th style="width:280px;"><?=lang('Działania') ?></th>
			</tr>
		</thead>
		<tbody>
			<? $i = 1 ?>
			<? foreach ($items as $row){ ?>
				<tr data-id="<?= $row->id ?>">
					<td><b><?= $i ?></b></td>
					<td class="text-left">
						<?= anchor("{$this->mod_url}/buildings/id/" . $this->params->id . "/action/edit/edib/" . $row->id, $row->name) ?>
					</td>
					<td class="text-center">
						<span class="fa fa-arrows" title="Zmień kolejność" class="move-value"></span>
					</td>
					<td class="text-center"><?= date('d.m.Y H:i:s', strtotime($row->lastmod_date)) ?></td>
					<td class="text-center nc-options">
						<?= anchor("{$this->mod_url}/buildings/id/" . $this->params->id . "/action/edit/edib/" . $row->id, lang('Edytuj')) ?> 
						<?
						if ($row->pub == 0){
							echo anchor("{$this->mod_url}/buildings/id/{$this->params->id}/action/pub/set/1/edib/{$row->id}", lang('Publikuj'));
						}else{
							echo anchor("{$this->mod_url}/buildings/id/{$this->params->id}/action/pub/set/0/edib/{$row->id}", lang('Odpublikuj'));
						}
						?>
						<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/buildings/id/{$this->params->id}/action/delete/edib/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>">
							<?=lang('Usuń') ?>
						</a>
					</td>
				</tr>
				<? $i++ ?>
			<? } ?>
		</tbody>
	</table>
	<?= form_close() ?>
	<script>
		$(document).ready(function () {
			$('.sortable tbody').sortable({
				items: "> tr[data-id]",
				//handle: ".move-value",
				update: function (event, ui) {
					var data = {};
					data.<?=config_item('csrf_token_name')?> = $('input[name="<?=config_item('csrf_token_name')?>"]').val();
					data.value = {};
					$('.sortable tbody > tr[data-id]').each(function (idx, el) {
						var el = $(el);
						data.value[el.data('id')] = idx;
					});
					$.ajax({
						url: "<?= base_url(); ?><?=$this->mod_url ?>/buildings/id/<?= $this->params->id?>/action/save_position",
						type: "POST",
						data: data,
						async: false,
						success: function (json) {
							$('#msg_ok').remove();
							$('.headline').after('<p id="msg_ok" class="cms_msg accept">Kolejność została zapisana.</p>');
						}
					});
				}
			});
		});
	</script>
<? }else{ ?>
	<?= msg(lang('Nie znaleziono żadnych budynków.'), 1) ?>
<? } ?>