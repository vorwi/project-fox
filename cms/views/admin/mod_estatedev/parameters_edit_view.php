<?= form_open(current_url()); ?>
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">
	<div class="row">
		<div class="col-md-7">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<?=form_label(lang('Nazwa parametru nieruchomości')) ?>
						<?=form_input('name', $item->name, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?=form_label(lang('Typ pola')) ?>
						<select name="type" class="select_simple form-control parameter_type">
							<? foreach($this->config->item('field_types', 'estatedev') as $ft){
								echo '<option '.set_select('type', $ft->fielt_type, ($item->type == $ft->fielt_type)).' value="'.$ft->fielt_type.'">'.lang($ft->field_name).'</option>';
							} ?>
						</select>
					</div>
					<div class="form-group parameter-field show-type-predefined_parameter">
						<?=form_label(lang('Predefioniowana wartość')) ?>
						<select name="predefined_parameter" class="select_simple form-control">
							<? foreach($this->config->item('predefined_parameters', 'estatedev') as $ft){
								echo '<option '.set_select('type', $ft->parameter_method, ($item->predefined_parameter == $ft->parameter_method)).' value="'.$ft->parameter_method.'">'.lang($ft->parameter_name).'</option>';
							} ?>
						</select>
					</div>
					<div class="form-group">
						<div class="row parameter-field all">
							<div class="col-sm-5 form-control-static"><?=lang('Czy widoczny w wyszukiwarce')?>:</div>
							<div class="col-sm-7 form-control-static">
								<label class="radio-inline"><input name="searchable" type="radio" value="1" <?=set_radio('searchable', 1, !!$item->searchable)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="searchable" type="radio" value="0" <?=set_radio('searchable', 0, !$item->searchable)?>> <?=lang('nie')?></label>
							</div>
						</div>
						<div class="row parameter-field all">
							<div class="col-sm-5 form-control-static"><?=lang('Czy widoczny na liście nieruchomości')?>:</div>
							<div class="col-sm-7 form-control-static">
								<label class="radio-inline"><input name="in_table" type="radio" value="1" <?=set_radio('in_table', 1, !!$item->in_table)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="in_table" type="radio" value="0" <?=set_radio('in_table', 0, !$item->in_table)?>> <?=lang('nie')?></label>
							</div>
						</div>
						<div class="row parameter-field show-type-select show-type-radio show-type-checkbox show-type-predefined_parameter">
							<div class="col-sm-5 form-control-static"><?=lang('Widnieje w nazwie nieruchomości')?>:</div>
							<div class="col-sm-7 form-control-static">
								<label class="radio-inline"><input name="in_name" type="radio" value="1" <?=set_radio('in_name', 1, !!$item->in_name)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="in_name" type="radio" value="0" <?=set_radio('in_name', 0, !$item->in_name)?>> <?=lang('nie')?></label>
							</div>
						</div>
						<div class="row parameter-field show-type-select show-type-radio show-type-checkbox show-type-predefined_parameter">
							<div class="col-sm-5 form-control-static"><?=lang('Nazwa parametru widoczna w nazwie nieruchomości')?>:</div>
							<div class="col-sm-7 form-control-static">
								<label class="radio-inline"><input name="in_name_show" type="radio" value="1" <?=set_radio('in_name_show', 1, !!$item->in_name_show)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="in_name_show" type="radio" value="0" <?=set_radio('in_name_show', 0, !$item->in_name_show)?>> <?=lang('nie')?></label>
							</div>
						</div>
						<div class="row parameter-field all">
							<div class="col-sm-5 form-control-static"><?=lang('Na stronie nieruchomości widoczny')?>:</div>
							<div class="col-sm-7 form-control-static radio">
								<label><input name="presentation" type="radio" value="1" <?=set_radio('presentation', 1, $item->presentation === '1')?>> <?=lang('W tabeli parametrów')?></label>
								<label><input name="presentation" type="radio" value="2" <?=set_radio('presentation', 2, $item->presentation === '2')?>> <?=lang('Jako przycisk pod tabelą parametrów')?></label>
								<label><input name="presentation" type="radio" value="3" <?=set_radio('presentation', 3, $item->presentation === '3')?>> <?=lang('W prawej kolumnie')?></label>
							</div>
						</div>
					</div>
					<div class="form-group parameter-field show-type-text">
						<?=form_label(lang('Jednostka')) ?>
						<?='('.lang('np. m<sup>2</sup>').')'?>
						<?=form_input('unit', $item->unit, 'class="form-control"') ?>
					</div>
					<div class="form-group parameter-field all">
						<?=form_label(lang('Ikona')) ?>
						<?=form_fileinput('icon', $item->icon)?>
					</div>
					<div class="form-group show-presentation-2">
						<?=form_label(lang('Ikona widoczna w przycisku')) ?>
						<?=form_fileinput('icon_button', $item->icon_button)?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Szczegóły')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group form-inline">
						<label><?=lang('Data dodania')?>:</label>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->add_date))?></p>
					</div>
					<div class="form-group form-inline">
						<label><?=lang('Data modyfikacji')?>:</label>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->lastmod_date))?></p>
					</div>
					<div class="form-group">
						<label><?=lang('Link do używania w panelu')?>:</label>
						<input type="text" value="<?=$item->url?>" class="form-control">
					</div>
					<div class="form-group form-inline">
						<label><?=lang('Język')?>:</label>
						<p class="form-control-static"><?= $item->lang->lang ?> <?=lang_flag($item->lang)?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div 
	class="box box-success parameter-field show-type-select show-type-radio show-type-checkbox parameter values"
	<? if ($item->type === 'text' || $item->type === 'predefined_parameter'){ echo ' style="display:none;"'; } ?>
	>
		<div class="box-header">
			<h3 class="box-title"><?=lang('Zdefioniowane wartości')?></h3>
		</div>
		<div class="box-body">
			<div>
				<button type="button" class="canvas-btn btn btn-info add-value"><i class="fa fa-plus"></i> <?=lang('Dodaj wartość') ?></button>
			
				<table class="added-values table table-bordered table-hover" style="width:766px;">
					<thead>
						<tr>
							<th style="width:100px;"><?=lang('ID') ?></th>
							<th><?=lang('Wartość') ?></th>
							<th style="width:180px;"><?=lang('Działania') ?></th>
						</tr>
					</thead>
					<tbody>
						<? if ($item->values){ ?>
							<? foreach ($item->values as $v){ ?>
								<tr class="value" data-id="<?= $v->id ?>" style="background-color: #fff;">
									<td style="vertical-align: middle;">
										<?=lang('Wartość') ?> #<?= $v->id ?>
									</td>
									<td>
										<input class="position" type="hidden" name="position[<?= $v->id ?>]" value="<?= $v->position ?>" />
										<input type="text" name="value[<?= $v->id ?>][<?= $lang ?>]" value="<?= (isset($v->content[$lang])) ? $v->content[$lang]->value : '' ?>" class="form-control" />
									</td>
									<td class="text-center">
										<button type="button" class="btn btn-danger delete-value"><span class="fa fa-trash"></span></button>
										<button type="button" class="btn btn-info move-value"><span class="fa fa-arrows"></span></button>
									</td>
								</tr>
							<? } ?>
						<? }else{ ?>
							<tr class="alert-msg"><td colspan="3"><?= msg(lang('Nie ma dodanych żadnych wartości'), 1) ?></td></tr>
						<? } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
		
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary" />
<?= form_close(); ?>

<script>
	$(document).ready(function () {
		var no_added_values = '<tr class="alert-msg"><td colspan="3"><?= str_replace(array("\r", "\n"), '', addslashes(msg(lang('Nie ma dodanych żadnych wartości'), 1))) ?></td></tr>';

		$('select[name=type]').on('change', function () {
			$('.parameter-field').not('.all').hide();
			$('.parameter-field.show-type-' + $(this).val()).show();
		});
		$('select[name=type]').trigger('change');

		$('input[name=presentation]').on('change', function () {
			if ($('input[name=presentation]:checked').val() !== '2') { 
				$('.show-presentation-2').hide();
			} else {
				$('.show-presentation-2').show();
			}
		});
		$('input[name=presentation]').trigger('change');

		$('.add-value').on('click', function () {
			$('.added-values .alert-msg').remove();
			var next_id = 0;
			$('.added-values tbody .value').each(function (idx, el) {
				var el = $(el);
				if (el.data('id') > next_id) {
					next_id = el.data('id');
				}
			});
			next_id++;

			var next_pos = 0;
			$('.added-values input.position').each(function (idx, el) {
				var el = $(el);
				if (el.val() > next_pos) {
					next_pos = el.val();
				}
			});
			next_pos++;
			var element = '<tr class="value" data-id="{id}" style="background-color: #fff;">';
			element += '<td style="vertical-align: middle;"><?=lang('Wartość')?> #{id}</td>';
			element += '<td>';
			element += '<input type="hidden" class="position" name="position[{id}]" value="' + next_pos + '" />';
			element += '<input type="text" name="value[{id}][<?= $lang ?>]" value="" class="form-control" /> ';
			element += '</td>';
			element += '<td class="text-center">';
			element += '<button type="button" class="btn btn-danger delete-value"><span class="fa fa-trash"></span></button> ';
			element += '<button type="button" class="btn btn-info move-value"><span class="fa fa-arrows"></span></button> ';
			element += '</td>';
			element += '</tr>';
			element = element.replace(new RegExp('{id}', 'g'), next_id);
			
			$('.added-values tbody').append(element);

		});

		$('.added-values').on('click', '.delete-value', function (e) {
			if (confirm('<?=lang('Czy na pewno chcesz usunąć? Wartość zostanie usunięta także z pozostałych języków.')?>')) {
				$(this).parents('tr').remove();
				if ($('.added-values tr.value').length <= 0) {
					$('.added-values tbody').append(no_added_values);
				}
			}
		});

		var fixHelper = function(e, ui) {  
			ui.children().each(function() {  
				$(this).width($(this).width());  
			});  
			return ui;  
		};
		$('.added-values tbody').sortable({
			helper: fixHelper,
			handle: '.move-value',
			cancel: '',
			items: '> tr.value',
			update: function (event, ui) {
				$('.added-values input.position').each(function (idx, el) {
					var el = $(el);
					el.val(idx + 1);
				});
			}
		});
	});
</script>
