<?=form_open(current_url().'/check/1');?>
<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">
<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<?=form_label(lang('Imię')) ?>
					<input type="text" name="name" value="<?=set_value('name', $item->name)?>" class="form-control" />
				</div>
				<div class="form-group">
					<?=form_label(lang('Nazwisko')) ?>
					<input type="text" name="surname" value="<?=set_value('surname', $item->surname)?>" class="form-control" />
				</div>
				<div class="form-group">
					<?=form_label(lang('E-mail')) ?>
					<input type="text" name="email" value="<?=set_value('email', $item->email)?>" class="form-control" />
				</div>
				<div class="form-group">
					<?=form_label(lang('Numer telefonu')) ?>
					<input type="text" name="phone" value="<?=set_value('phone', $item->phone)?>" class="form-control" />
				</div>
				<div class="form-group">
					<?=form_label(lang('Stanowisko')) ?>
					<input type="text" name="office" value="<?=set_value('office', $item->office)?>" class="form-control" />
				</div>
				<fieldset class="form-group" style="max-height:400px;overflow:auto;">
					<legend class="col-form-legend"><?=lang('Kontakt widoczny przy typach nieruchomości')?></legend>	
					<?php if(isset($types) && is_array($types) && count($types) > 0) {?>
						<?php $post_types = $this->input->post('type');?>
						<?php foreach($types as $type) {?>
							<div class="form-check">
								<label class="form-check-label">
									<?=form_checkbox(array('class'=>'form-check-input', 'name' => 'type[' . $type->id . ']', 'value' => $type->id, 'checked' => ($type->selected == 1) ? TRUE : FALSE))?>
									<?=$type->name?>
								</label>
							</div>
						<?php }?>
					<?php } else {?>
						<?=mag(lang('Aby kontakt był widoczny na stronie muszą być zdefioniowane typy nieruchomości oraz kontakt musi być przypisany co najmniej do jednego z nich.'),1)?>
					<? } ?>	
				</fieldset>
				<div class="form-group">
					<?=form_label(lang('Zdjęcie')) ?>
					<?=form_fileinput('image', $item->image)?>
				</div>
			</div>
		</div>	
	</div>
	<div class="col-md-5">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Szczegóły')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group form-inline">
					<label><?=lang('Data dodania')?>:</label>
					<p class="form-control-static"><?=$item->add_date?></p>
				</div>
				<div class="form-group form-inline">
					<label><?=lang('Data modyfikacji')?>:</label>
					<p class="form-control-static"><?=$item->lastmod_date?></p>
				</div>
				<div class="form-group">
					<label><?=lang('Opublikowany')?>:</label>
					<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$item->pub)?>> <?=lang('tak')?></label>
					<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$item->pub)?>> <?=lang('nie')?></label>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">

<?= form_close(); ?>
