<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj kontakt')?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add');?>
			<div class="form-group">
				<?=form_label(lang('Imię')) ?>
				<input type="text" name="name" class="form-control" value="<?=set_value('name')?>">
			</div>
			<div class="form-group">
				<?=form_label(lang('Nazwisko')) ?>
				<input type="text" name="surname" class="form-control" value="<?=set_value('surname')?>">
			</div>
			<div class="form-group">
				<?=form_label(lang('E-mail')) ?>
				<input type="text" name="email" class="form-control" value="<?=set_value('email')?>">
			</div>
			<div class="form-group">
				<?=form_label(lang('Numer telefonu')) ?>
				<input type="text" name="phone" class="form-control" value="<?=set_value('phone')?>">
			</div>
			<div class="form-group">
				<?=form_label(lang('Stanowisko')) ?>
				<input type="text" name="office" class="form-control" value="<?=set_value('office')?>">
			</div>			
			<fieldset class="form-group" style="max-height:400px;overflow:auto;">
				<legend class="col-form-legend"><?=lang('Kontakt widoczny przy typach nieruchomości')?></legend>	
				<?php if(isset($types) && is_array($types) && count($types) > 0) { ?>
					<?php $post_types = $this->input->post('type');?>
					<?php foreach($types as $type) {?>
						<div class="form-check">
							<label class="form-check-label">
								<?=form_checkbox(array('class'=>'form-check-input', 'name' => 'type[' . $type->id . ']', 'value' => $type->id, 'checked' => (!empty($post_types) && in_array($type->id, $post_types)) ? TRUE : FALSE))?>
								<?=$type->name?>
							</label>
						</div>
					<?php }?>
				<?php } else {?>
					<?=msg(lang('Aby kontakt był widoczny na stronie muszą być zdefioniowane typy nieruchomości oraz kontakt musi być przypisany co najmniej do jednego z nich.'),1)?>
				<? } ?>	
			</fieldset>
			<div class="form-group">
				<?=form_label(lang('Zdjęcie')) ?>
				<?=form_fileinput('image', set_value('image'))?>
			</div>
			<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>


<? if ($items !== FALSE){ ?>
<?
echo form_open();
$i = 1;
?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th><?=lang('Imię i nazwisko') ?></th>
				<th><?=lang('Widoczny przy typach nieruchomości') ?></th>
				<th style="width:180px;"><?=lang('Data modyfikacji') ?></th>
				<th style="width:40px;"><?=lang('Kolejność') ?></th>
				<th style="width:250px;"><?=lang('Działania') ?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox" /></th>
			</tr>
		</thead>
		<tbody class="sortable">
			<? foreach ($items as $row){ ?>
			<tr>
				<td><b><?=$i?></b></td>
				<td>
					<?=anchor($this->mod_url.'/edit/id/'.$row->id.'/lang/'.$this->admin->get_main_lang(), $row->name.' '.$row->surname)?>
					<? if (!empty($row->url)) echo ' <i class="fa fa-external-link" title="'.lang('Prowadzi do').': '.$row->url.'"></i>'; ?>
					<?=lang_versions($lang, $row, $this->mod_url.'/edit/')?>
				</td>
				<td class="text-center"><?=$row->selected_types?></td>
				<td class="text-center"><?= date('d.m.Y H:i:s', strtotime($row->lastmod_date)) ?></td>
				<td class="text-center">
					<span class="fa fa-arrows"></span><input class="currentposition" type="hidden" style="width:30px;" name="position[<?=$row->id?>]" value="<?=$row->position?>">
				</td>
				<td class="text-center nc-options">
					<?=anchor($this->mod_url.'/edit/id/'.$row->id.'/lang/'.$this->admin->get_main_lang(), lang('Edytuj'))?>
					<?
					if ($row->pub == 0)	{
						echo anchor($this->mod_url.'/vswitch/column/pub/set/1/id/'.$row->id, lang('Publikuj'));
					}else {
						echo anchor($this->mod_url.'/vswitch/column/pub/set/0/id/'.$row->id, lang('Odpublikuj'));
					}
					?>
					<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
				</td>
				<td><input type="checkbox"  value="<?=$row->id?>" name="check[]" /></td>
			</tr>
			<? $i++ ?>
			<? } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="5" class="text-right">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>" >
				</td>
				<td colspan="2" class="nc-options">
					<?=lang('Zaznaczone:') ?>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
				</td>
			</tr>
		</tfoot>
	</table>
<?= form_close() ?>
<? }else{ ?>
	<?= msg(lang('Nie znaleziono żadnych kontaktów.'), 1) ?>
<? } ?>