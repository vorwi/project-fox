<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj inwestycję') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?= form_open($this->mod_url . '/add'); ?>
			<div class="form-group">
				<?=form_label(lang('Nazwa')) ?>
				<?=form_input('name', set_value('name'), 'class="form-control"') ?>
			</div>
			<?=form_submit('', lang('Dodaj'), 'class="btn btn-primary"') ?>
		<?= form_close() ?>
	</div>
</div>

<? if ($items !== FALSE){ ?>
	<?=form_open();?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th><?=lang('Nazwa') ?></th>
				<th style="width:70px;"><?=lang('Kolejność') ?></th>
				<th style="width:180px;"><?=lang('Data modyfikacji') ?></th>
				<th style="width:300px;"><?=lang('Działania') ?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox"  /></th>
			</tr>
		</thead>
		<tbody class="sortable">
			<? $i = 1 ?>
			<? foreach ($items as $row){ ?>
				<tr id="listItem_<?=$row->id?>">
					<td><b><?= $i ?></b></td>
					<td class="text-left">
						<? 
						echo anchor("{$this->mod_url}/edit/id/" . $row->id . "/lang/" . $this->admin->get_main_lang(), $row->name);
						if(!empty($row->url)){
							echo ' <i class="fa fa-external-link" title="'.lang('Prowadzi do').': '.$row->url.'"></i>';
						}
						echo lang_versions($lang, $row, $this->mod_url.'/edit');
						?>
					</td>
					<td class="text-center">
						<span class="fa fa-arrows"></span>
						<input class="currentposition" type="hidden" value="<?=$row->position?>" name="position[<?=$row->id?>]">
					</td>
					<td class="text-center"><?= date('d.m.Y H:i:s', strtotime($row->lastmod_date)) ?></td>
					<td class="text-center nc-options">
						<?= anchor("{$this->mod_url}/edit/id/" . $row->id . "/lang/" . $this->admin->get_main_lang(), lang('Edytuj')) ?>
						<?= anchor("{$this->mod_url}/images/id/" . $row->id . "/lang/" . $this->admin->get_main_lang(), lang('Zdjęcia')) ?>
						<?= anchor("{$this->mod_url}/buildings/id/" . $row->id, lang('Budynki')) ?>
						<?
						if ($row->pub == 0){
							echo anchor("{$this->mod_url}/vswitch/column/pub/set/1/id/{$row->id}", lang('Publikuj'));
						}else{
							echo anchor("{$this->mod_url}/vswitch/column/pub/set/0/id/{$row->id}", lang('Odpublikuj'));
						}
						?>
						<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>">
							<?=lang('Usuń') ?>
						</a>
					</td>
					<td class="text-center"><input type="checkbox"  value="<?= $row->id ?>" name="check[]" /></td>
				</tr>
				<? $i++ ?>
			<? } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>">
				</td>
				<td colspan="3" class="nc-options">
					<?=lang('Zaznaczone:') ?>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>">
						<?=lang('Usuń') ?>
					</a>
				</td>
			</tr>
		</tfoot>
	</table>
	<?= form_close() ?>
<? }else{ ?>
	<?= msg(lang('Nie znaleziono żadnych inwestycji.'), 1) ?>
<? } ?>