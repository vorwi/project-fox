<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj nieruchomość')?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add');?>
			<div class="form-group">
				<label><?=lang('Typ nieruchomości')?></label>
				<select name="id_edt" class="form-control">
					<? foreach ($types as $type){ ?>
					<option value="<?=$type->id?>" <?=set_select('id_edt', $type->id)?>><?= $type->name ?></option>
					<? } ?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Inwestycja')?></label>
				<select name="id_edi" class="form-control">
					<? foreach ($investments as $investment){ ?>
					<option value="<?=$investment->id?>" <?=set_select('id_edi', $investment->id)?>><?= $investment->name ?></option>
					<? } ?>
				</select>
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<? if ($items !== FALSE){ ?>
	<?=form_open();?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Id')?></th>
				<th><?=lang('Nazwa')?></th>				
				<th style="width:180px;"><?=lang('Data modyfikacji') ?></th>
				<th style="width:40px;"><?=lang('Kolejność') ?></th>
				<th style="width:250px;"><?=lang('Działania') ?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox" /></th>
			</tr>
		</thead>
		<tbody class="sortable">
			<? foreach ($items as $row){ ?>
			<tr>
				<td><b>#<?=$row->id?></b></td>
				<td>
					<?=anchor($this->mod_url.'/edit/id/'.$row->id.'/lang/'.$this->admin->get_main_lang(), $row->name)?>
					<? 
					if (!empty($row->url)) {
						echo ' <i class="fa fa-external-link" title="'.lang('Prowadzi do').': '.$row->url.'"></i>'; 
					}
					?>
					<?=lang_versions($lang, $row, $this->mod_url.'/edit/')?>
				</td>
				<td class="text-center"><?= date('d.m.Y H:i:s', strtotime($row->lastmod_date)) ?></td>
				<td class="text-center">
					<span class="fa fa-arrows"></span><input class="currentposition" type="hidden" style="width:30px;" name="position[<?=$row->id?>]" value="<?=$row->position?>">
				</td>
				<td class="text-center nc-options">
					<?=anchor($this->mod_url.'/edit/id/'.$row->id.'/lang/'.$this->admin->get_main_lang(), lang('Edytuj'))?>
					<?
					if ($row->pub == 0)	{
						echo anchor($this->mod_url.'/vswitch/column/pub/set/1/id/'.$row->id, lang('Publikuj'));
					}else {
						echo anchor($this->mod_url.'/vswitch/column/pub/set/0/id/'.$row->id, lang('Odpublikuj'));
					}
					?>
					<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
				</td>
				<td><input type="checkbox"  value="<?=$row->id?>" name="check[]" /></td>
			</tr>
			<? } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4" class="text-right">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>" >
				</td>
				<td colspan="2" class="nc-options">
					<?=lang('Zaznaczone:') ?>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
				</td>
			</tr>
		</tbody>
	</table>
	<?= form_close() ?>

<? }else{ ?>
	<?=msg(lang('Nie znaleziono żadnych nieruchomości.'), 1)?>
<? } ?>