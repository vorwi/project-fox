<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj parametr nieruchomości') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?= form_open($this->mod_url . '/add'); ?>
			<div class="form-group">
				<?=form_label(lang('Nazwa parametru nieruchomości')) ?>
				<?=form_input('name', set_value('name'), 'class="form-control"') ?>
			</div>
			<div class="form-group">
				<?=form_label(lang('Typ pola')) ?>
				<select name="type" class="select_simple form-control parameter_type">
					<? foreach($this->config->item('field_types', 'estatedev') as $ft){
						echo '<option '.set_select('type', $ft->fielt_type).' value="'.$ft->fielt_type.'">'.lang($ft->field_name).'</option>';
					} ?>
				</select>
			</div>
			<div class="form-group parameter-field show-type-predefined_parameter">
				<?=form_label(lang('Predefioniowana wartość')) ?>
				<select name="predefined_parameter" class="select_simple form-control">
					<? foreach($this->config->item('predefined_parameters', 'estatedev') as $ft){
						echo '<option '.set_select('type', $ft->parameter_method).' value="'.$ft->parameter_method.'">'.lang($ft->parameter_name).'</option>';
					} ?>
				</select>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Czy widoczny w wyszukiwarce')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="searchable" type="radio" value="1" <?=set_radio('searchable', 1, TRUE)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="searchable" type="radio" value="0" <?=set_radio('searchable', 0)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Czy widoczny na liście nieruchomości')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="in_table" type="radio" value="1" <?=set_radio('in_table', 1, TRUE)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="in_table" type="radio" value="0" <?=set_radio('in_table', 0)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Widnieje w nazwie nieruchomości')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="in_name" type="radio" value="1" <?=set_radio('in_name', 1, TRUE)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="in_name" type="radio" value="0" <?=set_radio('in_name', 0)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Nazwa parametru widoczna w nazwie nieruchomości')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="in_name_show" type="radio" value="1" <?=set_radio('in_name_show', 1, TRUE)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="in_name_show" type="radio" value="0" <?=set_radio('in_name_show', 0)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Na stronie nieruchomości widoczny')?>:</div>
					<div class="col-sm-7 form-control-static radio">
						<label><input name="presentation" type="radio" value="1" <?=set_radio('presentation', 1, TRUE)?>> <?=lang('W tabeli parametrów')?></label>
						<label><input name="presentation" type="radio" value="2" <?=set_radio('presentation', 2)?>> <?=lang('Jako przycisk pod tabelą parametrów')?></label>
						<label><input name="presentation" type="radio" value="3" <?=set_radio('presentation', 3)?>> <?=lang('W prawej kolumnie')?></label>
					</div>
				</div>
			</div>
			<div class="form-group parameter-field show-type-select show-type-radio show-type-checkbox">
				<?=form_label(lang('Jednostka')) ?>
				<?='('.lang('np. m<sup>2</sup>').')'?>
				<?=form_input('unit', set_value('unit'), 'class="form-control"') ?>
			</div>
			<div class="form-group">
				<?=form_label(lang('Ikona')) ?>
				<?=form_fileinput('icon', set_value('icon'))?>
			</div>
			<div class="form-group show-presentation-2">
				<?=form_label(lang('Ikona widoczna w przycisku')) ?>
				<?=form_fileinput('icon_button', set_value('icon_button'))?>
			</div>
			<?=form_submit('', lang('Dodaj'), 'class="btn btn-primary"') ?>
		<?= form_close() ?>
	</div>
</div>
<script>
	$(document).ready(function () {
		$('.parameter_type').on('change', function () {
			$('.parameter-field').not('.all').hide();
			$('.parameter-field.show-type-' + $(this).val()).show();
		});
		$('.parameter_type').trigger('change');

		$('input[name=presentation]').on('change', function () {
			if ($('input[name=presentation]:checked').val() !== '2') {
				$('.show-presentation-2').hide();
			} else {
				$('.show-presentation-2').show();
			}
		});
		$('input[name=presentation]').trigger('change');
	});
</script>

<? if ($items !== FALSE){ ?>
	<?=form_open();?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th style=""><?=lang('Nazwa') ?></th>
				<th style="width:120px;"><?=lang('W nazwie nieruchomości') ?></th>
				<th style="width:120px;"><?=lang('Na liście nieruchomości') ?></th>
				<th style="width:120px;"><?=lang('W wyszukiwarce') ?></th>
				<th style="width:180px;"><?=lang('Data modyfikacji') ?></th>
				<th style="width:70px;"><?=lang('Kolejność') ?></th>
				<th style="width:250px;"><?=lang('Działania') ?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox"  /></th>
			</tr>
		</thead>
		<tbody class="sortable">
			<? $i = 1 ?>
			<? foreach ($items as $row){ ?>
				<tr id="listItem_<?=$row->id?>">
					<td><b><?= $i ?></b></td>
					<td class="text-center">
						<? 
						echo anchor("{$this->mod_url}/edit/id/" . $row->id . "/lang/" . $this->admin->get_main_lang(), $row->name);
						if(!empty($row->url)){
							echo ' <i class="fa fa-external-link" title="'.lang('Prowadzi do').': '.$row->url.'"></i>';
						}
						echo lang_versions($lang, $row, $this->mod_url.'/edit');
						?>
					</td>
					<td class="text-center">
						<? 
						if($row->in_name == 0) {
							echo anchor("{$this->mod_url}/vswitch/column/in_name/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Ustaw jako widoczny w nazwie nierochomości'), 'class' => 'anchor_icon'));
						}else{
							echo anchor("{$this->mod_url}/vswitch/column/in_name/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Ustaw jako niewidoczny w nazwie nierochomości'), 'class' => 'anchor_icon'));
						}
						?>
					</td>
					<td class="text-center">
						<? 
						if($row->in_table == 0) {
							echo anchor("{$this->mod_url}/vswitch/column/in_table/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Ustaw jako widoczny na liście nierochomości'), 'class' => 'anchor_icon'));
						}else{
							echo anchor("{$this->mod_url}/vswitch/column/in_table/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Ustaw jako niewidoczny na liście nierochomości'), 'class' => 'anchor_icon'));
						}
						?>
					</td>
					<td class="text-center">
						<? 
						if($row->searchable == 0) {
							echo anchor("{$this->mod_url}/vswitch/column/searchable/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Ustaw jako widoczny w wyszukiwarce'), 'class' => 'anchor_icon'));
						}else{
							echo anchor("{$this->mod_url}/vswitch/column/searchable/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Ustaw jako niewidoczny w wyszukiwarce'), 'class' => 'anchor_icon'));
						}
						?>
					</td>
					<td class="text-center"><?= date('d.m.Y H:i:s', strtotime($row->lastmod_date)) ?></td>
					<td class="text-center">
						<span class="fa fa-arrows"></span>
						<input class="currentposition" type="hidden" value="<?=$row->position?>" name="position[<?=$row->id?>]">
					</td>
					<td class="text-center nc-options">
						<?= anchor("{$this->mod_url}/edit/id/" . $row->id . "/lang/" . $this->admin->get_main_lang(), lang('Edytuj')) ?>
						<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>">
							<?=lang('Usuń') ?>
						</a>
					</td>
					<td class="text-center"><input type="checkbox"  value="<?= $row->id ?>" name="check[]" /></td>
				</tr>
				<? $i++ ?>
			<? } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="7">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>">
				</td>
				<td colspan="2" class="nc-options">
					<?=lang('Zaznaczone:') ?>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>">
						<?=lang('Usuń') ?>
					</a>
				</td>
			</tr>
		</tfoot>
	</table>
	<?= form_close() ?>
<? }else{ ?>
	<?= msg(lang('Nie znaleziono żadnych parametrów nieruchomości.'), 1) ?>
<? } ?>