<?= form_open(current_url()); ?>
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">
	<div class="row">
		<div class="col-md-7">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<?=form_label(lang('Inwestycja')) ?>
						<?=form_input('name', $item->name, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?=form_label(lang('URL')) ?>
						<?=form_input('url', $item->url, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?=form_label(lang('Meta-tagi (SEO)')) ?>
						<div class="row">
							<div class="col-sm-4 form-control-static"><?=lang('Tytuł (title)')?>:</div>
							<div class="col-sm-8">
								<input type="text" name="meta_title" value="<?=set_value('meta_title', $item->meta_title)?>" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 form-control-static"><?=lang('Opis (description)')?>:</div>
							<div class="col-sm-8">
								<input type="text" name="meta_description" value="<?=set_value('meta_description', $item->meta_description)?>" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 form-control-static"><?=lang('Słowa kluczowe (keywords)')?>:</div>
							<div class="col-sm-8">
								<input type="text" name="meta_keywords" value="<?=set_value('meta_keywords', $item->meta_keywords)?>" class="form-control">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Szczegóły')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group form-inline">
						<?=form_label(lang('Data dodania')) ?>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->add_date))?></p>
					</div>
					<div class="form-group form-inline">
						<?=form_label(lang('Data modyfikacji')) ?>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->lastmod_date))?></p>
					</div>
					<div class="form-group">
						<?=form_label(lang('Opublikowany')) ?>
						<div class="row">
							<label class="col-sm-3"><?=lang('cały wpis')?></label>
							<div class="col-sm-9">
								<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$item->pub)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$item->pub)?>> <?=lang('nie')?></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<?=form_label(lang('Link do używania w panelu')) ?>
						<input type="text" value="<?=$item->url?>" class="form-control">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box box-success">
		<div class="box-body">
			<div class="form-group">
				<?=form_label(lang('Adres')) ?>
				<?=form_input('address', $item->address, array('class' => 'form-control', 'id' => 'address')) ?>
			</div>

			<div class="form-group">
				<?=form_label(lang('Pozycja na mapie')) ?>
				<div id="map_msg"></div>
				<input type="hidden" id="map_coords" name="map_coords" value="<?= $item->map_coords ?>" />
				<div id="map" style="width:100%; height:200px;"></div>
				<script>
					var map;
					function initMap() {
						var geocoder = new google.maps.Geocoder();
						var position = new google.maps.LatLng<?= $item->map_coords ?>;
		
						map = new google.maps.Map(document.getElementById('map'), {
							center: position,
							zoom: 12
						});
						var marker = new google.maps.Marker({
							position: position,
							map: map,
							draggable: true,
							title: "<?=lang('Przeciągnij w odpowiednie miejsce')?>"
						});
						marker.addListener('dragend', function () {
							map.setCenter(marker.getPosition());
							$('#map_coords').val(marker.getPosition());
						});
		
						$('#adress_to_position').on('click', function () {
							var address = $('#address').val();
							geocoder.geocode({'address': address}, function (results, status) {
								if (status == google.maps.GeocoderStatus.OK) {
									marker.setPosition(results[0].geometry.location);
									$('#map_coords').val(results[0].geometry.location);
									map.setCenter(results[0].geometry.location);
									$('#map_msg').show().html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><?=lang('Pozycja na mapie została zmodyfikowana.')?></div>').delay(10000).fadeOut(250);
								} else {
									var real_status = '';
									switch (status) {
										case 'ZERO_RESULTS':
											real_status = '<?=lang('brak wyników wyszukiwania')?>';
											break;
										case 'OVER_QUERY_LIMIT':
											real_status = '<?=lang('przekroczony limit zapytań')?>';
											break;
										case 'REQUEST_DENIED':
											real_status = '<?=lang('odmowa dostępu')?>';
											break;
										case 'INVALID_REQUEST':
											real_status = '<?=lang('nieprawidłowe żądanie')?>';
											break;
										case 'UNKNOWN_ERROR':
											real_status = '<?=lang('nieznany powód')?>';
											break;
										default:
											real_status = '<?=lang('nieznany powód')?>';
											break;
									}
									$('#map_msg').show().html('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><?=lang('Błąd pobierania pozycji na mapie z adresu: ')?>' + real_status + '</div>').delay(10000).fadeOut(250);
								}
							});
						});
					}
				</script>
				<script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>
				<input type="button" id="adress_to_position" value="<?=lang('Pobierz pozycję z pola adresu')?>" class="btn" style="width:250px;" />
			</div>

			<div class="form-group">
				<?=form_label(lang('Skrócony opis')) ?>
				<?= ckeditor('admission', set_value('admission', $item->admission, FALSE), '100%', 'Simple', 120) ?>
			</div>
		
			<div class="form-group">
				<?=form_label(lang('Pełny opis')) ?>
				<?= ckeditor('description', set_value('description', $item->description, FALSE), '100%') ?>
			</div>
		</div>
	</div>
	<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">

<?= form_close(); ?>
