<?= form_open(current_url()); ?>
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">
	<div class="row">
		<div class="col-md-7">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<?=form_label(lang('Nazwa typu nieruchomości')) ?>
						<?=form_input('name', $item->name, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?=form_label(lang('Nazwa typu nieruchomości w liczbie mnogiej')) ?>
						<?=form_input('name_plural', $item->name_plural, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?=form_label(lang('URL')) ?>
						<?=form_input('url', $item->url, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?=form_label(lang('Ikona gdy nieaktywna')) ?>
						<?=form_fileinput('icon_inactive', $item->icon_inactive)?>
					</div>
					<div class="form-group">
						<?=form_label(lang('Ikona gdy aktywna')) ?>
						<?=form_fileinput('icon_active', $item->icon_active)?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Szczegóły')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group form-inline">
						<?=form_label(lang('Data dodania')) ?>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->add_date))?></p>
					</div>
					<div class="form-group form-inline">
						<?=form_label(lang('Data modyfikacji')) ?>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->lastmod_date))?></p>
					</div>
					<div class="form-group">
						<?=form_label(lang('Opublikowany')) ?>
						<div class="row">
							<label class="col-sm-3"><?=lang('cały wpis')?></label>
							<div class="col-sm-9">
								<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$item->pub)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$item->pub)?>> <?=lang('nie')?></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<?=form_label(lang('Link do używania w panelu')) ?>
						<input type="text" value="<?=$item->url?>" class="form-control">
					</div>
					<div class="form-group form-inline">
						<?=form_label(lang('Język')) ?>
						<p class="form-control-static"><?= $item->lang->lang ?> <?=lang_flag($item->lang)?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary" />
<?= form_close(); ?>