<script>
(function ($) {

	$.fn.canvasAreaDraw = function (options) {

		this.each(function (index, element) {
			init.apply(element, [index, element, options]);
		});

	}

	var init = function (index, input, options) {

		var points, activePoint, settings;
		var $reset, $canvas, ctx, image;
		var draw, mousedown, stopdrag, move, resize, reset, rightclick, record;

		settings = $.extend({
			imageUrl: $(this).attr('data-image-url')
		}, options);

		if ($(this).val().length) {
			points = $(this).val().split(',').map(function (point) {
				return parseInt(point, 10);
			});
		} else {
			points = [];
		}

		$reset = $('<button type="button" class="btn btn-warning"><i class="fa fa-refresh"></i> <?=lang('Czyść')?></button>');

		$canvas = $('<canvas>');
		ctx = $canvas[0].getContext('2d');

		image = new Image();
		resize = function () {
			$canvas.attr('height', image.height).attr('width', image.width);
			draw();
		};
		$(image).load(resize);
		image.src = settings.imageUrl;
		if (image.loaded)
			resize();
		$canvas.css({background: 'url(' + image.src + ')'});

		$(document).ready(function () {
			//var el = $('<div class="canvas-area"></div>').append($(input), );
			$(input).wrap('<div class="canvas-area"></div>').after('<div class="clearfix"></div>', $canvas, '<div class="clearfix"></div>', $reset);
		});

		reset = function () {
			points = [];
			draw();
		};

		move = function (e) {
			if (!e.offsetX) {
				e.offsetX = (e.pageX - $(e.target).offset().left);
				e.offsetY = (e.pageY - $(e.target).offset().top);
			}
			points[activePoint] = Math.round(e.offsetX);
			points[activePoint + 1] = Math.round(e.offsetY);
			draw();
		};

		stopdrag = function () {
			$(this).off('mousemove', move);
			record();
			activePoint = null;
		};

		rightclick = function (e) {
			e.preventDefault();
			if (!e.offsetX) {
				e.offsetX = (e.pageX - $(e.target).offset().left);
				e.offsetY = (e.pageY - $(e.target).offset().top);
			}
			var x = e.offsetX, y = e.offsetY;
			for (var i = 0; i < points.length; i += 2) {
				dis = Math.sqrt(Math.pow(x - points[i], 2) + Math.pow(y - points[i + 1], 2));
				if (dis < 6) {
					points.splice(i, 2);
					draw();
					record();
					return false;
				}
			}
			return false;
		};

		mousedown = function (e) {
			var x, y, dis, lineDis, insertAt = points.length;

			if (e.which === 3) {
				return false;
			}

			e.preventDefault();
			if (!e.offsetX) {
				e.offsetX = (e.pageX - $(e.target).offset().left);
				e.offsetY = (e.pageY - $(e.target).offset().top);
			}
			x = e.offsetX;
			y = e.offsetY;

			for (var i = 0; i < points.length; i += 2) {
				dis = Math.sqrt(Math.pow(x - points[i], 2) + Math.pow(y - points[i + 1], 2));
				if (dis < 6) {
					activePoint = i;
					$(this).on('mousemove', move);
					return false;
				}
			}

			for (var i = 0; i < points.length; i += 2) {
				if (i > 1) {
					lineDis = dotLineLength(
							x, y,
							points[i], points[i + 1],
							points[i - 2], points[i - 1],
							true
							);
					if (lineDis < 6) {
						insertAt = i;
					}
				}
			}

			points.splice(insertAt, 0, Math.round(x), Math.round(y));
			activePoint = insertAt;
			$(this).on('mousemove', move);

			draw();
			record();

			return false;
		};

		draw = function () {
			ctx.canvas.width = ctx.canvas.width;

			record();
			if (points.length < 2) {
				return false;
			}
			ctx.globalCompositeOperation = 'destination-over';
			ctx.strokeStyle = 'rgb(255,20,20)';
			ctx.lineWidth = 2;

			ctx.beginPath();
			ctx.moveTo(points[0], points[1]);
			for (var i = 0; i < points.length; i += 2) {
				if (i === 0) {
					ctx.fillStyle = 'rgb(255,0,0)'
				} else {
					ctx.fillStyle = 'rgb(255,255,255)'
				}
				ctx.fillRect(points[i] - 3, points[i + 1] - 3, 6, 6);
				ctx.strokeRect(points[i] - 3, points[i + 1] - 3, 6, 6);
				if (points.length > 2 && i > 1) {
					ctx.lineTo(points[i], points[i + 1]);
				}
			}
			ctx.closePath();
			ctx.fillStyle = 'rgba(255,0,0,0.1)';
			ctx.fill();
			ctx.stroke();

		};

		record = function () {
			$(input).val(points.join(','));
		};

		checkcursor = function (e) {
			var cursor = 'default';
			
			if (!e.offsetX) {
				e.offsetX = (e.pageX - $(e.target).offset().left);
				e.offsetY = (e.pageY - $(e.target).offset().top);
			}
			x = e.offsetX;
			y = e.offsetY;

			for (var i = 0; i < points.length; i += 2) {
				dis = Math.sqrt(Math.pow(x - points[i], 2) + Math.pow(y - points[i + 1], 2));
				if (dis < 6) {
					cursor = 'move';
					break;
				}
			}

			$(this).css('cursor', cursor);
		};

		$(input).on('change', function () {
			if ($(this).val().length) {
				points = $(this).val().split(',').map(function (point) {
					return parseInt(point, 10);
				});
			} else {
				points = [];
			}
			draw();
		});

		$(document).find($reset).click(reset);
		$(document).find($canvas).on('mousedown', mousedown);
		$(document).find($canvas).on('mousemove', checkcursor);
		$(document).find($canvas).on('contextmenu', rightclick);
		$(document).find($canvas).on('mouseup', stopdrag);

	};

	$(document).ready(function () {
		$('.canvas-area[data-image-url]').canvasAreaDraw();
	});

	var dotLineLength = function (x, y, x0, y0, x1, y1, o) {
		function lineLength(x, y, x0, y0) {
			return Math.sqrt((x -= x0) * x + (y -= y0) * y);
		}
		if (o && !(o = function (x, y, x0, y0, x1, y1) {
			if (!(x1 - x0))
				return {x: x0, y: y};
			else if (!(y1 - y0))
				return {x: x, y: y0};
			var left, tg = -1 / ((y1 - y0) / (x1 - x0));
			return {x: left = (x1 * (x * tg - y + y0) + x0 * (x * -tg + y - y1)) / (tg * (x1 - x0) + y0 - y1), y: tg * left - tg * x + y};
		}(x, y, x0, y0, x1, y1), o.x >= Math.min(x0, x1) && o.x <= Math.max(x0, x1) && o.y >= Math.min(y0, y1) && o.y <= Math.max(y0, y1))) {
			var l1 = lineLength(x, y, x0, y0), l2 = lineLength(x, y, x1, y1);
			return l1 > l2 ? l2 : l1;
		}
		else {
			var a = y0 - y1, b = x1 - x0, c = x0 * y1 - y0 * x1;
			return Math.abs(a * x + b * y + c) / Math.sqrt(a * a + b * b);
		}
	};
})(jQuery);

/*
* rwdImageMaps jQuery plugin v1.4
*
* Allows image maps to be used in a responsive design by recalculating the area coordinates to match the actual image size on load and window.resize
*
* Copyright (c) 2012 Matt Stow
* https://github.com/stowball/jQuery-rwdImageMaps
* http://mattstow.com
* Licensed under the MIT license
*/
;(function($) {
	$.fn.rwdImageMaps = function() {
		var $img = this,
		version = parseFloat($.fn.jquery);

		var rwdImageMap = function() {
			$img.each(function() {
				if (typeof($(this).attr('usemap')) == 'undefined')
					return;

				var that = this,
				$that = $(that);

				// Since WebKit doesn't know the height until after the image has loaded, perform everything in an onload copy
				$('<img />').load(function() {
					var w,
					h,
					attrW = 'width',
					attrH = 'height';

					// jQuery < 1.6 incorrectly uses the actual image width/height instead of the attribute's width/height
					if (version < 1.6)
						w = that.getAttribute(attrW),
						h = that.getAttribute(attrH);
					else
						w = $that.attr(attrW),
						h = $that.attr(attrH);

					if (!w || !h) {
						var temp = new Image();
						temp.src = $that.attr('src');
						if (!w)
							w = temp.width;
						if (!h)
							h = temp.height;
					}

					var wPercent = $that.width()/100,
					hPercent = $that.height()/100,
					map = $that.attr('usemap').replace('#', ''),
					c = 'coords';

					$('map[name="' + map + '"]').find('area').each(function() {
						var $this = $(this);
						if (!$this.data(c))
							$this.data(c, $this.attr(c));

						var coords = $this.data(c).split(','),
						coordsPercent = new Array(coords.length);

						for (var i = 0; i < coordsPercent.length; ++i) {
							if (i % 2 === 0)
								coordsPercent[i] = parseInt(((coords[i]/w)*100)*wPercent);
							else
								coordsPercent[i] = parseInt(((coords[i]/h)*100)*hPercent);
						}
						$this.attr(c, coordsPercent.toString());
					});

				}).attr('src', $that.attr('src'));
			});
		};
		
		$(window).resize(rwdImageMap).trigger('resize');

		return this;
	};
})(jQuery);
</script>