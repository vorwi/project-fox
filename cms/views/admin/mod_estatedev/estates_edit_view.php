<? $this->load->view('admin/'.config_item('template_a').$this->mod_dir.'scripts_view'); ?>

<?=form_open(current_url().'/check/1');?>
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">
	<div class="row">
		<div class="col-md-7">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<?=form_label(lang('Typ nieruchomości')) ?>:
						<select name="id_edt" class="select_simple form-control">
							<? foreach ($types as $type){ ?>
								<option value="<?= $type->id ?>"<? if ($type->id === $item->id_edt): ?> selected="selected"<? endif; ?>><?= $type->name ?></option>
							<? } ?>
						</select>
					</div>
					<div class="form-group">
						<label><?=lang('Inwestycja') ?>:</label>
						<select name="id_edi" class="select_simple form-control">
							<option value=""></option>
							<? foreach ($investments as $investment){ ?>
								<option value="<?= $investment->id ?>"<? if ($investment->id === $item->id_edi): ?> selected="selected"<? endif; ?>><?= $investment->name ?></option>
							<? } ?>
						</select>
					</div>
					
					<div class="form-group">
						<label><?=lang('Budynek') ?>:</label>
						<select name="id_edib" class="select_simple form-control">
							<option value=""></option>
							<?
							if (isset($investments_maps) && is_array($investments_maps)){
								foreach ($investments_maps as $building){
									?>
									<option data-edi="<?= $building->investment_id ?>" value="<?= $building->building_id ?>"<? if ($building->building_id === $item->id_edib): ?> selected="selected"<? endif; ?>><?= $building->building_name ?></option>
									<?
								}
							}
							?>
						</select>
					</div>
					<input type="hidden" name="edisfm" value="<?= $item->edisfm ?>" />
					<div class="form-group">
						<?=form_label(lang('URL')) ?>:
						<?=form_input('url', $item->url, 'class="form-control"') ?>
					</div>
					<div class="form-group">
						<?=form_label(lang('Meta-tagi (SEO)')) ?>:
						<div class="row">
							<div class="col-sm-4 form-control-static"><?=lang('Tytuł (title)')?>:</div>
							<div class="col-sm-8">
								<input type="text" name="meta_title" value="<?=set_value('meta_title', $item->meta_title)?>" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 form-control-static"><?=lang('Opis (description)')?>:</div>
							<div class="col-sm-8">
								<input type="text" name="meta_description" value="<?=set_value('meta_description', $item->meta_description)?>" class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 form-control-static"><?=lang('Słowa kluczowe (keywords)')?>:</div>
							<div class="col-sm-8">
								<input type="text" name="meta_keywords" value="<?=set_value('meta_keywords', $item->meta_keywords)?>" class="form-control">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Szczegóły')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group form-inline">
						<?=form_label(lang('Data dodania')) ?>:
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->add_date))?></p>
					</div>
					<div class="form-group form-inline">
						<?=form_label(lang('Data modyfikacji')) ?>:
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->lastmod_date))?></p>
					</div>
					<div class="form-group">
						<?=form_label(lang('Opublikowany')) ?>:
						<div class="row">
							<label class="col-sm-3"><?=lang('cały wpis')?></label>
							<div class="col-sm-9">
								<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$item->pub)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$item->pub)?>> <?=lang('nie')?></label>
							</div>
						</div>
					</div>
					<div class="form-group form-inline">
						<?=form_label(lang('Język')) ?>:
						<p class="form-control-static"><?= $item->lang->lang ?> <?=lang_flag($item->lang)?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box box-success">
		<div class="box-header">
			<h3 class="box-title"><?=lang('Umiejscowienie na rzutach') ?></h3>
		</div>
		<div class="box-body">
			<fieldset style="width:813px; padding-left:20px;padding-right:20px;">
				<?
				if (isset($investments_maps) && is_array($investments_maps)){
					foreach ($investments_maps as $m1){
						?>
						<fieldset class="building form-group" data-edi="<?= $m1->investment_id ?>" data-edib="<?= $m1->building_id ?>">
							<legend class="col-form-legend"><?= $m1->investment_name ?> <?=lang('budynek') ?> <?= $m1->building_name ?>:</legend>
							<?
							if (isset($m1->face_maps) && is_array($m1->face_maps)){
								foreach ($m1->face_maps as $m2){
									?>
									<fieldset data-edi="<?= $m1->investment_id ?>" data-edib="<?= $m1->building_id ?>" data-edif="<?= $m2->id ?>">
										<legend class="col-form-legend"><?=lang('piętro') ?> <?= $m2->floor ?>:</legend>
										<div class="image_map" style="position:relative;">
											<img 
											src="<?= $m1->face_image ?>" 
											usemap="#facemap<?= $m2->id ?>" 
											class="source" 
											style="max-width:200px; max-height:100px; position:relative; z-index:3; -ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=0)'; filter: alpha(opacity=0); opacity: 0;" 
											/>
											<map name="facemap<?= $m2->id ?>">
												<area 
												shape="poly" 
												coords="<?= $m2->coords ?>" 
												title="<?= $m1->investment_name ?> <?=lang('budynek') ?> <?= $m1->building_name ?> <?=lang('piętro') ?> <?= $m2->floor ?>" 
												alt="<?= $m1->investment_name ?> <?=lang('budynek') ?> <?= $m1->building_name ?> <?=lang('piętro') ?> <?= $m2->floor ?>"
												>
											</map>
											<canvas class="selected-area" style="position:absolute; top:0; left:0; z-index:3;"></canvas>
											<canvas class="all-areas" style="position:absolute; top:0; left:0; z-index:2;"></canvas>
											<img 
											src="<?= $m1->face_image ?>" 
											usemap="#facemap<?= $m2->id ?>" 
											class="underline" 
											style="max-width:200px; max-height:100px; position:absolute; top:0; left:0; z-index:1;" 
											/>
										</div>
										<?
										if (isset($m2->floors_sketches) && is_array($m2->floors_sketches)){
											foreach ($m2->floors_sketches as $m3){
												?>
												<div data-edi="<?= $m1->investment_id ?>" data-edib="<?= $m1->building_id ?>" data-edif="<?= $m2->id ?>" data-edisf="<?= $m3->id ?>">
													<div class="image_map" style="position:relative;">
														<img 
														src="<?= $m3->image ?>" 
														usemap="#floorsketchmap<?= $m3->id ?>" 
														class="source" 
														style="max-width:730px; max-height:500px; position:relative; z-index:4; -ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=0)'; filter: alpha(opacity=0); opacity: 0;" 
														/>
														<map name="floorsketchmap<?= $m3->id ?>">
															<?
															if (isset($m3->sketches_floors_maps) && is_array($m3->sketches_floors_maps)){
																foreach ($m3->sketches_floors_maps as $m4){
																	?>
																	<area 
																	shape="poly" 
																	data-idedisfm="<?= $m4->id ?>" 
																	coords="<?= $m4->coords ?>" 
																	title="<?= $m1->investment_name ?> <?=lang('budynek') ?> <?= $m1->building_name ?> <?=lang('piętro') ?> <?= $m2->floor ?> <?=lang('lokal') ?> <?= $m4->id ?>" 
																	alt="<?= $m1->investment_name ?> <?=lang('budynek') ?> <?= $m1->building_name ?> <?=lang('piętro') ?> <?= $m2->floor ?> <?=lang('lokal') ?> <?= $m4->id ?>"
																	>
																	<?
																}
															}
															?>
														</map>
														<canvas class="selected-area" style="position:absolute; top:0; left:0; z-index:3;"></canvas>
														<canvas class="all-areas" style="position:absolute; top:0; left:0; z-index:2;"></canvas>
														<img 
														src="<?= $m3->image ?>" 
														usemap="#floorsketchmap<?= $m3->id ?>" 
														class="underline" 
														data-idsfm="<?= $m4->id ?>" 
														style="max-width:730px; max-height:500px; position:absolute; top:0; left:0; z-index:1;" 
														/>
													</div>
												</div>
												<?
											}
										}
										?>
									</fieldset>
									<?
								}
							}
							?>
						</fieldset>
						<?
					}
				}
				?>
			</fieldset>
			
		</div>
	</div>
	
	<div class="box box-success">
		<div class="box-body">
			<div class="form-group">
				<label><?=lang('Skrócony opis') ?>:</label>
				<?= ckeditor('admission', $item->admission, '100%', 'Simple', 120) ?>
			</div>
			
			<div class="form-group">
				<label><?=lang('Pełny opis') ?>:</label>
				<?= ckeditor('description', $item->description, '100%') ?>
			</div>
		</div>
	</div>
	
	<div class="box box-success">
		<div class="box-header">
			<h3 class="box-title"><?=lang('Parametry nieruchomości') ?></h3>
		</div>
		<div class="box-body">
			<div class="form-group">
				<? if ($item->parameters){ ?>
					<? foreach ($item->parameters as $param){ ?>
						<div class="row" style="box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);">
							<div class="col-sm-5 form-control-static"><?= $param->name ?>:</div>
							<div class="col-sm-7 form-control-static">
							<? if ($param->type === 'select'){ ?>
								<select name="param[<?= $param->id ?>]" class="select_simple form-control">
									<option value=""><?=lang('brak') ?></option>
									<?
									if (isset($param->values) && is_array($param->values)){
										foreach ($param->values as $val){
											?>
											<option value="<?= $val->id ?>"<?= set_select('param[' . $param->id . ']', $val->id, $val->id === $param->id_edepv[0]) ?> ><?= $val->value ?></option>
											<?
										}
									}
									?>
								</select>
							<? }elseif ($param->type === 'checkbox'){ ?>
								<?
								if (isset($param->values) && is_array($param->values)){
									foreach ($param->values as $val){
										?>
										<label class="checkbox-inline">
											<input type="checkbox" name="param[<?= $param->id ?>][]" value="<?= $val->id ?>"<?= set_checkbox('param[' . $param->id . ']', $val->id, in_array($val->id, $param->id_edepv)) ?>> <?= $val->value ?>
										</label>
										<?
									}
								}
								?>
							<? }elseif ($param->type === 'text'){ ?>
								<div class="input-group">
									<input type="text" name="param[<?= $param->id ?>]" class="form-control" aria-describedby="param<?=$param->id ?>" value="<?= set_value('param[' . $param->id . ']', $param->text_value) ?>">
									<span class="input-group-addon" id="param<?=$param->id ?>"><?= $param->unit ?></span>
								</div>
							<? }elseif ($param->type === 'radio'){ ?>
								<?
								if (isset($param->values) && is_array($param->values)){
									foreach ($param->values as $val){
										?>
										<label class="radio-inline">
											<input type="radio" name="param[<?= $param->id ?>]" value="<?= $val->id ?>"<?= set_radio('param[' . $param->id . ']', $val->id, $val->id === $param->id_edepv[0]) ?>> <?= $val->value ?>
										</label>
										<?
									}
								}
								?>
							<? }elseif ($param->type === 'file'){ ?>
								<? $field_name = 'file'.$param->id; ?>
								<?=form_fileinput('param['.$param->id.']', $param->text_value, NULL, array('id' => $field_name))?>
							<? }elseif ($param->type === 'predefined_parameter'){ ?>
								<div style="color:#AAA;"><?=lang('Parametr predefiniowany') ?></div>
							<? } ?>
							</div>
						</div>
					<? } ?>
				<? }else{ ?>
					<?= msg(lang('Nie znaleziono żadnych parametrów nieruchomości.'), 1) ?>
				<? } ?>
			</div>
		</div>
	</div>

	<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">

<?= form_close(); ?>


<script>
	$(document).ready(function () {
		$('img[usemap]').rwdImageMaps();
	});
	$(window).load(function () {
		$('.image_map').each(function (idx, el) {
			var el = $(el);

			var canvas = el.find('canvas.all-areas');
			canvas.attr('height', el.find('img.source').height()).attr('width', el.find('img.source').width());
			var draw_canvas = canvas[0].getContext('2d');
			draw_canvas.lineWidth = 2;

			var canvas_selected = el.find('canvas.selected-area');
			canvas_selected.attr('height', el.find('img.source').height()).attr('width', el.find('img.source').width());

			el.find('area').each(function (idx, el) {
				var el = $(el);
				var points = el.attr('coords').split(',');

				draw_canvas.beginPath();
				draw_canvas.moveTo(points[0], points[1]);
				for (var i = 2; i < points.length; i += 2) {
					draw_canvas.lineTo(points[i], points[i + 1]);
				}
				draw_canvas.closePath();
				draw_canvas.fillStyle = 'rgba(255,0,0,0.1)';
				draw_canvas.fill();
				draw_canvas.strokeStyle = 'rgba(255,0,0,1)';
				draw_canvas.stroke();
			});
		});

		$('area[data-idedisfm]').on('click', function (e) {
			e.preventDefault();
			$('input[name=edisfm]').val($(this).data('idedisfm'));

			$(this).closest('.building').parent().find('canvas.selected-area').each(function (idx, el) {
				var canvas = $(el);
				var draw_canvas = canvas[0].getContext('2d');
				draw_canvas.clearRect(0, 0, canvas.width(), canvas.height());
			});

			var canvas = $(this).parent().parent().find('canvas.selected-area');
			var draw_canvas = canvas[0].getContext('2d');
			draw_canvas.lineWidth = 2;
			var points = $(this).attr('coords').split(',');
			draw_canvas.beginPath();
			draw_canvas.moveTo(points[0], points[1]);
			for (var i = 2; i < points.length; i += 2) {
				draw_canvas.lineTo(points[i], points[i + 1]);
			}
			draw_canvas.closePath();
			draw_canvas.fillStyle = 'rgba(0,255,0,0.5)';
			draw_canvas.fill();
			draw_canvas.strokeStyle = 'rgba(0,255,0,5)';
			draw_canvas.stroke();
		});

		if ($('input[name=edisfm]').val() !== '') {
			$('area[data-idedisfm=' + $('input[name=edisfm]').val() + ']').trigger('click');
		}

		$('select[name=id_edi]').on('change', function () {
			$('.building').hide();
			$('.building[data-edi=' + $(this).val() + ']').show();
			$('select[name=id_edib]').val('');
		});
		if ($('select[name=id_edi]').val() !== '') {
			$('.building').hide();
			$('.building[data-edi=' + $('select[name=id_edi]').val() + ']').show();
		}

		$('select[name=id_edib]').on('change', function () {
			$('.building').hide();
			$('.building[data-edib=' + $(this).val() + ']').show();
		});
		if ($('select[name=id_edib]').val() !== '') {
			$('.building').hide();
			$('.building[data-edib=' + $('select[name=id_edib]').val() + ']').show();
		}
	});

</script>