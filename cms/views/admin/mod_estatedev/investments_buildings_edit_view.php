<? $this->load->view('admin/'.config_item('template_a').$this->mod_dir.'scripts_view'); ?>
<?= form_open(current_url()); ?>
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">
	<div class="row">
		<div class="col-md-7">
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<?=form_label(lang('Budynek')) ?>
						<?=form_input('name', $item->name, 'class="form-control"') ?>
					</div>
					<? $field_name = 'face_image'; ?>
					<div class="form-group">
						<?=form_label(lang('Rzut boku budynku')) ?>
						<div class="input-group">
							<input id="<?= $field_name ?>" type="text" name="<?= $field_name ?>" value="<?= $item->$field_name ?>" class="form-control" />
							<div class="input-group-btn">
								<button class="btn btn-info" id="select-<?= $field_name ?>"><?=lang('Wybierz_plik') ?></button>
								<button class="btn btn-danger" id="clear-<?= $field_name ?>"><?=lang('Wyczyść') ?></button>
							</div>
						</div>
						<script type="text/javascript">
							$(document).ready(function () {
								$('#select-<?= $field_name ?>').popupWindow({
									windowURL: '/admin/mod_elfinder/elfinder_init/input_view/functionReturn/process<?= $field_name?>',
									windowName: '<?=lang('Menadżer plików')?>',
									height: 552,
									width: 950,
									centerScreen: 1
								});
							});
							function process<?= $field_name ?>(file) {
								$('#<?= $field_name ?>').val(file);
								$('#img-<?= $field_name ?>').attr('src', file);
								$('.canvas-floor').attr('data-image-url', file);
								$('.canvas-floor').each(function(idx, el){
									var el = $(el);
									el.parent().find('canvas').remove();
									el.canvasAreaDraw(); 
								});
							}
							$("#clear-<?= $field_name ?>").click(function(e){
								e.preventDefault();
								$("#<?= $field_name ?>").val("");
							});
						</script>
						<? if (!empty($item->face_image)){ ?>
							<div style="color:#AAA;"><?=lang('Aktualnie wybrany rzut budynku') ?></div><img id="img-<?= $field_name ?>" src="<?= $item->face_image ?>" style="max-width:200px; max-height:200px;" />
						<? } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title"><?=lang('Szczegóły')?></h3>
				</div>
				<div class="box-body">
					<div class="form-group form-inline">
						<?=form_label(lang('Data dodania')) ?>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->add_date))?></p>
					</div>
					<div class="form-group form-inline">
						<?=form_label(lang('Data modyfikacji')) ?>
						<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($item->lastmod_date))?></p>
					</div>
					<div class="form-group">
						<?=form_label(lang('Opublikowany')) ?>
						<div class="row">
							<label class="col-sm-3"><?=lang('cały wpis')?></label>
							<div class="col-sm-9">
								<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$item->pub)?>> <?=lang('tak')?></label>
								<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$item->pub)?>> <?=lang('nie')?></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box box-success">
		<div class="box-body">
			<div class="list-floor">
				<button type="button" class="btn btn-info add-floor"><i class="fa fa-plus"></i> <span><?=lang('Dodaj nowe piętro') ?></span></button>
				<? if (is_array($facemaps)){ ?>
					<?
					$i = 0;
					foreach ($facemaps as $map){
						?>
						<div class="floor" data-id="<?= $map->id ?>">
							<fieldset>
								<legend><?=lang('Piętro') ?></legend>
								<fieldset>
									<legend><?=lang('Widok z boku budynku') ?></legend>
									<label><?=lang('Numer piętra') ?></label>
									<div class="input-group">
										<input type="text" name="floor[<?= $map->id ?>]" value="<?= $map->floor ?>" class="form-control" />
										<div class="input-group-btn">
											<button type="button" class="btn btn-info resize-facemap"><i class="fa fa-expand"></i> <span><?=lang('Ukryj widok boku budynku') ?></span></button>
											<button type="button" class="btn btn-danger delete-facemap"><i class="fa fa-trash"></i> <?=lang('Usuń piętro') ?></button>
											<button type="button" class="btn btn-info add-floor-sketch" data-mapid="<?= $map->id ?>"><i class="fa fa-plus"></i> <span><?=lang('Dodaj rzut piętra') ?></span></button>
										</div>
									</div>
									<textarea name="coords[<?= $map->id ?>]" style="display:none;" class="canvas-floor canvas<?= $map->id ?>" data-image-url="<?= $item->face_image ?>"><?= $map->coords ?></textarea>
									<script>
									$(window).load(function () {
										$('.canvas<?= $map->id ?>').canvasAreaDraw();
									});
									</script>
								</fieldset>
								<? if (is_array($map->floors)){ ?>
									<? foreach ($map->floors as $floor){ ?>
										<fieldset>
											<legend><?=lang('Rzut piętra budynku') ?></legend>
			
											<? $field_name = 'floor_sketch_' . $map->id . '_' . $floor->id; ?>
											<fieldset data-mapid="<?= $map->id ?>" data-floorid="<?= $floor->id ?>">
												<legend><?=lang('Obraz rzutu piętra budynku') ?></legend>
												<div class="input-group">
													<input id="<?= $field_name ?>" type="text" name="floor_sketch_<?= $map->id ?>[<?= $floor->id ?>]" value="<?= $floor->image ?>" class="form-control" />
													<div class="input-group-btn">
														<button type="button" class="btn btn-info" id="select-<?= $field_name ?>-button"><i class="fa fa-file-image-o"></i> <span><?=lang('Wybierz plik rzutu piętra') ?></span></button>
														<button type="button" class="btn btn-danger delete-floor-map"><i class="fa fa-trash"></i> <span><?=lang('Usuń rzut piętra') ?></span></button>
														<button type="button" class="btn btn-info add-floor-sketch-map" data-mapid="<?= $map->id ?>" data-floorid="<?= $floor->id ?>"><i class="fa fa-plus"></i> <span><?=lang('Oznacz kolejne mieszkanie') ?></span></button>
													</div>
												</div>
												<script type="text/javascript">
													$(document).ready(function () {
														$('#select-<?= $field_name ?>-button').popupWindow({
															windowURL: '/admin/mod_elfinder/elfinder_init/input_view/functionReturn/process<?= $field_name ?>',
															windowName: '<?=lang('Menadżer plików')?>',
															height: 552,
															width: 950,
															centerScreen: 1
														});
														$('#clear-<?= $field_name ?>-button').click(function (e) {
															e.preventDefault();
															clear<?= $field_name ?>();
														});
			
													});
			
													function process<?= $field_name ?>(file) {
														$('#<?= $field_name ?>').val(file);
														$('.floor_sketch_map[data-floorid=<?= $floor->id ?>][data-mapid=<?= $map->id ?>] textarea').data('image-url', file);
														$('.floor_sketch_map[data-floorid=<?= $floor->id ?>][data-mapid=<?= $map->id ?>] canvas').css('background-image', 'url(' + file + ')');
													}
													function clear<?= $field_name ?>() {
														$('#<?= $field_name ?>').val('');
													}
												</script>
											</fieldset>
											<fieldset class="floor_sketch_maps">
												<legend><?=lang('Mieszkanie') ?></legend>
												<? if (is_array($floor->sketches) && count($floor->sketches) > 0){ ?>
													<? foreach ($floor->sketches as $floor_sketch){ ?>
														<div class="floor_sketch_map" data-mapid="<?= $map->id ?>" data-floorid="<?= $floor->id ?>" data-floorsketchid="<?= $floor_sketch->id ?>">
															<textarea name="coords_floor_sketch_<?= $map->id ?>_<?= $floor->id ?>[<?= $floor_sketch->id ?>]" style="display:none;" class="canvas_<?= $map->id ?>_<?= $floor->id ?>_<?= $floor_sketch->id ?>" data-image-url="<?= $floor->image ?>"><?= $floor_sketch->coords ?></textarea>
															<button type="button" class="btn btn-danger delete-floor-sketch-map"><i class="fa fa-trash"></i> <span><?=lang('Usuń mieszkanie') ?></span></button>
															<script>
																$(window).load(function () {
																	$('.canvas_<?= $map->id ?>_<?= $floor->id ?>_<?= $floor_sketch->id ?>').canvasAreaDraw();
																});
															</script>
														</div>
													<? } ?>
												<? }else{ ?>
													<?= msg(lang('Brak zdefiniowanych mieszkań.'), 1) ?>
												<? } ?>
											</fieldset>
										</fieldset>
									<? } ?>
								<? } ?>
							</fieldset>
						</div>
						<?
						$i ++;
					}
					?>
				<? }else{ ?>
					<?= msg(lang('Nie znaleziono żadnych zapisanych pięter w tym budynku.'), 1) ?>
				<? } ?>
			</div>
			<script>
				var no_facemaps = '<?= str_replace(array("\r", "\n"), '', addslashes(msg(lang('Nie znaleziono żadnych zapisanych pięter w tym budynku.'), 1))) ?>';
				var no_sketchemaps = '<?= str_replace(array("\r", "\n"), '', addslashes(msg(lang('Brak zdefiniowanych mieszkań.'), 1))) ?>';
				$(document).ready(function () {
					$(document).on('click', '.btn.delete-facemap', function () {
						if (confirm('<?=lang('Czy na pewno chcesz usunąć?')?>')){
							$(this).closest('.floor').remove();
							if($('.list-floor .floor').length <= 0){
								$('.list-floor').append(no_facemaps);
							}
						}
					});
					$(document).on('click', '.btn.resize-facemap', function () {
						<?/*parenty poprawione*/?>
						var element = $(this).parent().parent().parent().find('.canvas-area');
						if (element.data('minimazed') !== '1') {
							$(this).find('span').html('<?=lang('Pokaż widok boku budynku')?>');
							element.data('minimazed', '1').stop().slideToggle('fast');
						} else {
							$(this).find('span').html('<?=lang('Ukryj widok boku budynku')?>');
							element.data('minimazed', '0').stop().slideToggle('fast');
						}
			
					});
					$(document).on('click', '.add-floor-sketch-map', function () {
						var maxfloorsketch = 0;
						<?/*parenty poprawione*/?>
						$(this).parent().parent().parent().parent().find('.floor_sketch_map').each(function (idx, el) {
							var el = $(el);
							if (maxfloorsketch < el.data('floorsketchid')) {
								maxfloorsketch = el.data('floorsketchid');
							}
						});
						maxfloorsketch += 1;
						<?/*parenty poprawione*/?>
						var image = $(this).parent().parent().parent().find('#floor_sketch_' + $(this).data('mapid') + '_' + $(this).data('floorid') + '').val();
						if (image !== '') {
							var html = '';
							html += '<div class="floor_sketch_map" data-mapid="' + $(this).data('mapid') + '" data-floorid="' + $(this).data('floorid') + '" data-floorsketchid="' + maxfloorsketch + '">';
							html += '<textarea name="coords_floor_sketch_' + $(this).data('mapid') + '_' + $(this).data('floorid') + '[' + maxfloorsketch + ']" style="display:none;" class="canvas_' + $(this).data('mapid') + '_' + $(this).data('floorid') + '_' + maxfloorsketch + '" data-image-url="' + image + '"></textarea>';
							html += '<button type="button" class="btn btn-danger delete-floor-sketch-map"><i class="fa fa-trash"></i> <span><?=lang('Usuń mieszkanie')?></span></button>';
							html += '</div>';
							<?/*parenty poprawione*/?>
							$(this).parent().parent().parent().parent().find('.floor_sketch_maps').append(html);
							$(this).parent().parent().parent().parent().find('.floor_sketch_maps .alert').remove();
							$('.canvas_' + $(this).data('mapid') + '_' + $(this).data('floorid') + '_' + maxfloorsketch + '').canvasAreaDraw();
						} else {
							alert('<?=lang('Przed dodaniem mieszkania wybierz plik rzutu piętra budynku')?>');
						}
					});
					$(document).on('click', '.delete-floor-sketch-map', function () {
						if (confirm('<?=lang('Czy na pewno chcesz usunąć?')?>')){
							var maps_list = $(this).closest('.floor_sketch_maps');
							$(this).closest('.floor_sketch_map').remove();
							if (maps_list.find('.floor_sketch_map').length <= 0) {
								maps_list.append(no_sketchemaps);
							}
						}
					});
					$(document).on('click', '.delete-floor-map', function () {
						if (confirm('<?=lang('Czy na pewno chcesz usunąć?')?>')){
							<?/*parenty poprawione*/?>
							$(this).parent().parent().parent().parent().remove();
						}
					});
					$(document).on('click', '.add-floor-sketch', function () {
						var maxfloors = 0;
						<?/*parenty poprawione*/?>
						$(this).parent().parent().parent().parent().find('fieldset[data-floorid]').each(function (idx, el) {
							var el = $(el);
							if (maxfloors < el.data('floorid')) {
								maxfloors = el.data('floorid');
							}
						});
						maxfloors += 1;
			
						var html = '';
						html += '<fieldset>';
						html += '<legend><?=lang('Rzut piętra budynku')?></legend>';
						var field_name = 'floor_sketch_' + $(this).data('mapid') + '_' + maxfloors;
						html += '<fieldset data-mapid="' + $(this).data('mapid') + '" data-floorid="' + maxfloors + '">';
						html += '<legend><?=lang('Obraz rzutu piętra budynku')?></legend>';
						html += '<div class="input-group">';
						html += '<input id="' + field_name + '" type="text" name="floor_sketch_' + $(this).data('mapid') + '[' + maxfloors + ']" value="" class="form-control" />';
						html += '<div class="input-group-btn">';
						html += '<button type="button" class="btn btn-info" id="select-' + field_name + '-button"><i class="fa fa-file-image-o"></i> <span><?=lang('Wybierz plik rzutu piętra')?></span></button>';
						html += '<button type="button" class="btn btn-danger delete-floor-map"><i class="fa fa-trash"></i> <span><?=lang('Usuń rzut piętra')?></span></button>';
						html += '<button type="button" class="btn btn-info add-floor-sketch-map" data-mapid="' + $(this).data('mapid') + '" data-floorid="' + maxfloors + '"><i class="fa fa-plus"></i><span><?=lang('Oznacz kolejne mieszkanie')?></span></button>';
						html += '</div>';
						html += '</div>';
						html += '</fieldset>';
						html += '<fieldset class="floor_sketch_maps">';
						html += '<legend><?=lang('Mieszkanie')?></legend>';
						html += '<?= str_replace(array("\r", "\n"), '', addslashes(msg(lang('Brak zdefiniowanych mieszkań.'), 1))) ?>';
						html += '</fieldset>';
						html += '</fieldset>';
						<?/*parenty poprawione*/?>
						$(this).parent().parent().parent().parent().append(html);
			
						var js = '';
						$('#select-' + field_name + '-button').popupWindow({
							windowURL: '/admin/mod_elfinder/elfinder_init/input_view/functionReturn/process' + field_name,
							windowName: '<?=lang('Menadżer plików')?>',
							height: 552,
							width: 950,
							centerScreen: 1
						});
						js += '$(\'#' + field_name + '\').val(file);';
						js += '$(\'.floor_sketch_map[data-floorid=' + maxfloors + '][data-mapid=' + $(this).data('mapid') + '] textarea\').data(\'image-url\', file);';
						js += '$(\'.floor_sketch_map[data-floorid=' + maxfloors + '][data-mapid=' + $(this).data('mapid') + '] canvas\').css(\'background-image\', \'url(\' + file + \')\');';
						window['process' + field_name] = new Function('file', js);
					});
			
					var maxfloorid = 0;
					$('.floor').each(function (idx, el) {
						var el = $(el);
						if (maxfloorid < parseInt(el.data('id'))) {
							maxfloorid = el.data('id');
						}
					});
					$('.add-floor').on('click', function () {
						$('.list-floor .alert').remove();
						maxfloorid++;
						var html = '';
						html += '<div class="floor" data-id="' + maxfloorid + '">';
						html += '<fieldset>';
						html += '<legend><?=lang('Piętro')?></legend>';
						html += '<fieldset>';
						html += '<legend><?=lang('Widok z boku budynku')?></legend>';
						html += '<label><?=lang('Numer piętra')?></label>';
						html += '<div class="input-group">';
						html += '<input type="text" name="floor[' + maxfloorid + ']" value="" class="form-control" />';
						html += '<div class="input-group-btn">';
						html += '<button type="button" class="btn btn-danger delete-facemap"><i class="fa fa-trash"></i> <?=lang('Usuń piętro')?></button>';
						html += '<button type="button" class="btn btn-info resize-facemap"><i class="fa fa-expand"></i> <span><?=lang('Ukryj mapowanie boku budynku')?></span></button>';
						html += '<button type="button" class="btn btn-info add-floor-sketch" data-mapid="' + maxfloorid + '"><i class="fa fa-plus"></i> <span><?=lang('Dodaj rzut piętra')?></span></button>';
						html += '</div>';
						html += '</div>';
						html += '<textarea name="coords[' + maxfloorid + ']" style="display:none;" class="canvas-floor canvas' + maxfloorid + '" data-image-url="'+$('#face_image').val()+'"></textarea>';
						html += '</fieldset>';
						html += '</fieldset>';
						html += '</div>';
						$('.list-floor').append(html);
						$('.canvas' + maxfloorid + '').canvasAreaDraw();
					});
			
					$(window).load(function () {
						setTimeout(function () {
							$('.btn.resize-facemap').trigger('click');
						}, 100);
					});
				});
			</script>
		</div>
	</div>
	<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary" />

<?= form_close(); ?>