<?=form_open_multipart(current_url().'/check/1', array('id' => 'edit'));?>
<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">

<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Tytuł')?></label>
					<input type="text" name="title" value="<?=set_value('title', $art->title)?>" class="form-control" />
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Szczegóły')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Pozycja w drzewie')?>:</label>
					<select class="select_simple" name="id_tree">
						<option value="0"><?=lang('główna pozycja')?></option>
						<?php foreach($all as $row) {?>
						<?php 
						if($row->id == $art->id_art) $add = ' disabled="disabled"';
						else $add = '';
						?>
						<option <?=set_select('id_tree', $row->id, $row->id == $art->id_tree)?><?=$add?> value="<?=$row->id?>"><?=$row->title?></option>
						<?php if(isset($row->children) && !empty($row->children)) {?>
						<?php foreach($row->children as $sub) {?>
						<?php 
						if($sub->id == $art->id_art) $add = ' disabled="disabled"';
						else $add = '';
						?>
						<option <?=set_select('id_tree', $sub->id, $sub->id == $art->id_tree)?><?=$add?> value="<?=$sub->id?>">&#x2001; <?=$sub->title?></option>
						<?php if(isset($sub->children) && !empty($sub->children)) {?>
						<?php foreach($sub->children as $subsub) {?>
						<?php 
						if($subsub->id == $art->id_art) $add = ' disabled="disabled"';
						else $add = '';
						?>
						<option <?=set_select('id_tree', $subsub->id, $subsub->id == $art->id_tree)?><?=$add?> value="<?=$subsub->id?>">&#x2001;&#x2001; <?=$subsub->title?></option>
						<?php }?>
						<?php }?>
						<?php }?>
						<?php }?>						
						<?php }?>					
					</select>
				</div>
				
				<div class="form-group form-inline">
					<label><?=lang('Autor')?>:</label>
					<p class="form-control-static"><?=(isset($art->author->login) ? $art->author->login." (".$art->author->name.")" : "")?></p>
				</div>
				<div class="form-group form-inline">
					<label><?=lang('Data modyfikacji')?>:</label>
					<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($art->date_modified))?></p>
				</div>
				
				<div class="form-group">
					<label><?=lang('Opublikowany')?>:</label>
					<div class="row">
						<label class="col-sm-3"><?=lang('cały artykuł')?></label>
						<div class="col-sm-9">
							<label class="radio-inline"><input name="pub_all" type="radio" value="1" <?=set_radio('pub_all', 1, !!$art->pub_all)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="pub_all" type="radio" value="0" <?=set_radio('pub_all', 0, !$art->pub_all)?>> <?=lang('nie')?></label>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3"><?=lang('język')?> <?=lang_flag($art->lang)?></label>
						<div class="col-sm-9">
							<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$art->pub)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$art->pub)?>> <?=lang('nie')?></label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="box box-success">
	<div class="box-body">
		<div class="form-group">
			<label><?=lang('Treść')?>:</label>
			<?=ckeditor('art_content', set_value('art_content', $art->content, FALSE), '100%')?>
		</div>
	</div>
</div>

<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">

<?=form_close(); ?>
