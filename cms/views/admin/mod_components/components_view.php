<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj komponent') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
    	<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
	    	<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Link') ?></label>
				<input type="text" name="link" value="<?=set_value('link')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Opis') ?></label>
				<textarea name="description" class="form-control" style="height: 75px;"><?=set_value('description')?></textarea>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Opublikowany')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, TRUE)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Tylko dla Artneo')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="main" type="radio" value="1" <?=set_radio('main', 1)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="main" type="radio" value="0" <?=set_radio('main', 0, TRUE)?>> <?=lang('nie')?></label>
					</div>
				</div>
			</div>
    		<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
    	<?=form_close()?>
	</div>
</div>

<? if($rows !== FALSE) { ?>
	<?=form_open();?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th><?=lang('Nazwa') ?></th>
				<th><?=lang('Opis') ?></th>
				<th style="width:40px;"><?=lang('Kolejność') ?></th>
				<th style="width:130px;"><?=lang('Tylko dla Artneo') ?></th>
				<th style="width:230px;"><?=lang('Działania') ?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox"></th>
			</tr>
		</thead>
		<tbody class="sortable">
			<? $i=1; ?>
			<? foreach($rows as $row): ?>
				<tr id="listItem_<?=$row->id?>">
					<td><?=$i?></td>
					<td>
						<? if(!empty($row->icon) && file_exists(str_replace('%20', ' ', './'.config_item('site_path').$row->icon))){
							echo '<img src="'.$row->icon.'" alt="" class="absmiddle" />';
						}
						echo anchor("{$this->mod_url}/edit/lang/".config_item('main_lang')."/id/{$row->id}", $row->name);
						echo lang_versions($lang, $row, $this->mod_url.'/edit');
						?>
					</td>
					<td><?=$row->description?></td>
					<td class="text-center"><span class="fa fa-arrows"></span><input class="currentposition" type="hidden" value="<?=$row->position?>" name="position[<?=$row->id?>]"></td>
					<td class="text-center">
						<? if($row->main == 0) echo anchor("{$this->mod_url}/vswitch/column/main/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Ustaw jako widoczne tylko dla Artneo'), 'class' => 'anchor_icon')); else echo anchor("{$this->mod_url}/vswitch/column/main/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Ustaw jako widoczne dla wszystkich'), 'class' => 'anchor_icon'));?>
					</td>
					<td class="text-center nc-options">
						<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
						<? if($row->pub == 0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/0", lang('Odpublikuj'));?>
						<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
					</td>
					<td class="tcenter"><input type="checkbox" value="<?=$row->id?>" name="check[]" /></td>
				</tr>
			<? $i++; ?>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="Zapisz kolejność" >
				</td>
				<td colspan="3" class="nc-options">
					<?=lang('Zaznaczone:') ?>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
					
					<br><?=lang('Tylko dla Artneo') ?>:
					<a title="<?=lang('Ustaw jako widoczne tylko dla Artneo') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/main/set/1")?>"><i class="fa fa-plus"></i></a>
					<a title="<?=lang('Ustaw jako widoczne dla wszystkich') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/main/set/0")?>"><i class="fa fa-minus"></i></a>
				</td>
			</tr>
		</tfoot>
	</table>
	<?=form_close()?>
	
<? } else { 
	echo msg(lang('Nie znaleziono żadnych wpisów.'), 1);
}
?>
