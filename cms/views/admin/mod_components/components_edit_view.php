<?=form_open(current_url().'/check/1');?>
<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">

<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa')?></label>
					<input type="text" name="name" value="<?=set_value('name', $row->name)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Link')?></label>
					<input type="text" name="link" value="<?=set_value('link', $row->link)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Ikona')?></label>
					<?=form_fileinput('icon', $row->icon)?>
				</div>
				<div class="form-group">
					<label><?=lang('Opis')?></label>
					<textarea name="description" class="form-control" style="height: 75px;"><?=set_value('description', $row->description)?></textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Szczegóły')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-5 form-control-static"><?=lang('Opublikowany')?>:</div>
						<div class="col-sm-7 form-control-static">
							<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$row->pub)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$row->pub)?>> <?=lang('nie')?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-5 form-control-static"><?=lang('Tylko dla Artneo')?>:</div>
						<div class="col-sm-7 form-control-static">
							<label class="radio-inline"><input name="main" type="radio" value="1" <?=set_radio('main', 1, !!$row->main)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="main" type="radio" value="0" <?=set_radio('main', 0, !$row->main)?>> <?=lang('nie')?></label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
<?=form_close(); ?>