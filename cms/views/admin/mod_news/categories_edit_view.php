<?
if($all !== FALSE ) {
	$tab = $this->uri->uri_to_assoc();
	if(@is_numeric($tab['page'])) $add = $tab['page'];
	else $add = 1;
	
	echo form_open($this->mod_url.'/edit/page/'.$add.'/check/1', array('class' => 'row'));
	
	foreach($all as $cat) {
		echo form_hidden('check[]', $cat->id);
?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Kategoria')?> #<?=$cat->id?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa') ?></label>
					<input type="text" name="name_<?=$cat->id?>" value="<?=set_value('name_'.$cat->id, $cat->name)?>" class="form-control">
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?
	}
	echo form_close();
} 
?>
