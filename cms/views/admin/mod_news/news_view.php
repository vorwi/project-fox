<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj aktualność') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Tytuł') ?></label>
				<input type="text" name="title" value="<?=set_value('title')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Wyświetlanie na podstronie')?></label>
				<select name="id_art" class="select_simple form-control">
				<? foreach($articles['categories'] as $cat){
			 		echo '<optgroup label="'.$cat->name.'">';
					if(is_array($cat->articles)) {
						foreach($cat->articles as $row){
							$padd = repeater('&#x2001;', $row->tree);
							
							echo '<option value="'.$row->id.'" '.set_select('id_art', $row->id).'>'.$padd.' '.$row->title.'</option>';
						}
					}
					echo '</optgroup>';
				} ?>
				</select>
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>
<?
if($news !== FALSE) {
	
	echo form_open();
	echo $pagination;
?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr')?></th>
				<th><?=lang('Tytuł')?></th>
				<th><?=lang('Podstrona')?></th>
				<th style="width:170px;"><?=lang('Data dodania')?></th>
				<th style="width:250px;"><?=lang('Działania')?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox"  /></th>
			</tr>
		</thead>
		<tbody>
			<? $i=1 ?>
			<? foreach($news as $row): ?>
			<tr>
				<td><?=$i?></td>
				<td>
					<?=anchor($this->mod_url.'/edit/id/'.$row->id, $row->title)?>
					<?php if(!empty($row->url)) {?>
					<i class="fa fa-external-link" title="<?=lang('Prowadzi do')?>: <?=$row->url?>"></i>
					<?php }?>
				</td>
				<td><?=$row->art_path?></td>
				<td class="text-center"><?=date('d.m.Y H:i:s', strtotime($row->date_add))?></td>
				<td class="text-center nc-options">
					<?=anchor($this->mod_url.'/edit/id/'.$row->id, lang('Edytuj'))?>
					<? if($row->pub==0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/0", lang('Odpublikuj'));?>
					<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
				</td>
				<td><input type="checkbox" value="<?=$row->id?>" name="check[]"></td>
			</tr>
			<? $i++ ?>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="2"></td>
				<td colspan="2">
					<? if(false): ?><input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>"><? endif;?>
				</td>
				<td colspan="2" class="nc-options">
					<?=lang('Zaznaczone')?>:
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
				</td>
			</tr>
		</tfoot>
	</table>

<?
	echo $pagination;
	echo form_close();
} else {
	echo msg(lang('Nie znaleziono żadnych aktualności.'), 1);
}
?>
