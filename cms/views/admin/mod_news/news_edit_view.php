<?=form_open_multipart(current_url().'/check/1', array('id' => 'edit'));?>
<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">

<?=$this->lang_fields->global_switcher($news)?>

<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Tytuł')?></label>
					<?=lang_field($news, 'text', 'title')?>
				</div>
				<div class="form-group">
					<label><?=lang('Zewnętrzny odnośnik')?></label>
					<?=form_field_tooltip(lang('Link do którego ma kierować aktualność, zamiast wyświetlania jej treści (np.: http://artneo.pl). Pole opcjonalnie.'))?>
					<?=lang_field($news, 'text', 'url')?>
				</div>
				<div class="form-group">
					<label><?=lang('Niestandardowy adres URL')?></label>
					<?=form_field_tooltip(lang('Adres, pod którym będzie dostępna ta aktualność. Podawać bez domeny na początku. Pole opcjonalnie.'))?>
					<?=lang_field($news, 'text', 'user_url')?>
				</div>
				<div class="form-group">
					<label><?= lang('Meta-tagi (SEO)') ?></label>
					<div class="row">
						<div class="col-sm-4 form-control-static"><?= lang('Tytuł (title)') ?>:</div>
						<div class="col-sm-8">
							<?=lang_field($news, 'text', 'meta_title')?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 form-control-static"><?= lang('Opis (description)') ?>:</div>
						<div class="col-sm-8">
							<?=lang_field($news, 'text', 'meta_description')?>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 form-control-static"><?= lang('Słowa kluczowe (keywords)') ?>:</div>
						<div class="col-sm-8">
							<?=lang_field($news, 'text', 'meta_keywords')?>
						</div>
					</div>
				</div>
				<div class="form-group nc-image">
					<label><?=lang('Zdjęcie')?></label>
					<div>
					<?
					if(!empty($news->img) && file_exists('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$news->img)) {
						$tab = explode('.', $news->img);
						echo '<a href="'.config_item('upload_path').$this->img_dir.$news->img.'" class="img">
							<img src="'.config_item('upload_path').$this->img_dir.$tab[0].'_thumb.'.$tab[1].'" alt=""></a>';
						
						echo '<p class="checkbox"><label><input type="checkbox" value="1" name="del_img" /> '.lang('usuń zdjęcie').'</label></p>';
						echo '<p><a href="'.$this->mod_url.'/crop/id/'.$news->id.'" class="btn btn-sm btn-success modal-box" data-iframe="true">'.lang('Kadruj').'</a></p>';
					}
					?>
						<p class="form-control-static"><input type="file" name="img" size="65"></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Szczegóły')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Wyświetlanie na podstronie')?>:</label>
					<select class="select_simple" name="id_art">
						<? foreach($articles['categories'] as $cat){
					 		echo '<optgroup label="'.$cat->name.'">';
							if(is_array($cat->articles)) {
								foreach($cat->articles as $row){
									$padd = repeater('&#x2001;', $row->tree);
									
									echo '<option value="'.$row->id.'" '.set_select('id_art', $row->id, $row->id == $news->id_art).'>'.$padd.' '.$row->title.'</option>';
								}
							}
							echo '</optgroup>';
						} ?>
					</select>
				</div>
				
				<div class="form-group">
					<div class="row">
						<label class="col-sm-3"><?=lang('Autor')?>:</label>
						<div class="col-sm-9">
							<?=lang_value($news, 'author_name')?>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3"><?=lang('Data modyfikacji')?>:</label>
						<div class="col-sm-9">
							<?=lang_value($news, 'date_modified')?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<label class="col-sm-3"><?=lang('Data dodania')?>:</label>
						<div class="col-sm-9">
						<?=lang_field($news, 'datetime', 'date_add', array('class'=>'form-control datetimepicker'))?>
						</div>
					</div>			
				</div>

				<div class="form-group">
					<label><?= lang('Opublikowana') ?>:</label>
					<div class="row">
						<label class="col-sm-3"><?= lang('cała aktualność') ?></label>
						<div class="col-sm-9">
							<select class="select_simple" name="pub">
								<option value="1" <?=set_select('pub', 1, !!$news->pub)?>><?= lang('tak') ?></option>
								<option value="0" <?=set_select('pub', 0, !$news->pub)?>><?= lang('nie') ?></option>
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3"><?= lang('języki') ?></label>
						<div class="col-sm-9">
							<?=lang_field($news, 'select', 'pub', array('values'=>array(1=>lang('tak'),0=>lang('nie'))))?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><?=lang('Link do aktualności')?>:</label>
					<?=lang_field($news, 'text', 'URI', array('readonly' => true))?>
				</div>
				<div class="form-group">
					<label><?=lang('Link do używania w panelu')?>:</label>
					<?=lang_field($news, 'text', 'link', array('readonly' => true))?>
				</div>
				<div class="form-group">
					<label><?=lang('Sposób otwierania')?>:</label>
					<?=lang_field($news, 'select', 'target', array('values'=>array('0'=>lang('normalnie'),'_blank'=>lang('nowe okno'))))?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="box box-success">
	<div class="box-body">
		<div class="form-group">
			<label><?=lang('Wstęp')?>:</label>
			<?=lang_field($news, 'ckeditor', 'admission', array('width' => '100%', 'height' => '120px', 'toolbar' => 'Simple'))?>
		</div>
		<div class="form-group">
			<label><?=lang('Treść')?>:</label>
			<?=lang_field($news, 'ckeditor', 'content', array('width' => '100%'))?>
		</div>
	</div>
</div>

<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">

<?=form_close(); ?>