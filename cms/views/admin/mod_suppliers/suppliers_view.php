<div class="box box-primary box-solid collapsed-box add-entity">
    <div class="box-header with-border">
        <h3 class="box-title"><?=lang('Dodaj dostawcę') ?></h3>
        <div class="box-tools pull-right">
            <button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
            <div class="form-group">
                <label><?=lang('Nazwa') ?></label>
                <input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
            </div>
            <input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
        <?=form_close()?>
    </div>
</div>
<?
if($suppliers !== FALSE) {
    
    echo form_open();
    echo $pagination;
?>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th style="width:25px;"><?=lang('Nr')?></th>
                <th><?=lang('Nazwa')?></th>
                <th><?=lang('Oferta')?></th>
                <th style="width:170px;"><?=lang('Logotyp')?></th>
                <th style="width:170px;"><?=lang('Data dodania')?></th>
                <th style="width:250px;"><?=lang('Działania')?></th>
                <th style="width:25px;"><input class="checkall" type="checkbox"  /></th>
            </tr>
        </thead>
        <tbody>
            <? $i=1 ?>
            <? foreach($suppliers as $row): ?>
            <tr>
                <td><?=$i?></td>
                <td>
                    <?=anchor($this->mod_url.'/edit/id/'.$row->id_supplier, $row->name)?>
                </td>
                <td><?=$row->offer?></td>
                <td>
                <?if(!empty($row->img) && file_exists('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->img)) {
                    $tab = explode('.', $row->img);
                    echo '<img src="'.config_item('upload_path').$this->img_dir.$tab[0].'_thumb.'.$tab[1].'" alt="">';
                } ?>
                </td>
                <td class="text-center"><?=date('d.m.Y H:i:s', strtotime($row->date_add))?></td>
                <td class="text-center nc-options">
                    <?=anchor($this->mod_url.'/edit/id/'.$row->id_supplier, lang('Edytuj'))?>
                    <? if($row->pub==0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id_supplier}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id_supplier}/set/0", lang('Odpublikuj'));?>
                    <a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id_supplier}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
                </td>
                <td><input type="checkbox" value="<?=$row->id_supplier?>" name="check[]"></td>
            </tr>
            <? $i++ ?>
            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5"></td>
                <td colspan="2" class="nc-options">
                    <?=lang('Zaznaczone')?>:
                    <a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
                    <a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
                    <a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
                </td>
            </tr>
        </tfoot>
    </table>

<?
    echo $pagination;
    echo form_close();
} else {
    echo msg(lang('Nie znaleziono żadnego dostawcy.'), 1);
}
?>
