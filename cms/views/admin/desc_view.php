<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title><?=ifset($meta_title, 'NeoCMS')?></title>
		<base href="<?=base_url();?>" />
		<meta name="author" content="Artneo.pl" />
		<meta name="copyright" content="<?=lang('Wszelkie prawa zatrzeżone')?> Artneo.pl" />
		<meta name="Robots" content="noindex, nofollow" />
			
		<?=put_headers('admin')?>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="iframe">
		<section class="content">
			<? 
			if(is_array($this->session->flashdata('msg_files'))) {
				foreach($this->session->flashdata('msg_files') as $row){
					$msg = $this->session->flashdata('msg_files');
					echo msg($row[0], $row[1]);
				}
			} elseif(!isset($img)) {
				echo msg(lang('Wybrane zdjęcie nie istnieje lub odnośnik jest nieprawidłowy.'), 1);
			} else {
				$thumb_img = thumb_name($img->image);
			?>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<?=form_open($form_url, array('class' => 'box box-borders', 'id' => 'img-edit'))?>
						<div class="box-header with-border">
							<h3 class="box-title"><?=lang('Edycja zdjęcia')?></h3>
						</div>
						<div class="box-body">
							<p class="text-center">
								<img src="<?=$path.$thumb_img;?>" alt="">
							</p>
							<?
							if (is_array($languages)){
								foreach ($languages as $l){
									?>
									<div class="form-group">
										<?= $l->lang ?> <img alt="<?= $l->short ?>" title="<?= $l->lang ?>" src="<?= config_item('gfx_c') . 'img/flags/' . $l->short ?>.png" border="0" style="vertical-align: middle;" /><br />
										<textarea name="desc_value[<?= $l->short ?>]" class="form-control"><?= (isset($img->desc[$l->short]->description)) ? $img->desc[$l->short]->description : '' ?></textarea>
									</div>
									<?
								}
							}
							?>
							<?=form_hidden('edit_img', 1)?>
							<?=form_hidden('desc', 'cos'); ?>
						</div>
						<div class="box-footer">
							<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary" style="width: 150px;">
						</div>
					<?=form_close()?>
				</div>
			</div>
			
			<script type="text/javascript">
				$('#img-edit').submit(function(e) {
					e.preventDefault();
					$.ajax({
						type: "POST",
						url: '<?=$form_url?>',
						data: $(this).serialize(),
						success: function(){
							parent.location.reload();
						}
					});
				});
			</script>
			<? } ?>
		</section>
	</body>	
</html>