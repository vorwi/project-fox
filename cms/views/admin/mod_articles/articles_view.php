<?
if ($all !== FALSE) {
	foreach ($all['categories'] as $cat) {
		?>

		<h2><?= $cat->name ?></h2>

		<div class="box box-primary box-solid collapsed-box add-entity">
			<div class="box-header with-border">
				<h3 class="box-title"><?= lang('Dodaj artykuł') ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<?= form_open($this->mod_url . '/add/cat/' . $cat->id); ?>
				<div class="form-group">
					<select name="id_tree" class="select_simple form-control">
						<option value="0" <?= set_select('id_tree', 0, TRUE) ?>><?= lang('główna pozycja') ?></option>
						<?
						if (is_array($cat->articles)) {
							echo_article_select_options($cat->articles, 'id_tree');
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label><?= lang('Tytuł') ?>:</label>
					<input type="text" name="title" class="form-control" value="<?= set_value('title') ?>">
				</div>
				<input type="submit" value="<?= lang('Dodaj') ?>" class="btn btn-primary" />
				<?= form_hidden('id_cat', $cat->id) ?>
				<?= form_close() ?>
			</div>
		</div>

		<?
		if ($cat->articles !== FALSE && !empty($cat->articles)) {
			echo form_open();
			?>

			<div id="ns_<?= $cat->id ?>" class="ns">
				<div class="ns-options clearfix">
					<div class="ns-menu">
						<input data-url="<?= site_url("{$this->mod_url}/position/cat/{$cat->id}") ?>" style="clear: none;" class="btn btn-success btn-sm ns-save" type="button" value="<?= lang('Zapisz zmiany') ?>" />
						<button class="btn btn-default btn-sm" data-action="expand-all" type="button"><?= lang('Rozwiń wszystkie') ?></button>
						<button class="btn btn-default btn-sm" data-action="collapse-all" type="button"><?= lang('Zwiń wszystkie') ?></button>
					</div>

					<label><?= lang('Zaznacz wszystkie') ?>: <input class="checkall" type="checkbox"></label>
				</div>

				<?
				if (!function_exists('echo_article_rows')) {

					function echo_article_rows($items, $mod_dir, $mod_name, $cat_id, $lang) {
						$mod_url = "admin/{$mod_dir}{$mod_name}";
						foreach ($items as $sub) {
							if (isset($sub->children) && !empty($sub->children)) {
								$li_class = ' mjs-nestedSortable-branch mjs-nestedSortable-expanded';
							} else {
								$li_class = ' mjs-nestedSortable-leaf';
							}
							echo '<li class="ns-item' . $li_class . '" data-id="' . $sub->id . '" id="menuItem_' . $sub->id . '">';
							echo '<div class="ns-container">';

							echo '<div class="ns-col ns-col-action">';
							echo '<span class="disclose ui-icon ui-icon-minusthick"></span>';
							echo '</div>';

							echo '<div class="ns-col ns-col-action">';
							echo '<span class="fa fa-arrows ns-handle"></span>';
							echo '</div>';

							echo '<div class="ns-col">';
							echo anchor("{$mod_url}/edit/id/{$sub->id}", $sub->title);
							if (!empty($sub->url)) {
								echo ' <i class="fa fa-external-link" title="' . lang('Prowadzi do') . ': ' . $sub->url . '"></i>';
							}
							echo '</div>';

							echo '<div class="ns-col nc-options ns-col-options">';
							echo anchor("{$mod_url}/edit/id/{$sub->id}", lang('Edytuj')) . ' ';
							if ($sub->id != config_item('main_art')) {
								if ($sub->pub == 0) {
									echo anchor("{$mod_url}/pub/cat/{$cat_id}/id/{$sub->id}/set/1", lang('Publikuj')) . ' ';
								} else {
									echo anchor("{$mod_url}/pub/cat/{$cat_id}/id/{$sub->id}/set/0", lang('Odpublikuj')) . ' ';
								}
								echo '<a class="click-confirm" data-href="' . site_url("{$mod_url}/delete/cat/{$cat_id}/del/{$sub->id}") . '" data-confirm="' . lang('Czy na pewno chcesz usunąć?') . '">' . lang('Usuń') . '</a>';
							}
							echo '</div>';
							echo '<div class="ns-col ns-col-check">';
							if ($sub->id != config_item('main_art')) {
								echo ' <input type="checkbox"  value="' . $sub->id . '" name="check[]" />';
							}
							echo '</div>';

							echo '</div>';
							if (isset($sub->children) && !empty($sub->children)) {
								echo '<ol>';
								echo_article_rows($sub->children, $mod_dir, $mod_name, $cat_id, $lang);
								echo '</ol>';
							}
							echo '</li>';
						}
					}

				}
				?>

				<ol class="nestable-sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">
					<?= echo_article_rows($cat->articles, $this->mod_dir, $this->mod_name, $cat->id, $lang) ?>
				</ol>

				<div class="ns-options clearfix">
					<div class="ns-menu">
						<input data-url="<?= site_url("{$this->mod_url}/position/cat/{$cat->id}") ?>" class="btn btn-success btn-sm ns-save" type="button" value="<?= lang('Zapisz zmiany') ?>" />
					</div>
					<label><?= lang('Zaznacz wszystkie') ?>: <input class="checkall" type="checkbox"></label>
				</div>
			</div>

			<div class="nc-options text-right">
				<?= lang('Zaznaczone') ?>:
				<a class="click-submit" data-action="<?= site_url("{$this->mod_url}/pub/cat/{$cat->id}/set/1") ?>"><?= lang('Publikuj') ?></a>
				<a class="click-submit" data-action="<?= site_url("{$this->mod_url}/pub/cat/{$cat->id}/set/0") ?>"><?= lang('Odpublikuj') ?></a>
				<a class="click-submit" data-action="<?= site_url("{$this->mod_url}/delete/cat/{$cat->id}") ?>" data-confirm="<?= lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?= lang('Usuń') ?></a>
			</div>
			<?
			echo form_close();
		} else {
			echo msg(lang('Nie znaleziono żadnych artykułów.'), 1);
		}
	}
} else {
	echo msg(lang('Nie znaleziono żadnych kategorii.'), 1);
}
?>