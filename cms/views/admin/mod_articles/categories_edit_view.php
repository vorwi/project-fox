<?
if($all !== FALSE ) {
	echo form_open(current_url().'/check/1', array('class' => 'row'));
	
	foreach($all as $row) {
		echo form_hidden('check[]', $row->id);
?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Kategoria')?> #<?=$row->id?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa')?></label>
					<input type="text" name="name_<?=$row->id?>" class="form-control" value="<?=set_value('name_'.$row->id, $row->name)?>">
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?
	}
	echo form_close();
}
?>
