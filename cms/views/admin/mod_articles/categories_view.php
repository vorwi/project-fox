<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj kategorię') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<?
if($all !== FALSE) { 
	echo form_open();
	$i = 1;
?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th><?=lang('Nazwa') ?></th>
				<th style="width:40px;"><?=lang('Kolejność') ?></th>
				<th style="width: 250px;"><?=lang('Działania') ?></th>
				<th style="width: 25px;"><input class="checkall" type="checkbox"></th>
			</tr>
		</thead>
		<tbody class="sortable">
		<? foreach($all as $row) { ?>
			<tr>
				<td><b><?=$i?></b></td>
				<td><?=$row->name?></td>
				<td class="text-center">
					<span class="fa-arrows"></span><input class="currentposition" type="hidden" style="width:30px;" name="position[<?=$row->id?>]" value="<?=$row->position?>">
				</td>
				<td class="text-center nc-options">
					<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
					<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
				</td>
				<td class="text-center"><input type="checkbox"  value="<?=$row->id?>" name="check[]"></td>
			</tr>
			<? $i++; ?>
		<? } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3" class="text-right">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>" >
				</td>
				<td colspan="2" class="nc-options">
					<?=lang('Zaznaczone:') ?> 
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/edit")?>"><?=lang('Edytuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
				</td>
			</tr>
		</tfoot>
	</table>
<?
	echo form_close();
} else {
	echo msg(lang('Nie znaleziono żadnych kategorii.'), 1);
}
?>
