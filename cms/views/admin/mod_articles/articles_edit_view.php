<?= form_open_multipart(current_url() . '/check/1', array('id' => 'edit')); ?>
<input type="submit" value="<?= lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">

<?=$this->lang_fields->global_switcher($art)?>

<?if($art->id == 1):?>
<div class="alert alert-info" role="alert">Edycja strony głównej.</div>
<?endif;?>

<div class="row">
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?= lang('Dane podstawowe') ?></h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label><?= lang('Tytuł') ?></label>
                    <?=lang_field($art, 'text', 'title')?>
                </div>
                <div class="form-group">
                    <label><?= lang('Skrócony tytuł') ?></label>
                    <?= form_field_tooltip(lang('Skrócony tytuł będzie widoczny tylko w menu, natomiast dłuższa wersja jako nagłówek strony. Pole opcjonalnie.')) ?>
                    <?=lang_field($art, 'text', 'short_title')?>
                </div>
                <div class="form-group">
                    <label><?= lang('Zewnętrzny odnośnik') ?></label>
                    <?= form_field_tooltip(lang('Link do którego ma kierować podstrona z menu, zamiast wyświetlania jej treści (np.: http://artneo.pl). Pole opcjonalnie.')) ?>
                    <?=lang_field($art, 'text', 'url')?>
                </div>
                <div class="form-group">
                    <label><?= lang('Niestandardowy adres URL') ?></label>
                    <?= form_field_tooltip(lang('Adres, pod którym będzie dostępna ta podstrona. Podawać bez domeny na początku. Pole opcjonalnie.')) ?>
                    <?=lang_field($art, 'text', 'user_url')?>
                </div>

                <div class="form-group">
                    <label><?= lang('Meta-tagi (SEO)') ?></label>
                    <div class="row">
                        <div class="col-sm-4 form-control-static"><?= lang('Tytuł (title)') ?>:</div>
                        <div class="col-sm-8">
                            <?=lang_field($art, 'text', 'meta_title')?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-control-static"><?= lang('Opis (description)') ?>:</div>
                        <div class="col-sm-8">
                            <?=lang_field($art, 'text', 'meta_description')?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-control-static"><?= lang('Słowa kluczowe (keywords)') ?>:</div>
                        <div class="col-sm-8">
                            <?=lang_field($art, 'text', 'meta_keywords')?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label><?= lang('Meta tagi Open Graph') ?></label>
                    <div class="row">
                        <div class="col-sm-4 form-control-static"><?= lang('og:url') ?>:</div>
                        <div class="col-sm-8">
                            <?=lang_field($art, 'text', 'og_url')?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-control-static"><?= lang('og:title') ?>:</div>
                        <div class="col-sm-8">
                            <?=lang_field($art, 'text', 'og_title')?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-control-static"><?= lang('og:description') ?>:</div>
                        <div class="col-sm-8">
                            <?=lang_field($art, 'text', 'og_description')?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-control-static"><?= lang('og:image') ?>:</div>
                        <div class="col-sm-8">
                            <?=lang_field($art, 'text', 'og_image')?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><?= lang('Szczegóły') ?></h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label><?= lang('Pozycja w drzewie') ?>:</label>
                    <select class="select_simple form-control" name="id_tree">
                        <option value="0"><?= lang('główna pozycja') ?></option>
                        <?
                        foreach ($cat['categories'][$art->id_cat]->articles as $row) {
                            $padd = repeater('&#x2001;', $row->tree);

                            if ($row->id == $art->id_art)
                                $add = 'disabled="disabled"';
                            else
                                $add = '';

                            if ($row->tree < 3)
                                echo '<option ' . set_select('id_tree', $row->id, $row->id == $art->id_tree) . ' ' . $add . ' value="' . $row->id . '">' . $padd . ' ' . $row->title . '</option>';
                        }
                        ?>
                    </select>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3"><?=lang('Autor')?>:</label>
                        <div class="col-sm-9">
                            <?=lang_value($art, 'author_name')?>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3"><?=lang('Data modyfikacji')?>:</label>
                        <div class="col-sm-9">
                            <?=lang_value($art, 'date_modified')?>
                        </div>
                    </div>
                </div>



                <div class="form-group">
                    <label><?= lang('Opublikowany') ?>:</label>
                    <div class="row">
                        <label class="col-sm-3"><?= lang('w menu') ?></label>
                        <div class="col-sm-9">
                            <select class="select_simple" name="pub_menu">
                                <option value="1" <?=set_select('pub_menu', 1, !!$art->pub_menu)?>><?= lang('tak') ?></option>
                                <option value="0" <?=set_select('pub_menu', 0, !$art->pub_menu)?>><?= lang('nie') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3"><?= lang('cały artykuł') ?></label>
                        <div class="col-sm-9">
                            <select class="select_simple" name="pub">
                                <option value="1" <?=set_select('pub', 1, !!$art->pub)?>><?= lang('tak') ?></option>
                                <option value="0" <?=set_select('pub', 0, !$art->pub)?>><?= lang('nie') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3"><?= lang('języki') ?></label>
                        <div class="col-sm-9">
                            <?=lang_field($art, 'select', 'pub', array('values'=>array(1=>lang('tak'),0=>lang('nie'))))?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label><?= lang('Link do podstrony') ?>:</label>
                    <?=lang_field($art, 'text', 'URI', array('readonly' => true))?>
                </div>
                <div class="form-group">
                    <label><?= lang('Link do używania w panelu') ?>:</label>
                    <?=lang_field($art, 'text', 'link', array('readonly' => true))?>
                </div>
                <div class="form-group">
                    <label><?= lang('Sposób otwierania') ?>:</label>
                    <?=lang_field($art, 'select', 'target', array('values'=>array('0'=>lang('normalnie'),'_blank'=>lang('nowe okno'))))?>
                </div>
            </div>
        </div>
    </div>
</div>

<?if($art->id == 4):?>
<div class="box box-success" id="section_main">
    <div class="box-header with-border">
        <h3 class="box-title">"Sekcja główna" (Zajawka)</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="form-group">
            <label><?= lang('Nagłówek') ?>:</label>
            <?=lang_field($art, 'text', 'main_header')?>
        </div>
        <div class="form-group">
            <label><?= lang('Podtytuł') ?>:</label>
            <?=lang_field($art, 'text', 'main_subtitle')?>
        </div>
        <div class="form-group">
            <label><?= lang('Link') ?>:</label>
            <?=lang_field($art, 'text', 'main_link')?>
        </div>
    </div>
</div>
<!-- /.section_main -->
<? elseif ($art->id == 5): ?>

<div class="box box-success" id="section_about">
    <div class="box-header with-border">
        <h3 class="box-title">Sekcja "O nas"</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="form-group">
            <label><?= lang('Nagłówek') ?>:</label>
            <?=lang_field($art, 'text', 'about_header')?>
        </div>
        <div class="form-group">
            <label><?= lang('Wstęp') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'about_admission', array('width' => '100%'))?>
        </div>
        <div class="form-group">
            <label><?= lang('Treść') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'about_content', array('width' => '100%'))?>
        </div>
    </div>
</div>
<!-- /.section_about -->
<? elseif ($art->id == 6): ?>

<div class="box box-success" id="section_clients">
    <div class="box-header with-border">
        <h3 class="box-title">Sekcja "Nasi dotychczasowi klienci"</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="row">
            <div class="form-group col-md-12">
                <label><?= lang('Nagłówek') ?>:</label>
                <?=lang_field($art, 'text', 'clients_header')?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label><?= lang('Tekst obok linku') ?>:</label>
                <?=lang_field($art, 'text', 'clients_link_label')?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label><?= lang('Link') ?>:</label>
                <?=lang_field($art, 'text', 'clients_link')?>
            </div>
        </div>
    </div>
</div>
<!-- /.section_clients -->

<? elseif ($art->id == 7): ?>

<div class="box box-success" id="section_offer">
    <div class="box-header with-border">
        <h3 class="box-title">Sekcja "Oferta"</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="form-group">
            <label><?= lang('Nagłówek') ?>:</label>
            <?=lang_field($art, 'text', 'offer_header')?>
        </div>
        <div class="form-group">
            <label><?= lang('Wstęp') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'offer_admission', array('width' => '100%'))?>
        </div>
        <div class="form-group">
            <label><?= lang('Treść') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'offer_content', array('width' => '100%'))?>
        </div>
    </div>
</div>
<!-- /.section_offer -->
<? elseif ($art->id == 8): ?>

<div class="box box-success" id="section_collaboration">
    <div class="box-header with-border">
        <h3 class="box-title">Sekcja "Współpraca"</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="form-group">
            <label><?= lang('Nagłówek') ?>:</label>
            <?=lang_field($art, 'text', 'collaboration_header')?>
        </div>
        <?/*<div class="form-group">
            <label><?= lang('Treść po lewej stronie') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'collaboration_content_left', array('width' => '100%'))?>
        </div>
        <div class="form-group">
            <label><?= lang('Treść po prawej stronie') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'collaboration_content_right', array('width' => '100%'))?>
        </div>*/?>
    </div>
</div>
<!-- /.section_collaboration -->
<? elseif ($art->id == 9): ?>

<div class="box box-success" id="section_distinguish">
    <div class="box-header with-border">
        <h3 class="box-title">Sekcja "Co nas wyróżnia?"</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="row">
            <div class="form-group col-md-12">
                <label><?= lang('Nagłówek') ?>:</label>
                <?=lang_field($art, 'text', 'distinguish_header')?>
            </div>
        </div>
        <?/*
        <?for ($i=1; $i <= 5; $i++):?>
        <div class="row">
            <div class="form-group col-md-12">
                <label><?= lang('Tekst ponad ikoną nr.') ?>: <?=$i?></label>
                <?=lang_field($art, 'text', 'distinguish_icontext_'.$i)?>
            </div>
        </div>
        <?endfor;?>
        */?>
        <div class="row">
            <div class="form-group col-md-12">
                <label><?= lang('Tekst obok linku') ?>:</label>
                <?=lang_field($art, 'text', 'distinguish_link_label')?>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-12">
                <label><?= lang('Link') ?>:</label>
                <?=lang_field($art, 'text', 'distinguish_link')?>
            </div>
        </div>
    </div>
</div>
<!-- /.section_distinguish -->
<? elseif ($art->id == 10): ?>

<div class="box box-success" id="section_contact">
    <div class="box-header with-border">
        <h3 class="box-title">Sekcja "Kontakt"</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="form-group">
            <label><?= lang('Treść') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'contact_content', array('width' => '100%'))?>
        </div>
        <?/*
        <div class="form-group">
            <div class="form-group nc-image">
                <label><?=lang('Zdjęcie - dojazd')?></label>
                <div>
                <?
                if(!empty($art->img) && file_exists('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$art->img)) {
                    $tab = explode('.', $art->img);
                    echo '<a href="'.config_item('upload_path').$this->img_dir.$art->img.'" class="img">
                        <img src="'.config_item('upload_path').$this->img_dir.$tab[0].'_thumb.'.$tab[1].'" alt=""></a>';
                    
                    echo '<p class="checkbox"><label><input type="checkbox" value="1" name="del_img" /> '.lang('usuń zdjęcie').'</label></p>';
                    echo '<p><a href="'.$this->mod_url.'/crop/id/'.$art->id.'" class="btn btn-sm btn-success modal-box" data-iframe="true">'.lang('Kadruj').'</a></p>';
                }
                ?>
                    <p class="form-control-static"><input type="file" name="img" size="65"></p>
                </div>
            </div>
        </div> 
        */?>
        <div class="form-group">
            <label><?= lang('Wskazówki - dojazd') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'contact_directions', array('width' => '100%'))?>
        </div>
    </div>
</div>
<!-- /.section_contact -->
<?else:?>
<div class="box box-success">
    <div class="box-body">
        <div class="form-group">
            <label><?= lang('Wstęp') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'admission', array('width' => '100%', 'height' => '120px', 'toolbar' => 'Simple'))?>
        </div>
        <div class="form-group">
            <label><?= lang('Treść') ?>:</label>
            <?=lang_field($art, 'ckeditor', 'content', array('width' => '100%'))?>
        </div>
    </div>
</div>
<?endif;?>

<input type="submit" value="<?= lang('Zapisz') ?>" class="btn btn-primary">


<?= form_close('<br>'); ?>

<?if ($art->id == 6): ?>
<div class="box box-success" id="section_clients_numbers">
    <div class="box-header with-border">
        <h3 class="box-title">Liczby w sekcji</h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="row">
            <?=form_open(site_url($this->mod_url.'/add_number/id/'.$art->id))?>
            <div class="col-md-12">
            <h4><?= lang('Dodaj liczbę') ?></h4>
            </div>
            <div class="form-group col-md-5">
                <label><?= lang('Tekst ponad liczbą') ?>:</label>
                <?=form_input(['name' => 'top', 'value' => set_value('top'), 'class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-1">
                <label><?= lang('Liczba') ?>:</label>
                <?=form_input(['name' => 'number', 'value' => set_value('number'), 'class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-5">
                <label><?= lang('Tekst pod liczbą') ?>:</label>
                <?=form_input(['name' => 'bottom', 'value' => set_value('bottom'), 'class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-1">
                <br>    
                <input type="submit" value="Dodaj" class="btn btn-success btn-small">
            </div>
            <?= form_close() ?>
        </div>
        <h4><?=lang('Edytuj liczby')?></h4>
        <hr>
        
        <?=form_open(site_url($this->mod_url.'/save_numbers/id/'.$art->id))?>
        <?if($numbers):
        foreach($numbers as $number):?>
        <div class="row">
            <!-- <div class="col-md-12">
            <span><?= lang('Liczba nr.') ?>: <?=$number->id?></span>
            </div> -->
            <div class="form-group col-md-5">
                <label><?= lang('Tekst ponad liczbą') ?>:</label>
                <?=form_input(['name' => 'numbers['.$number->id.'][top]', 'value' => $number->top, 'class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-1">
                <label><?= lang('Liczba') ?>:</label>
                <?=form_input(['name' => 'numbers['.$number->id.'][number]', 'value' => $number->number, 'class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-5">
                <label><?= lang('Tekst pod liczbą') ?>:</label>
                <?=form_input(['name' => 'numbers['.$number->id.'][bottom]', 'value' => $number->bottom, 'class' => 'form-control'])?>
            </div>
            <br>
            <div class="form-group col-md-1">
                <br>
                <a 
                    href="<?=site_url($this->mod_url.'/delete_number/id/'.$art->id.'/number_id/'.$number->id)?>"
                    class="btn btn-danger btn-small"
                ><?=lang('Usuń')?></a>
            </div>
        </div>
            <?endforeach; endif;?>
        <div class="row">
            <div class="form-group col-md-1">
                <br>    
                <input type="submit" value="Zapisz Liczby" class="btn btn-primary btn-small">
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>
<!-- /.section_clients_numbers -->
<?endif;?>

<?if ($art->id == 8): ?>
<div class="box box-success" id="section_clients_offers">
    <div class="box-header with-border">
        <h3 class="box-title"><?=lang('Wypunktowanie w sekcji')?></h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="row">
            <?=form_open(site_url($this->mod_url.'/add_offer/id/'.$art->id))?>
            <div class="col-md-12">
            <h4><?= lang('Dodaj wypunktowanie') ?></h4>
            </div>
            <div class="form-group col-md-5">
                <label><?= lang('Tekst') ?>:</label>
                <?=form_textarea(['name' => 'text', 'value' => set_value('text'), 'class' => 'form-control', 'rows' => '3'])?>
            </div>
            <div class="form-group col-md-1">
                <br>    
                <input type="submit" value="Dodaj" class="btn btn-success btn-small">
            </div>
            <?= form_close() ?>
        </div>
        <h4><?=lang('Edytuj wypunktowanie')?></h4>
        <hr>
        
        <?=form_open(site_url($this->mod_url.'/save_offers/id/'.$art->id))?>
        <?if($offers):
        foreach($offers as $offer):?>
        <div class="row">
            <!-- <div class="col-md-12">
            <span><?= lang('Liczba nr.') ?>: <?=$offer->id?></span>
            </div> -->
            <div class="form-group col-md-5">
                <label><?= lang('Tekst') ?>:</label>
                <?=form_textarea(['name' => 'offers['.$offer->id.'][text]', 'value' => $offer->text, 'class' => 'form-control', 'rows' => '3'])?>
            </div>
            <br>
            <div class="form-group col-md-1">
                <br>
                <a 
                    href="<?=site_url($this->mod_url.'/delete_offer/id/'.$art->id.'/offer_id/'.$offer->id)?>"
                    class="btn btn-danger btn-small"
                ><?=lang('Usuń')?></a>
            </div>
        </div>
            <?endforeach; endif;?>
        <div class="row">
            <div class="form-group col-md-1">
                <br>    
                <input type="submit" value="Zapisz wypunktowanie" class="btn btn-primary btn-small">
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>
<!-- /.section_clients_offers -->
<?endif;?>

<?if ($art->id == 9): ?>
<div class="box box-success" id="section_clients_skills">
    <div class="box-header with-border">
        <h3 class="box-title"><?=lang('Ikony w sekcji')?></h3>
        <div class="box-tools pull-right">
        <!-- Collapse Button -->
        <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-plus"></i>
        </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="row">
            <?=form_open(site_url($this->mod_url.'/add_skill/id/'.$art->id))?>
            <div class="col-md-12">
            <h4><?= lang('Dodaj liczbę') ?></h4>
            </div>
            <div class="form-group col-md-5">
                <label><?= lang('Ikona') ?>:</label>
                <?=form_input(['name' => 'icon', 'value' => set_value('icon'), 'class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-6">
                <label><?= lang('Tekst') ?>:</label>
                <?=form_input(['name' => 'text', 'value' => set_value('text'), 'class' => 'form-control', 'rows' => '3'])?>
            </div>
            <div class="form-group col-md-1">
                <br>    
                <input type="submit" value="Dodaj" class="btn btn-success btn-small">
            </div>
            <?= form_close() ?>
        </div>
        <h4><?=lang('Edytuj wypunktowanie')?></h4>
        <hr>
        
        <?=form_open(site_url($this->mod_url.'/save_skills/id/'.$art->id))?>
        <?if($skills):
        foreach($skills as $skill):?>
        <div class="row">
            <!-- <div class="col-md-12">
            <span><?= lang('Liczba nr.') ?>: <?=$skill->id?></span>
            </div> -->
            <div class="form-group col-md-5">
                <label><?= lang('Ikona') ?>:</label>
                <?=form_input(['name' => 'skills['.$skill->id.'][icon]', 'value' => $skill->icon, 'class' => 'form-control'])?>
            </div>
            <div class="form-group col-md-6">
                <label><?= lang('Tekst') ?>:</label>
                <?=form_input(['name' => 'skills['.$skill->id.'][text]', 'value' => $skill->text, 'class' => 'form-control', 'rows' => '3'])?>
            </div>
            <br>
            <div class="form-group col-md-1">
                <a 
                    href="<?=site_url($this->mod_url.'/delete_skill/id/'.$art->id.'/skill_id/'.$skill->id)?>"
                    class="btn btn-danger btn-small"
                ><?=lang('Usuń')?></a>
            </div>
        </div>
            <?endforeach; endif;?>
        <div class="row">
            <div class="form-group col-md-1">
                <br>    
                <input type="submit" value="Zapisz ikony" class="btn btn-primary btn-small">
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>
<!-- /.section_clients_skills -->
<?endif;?>