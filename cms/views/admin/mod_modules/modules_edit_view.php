<?=form_open(current_url().'/check/1');?>
<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">

<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Tytuł')?></label>
					<input type="text" name="title" value="<?=set_value('title', $row->title)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Etykieta')?></label>
					<?=form_field_tooltip(lang('Etykieta wyświetla się nad pozycją modułu w menu i służy do wizualnego grupowania. Pole opcjonalnie.'))?>
					<input type="text" name="label" value="<?=set_value('label', $row->label)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Parametry modułu')?></label>
					<div class="row">
						<div class="col-sm-3 form-control-static">mod:</div>
						<div class="col-sm-9">
							<input type="text" name="mod" value="<?=set_value('mod', $row->mod)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static">mod_dir:</div>
						<div class="col-sm-9">
							<input type="text" name="mod_dir" value="<?=set_value('mod_dir', $row->mod_dir)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static">mod_function:</div>
						<div class="col-sm-9">
							<input type="text" name="mod_function" value="<?=set_value('mod_function', $row->mod_function)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static">mod_table:</div>
						<div class="col-sm-9">
							<input type="text" name="mod_table" value="<?=set_value('mod_table', $row->mod_table)?>" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><?=lang('Opis')?></label>
					<textarea name="description" class="form-control" style="height: 75px;"><?=set_value('description', $row->description)?></textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Szczegóły')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Komponent')?>:</label>
					<select class="select_with_rm form-control" name="id_c">
						<option value=""><?=lang('brak') ?></option>
						<? if(is_array($components)) {
							foreach($components as $com){
								echo '<option value="'.$com->id.'" '.set_select('id_c', $com->id, ($row->id_c == $com->id)).'>'.$com->name.'</option>';
							}
						} ?>
					</select>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-sm-5 form-control-static"><?=lang('Opublikowany')?>:</div>
						<div class="col-sm-7 form-control-static">
							<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$row->pub)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$row->pub)?>> <?=lang('nie')?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-5 form-control-static"><?=lang('Wyświetlaj w menu')?>:</div>
						<div class="col-sm-7 form-control-static">
							<label class="radio-inline"><input name="menu" type="radio" value="1" <?=set_radio('menu', 1, !!$row->menu)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="menu" type="radio" value="0" <?=set_radio('menu', 0, !$row->menu)?>> <?=lang('nie')?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-5 form-control-static"><?=lang('Globalny')?>:</div>
						<div class="col-sm-7 form-control-static">
							<label class="radio-inline"><input name="global" type="radio" value="1" <?=set_radio('global', 1, !!$row->global)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="global" type="radio" value="0" <?=set_radio('global', 0, !$row->global)?>> <?=lang('nie')?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-5 form-control-static"><?=lang('Domyślny')?>:</div>
						<div class="col-sm-7 form-control-static">
							<label class="radio-inline"><input name="default" type="radio" value="1" <?=set_radio('default', 1, !!$row->default)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="default" type="radio" value="0" <?=set_radio('default', 0, !$row->default)?>> <?=lang('nie')?></label>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-5 form-control-static"><?=lang('Tylko dla Artneo')?>:</div>
						<div class="col-sm-7 form-control-static">
							<label class="radio-inline"><input name="main" type="radio" value="1" <?=set_radio('main', 1, !!$row->main)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="main" type="radio" value="0" <?=set_radio('main', 0, !$row->main)?>> <?=lang('nie')?></label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
<?=form_close(); ?>