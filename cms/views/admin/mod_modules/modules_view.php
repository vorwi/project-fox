<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj moduł') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Komponent') ?></label>
				<select name="id_c" class="select_with_rm form-control">
					<option value=""><?=lang('brak') ?></option>
					<? if(is_array($components)) {
						foreach($components as $com){
							echo '<option value="'.$com->id.'" '.set_select('id_c', $com->id).'>'.$com->name.'</option>';
						}
					} ?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Tytuł') ?></label>
				<input type="text" name="title" value="<?=set_value('title')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Etykieta') ?></label>
				<?=form_field_tooltip(lang('Etykieta wyświetla się nad pozycją modułu w menu i służy do wizualnego grupowania. Pole opcjonalnie.'))?>
				<input type="text" name="label" value="<?=set_value('label')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Parametry modułu') ?></label>
				<div class="row">
					<div class="col-sm-3 form-control-static">mod:</div>
					<div class="col-sm-9">
						<input type="text" name="mod" value="<?=set_value('mod')?>" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 form-control-static">mod_dir:</div>
					<div class="col-sm-9">
						<input type="text" name="mod_dir" value="<?=set_value('mod_dir')?>" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 form-control-static">mod_function:</div>
					<div class="col-sm-9">
						<input type="text" name="mod_function" value="<?=set_value('mod_function')?>" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 form-control-static">mod_table:</div>
					<div class="col-sm-9">
						<input type="text" name="mod_table" value="<?=set_value('mod_table')?>" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label><?=lang('Opis') ?></label>
				<textarea name="description" rows="8" cols="40" style="height: 75px;" class="form-control"><?=set_value('description')?></textarea>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Opublikowany')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, TRUE)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Wyświetlaj w menu')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="menu" type="radio" value="1" <?=set_radio('menu', 1, TRUE)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="menu" type="radio" value="0" <?=set_radio('menu', 0)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Globalny')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="global" type="radio" value="1" <?=set_radio('global', 1)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="global" type="radio" value="0" <?=set_radio('global', 0, TRUE)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Domyślny')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="default" type="radio" value="1" <?=set_radio('default', 1)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="default" type="radio" value="0" <?=set_radio('default', 0, TRUE)?>> <?=lang('nie')?></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 form-control-static"><?=lang('Tylko dla Artneo')?>:</div>
					<div class="col-sm-7 form-control-static">
						<label class="radio-inline"><input name="main" type="radio" value="1" <?=set_radio('main', 1)?>> <?=lang('tak')?></label>
						<label class="radio-inline"><input name="main" type="radio" value="0" <?=set_radio('main', 0, TRUE)?>> <?=lang('nie')?></label>
					</div>
				</div>
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<?
	if($rows !== FALSE) {
	$i_com = 1; 
?>
	<? foreach($rows as $com) { ?>
		<h2>
		<?
		if(!empty($com->icon) && file_exists(str_replace('%20', ' ', './'.config_item('site_path').$com->icon))){
			echo '<img src="'.$com->icon.'" alt="" class="absmiddle" />';
		}
		echo $com->name;
		?>
		</h2>
		
		<? if($com->modules !== FALSE) { ?>
			<?=form_open()?>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th style="width:25px;"><?=lang('Nr') ?></th>
						<th><?=lang('Tytuł') ?></th>
						<th><?=lang('Ścieżka') ?></th>
						<th style="width:40px;"><?=lang('Kolejność') ?></th>
						<th style="width:60px;"><?=lang('Globalny') ?></th>
						<th style="width:60px;"><?=lang('Domyślny') ?></th>
						<th style="width:80px;"><?=lang('Wyświetlaj w menu') ?></th>
						<th style="width:80px;"><?=lang('Tylko dla Artneo') ?></th>
						<th style="width:180px;"><?=lang('Działania') ?></th>
						<th style="width:25px;"><input class="checkall" type="checkbox"></th>
					</tr>
				</thead>
				<tbody class="sortable">
				   	<? $i=1; ?>
				   	<? foreach($com->modules as $row): ?>
						<tr id="listItem_<?=$row->id?>">
							<td><b><?=$i?></b></td>
							<td>
							<?
								echo anchor("{$this->mod_url}/edit/lang/".$_SESSION['admin_lang']."/id/{$row->id}", $row->title, array('title' => htmlspecialchars($row->description)));
								echo lang_versions($lang, $row, $this->mod_url.'/edit');
							?>
							</td>
							<td><?=$row->mod_dir.$row->mod.'/'.$row->mod_function?></td>
							<td class="text-center"><span class="fa fa-arrows"></span><input class="currentposition" type="hidden" value="<?=$row->position?>" name="position[<?=$row->id?>]"></td>
							<td class="text-center">
							<? if($row->global == 0) echo anchor("{$this->mod_url}/vswitch/column/glob/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Ustaw dostęp globalny'), 'class' => 'anchor_icon')); else echo anchor("{$this->mod_url}/vswitch/column/glob/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Usuń z dostępu globalnego'), 'class' => 'anchor_icon'));?>
							</td>
							<td class="text-center">
							<? if($row->default == 0) echo anchor("{$this->mod_url}/vswitch/column/default/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Ustaw jako domyślny moduł'), 'class' => 'anchor_icon')); else echo anchor("{$this->mod_url}/vswitch/column/default/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Usuń z domyślnych'), 'class' => 'anchor_icon'));?>
							</td>
							<td class="text-center">
							<? if($row->menu == 0) echo anchor("{$this->mod_url}/vswitch/column/menu/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Wyświetlaj w menu'), 'class' => 'anchor_icon')); else echo anchor("{$this->mod_url}/vswitch/column/menu/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Nie wyświetlaj w menu'), 'class' => 'anchor_icon'));?>
							</td>
							<td class="text-center">
							<? if($row->main == 0) echo anchor("{$this->mod_url}/vswitch/column/main/id/{$row->id}/set/1", '<i class="fa fa-plus"></i>', array('title' => lang('Ustaw jako widoczne tylko dla Artneo'), 'class' => 'anchor_icon')); else echo anchor("{$this->mod_url}/vswitch/column/main/id/{$row->id}/set/0", '<i class="fa fa-minus"></i>', array('title' => lang('Ustaw jako widoczne dla wszystkich'), 'class' => 'anchor_icon'));?>
							</td>
							<td class="text-center nc-options">
								<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
								<? if($row->pub == 0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/0", lang('Odpublikuj'));?>
								<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
							</td>
							<td class="text-center"><input type="checkbox" value="<?=$row->id?>" name="check[]" /></td>
						</tr>
					<? $i++; ?>
					<? endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">
							<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>" >
						</td>
						<td colspan="6" class="nc-options">
							<?=lang('Zaznaczone:') ?>
							<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
							<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
							<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
							
							<br><?=lang('Globalny') ?>:
							<a title="<?=lang('Ustaw dostęp gloabalny') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/glob/set/1")?>" ><i class="fa fa-plus"></i></a>
							<a title="<?=lang('Usuń z dostępu globalnego') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/glob/set/0")?>"><i class="fa fa-minus"></i></a>
							
							<br><?=lang('Domyślny') ?>:
							<a title="<?=lang('Ustaw jako domyślny moduł') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/default/set/1")?>"><i class="fa fa-plus"></i></a>
							<a title="<?=lang('Usuń z domyślnych') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/default/set/0")?>"><i class="fa fa-minus"></i></a>
							
							<br><?=lang('Wyświetlaj w menu') ?>:
							<a title="<?=lang('Wyświetlaj w menu') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/menu/set/1")?>"><i class="fa fa-plus"></i></a>
							<a title="<?=lang('Nie wyświetlaj w menu') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/menu/set/0")?>"><i class="fa fa-minus"></i></a>
							
							<br><?=lang('Tylko dla Artneo') ?>:
							<a title="<?=lang('Ustaw jako widoczne tylko dla Artneo') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/main/set/1")?>"><i class="fa fa-plus"></i></a>
							<a title="<?=lang('Ustaw jako widoczne dla wszystkich') ?>" class="anchor_icon click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/main/set/0")?>"><i class="fa fa-minus"></i></a>
						</td>
					</tr>
				</tfoot>
			</table>
			<?=form_close()?>
<?
		} else {
			echo msg(lang('Nie znaleziono żadnych modułów.'), 1);
		} 
		$i_com++;
	}
} else {
	echo msg(lang('Nie znaleziono żadnych komponentów.'), 1);
}
?>
