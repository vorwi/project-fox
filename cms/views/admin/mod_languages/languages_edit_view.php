<?
if(isset($edit)) {
	echo form_open(current_url(), array('class' => 'row'));
?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa języka') ?></label>
					<input type="text" name="lang" value="<?=set_value('lang', $edit->lang)?>" class="form-control">
				</div>
				<div class="form-group">
					<label><?=lang('Kod ISO 3166-1 alpha-2') ?></label>
					<select name="short" class="form-control">
					<? if(is_array($flags)) {
						foreach($flags as $flag) {
							list($code, $ext) = explode('.', $flag);
							if($ext == 'png') {
								echo '<option value="'.$code.'" '.set_select('short', $code, ($code == $edit->short)).' style="background: url('.config_item('gfx_c').'img/flags/'.$flag.') no-repeat left center; padding-left: 20px;">'.$code.'</option>';
							}
						}
					} ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('Nazwa katalogu w ')?><i>cms/language/</i></label>
					<input type="text" name="dir" value="<?=set_value('dir', $edit->dir)?>" class="form-control">
				</div>
				<div class="form-group">
					<label><?=lang('Opublikowany')?>:</label>
					<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$edit->pub)?>> <?=lang('tak')?></label>
					<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$edit->pub)?>> <?=lang('nie')?></label>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?
	echo form_close();
}
?>
