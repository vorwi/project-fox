<?
if(!empty($files)) {
	if(empty($file)) {
?>
<p>
	<a class="btn btn-default" href="<?="{$this->mod_url}"?>"><?=lang('Powrót do języków')?></a>
</p>
<p class="callout callout-info">
	<?=lang('Proszę wybrać plik do edycji:')?>
</p>
<?

		$url_base = "{$this->mod_url}/translate/{$language->short}/";
		draw_dir_tree($files, $url_base);

	} else {

		$is_main = (config_item('main_lang') == $language->short);

		if(!$is_main) {
			$trs = $lang_path.$main_language->dir.'/'.$file;
			if(file_exists($trs)) {
				$lang = array();
				include($trs);

				$main_lang = $lang;
			} else {
				echo msg(lang('Plik').' <i>'.$trs.'</i> '.lang('języka głównego jest pusty!'), 1);
				$main_lang = array();
			}
		}

		$trs = $lang_path.$language->dir.'/'.$file;

		$lang = array();
		if(file_exists($trs)) {
			include($trs);

			if($is_main) {
				$all_keys = array_keys($lang);
			} else {
				$all_keys = array_merge(array_keys($main_lang), array_keys($lang));
				$all_keys = array_unique($all_keys);
			}
		}

?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.translate textarea').flexible();
	})
</script>
<p>
	<a class="btn btn-default" href="<?="{$this->mod_url}/translate/{$language->short}"?>"><?=lang('Powrót do plików')?></a>
</p>

	<? if(!empty($all_keys)){?>
<?=form_open()?>
	<table class="table table-bordered table-hover translate">
		<thead>
			<tr>
				<th style="width: 18%"><?=lang('Klucz')?></th>
				<th><?=lang('Język główny').' '.lang_flag($main_language)?></th>
				<? if(!$is_main){ ?>
				<th style="width: 41%"><?=lang('Tłumaczenie')?>: <?=$language->lang?> <?=lang_flag($language)?></th>
				<? } ?>
			</tr>
		</thead>
		<tbody>
<?
		$i = 1;
		foreach($all_keys as $k) {

			echo '
			<tr>
			<td>'.$k.'</td>';
			if(!$is_main) {
				$mv = (isset($main_lang[$k]) ? $main_lang[$k] : FALSE);
				echo '<td class="padding-xs'.($mv === FALSE ? ' not_exists' : '').'">';

				if(is_array($mv)) inputs_table($k, $mv, TRUE, TRUE);
				else echo lang_input(FALSE, $mv, TRUE);
				echo '</td>';
			}

			$v = (isset($lang[$k]) ? $lang[$k] : FALSE);
			if($v === FALSE) $v = ($mv !== FALSE ? $mv : FALSE);

			echo '<td class="padding-xs'.($v === FALSE ? ' not_exists' : '').'">';
			if(is_array($v)) inputs_table($k, $v);
			else echo lang_input($k, $v);
			echo '</td>
			</tr>';
		}
?>
		</tbody>
	</table>
	<input type="submit" value="<?=lang('Zapisz')?>" name="save" class="btn btn-success" style="width: 150px;" />
<?=form_close()?>

<?
		} else {
			echo msg(lang('Plik jest pusty!'), 1);
		}
	}
} else {
	echo msg(lang('Nie znaleziono żadnych plików językowych'), 1);
}
?>


<script type="text/javascript">
/*!
* flexibleArea.js v1.2
* A jQuery plugin that dynamically updates textarea's height to fit the content.
* http://flaviusmatis.github.com/flexibleArea.js/
*
* Copyright 2012, Flavius Matis
* Released under the MIT license.
* http://flaviusmatis.github.com/license.html
*/
(function($){var methods={init:function(){var styles=["paddingTop","paddingRight","paddingBottom","paddingLeft","fontSize","lineHeight","fontFamily","width","fontWeight","border-top-width","border-right-width","border-bottom-width","border-left-width","-moz-box-sizing","-webkit-box-sizing","box-sizing"];return this.each(function(){if(this.type!=="textarea"){return false}var $textarea=$(this).css({resize:"none",overflow:"hidden"});var $clone=$("<div></div>").css({position:"absolute",display:"none","word-wrap":"break-word","white-space":"pre-wrap","border-style":"solid"}).appendTo(document.body);function copyStyles(){for(var i=0;i<styles.length;i++){$clone.css(styles[i],$textarea.css(styles[i]))}}copyStyles();var hasBoxModel=$textarea.css("box-sizing")=="border-box"||$textarea.css("-moz-box-sizing")=="border-box"||$textarea.css("-webkit-box-sizing")=="border-box";var heightCompensation=parseInt($textarea.css("border-top-width"))+parseInt($textarea.css("padding-top"))+parseInt($textarea.css("padding-bottom"))+parseInt($textarea.css("border-bottom-width"));var textareaHeight=parseInt($textarea.css("height"),10);var lineHeight=parseInt($textarea.css("line-height"),10)||parseInt($textarea.css("font-size"),10);var minheight=lineHeight>textareaHeight?lineHeight:textareaHeight;var maxheight=parseInt($textarea.css("max-height"),10)>-1?parseInt($textarea.css("max-height"),10):Number.MAX_VALUE;function updateHeight(){var textareaContent=$textarea.val().replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/&/g,"&amp;").replace(/\n/g,"<br/>");$clone.html(textareaContent+"&nbsp;");setHeightAndOverflow()}function setHeightAndOverflow(){var cloneHeight=$clone.height();var overflow="hidden";var height=hasBoxModel?cloneHeight+heightCompensation:cloneHeight;if(height>maxheight){height=maxheight;overflow="auto"}else{if(height<minheight){height=minheight}}if($textarea.height()!==height){$textarea.css({overflow:overflow,height:height+"px"})}}$textarea.bind("keyup change cut paste",function(){updateHeight()});$(window).bind("resize",function(){var cleanWidth=parseInt($textarea.width(),10);if($clone.width()!==cleanWidth){$clone.css({width:cleanWidth+"px"});updateHeight()}});$textarea.bind("blur",function(){setHeightAndOverflow()});$textarea.bind("updateHeight",function(){copyStyles();updateHeight()});$(function(){updateHeight()})})}};$.fn.flexible=function(method){if(methods[method]){return methods[method].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof method==="object"||!method){return methods.init.apply(this,arguments)}else{$.error("Method "+method+" does not exist on jQuery.flexible")}}}})(jQuery);
</script>