<?
$language_dir = str_replace(FCPATH, '', APPPATH).'language'.DIRECTORY_SEPARATOR;
?>
<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj język') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
    	<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
	    	<div class="form-group">
				<label><?=lang('Nazwa języka') ?></label>
				<input type="text" name="lang" value="<?=set_value('lang')?>" class="form-control">
			</div>
	    	<div class="form-group">
		    	<label><?=lang('Kod ISO 3166-1 alpha-2') ?></label>
		    	<select name="short" class="form-control">
				<? if(is_array($flags)) {
					foreach($flags as $flag) {
						list($code, $ext) = explode('.', $flag);
						if($ext == 'png') {
							echo '<option value="'.$code.'" '.set_select('short', $code).' style="background: url('.config_item('gfx_c').'img/flags/'.$flag.') no-repeat left center; padding-left: 20px;">'.$code.'</option>';
						}
					}
				} ?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Nazwa katalogu w ')?><i><?=$language_dir?></i></label>
				<input type="text" name="dir" value="<?=set_value('dir')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Stworzyć kopię wszystkich artykułów z języka głównego?') ?></label>
				<div class="checkbox">
					<label><input name="copy_articles" type="checkbox" value="1" <?=set_radio('copy_articles', 1)?>> <?=lang('tak') ?></label>
				</div>
			</div>
    		<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
    	<?=form_close()?>
	</div>
</div>

<?=form_open($this->mod_url.'/index/change/1')?>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
		    <th style="width:25px;"><?=lang('Nr')?></th>
			<th><?=lang('Nazwa języka')?></th>
			<th style="width: 50px"><?=lang('Kod')?></th>
			<th><?=lang('Katalog')?></th>
			<th style="width:40px;"><?=lang('Kolejność')?></th>
			<th><?=lang('Działania')?></th>
			<th style="width:25px;"><input class="checkall" type="checkbox"></th>
		</tr>
	</thead>
	<tbody class="sortable">
	<?
	$i=1;
	foreach($lang as $row){
	?>
		<tr>
			<td><?=$i?></td>
			<td><?=$row->lang.' '.lang_flag($row)?></td>
			<td class="text-center"><?=strtoupper($row->short)?></td>
			<td><?
				echo $language_dir.$row->dir;
				echo (is_dir('./'.$language_dir.$row->dir) ? ' : <span style="color: #02C015;">'.lang('istnieje').'</span>' : ' : <span style="color: #F10B0B;">'.lang('nie istnieje').'</span>');
			?></td>
			<td class="text-center"><span class="fa-arrows"></span><input class="currentposition" type="hidden" name="position[<?=$row->id?>]" value="<?=$row->position?>"></td>
			<td class="text-center nc-options">
		    	<?=anchor("{$this->mod_url}/index/edit/{$row->short}", lang('Edytuj język'))?>
		    	<?=anchor("{$this->mod_url}/translate/{$row->short}", lang('Edytuj tłumaczenia'))?>
		    	<? if($row->short != config_item('main_lang')) { ?>
		    		<? if($row->pub==0) echo anchor("{$this->mod_url}/pub/id/{$row->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/pub/id/{$row->id}/set/0", lang('Odpublikuj'));?>
		    		<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
    			<? } ?>
            </td>
            <td class="text-center">
            	<? if($row->short != config_item('main_lang')) { ?>
            	<input type="checkbox"  value="<?=$row->id?>" name="check[]">
            	<? } ?>
            </td>
		</tr>
	<?
		$i++;
	}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">
				<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>">
			</td>
			<td colspan="2" class="nc-options">
				<?=lang('Zaznaczone')?>:
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/pub/set/1")?>"><?=lang('Publikuj') ?></a>
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone języki?') ?>"><?=lang('Usuń') ?></a>
			</td>
		</tr>
	</tfoot>
</table>
<?=form_close()?>
