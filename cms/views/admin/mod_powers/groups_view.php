<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj grupę') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'edit'));?>
			<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Opis') ?></label>
				<textarea name="description" rows="8" cols="40" class="form-control"><?=set_value('description')?></textarea>
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<? if($rows !== FALSE): ?>
	<?=form_open();?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th><?=lang('Nazwa') ?></th>
				<th><?=lang('Opis') ?></th>
				<th style="width:40px;"><?=lang('Kolejność') ?></th>
				<th style="width:250px;"><?=lang('Działania') ?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox"></th>
			</tr>
		</thead>
		<tbody class="sortable">
		   	<? $i=1; ?>
		   	<? foreach($rows as $row): ?>
				<tr id="listItem_<?=$row->id?>">
					<td><b><?=$i?></b></td>
					<td>
					<?
						echo anchor("{$this->mod_url}/edit/lang/".config_item('main_lang')."/id/{$row->id}", $row->name);
						echo lang_versions($lang, $row, $this->mod_url.'/edit');
					?>
					</td>
					<td><?=$row->description?></td>
					<td  class="text-center"><span class="fa-arrows"></span><input class="currentposition" type="hidden" value="<?=$row->position?>" name="position[<?=$row->id?>]" style="width:30px;" /></td>
					<td class="text-center nc-options">
						<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
						<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
						<?=anchor("{$this->mod_url}/powers/id/{$row->id}",lang('Uprawnienia')); ?>
					</td>
					<td class="text-center"><input type="checkbox" value="<?=$row->id?>" name="check[]"></td>
				</tr>
			<? $i++; ?>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="4">
					<input class="btn btn-sucess btn-sm click-submit" type="button" data-href="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność') ?>" />
				</td>
				<td colspan="2" class="nc-options">
					<?=lang('Zaznaczone:') ?>
					<a class="click-submit" data-href="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?')?>"><?=lang('Usuń') ?></a>
				</td>
			</tr>
		</tfoot>
	</table>
	<?=form_close()?>
<? else: ?>
	<?=msg(lang('Nie znaleziono żadnych wpisów.'),1)?>
<? endif ?>