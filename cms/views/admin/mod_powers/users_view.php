<? if($this->session->flashdata('changed')!=''):?>
	<?
		$msg = null;
		$changed = $this->session->flashdata('changed');
		foreach($changed as $row){ $msg .= lang('Login').': <b>'.$row['login'].'</b>: '.lang('nowe hasło').': <b>'.$row['password'].'</b><br />'; }

		$msg .= '<b>'.lang('Nowe hasła zostały wysłane na podane adresy email.').'</b>';

		echo msg($msg,3);
	?>
<? endif ?>

<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj użytkownika') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Grupa') ?></label>
				<select name="id_group" class="select_with_rm form-control">
					<option value=""><?=lang('brak') ?></option>
					<?
					if(is_array($groups)) {
						foreach($groups as $gr){
							echo '<option value="'.$gr->id.'" '.set_select('id_group', $gr->id).'>'.$gr->name.'</option>';
						}
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Imię i nazwisko') ?></label>
				<input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Login') ?></label>
				<input type="text" name="login" value="<?=set_value('login')?>" class="form-control">
			</div>
			<div class="form-group">
				<label><?=lang('Dane kontaktowe') ?></label>
				<div class="row">
					<div class="col-sm-3 form-control-static"><?=lang('Email') ?>:</div>
					<div class="col-sm-9">
						<input type="text" name="email" value="<?=set_value('email')?>" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 form-control-static"><?=lang('Telefon') ?>:</div>
					<div class="col-sm-9">
						<input type="text" name="phone" value="<?=set_value('phone')?>" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label><?=lang('Dane adresowe') ?></label>
				<div class="row">
					<div class="col-sm-3 form-control-static"><?=lang('Ulica i nr budynku') ?>:</div>
					<div class="col-sm-9">
						<input type="text" name="address" value="<?=set_value('address')?>" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 form-control-static"><?=lang('Kod pocztowy') ?>:</div>
					<div class="col-sm-9">
						<input type="text" name="post_code" value="<?=set_value('post_code')?>" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3 form-control-static"><?=lang('Miasto') ?>:</div>
					<div class="col-sm-9">
						<input type="text" name="city" value="<?=set_value('city')?>" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label><?=lang('Wysłanie hasła') ?></label>
				<div class="checkbox">
					<label><input checked="checked" type="checkbox" value="1" name="send_password"> <?=lang('Wyślij hasło na podany adres email użytkownika')?></label>
				</div>
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<?=$filters_form?>

<p><?=lang('Liczba wyników spełniających kryteria:') ?> <b><?=$count?></b></p>

<?
if($rows !== FALSE) { 
	echo $pagination;
	echo form_open();
?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr') ?></th>
				<th style="width:25px;"><?=lang('ID') ?></th>
				<th><?=lang('Login') ?></th>
				<th><?=lang('Nazwa') ?></th>
				<th><?=lang('Email') ?></th>
				<th><?=lang('Grupa') ?></th>
				<th style="width: 265px;"><?=lang('Działania') ?></th>
				<th style="width: 25px;"><input class="checkall" type="checkbox"></th>
			</tr>
		</thead>
		<tbody>
			<? $i = (isset($params->offset) ? $params->offset + 1 : 1); ?>
			<? foreach($rows as $row): ?>
				<tr>
					<td><b><?=$i?></b></td>
					<td class="text-center"><?=$row->id?></td>
					<td class="text-center"><?=$row->login?></td>
					<td class="text-center"><?=$row->name?></td>
					<td class="text-center"><?=$row->email?></td>
					<td class="text-center">
						<? if(!empty($row->id_group) && isset($groups[$row->id_group])){ ?>
							<?=$groups[$row->id_group]->name?>
						<? } ?>
					</td>
					<td class="text-center nc-options">
					<? if(config_item('main_account_protect') === FALSE || $row->main == 0) { ?>
						<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
						<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
						<? if($row->status==1) echo anchor("{$this->mod_url}/active/id/{$row->id}/set/0", lang('Aktywny')); else echo anchor("{$this->mod_url}/active/id/{$row->id}/set/1", lang('Zablokowany')); ?>
						<?=anchor("admin/{$this->mod_dir}users/powers/id/{$row->id}", lang('Uprawnienia')); ?>
					<? } ?>
					</td>
					<td class="text-center"><? if($row->main != 1){ ?><input type="checkbox"  value="<?=$row->id?>" name="check[]" /><? } ?></td>
				</tr>
			<? $i++ ?>
			<? endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="8" class="nc-options">
					<?=lang('Zaznaczone:') ?> 
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/edit")?>"><?=lang('Edytuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/powers")?>"><?=lang('Uprawnienia') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
				</td>
			</tr>
		</tfoot>
	</table>
<?
	echo form_close();
	echo $pagination;

} else {
	echo msg(lang('Nie znaleziono żadnych wpisów.'), 1);
}
?>
