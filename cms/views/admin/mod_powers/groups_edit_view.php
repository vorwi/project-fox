<?=form_open(current_url().'/check/1');?>

	<div class="row">
		<div class="col-md-6">
			<div class="box box-primary">
				<div class="box-body">
					<div class="form-group">
						<label><?=lang('Nazwa')?></label>
						<input type="text" name="name" class="form-control" value="<?=set_value('name', $row->name)?>">
					</div>
					<div class="form-group">
						<label><?=lang('Opis')?></label>
						<textarea name="description" class="form-control" rows="8" cols="40"><?=set_value('description', $row->description)?></textarea>
					</div>
				</div>
				<div class="box-footer">
					<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
				</div>
			</div>
		</div>
	</div>

<?=form_close(); ?>

<script type="text/javascript">
$(document).ready(function(){
	$("#edit").validate({
		<?=jqueryValidateCommonVars()?>
		rules: {
			name: {
				required: true
			}
		},
		messages: {
			name: "<?=lang('Proszę wprowadzić nazwę') ?>"
		}
	});
});
</script>