<?
if($all !== FALSE ) {
	$tab = $this->uri->uri_to_assoc();
	if(isset($tab['page']) && @is_numeric($tab['page'])) $add = $tab['page'];
	else $add = 1;
	
	echo form_open($this->mod_url.'/edit/page/'.$add.'/check/1', array('class' => 'row'));
	
	foreach($all as $user) {
		echo form_hidden('check[]', $user->id);
?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Użytkownik')?> #<?=$user->id?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<p><?=lang('Data dodania') ?>: <?=date("d.m.Y", $user->date_add)?></p>
				
				<div class="form-group">
					<label><?=lang('Grupa') ?>:</label>
					<select name="id_group_<?=$user->id?>" class="form-control select_with_rm">
						<option value=""><?=lang('brak') ?></option>
						<? if(is_array($groups)) {
							foreach($groups as $gr){
								echo '<option value="'.$gr->id.'" '.set_select('id_group_'.$user->id, $gr->id, ($user->id_group == $gr->id)).'>'.$gr->name.'</option>';
							}
						} ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('Imię i nazwisko') ?></label>
					<input type="text" name="name_<?=$user->id?>" class="form-control" value="<?=set_value('name_'.$user->id, $user->name)?>">
				</div>
				<div class="form-group">
					<label><?=lang('Login') ?></label>
					<input type="text" name="login_<?=$user->id?>" class="form-control" value="<?=set_value('login_'.$user->id, $user->login)?>">
				</div>
				<div class="form-group">
					<label><?=lang('Dane kontaktowe') ?></label>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Email') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="email_<?=$user->id?>" value="<?=set_value('email_'.$user->id, $user->email)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Telefon') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="phone_<?=$user->id?>" value="<?=set_value('phone_'.$user->id, $user->phone)?>" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><?=lang('Dane adresowe') ?></label>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Ulica i nr budynku') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="address_<?=$user->id?>" value="<?=set_value('address_'.$user->id, $user->address)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Kod pocztowy') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="post_code_<?=$user->id?>" value="<?=set_value('post_code_'.$user->id, $user->post_code)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 form-control-static"><?=lang('Miasto') ?>:</div>
						<div class="col-sm-9">
							<input type="text" name="city_<?=$user->id?>" value="<?=set_value('city_'.$user->id, $user->city)?>" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label><?=lang('Konto aktywne') ?>:</label>
					<label class="radio-inline"><input name="status_<?=$user->id?>" type="radio" value="1" <?=set_radio('status_'.$user->id, 1, !!$user->status)?>> <?=lang('tak') ?></label>
					<label class="radio-inline"><input name="status_<?=$user->id?>" type="radio" value="0" <?=set_radio('status_'.$user->id, 0, !$user->status)?>> <?=lang('nie') ?></label>
				</div>
				<div class="form-group">
					<?=form_field_tooltip(lang('Jeśli pole będzie zaznaczone, nowe, losowe hasło zostanie wysłane na e-mail użytkownika.'))?>
					<label class="checkbox-inline"><input name="pass_reset_<?=$user->id?>" type="checkbox" value="1" <?=set_radio('pass_reset_'.$user->id, 1)?>> <?=lang('resetuj hasło') ?></label>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?
	}
	echo form_close();
}
?>
