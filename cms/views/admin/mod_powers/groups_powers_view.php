<?=form_open(current_url().'/check/1', array('class' => 'row'));?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=$group->name?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<?=(!empty($group->description) ? "<p>{$group->description}</p>" : '')?>
				
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th style="width:25px;"><?=lang('Nr') ?></th>
							<th><?=lang('Moduł') ?></th>
							<th><?=lang('Opis')?></th>
							<th style="width: 25px;"><input class="checkall" type="checkbox"></th>
						</tr>
					</thead>
					<tbody>
					<?
					$i = 1;
					foreach($modules as $mod) {
						$mod_dir = preg_replace('#^mod_(.*?)\/.*#', '\1', $mod->mod_dir);
						echo '<tr class="click-check">';
						echo '<td>'.$i.'</td>';
						echo '<td>'.$mod_dir.' / '.$mod->mod.'</td>';
						echo '<td>'.(!empty($mod->description) ? $mod->description : $mod->title).'</td>';
						
						$add = '';
						if(is_array($rights) && count($rights) > 0){
							foreach($rights as $right) {
								if($mod->id == $right->id_m){ $add = 'checked';} 
							}
						}
						echo '<td><input type="checkbox" class="check" name="rights[]" '.$add.' value="'.$mod->id.'" /></td>';
						
						$i++;
						echo '</tr>';
					}
					?>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?=form_close()?>