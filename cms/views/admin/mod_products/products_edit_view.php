<?=form_open_multipart(current_url().'/check/1', array('id' => 'edit'));?>
<input type="submit" value="<?=lang('Zapisz') ?>" class="btn btn-primary btn-sm btn-top-right" style="width:150px;">

<div class="row">
	<div class="col-md-7">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Dane podstawowe')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa')?></label>
					<input type="text" name="title" value="<?=set_value('title', $prod->title)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Skrócona nazwa')?></label>
					<?=form_field_tooltip(lang('Skrócona nazwa będzie widoczna tylko w menu, natomiast dłuższa wersja jako nagłówek strony. Pole opcjonalnie.'))?>
					<input type="text" name="short_title" value="<?=set_value('short_title', $prod->short_title)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Zewnętrzny odnośnik')?></label>
					<?=form_field_tooltip(lang('Link do którego ma kierować kategoria z menu, zamiast wyświetlania jej treści (np.: http://artneo.pl). Pole opcjonalnie.'))?>
					<input type="text" name="url" value="<?=set_value('url', $prod->url)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Niestandardowy adres URL')?></label>
					<?=form_field_tooltip(lang('Adres, pod którym będzie dostępna ta kategoria. Podawać bez domeny na początku. Pole opcjonalnie.'))?>
					<input type="text" name="user_url" value="<?=set_value('user_url', $prod->user_url)?>" class="form-control" />
				</div>
				<div class="form-group">
					<label><?=lang('Cena')?></label>
					<input type="text" name="price" value="<?=set_value('price', $prod->price)?>" class="form-control" />
				</div>
				
				<div class="form-group">
					<label><?=lang('Meta-tagi (SEO)')?></label>
					<div class="row">
						<div class="col-sm-4 form-control-static"><?=lang('Tytuł (title)')?>:</div>
						<div class="col-sm-8">
							<input type="text" name="meta_title" value="<?=set_value('meta_title', $prod->meta_title)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 form-control-static"><?=lang('Opis (description)')?>:</div>
						<div class="col-sm-8">
							<input type="text" name="meta_description" value="<?=set_value('meta_description', $prod->meta_description)?>" class="form-control">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 form-control-static"><?=lang('Słowa kluczowe (keywords)')?>:</div>
						<div class="col-sm-8">
							<input type="text" name="meta_keywords" value="<?=set_value('meta_keywords', $prod->meta_keywords)?>" class="form-control">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Szczegóły')?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Kategoria')?>:</label>
					<select class="select_simple" name="id_cat">
					<? if(is_array($categories)) {
						foreach($categories as $row){
							$padd = repeater('&#x2001;', $row->tree);
			
							if($row->id == $prod->id_cat) $add = 'disabled="disabled"';
							else $add = '';
			
							if($row->tree < 3) echo '<option '.set_select('id_tree', $row->id, $row->id == $prod->id_cat).' '.$add.' value="'.$row->id.'">'.$padd.' '.$row->title.'</option>';
						}
					} ?>
					</select>
				</div>
				
				<div class="form-group form-inline">
					<label><?=lang('Autor')?>:</label>
					<p class="form-control-static"><?=(isset($prod->author->login) ? $prod->author->login." (".$prod->author->name.")" : "")?></p>
				</div>
				<div class="form-group form-inline">
					<label><?=lang('Data modyfikacji')?>:</label>
					<p class="form-control-static"><?=date("d-m-Y H:i", strtotime($prod->date_modified))?></p>
				</div>
				
				<div class="form-group">
					<label><?=lang('Opublikowany')?>:</label>
					<div class="row">
						<label class="col-sm-3"><?=lang('cały produkt')?></label>
						<div class="col-sm-9">
							<label class="radio-inline"><input name="pub_all" type="radio" value="1" <?=set_radio('pub_all', 1, !!$prod->pub_all)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="pub_all" type="radio" value="0" <?=set_radio('pub_all', 0, !$prod->pub_all)?>> <?=lang('nie')?></label>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-3"><?=lang('język')?> <?=lang_flag($prod->lang)?></label>
						<div class="col-sm-9">
							<label class="radio-inline"><input name="pub" type="radio" value="1" <?=set_radio('pub', 1, !!$prod->pub)?>> <?=lang('tak')?></label>
							<label class="radio-inline"><input name="pub" type="radio" value="0" <?=set_radio('pub', 0, !$prod->pub)?>> <?=lang('nie')?></label>
						</div>
					</div>
				</div>
				<?
					if(!empty($prod->url)) $link = $prod->url;
					elseif(!empty($prod->user_url)) $link = $prod->user_url;
					else $link = config_item('px_product').'/'.$prod->id_art.'/'.iso_clear($prod->title);
				?>
				<div class="form-group">
					<label><?=lang('Link do podstrony')?>:</label>
					<input type="text" value="<?=(!empty($prod->url) && preg_match('#^http(s)?:\/\/#i', $prod->url) ? $link : site_url($link))?>" class="form-control">
				</div>
				<div class="form-group">
					<label><?=lang('Link do używania w panelu')?>:</label>
					<input type="text" value="<?=$link?>" class="form-control">
				</div>
				<div class="form-group">
					<label><?=lang('Sposób otwierania')?>:</label>
					<select name="target" class="form-control">
						<option value=""><?=lang('normalnie')?></option>
						<option <?=set_select('target', '_blank', $prod->target == '_blank')?> value="_blank"><?=lang('nowe okno')?></option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="box box-success">
	<div class="box-body">
		<div class="form-group">
			<label><?=lang('Wstęp')?>:</label>
			<?=ckeditor('admission', set_value('admission', $prod->admission, FALSE), '100%', 'Simple', 120)?>
		</div>
		<div class="form-group">
			<label><?=lang('Treść')?>:</label>
			<?=ckeditor('prod_content', set_value('prod_content', $prod->content, FALSE), '100%')?>
		</div>
	</div>
</div>

<input type="hidden" value="1" name="admission_on">
<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">

<?=form_close(); ?>
