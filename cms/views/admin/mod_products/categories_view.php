<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj kategorię') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<select class="select_simple form-control" name="id_tree">
				<?
					echo '<option value="0">'.lang('główna pozycja').'</option>';
					if(is_array($all)) {
						foreach($all as $row){
							$padd = repeater('&#x2001;', $row->tree);
							if($row->tree < 3) echo '<option value="'.$row->id.'">'.$padd.' '.$row->title.'</option>';
						}
					}
				?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="title" value="<?=set_value('title')?>" class="form-control">
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<?
if($all !== FALSE) {
	echo form_open();
?>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="width:25px;">Nr</th>
			<th>Tytuł</th>
			<th style="width:40px;">Kolejność</th>
			<th style="width:160px;">Data modyfikacji</th>
			<th style="width:220px;">Działania</th>
			<th style="width:25px;"><input class="checkall" type="checkbox"></th>
		</tr>
	</thead>
	<tbody class="sortable">
		<? $i=1 ?>
		<? foreach($all as $row): ?>
			<tr>
				<td><?=$i?></td>
				<td style="<? if($row->tree != 0) echo 'padding-left:'.($row->tree * 30).'px';?>">
				<?
					echo anchor("{$this->mod_url}/edit/lang/".config_item('main_lang')."/id/{$row->id}", $row->title);
					if(!empty($row->url)) echo ' <i class="fa fa-external-link" title="'.lang('Prowadzi do').': '.$row->url.'"></i>';
					echo lang_versions($lang, $row, $this->mod_url.'/edit/');
				?>
				</td>
				<td class="text-center"><span class="fa-arrows"></span><input class="currentposition" type="hidden" name="position[<?=$row->id?>]" value="<?=$row->position?>"></td>
				<td class="text-center"><?=date('d.m.Y H:i:s', strtotime($row->date_modified))?></td>
				<td class="text-center nc-options">
					<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
					<? if($row->pub==0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/0", lang('Odpublikuj'));?>
					<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
				</td>
				<td class="text-center"><input type="checkbox"  value="<?=$row->id?>" name="check[]"></td>
			</tr>
		<? $i++ ?>
		<? endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3">
				<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>">
			</td>
			<td colspan="3" class="nc-options">
				<?=lang('Zaznaczone')?>:
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
			</td>
		</tr>
	</tfoot>
</table>
<? 
	echo form_close();
} else {
	echo msg(lang('Nie znaleziono żadnych kategorii produktowych.'), 1);
}
?>