<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj produkt') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Kategoria') ?></label>
				<select class="select_simple form-control" name="id_cat">
				<? if(is_array($categories)) {
					foreach($categories as $row){
						$padd = repeater('&#x2001;', $row->tree);
						if($row->tree < 3) echo '<option value="'.$row->id.'">'.$padd.' '.$row->title.'</option>';
					}
				} ?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="title" value="<?=set_value('title')?>" class="form-control">
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<?
if(!empty($all)) {
	echo form_open();
?>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="width:68px;">Nr kategorii</th>
			<th style="width:68px;">Nr produktu</th>
			<th>Tytuł</th>
			<th style="width:40px;">Kolejność</th>
			<th style="width:150px;">Data modyfikacji</th>
			<th style="width:250px;">Działania</th>
			<th style="width:25px;"><input class="checkall" type="checkbox"></th>
		</tr>
	</thead>
	<tbody>
	<?
		$i_cat = 1;
		foreach($all as $row) {
			$add_padding = 30;
			if($row->tree != 0) {
				$add_padding += $row->tree * 30;
			}
	?>
			<tr>
				<td class="text-center"><?=$i_cat?></td>
				<td></td>
				<td colspan="7" style="<? if($row->tree != 0) echo 'padding-left:'.($row->tree * 30).'px';?>">
					<b><?=$row->title?></b>
				</td>
			</tr>
			
			<? if(isset($row->products) && !empty($row->products)): ?>
				<? $i=1 ?>
				<? foreach($row->products as $prod): ?>
					<tr>
						<td></td>
						<td class="text-center"><?=$i?></td>
						<td style="<? if($add_padding !=0) echo 'padding-left:'.$add_padding.'px';?>">
						<?
							echo anchor("{$this->mod_url}/edit/lang/".config_item('main_lang')."/id/{$prod->id}", $prod->title);
							if(!empty($prod->url)) echo ' <i class="fa fa-external-link" title="'.lang('Prowadzi do').': '.$prod->url.'"></i>';
							echo lang_versions($lang, $prod, $this->mod_url.'/edit/');
						?>
						</td>
						<td class="padding-xs"><input type="text" class="form-control input-sm text-center" value="<?=$prod->position?>" name="position[<?=$prod->id?>]"></td>
						<td class="text-center" style="width:130px;"><?=date('d.m.Y H:i:s', strtotime($prod->date_modified))?></td>
						<td class="text-center nc-options">
							<?=anchor("{$this->mod_url}/edit/id/{$prod->id}", lang('Edytuj'))?>
							<?=anchor("{$this->mod_url}/images/id/{$prod->id}", lang('Galeria'))?>
							<? if($prod->pub==0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$prod->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$prod->id}/set/0", lang('Odpublikuj'));?>
							<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$prod->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
						</td>
						<td class="text-center"><input type="checkbox"  value="<?=$prod->id?>" name="check[]"></td>
					</tr>
				<? $i++ ?>
				<? endforeach; ?>
			<? endif; ?>
	<?
			$i_cat++;
		}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">
				<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>">
			</td>
			<td colspan="3" class="nc-options">
				<?=lang('Zaznaczone')?>:
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
			</td>
		</tr>
	</tfoot>
</table>
<?
	echo form_close();
} else {
	echo msg(lang('Nie znaleziono żadnych produktów.'), 1);
}
?>