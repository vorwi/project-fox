<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj formularz')?></h3>
		<div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add');?>
			<div class="form-group">
				<label><?=lang('Wyświetlanie na podstronie')?></label>
				<select name="id_art" class="select_simple form-control">
				<? foreach($art['categories'] as $cat){
			 		echo '<optgroup label="'.$cat->name.'">';
					if(is_array($cat->articles)) {
						foreach($cat->articles as $row){
							$padd = repeater('&#x2001;', $row->tree);
							
							echo '<option value="'.$row->id.'" '.set_select('id_art', $row->id).'>'.$padd.' '.$row->title.'</option>';
						}
					}
					echo '</optgroup>';
				} ?>
				</select>
			</div>
			<div class="form-group" <?=(count($lang) == 1 ? 'style="display: none;"' : '')?>>
				<label><?=lang('Język')?></label>
				<select name="lang" class="select_simple form-control">
				<? foreach($lang as $row){
					echo '<option '.set_select('lang', $row->short).' value="'.$row->short.'">'.$row->lang.'</option>';
				} ?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Adres e-mail')?></label>
				<?=form_field_tooltip(lang('Adres na który będą wysyłane wiadomości z formularza.'))?>
				<input type="text" name="email" class="form-control" value="<?=set_value('email')?>">
			</div>
			<div class="form-group">
				<label><?=lang('Nazwa wyświetlana')?></label>
				<?=form_field_tooltip(lang('Tekst wprowadzony w tym polu, będzie wyświetlany zamiast powyższego e-maila. Pole opcjonalnie.'))?>
				<input type="text" name="name" class="form-control" value="<?=set_value('name')?>">
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
			<?=form_hidden('id_cat', $cat->id)?>
		<?=form_close()?>
	</div>
</div>

<?
if($all !== FALSE):
	echo form_open();
?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr')?></th>
				<th><?=lang('Email')?></th>
				<th><?=lang('Artykuł')?></th>
				<? if(count($lang) > 1) {?>
					<th style="width:30px;"><?=lang('Język')?></th>
				<? }?>
				<th style="width:300px;"><?=lang('Działania')?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox"></th>
			</tr>
		</thead>
		<tbody>
		<?
		$i=1;
		foreach($all as  $row):
		?>
			<tr>
				<td><?=$i?></td>
				<td><?=$row->email.(!empty($row->name) ? " ({$row->name})" : "")?></td>
				<td><?=$row->title?></td>
				<? if(count($lang)>1) {?>
					<td><?=lang_flag($row->lang)?></td>
				<? }?>
				<td class="text-center nc-options">
					<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
					<? if($row->pub==0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/0", lang('Odpublikuj'));?>
					<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?')?>"><?=lang('Usuń')?></a>
				</td>
				<td class="text-center" style="width:25px;"><input type="checkbox" value="<?=$row->id?>" name="check[]" /></td>
			</tr>
		<?
		$i++;
		endforeach;
		?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="<?=(count($lang) > 1 ? 6 : 5)?>" class="nc-options">
					<?=lang('Zaznaczone')?>:
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/edit")?>"><?=lang('Edytuj')?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?')?>"><?=lang('Usuń')?></a>
				</td>
			</tr>
		</tfoot>
	</table>
<?
	echo form_close();
else:
	echo msg(lang('Nie znaleziono żadnych formularzy.'),1);
endif;
?>