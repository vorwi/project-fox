<?
if($all !== FALSE ) {
	echo form_open(current_url().'/check/1', array('class' => 'row'));
	
	foreach($all as $form) {
		echo form_hidden('check[]', $form->id);
?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Formularz')?> #<?=$form->id?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="form-group" <?=(count($lang) == 1 ? 'style="display: none;"' : '')?>>
					<label><?=lang('Język')?>:</label>
					<select name="lang_<?=$form->id?>" class="select_simple form-control">
					<? foreach($lang as $row){
						echo '<option '.set_select('lang_'.$row->id, $row->short, ($row->short == $form->lang->short)).' value="'.$row->short.'">'.$row->lang.'</option>';
					} ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('Wyświetlanie na podstronie')?></label>
					<select name="id_art_<?=$form->id?>" class="select_simple form-control">
					<? foreach($art['categories'] as $cat){
				 		echo '<optgroup label="'.$cat->name.'">';
						if(is_array($cat->articles)) {
							foreach($cat->articles as $row){
								$padd = repeater('&#x2001;', $row->tree);
								
								echo '<option value="'.$row->id.'" '.set_select('id_art_'.$row->id, $row->id, ($row->id == $form->id_art)).'>'.$padd.' '.$row->title.'</option>';
							}
						}
						echo '</optgroup>';
					} ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('Adres e-mail')?>:</label>
					<?=form_field_tooltip(lang('Adres na który będą wysyłane wiadomości z formularza.'))?>
					<input type="text" name="email_<?=$form->id?>" class="form-control" value="<?=set_value('email_'.$form->id, $form->email)?>">
				</div>
				<div class="form-group">
					<label><?=lang('Nazwa wyświetlana')?>:</label>
					<?=form_field_tooltip(lang('Tekst wprowadzony w tym polu, będzie wyświetlany zamiast powyższego e-maila. Pole opcjonalnie.'))?>
					<input type="text" name="name_<?=$form->id?>" class="form-control" value="<?=set_value('name_'.$form->id, $form->name)?>">
				</div>
				<div class="form-group">
					<label><?=lang('Opublikowany')?>:</label>
					<label class="radio-inline"><input name="pub_<?=$form->id?>" type="radio" value="1" <?=set_radio('pub_'.$form->id, 1, !!$form->pub)?>> <?=lang('tak')?></label>
					<label class="radio-inline"><input name="pub_<?=$form->id?>" type="radio" value="0" <?=set_radio('pub_'.$form->id, 0, !$form->pub)?>> <?=lang('nie')?></label>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?
	}
	echo form_close();
} 
?>