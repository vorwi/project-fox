<div class="row<?=(count($this->languages) > 1 ? '' : ' hidden')?>">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group form-inline">
			<label><?=lang('Wybierz język')?>:</label>
			<select name="lang" class="form-control" onchange="location.href='<?=site_url("{$this->mod_url}/index/lang")?>/'+this.value">
			<? foreach($this->languages as $row){
				echo '<option value="'.$row->short.'" '.(($curr_lang == $row->short) ? 'selected="selected"' : '').'>'.$row->lang.'</option>';
			} ?>
			</select>
		</div>
	</div>
</div>

<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj projekt') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Tytuł') ?></label>
				<input type="text" name="title" value="<?=set_value('title')?>" class="form-control">
			</div>
			<div class="form-group" <?=(count($lang) > 1 ? '' : 'style="display: none"')?>>
				<label><?=lang('Język') ?></label>
				<select name="lang" class="select_simple form-control">
					<? foreach($lang as $l){
						echo '<option '.set_select('lang', $l->short).' value="'.$l->short.'">'.$l->lang.'</option>';
					} ?>
				</select>
			</div>
			
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>




<? if($projects!==FALSE) {?>
<?=form_open()?>
<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th style="width:25px;">Nr</th>
			<th>Tytuł</th>
			<? if(count($lang)>1) {?><th style="width:30px;"><?=lang('Język')?></th><? }?>
			<th style="width:250px;">Działania</th>
			<th style="width:25px;"><input class="checkall" id="checkboxall" type="checkbox"  /></th>
		</tr>
	</thead>
	<tbody>
		<? $i=1 ?>
		<? foreach($projects as $row) { ?>
		<tr>
			<td><?=$i?></td>
			<td><?=anchor("{$this->mod_url}/edit/id/{$row->id}", $row->title)?></td>
			<?php if(count($lang) > 1) { echo '<td class="text-center">'.lang_flag($row->lang).'</td>'; }?>
			<td class="text-center nc-options">
				<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
				<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
	 		</td>
			<td><input type="checkbox"  value="<?=$row->id?>" name="check[]" /></td>
		</tr>
		<? $i++ ?>	
		<? } ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="<?=count($lang) == 1 ? '2' : '3'?>"></td>
			<td colspan="2" class="nc-options">
				<?=lang('Zaznaczone')?>:
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?')?>"><?=lang('Usuń')?></a>
			</td>
		</tr>		
	</tfoot>
</table><?=form_close()?>

<? } else { ?>
	<?=msg(lang('Nie znaleziono żadnych projektów.'),1)?>
<? } ?>


