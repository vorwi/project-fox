
<?
if($newsletters && count($newsletters)>0){
	echo $pagination;
	echo form_open();
	?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr')?></th>
				<th><?=lang('Tytuł')?></th>
				<th style="width:150px;"><?=lang('Data wysłania')?></th>
				<th style="width:150px;"><?=lang('Ile wysłanych')?></th>
				<th style="width:250px;"><?=lang('Działania')?></th>
				<th style="width:25px;"><input class="checkall" id="checkboxall" type="checkbox"  /></th>
			</tr>
		</thead>
	<tbody>
	<?
	$i=1;
	foreach($newsletters as  $row):
	?>
		<tr>
			<td><b><?=$i?></b></td>
			<td><a href="<?=site_url("{$this->mod_url}/edit/id/{$row->id}")?>"><?=$row->title?></a></td>
			<td class="text-center"><?=$row->date_sent?></td>
			<td class="text-center"><?=$row->sent_ok?></td>
			<td class="text-center nc-options">
				<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
			</td>
			<td><input type="checkbox"  value="<?=$row->id?>" name="check[]" /></td>
		</tr>
	<?
	$i++;
	endforeach;
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2"></td>
			<td colspan="4" class="nc-options">
				<?=lang('Zaznaczone')?>: 
				<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
			</td>
		</tr>
	</tfoot>
	</table>
	<?=form_close();?>
	<?
	echo $pagination;
}
else echo msg(lang('Brak wysłanych newsletterów w bazie.'),1);
?>