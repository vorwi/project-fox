<?
$msg_e = (array)$this->session->flashdata('error');
if(!empty($msg_e[0])){

	$str = '<b>'.lang('Nie dodane adresy:').'</b><br />';
	foreach($msg_e[0] as $row) $str .= $row.', ';

	$str = substr($str, 0, -2);

	echo msg($str,$msg_e[1]);
}
?>
<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj adresy') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Adresy email oddzielone przecinkami lub średnikami') ?></label>
				<textarea name="add" class="form-control" style="height: 75px;"><?=set_value('add')?></textarea>
			</div>
			<?php if($groups) {?>
			<fieldset class="form-group">
				<legend class="col-form-legend"><?=lang('Przypisz do grup')?></legend>				
				<?php foreach($groups as $group) {?>
				<div class="form-check">
					<label class="form-check-label">
						<input class="form-check-input" type="checkbox" name="id_groups[]" value="<?=$group->id?>" <?=set_checkbox('id_groups[]', $group->id)?> />
						<?=$group->name?>
					</label>
				</div>
				<?php }?>			
			</fieldset>
			<?php }?>
			
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>
<?


if($emails && count($emails)>0){
echo $pagination;
	echo form_open();
	?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr')?></th>
				<th><?=lang('Email')?></th>
				<?php if($groups) {?>
				<?php foreach($groups as $group) {?>
				<th style="width:100px;"><?=$group->name?> <input type="checkbox" class="checkgroup" data-rel-group-id="<?=$group->id?>" /></th>
				<?php }?>
				<?php }?>
				<th style="width:250px;"><?=lang('Działania')?></th>
				<th style="width:25px;"><input class="checkall" id="checkboxall" type="checkbox"  /></th>
			</tr>
		</thead>
	<tbody>
	<?
	$i = $start+1;

	foreach($emails as  $row):
	?>
		<tr>
			<td><b><?=$i?></b></td>
			<td><?=$row->email?></td>
			<?php if($groups) {?>
			<?php foreach($groups as $group) {?>
			<td class="text-center">
				<input type="checkbox" name="id_group[<?=$row->id?>][]" value="<?=$group->id?>"<?=in_array($group->id,$row->groups) ? ' checked' : ''?> />
			</td>
			<?php }?>
			<?php }?>
			<td class="text-center nc-options">
				<?
				if($row->confirm==0) echo '<a href="'.site_url("{$this->mod_url}/confirm/id/{$row->id}").'">'.lang('Potwierdź').'</a>';
				?>
				<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
			</td>
			<td><input type="checkbox"  value="<?=$row->id?>" name="check[]" /></td>
		</tr>
	<?
	$i++;
	endforeach;
	?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="2"></td>
		<?php if($groups) {?>
		<td colspan="<?=count($groups)?>">
			<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/assignGroups")?>" value="<?=lang('Zapisz grupy')?>" />
		</td>
		<?php }?>
		<td colspan="2" class="nc-options">
			<?=lang('Zaznaczone')?>:
			<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/confirm")?>" data-confirm="<?=lang('Czy na pewno chcesz potwierdzić zaznaczone wpisy?') ?>"><?=lang('Potwierdź') ?></a>
			<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
		</td>
	</tr>
	</tfoot>
	</table>
	<?=form_close();?>
	<?
	echo $pagination;
}
else echo msg(lang('Brak adresów email w bazie.'),1);
?>