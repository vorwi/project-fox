
<?=form_open(current_url().'/check/1', array('id' => 'edit'));?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label><?=lang('Tytuł')?></label>
			<input type="text" name="title" value="<?=$show->title?>" class="form-control" />
		</div>
		<div class="form-group">
			<label><?=lang('Treść newslettera')?>:</label>
			<?=ckeditor('newsletter', htmlspecialchars_decode($show->content), '100%', 'Newsletter', 300)?>
		</div>
		<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
	</div>
</div>


<?=form_close();?>
