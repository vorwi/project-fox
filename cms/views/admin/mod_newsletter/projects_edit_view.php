<?=form_open($this->mod_url.'/edit/id/'.$project->id.'/check/1');?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label><?=lang('Tytuł')?></label>
			<input type="text" name="title" value="<?=set_value('title',$project->title)?>" class="form-control" />
		</div>
		<div class="form-group" <?=(count($lang) > 1 ? '' : 'style="display: none"')?>>
			<label><?=lang('Język') ?></label>
			<select name="lang" class="select_simple form-control">
				<? foreach($lang as $l){
					echo '<option '.set_select('lang', $l->short, ($l->short == $project->lang->short)).' value="'.$l->short.'">'.$l->lang.'</option>';
				} ?>
			</select>
		</div>
		<div class="form-group">
			<label><?=lang('Podgląd newslettera')?>:</label>
			<iframe src="about:blank" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 embed-responsive-item" style="height:400px" id="newsletter_preview"></iframe>
			<textarea id="newsletter_preview_html" name="mail" class="form-control hidden"><?=$newsletter?></textarea>
		</div>
		<div class="form-group">
			<label><?=lang('Treść')?>:</label>
			<?=ckeditor('newsletter', htmlspecialchars_decode(set_value('newsletter', $newsletter_content)), '100%', 'Newsletter', 300)?>
		</div>
		<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
	</div>
</div>
<?=form_close(); ?>
