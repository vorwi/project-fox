<?
$msg_o = (array)$this->session->flashdata('ok');
if(!empty($msg_o[0])){

	$str = '<b>'.lang('Newsletter został wysłany.').'</b><br />'.lang('Adresaci:').'<br />';
	foreach($msg_o[0] as $row) $str .= $row.', ';

	$str = substr($str, 0, -2);
	
	echo msg($str,$msg_o[1]);
}

$msg_e = (array)$this->session->flashdata('error');
if(!empty($msg_e[0])){

	$str = '<b>'.lang('Newsletter został wysłany.').'</b><br />'.lang('Adresaci:').'<br />';
	foreach($msg_e[0] as $row) $str .= $row.', ';

	$str = substr($str, 0, -2);
	
	echo msg($str,$msg_e[1]);
}
?>
<?php if($projects) {?>
<?=form_open()?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group form-inline">
			<label><?=lang('Wczytaj projekt')?>:</label>
			<select name="project" class="form-control">
			<? foreach($projects as $row){
				echo '<option value="'.$row->id.'">'.$row->title.' ('.$row->lang.')</option>';
			} ?>
			</select>
		</div>
		<input type="submit" value="<?=lang('Wczytaj') ?>" class="btn btn-primary" />
	</div>
</div>		
<?=form_close()?>
<?php }?>
<?=form_open($this->mod_url.'/index/check/1',array('id'=>'newsletterform'));?>
<?php
$link = '/'.config_item('px_newsletter').'/'.date("d/m/Y").'/'.$numer.'.html';
					
$tab = array(
	'{%adres%}' => '<a href="http://'.$_SERVER['SERVER_NAME'].'>http://'.$_SERVER['SERVER_NAME'].'</a>',
	'{%numer%}' => date("d/m/Y").', nr '.$numer,
	'{%link%}' => '<a href="http://'.$_SERVER['SERVER_NAME'].$link.'">http://'.$_SERVER['SERVER_NAME'].$link.'</a>'
);
			
$title = 'Newsletter '.date("d/m/Y").', nr '.$numer;
			
$newsletter = str_replace(array_keys($tab), array_values($tab), $newsletter);						
?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="form-group form-inline<?=(count($this->languages) > 1 ? '' : ' hidden')?>">
			<label><?=lang('Wybierz język')?>:</label>
			<select name="lang" class="form-control" onchange="location.href='<?=site_url("{$this->mod_url}/index/lang")?>/'+this.value">
			<? foreach($this->languages as $row){
				echo '<option value="'.$row->short.'" '.(($curr_lang == $row->short) ? 'selected="selected"' : '').'>'.$row->lang.'</option>';
			} ?>
			</select>
		</div>
		<div class="form-group">
			<label><?=lang('Tytuł')?></label>
			<input type="text" name="title" value="<?=set_value('title',$title)?>" class="form-control" />
			<input type="hidden" value="<?=$link;?>" name="link" />
		</div>
		<div class="form-group">
			<label><?=lang('Podgląd newslettera')?>:</label>
			<textarea id="newsletter_preview_html" name="mail" class="form-control hidden"><?=$newsletter?></textarea>
			<iframe src="about:blank" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 embed-responsive-item" style="height:400px" id="newsletter_preview"></iframe>
		</div>
		<div class="form-group">
			<label><?=lang('Treść')?>:</label>
			<?=ckeditor('newsletter', htmlspecialchars_decode(set_value('newsletter', $newsletter_content)), '100%', 'Newsletter', 300)?>
		</div>
		<fieldset class="form-group">
			<legend class="col-form-legend"><?=lang('Wybierz grupę')?></legend>
			<div class="form-check">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="sendtoall" value="1" <?=set_checkbox('sendtoall', '1')?> />
				   	<?=lang('Wszyscy')?>
			 	</label>
			</div>
			<?php if($groups) {?>
			<?php foreach($groups as $group) {?>
			<div class="form-check">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="sendtogroups[]" value="<?=$group->id?>" <?=set_checkbox('sendtogroups[]', $group->id)?> />
					<?=$group->name?> (<?=$group->num_members?> <?=lang('członków')?>)
				</label>
			</div>
			<?php }?>
			<?php }?>
		</fieldset>
		<?php if($emails) {?>
		<fieldset class="form-group" style="max-height:400px;overflow:auto;">
			<legend class="col-form-legend"><?=lang('Lub wybierz adresy')?></legend>			
			<?php foreach($emails as $row) {?>
			<div class="form-check">
				<label class="form-check-label">
					<input class="form-check-input" type="checkbox" name="emails[]" value="<?=$row->email?>" <?=set_checkbox('emails[]', $row->email)?> />
					<?=$row->email?>
				</label>
			</div>
			<?php }?>			
		</fieldset>
		<?php }?>
		<input type="submit" value="<?=lang('Wyślij')?>" class="btn btn-primary">
	</div>
</div>
<?=form_close()?>