
<? if($all!==FALSE ):?>
<? 
	$tab = $this->uri->uri_to_assoc();
	if(@is_numeric($tab['page'])) $add = $tab['page'];
	else $add = 1 
?>  
<?=form_open($this->mod_url.'/edit/check/1');?>
<? foreach($all as $row):?>
	
	<?=form_hidden('check[]', $row->id)?>
	
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Grupa')?> <?=$row->name?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa')?>:</label>
					<input type="text" name="name_<?=$row->id?>" class="form-control" value="<?=set_value('name_'.$row->id, $row->name)?>">
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>

<? endforeach ?>
<?=form_close()?>
<? endif ?>
