<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj grupę') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Nazwa grupy') ?></label>
				<input type="text" class="form-control" name="name" value="<?=set_value('name')?>" />
			</div>			
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>
<? 

if($groups && count($groups)>0){
echo $pagination;
	echo form_open();
	?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr')?></th>
				<th><?=lang('Nazwa')?></th>
				<th style="width:250px;"><?=lang('Działania')?></th>
				<th style="width:25px;"><input class="checkall" id="checkboxall" type="checkbox"  /></th>
			</tr>
		</thead>
	<tbody>
	<? 
	$i = $start+1;

	foreach($groups as  $row):
	?>
		<tr>
			<td style="width:25px;"><b><?=$i?></b></td>
			<td><?=$row->name?></td>
			<td class="text-center nc-options">
				<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
				<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/id/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
			</td>
			<td><input type="checkbox"  value="<?=$row->id?>" name="check[]" /></td>
		</tr>
	<? 
	$i++;
	endforeach;
	?>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td colspan="3" class="nc-options">
			<?=lang('Zaznaczone')?>:
			<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/edit")?>"><?=lang('Edytuj')?></a>
			<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?')?>"><?=lang('Usuń')?></a>
		</td>
	</tr>
	</tfoot>
	</table>
	<?=form_close();?>
	<?
	echo $pagination;
}
else echo msg(lang('Brak grup adresatów.'),1);
?>