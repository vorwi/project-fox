<?
if($all !== FALSE ) {
	$tab = $this->uri->uri_to_assoc();
	if(@is_numeric($tab['page'])) $add = $tab['page'];
	else $add = 1;
	
	echo form_open($this->mod_url.'/edit/page/'.$add.'/check/1', array('class' => 'row'));
	
	foreach($all as $gal) {
		echo form_hidden('check[]', $gal->id);
?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Galeria')?> #<?=$gal->id?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa') ?></label>
					<input type="text" name="name_<?=$gal->id?>" value="<?=set_value('name_'.$gal->id, $gal->name)?>" class="form-control">
				</div>
				<div class="form-group" <?=(count($lang) == 1 ? 'style="display: none;"' : '')?>>
					<label><?=lang('Język')?>:</label>
					<select name="lang_<?=$gal->id?>" class="select_simple form-control">
					<? foreach($lang as $row){
						echo '<option '.set_select('lang_'.$row->id, $row->short, ($row->short == $gal->lang)).' value="'.$row->short.'">'.$row->lang.'</option>';
					} ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('Miejsce wyświetlania') ?></label>
					<select name="id_art_<?=$gal->id?>" class="select_simple form-control">
				 		<option value="0"><?=lang('wybierz artykuł')?></option>
					<? foreach($art['categories'] as $cat){
						if(!empty($cat->articles)) {
					 		echo '<optgroup label="'.$cat->name.'">';
							foreach($cat->articles as $row){
								$padd = repeater('&#x2001;', $row->tree);
				
								echo '<option value="'.$row->id.'" '.set_select('id_art_'.$row->id, $row->id, ($row->id == $gal->id_art)).'>'.$padd.' '.$row->title.'</option>';
							}
					 		echo '</optgroup>';
						}
					}
					?>
					</select>
					<?=lang('lub')?>
				 	<select name="id_news_<?=$gal->id?>" class="select_simple form-control">
				 		<option value="0"><?=lang('wybierz aktualność')?></option>
					<? foreach($news as $cat){
						if(!empty($cat->news)) {
							$prev_path = '';
							$i = 0;
							foreach($cat->news as $row){
				
								$curr_path = '';
								if(!empty($row->art_grandparent_title)) $curr_path .= $row->art_grandparent_title.' / ';
								if(!empty($row->art_parent_title)) $curr_path .= $row->art_parent_title.' / ';
								$curr_path .= $row->art_title;
				
								if($curr_path != $prev_path && !empty($prev_path) || $i == count($cat->news)) echo '</optgroup>';
								if($curr_path != $prev_path) echo '<optgroup label="'.$curr_path.'">';
				
								echo '<option value="'.$row->id.'" '.set_select('id_news_'.$gal->id, $row->id, ($row->id == $gal->id_news)).'>'.$row->title.'</option>';
				
								$prev_path = $curr_path;
								$i++;
							}
						}
					}
					?>
					</select>
				</div>
		
				<div class="form-group">
					<label><?=lang('Wstęp') ?></label>
					<?=form_field_tooltip(lang('Krótki opis, który będzie wyświetlany przed wejściem do galerii. Pole opcjonalnie.'))?>
					<textarea name="intro_<?=$gal->id?>" style="height: 75px;" class="form-control"><?=set_value('intro_'.$gal->id, $gal->intro)?></textarea><br />
				</div>
				<div class="form-group">
					<label><?=lang('Opis') ?></label>
					<?=ckeditor('desc_'.$gal->id, set_value('desc_'.$gal->id, $gal->desc, FALSE), '', 'Simple', '240px')?>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?
	}
	echo form_close();
} 
?>
