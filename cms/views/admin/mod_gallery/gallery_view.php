<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj galerię') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<?=form_open($this->mod_url.'/add', array('id' => 'add'));?>
			<div class="form-group">
				<label><?=lang('Nazwa') ?></label>
				<input type="text" name="name" value="<?=set_value('name')?>" class="form-control">
			</div>
			<div class="form-group" <?=(count($lang) > 1 ? '' : 'style="display: none"')?>>
				<label><?=lang('Język') ?></label>
				<select name="lang" class="select_simple form-control">
					<? foreach($lang as $l){
						echo '<option '.set_select('lang', $l->short).' value="'.$l->short.'">'.$l->lang.'</option>';
					} ?>
				</select>
			</div>
			<div class="form-group">
				<label><?=lang('Miejsce wyświetlania') ?></label>
				
				<select name="id_art" class="select_simple form-control">
			 		<option value="0"><?=lang('wybierz artykuł')?></option>
				<? foreach($art['categories'] as $cat){
					if(!empty($cat->articles)) {
				 		echo '<optgroup label="'.$cat->name.'">';
						foreach($cat->articles as $row){
							$padd = repeater('&#x2001;', $row->tree);
			
							echo '<option value="'.$row->id.'" '.set_select('id_art', $row->id).'>'.$padd.' '.$row->title.'</option>';
						}
				 		echo '</optgroup>';
					}
				}
				?>
				</select>
				<?=lang('lub')?>
			 	<select name="id_news" class="select_simple form-control">
			 		<option value="0"><?=lang('wybierz aktualność')?></option>
				<? foreach($news as $cat){
					if(!empty($cat->news)) {
						$prev_path = '';
						$i = 0;
						foreach($cat->news as $row){
			
							$curr_path = '';
							if(!empty($row->art_grandparent_title)) $curr_path .= $row->art_grandparent_title.' / ';
							if(!empty($row->art_parent_title)) $curr_path .= $row->art_parent_title.' / ';
							$curr_path .= $row->art_title;
			
							if($curr_path != $prev_path && !empty($prev_path) || $i == count($cat->news)) echo '</optgroup>';
							if($curr_path != $prev_path) echo '<optgroup label="'.$curr_path.'">';
			
							echo '<option value="'.$row->id.'" '.set_select('id_news', $row->id).'>'.$row->title.'</option>';
			
							$prev_path = $curr_path;
							$i++;
						}
					}
				}
				?>
				</select>
			</div>
	
			<div class="form-group">
				<label><?=lang('Wstęp') ?></label>
				<?=form_field_tooltip(lang('Krótki opis, który będzie wyświetlany przed wejściem do galerii. Pole opcjonalnie.'))?>
				<textarea name="intro" style="height: 75px;" class="form-control"><?=set_value('intro')?></textarea><br />
			</div>
			<div class="form-group">
				<label><?=lang('Opis') ?></label>
				<?=ckeditor('desc', set_value('desc', '', FALSE), '', 'Simple', '140px')?>
			</div>
			<input type="submit" value="<?=lang('Dodaj') ?>" class="btn btn-primary" />
		<?=form_close()?>
	</div>
</div>

<?
if($galleries !== FALSE) {
	echo form_open();
?>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:25px;"><?=lang('Nr')?></th>
				<th><?=lang('Nazwa')?></th>
				<? if(count($lang)>1) {?><th style="width:30px;"><?=lang('Język')?></th><? }?>
				<th><?=lang('Miejsce wyświetlania')?></th>
				<th style="width:78px;"><?=lang('Kolejność')?></th>
				<th style="width:320px;"><?=lang('Działania')?></th>
				<th style="width:25px;"><input class="checkall" type="checkbox"></th>
			</tr>
		</thead>
		<tbody class="sortable">
		<?
		$i=1;
		foreach($galleries as $row):
		?>
			<tr id="listItem_<?=$row->id?>">
				<td><?=$i?></td>
				<td><?=$row->name?></td>
				<? if(count($lang) > 1) echo '<td class="text-center">'.lang_flag($row->lang).'</td>'; ?>
				<td><?=$row->title?></td>
				<td class="text-center"><span class="fa fa-arrows"></span><input class="currentposition" type="hidden" name="position[<?=$row->id?>]" value="<?=$row->position?>"></td>
				<td class="text-center nc-options">
					<?=anchor("{$this->mod_url}/edit/id/{$row->id}", lang('Edytuj'))?>
					<?=anchor("{$this->mod_url}/galleries/id/{$row->id}", lang('Zarządzaj zdjęciami'))?>
					<? if($row->pub==0) echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/1", lang('Publikuj')); else echo anchor("{$this->mod_url}/vswitch/column/pub/id/{$row->id}/set/0", lang('Odpublikuj'));?>
					<a class="click-confirm" data-href="<?=site_url("{$this->mod_url}/delete/del/{$row->id}")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć?') ?>"><?=lang('Usuń') ?></a>
				</td>
				<td class="text-center"><input type="checkbox" value="<?=$row->id?>" name="check[]"></td>
			</tr>
		<?
		$i++;
		endforeach;
		?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="<?=count($lang)==1 ? '4' : '5'?>">
					<input class="btn btn-default btn-sm click-submit" type="button" data-action="<?=site_url("{$this->mod_url}/position")?>" value="<?=lang('Zapisz kolejność')?>">
				</td>
				<td colspan="2" class="nc-options">
					Zaznaczone:
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/edit")?>"><?=lang('Edytuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/1")?>"><?=lang('Publikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/vswitch/column/pub/set/0")?>"><?=lang('Odpublikuj') ?></a>
					<a class="click-submit" data-action="<?=site_url("{$this->mod_url}/delete")?>" data-confirm="<?=lang('Czy na pewno chcesz usunąć zaznaczone wpisy?') ?>"><?=lang('Usuń') ?></a>
				</td>
			</tr>
		</tfoot>
	</table>
<?
	echo form_close();
} else {
	echo msg(lang('Nie znaleziono żadnych galerii.'), 1);
}
?>