<div class="box box-primary box-solid collapsed-box add-entity">
	<div class="box-header with-border">
		<h3 class="box-title"><?=lang('Dodaj zdjęcie') ?></h3>
		<div class="box-tools pull-right">
			<button type="button" data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-plus"></i></button>
		</div>
	</div>
	<div class="box-body">
		<p class="help-block"><?=lang('Sugerowany wymiar obrazka').": {$this->img_width}x{$this->img_height} px"?></p>
		
    	<?=form_open_multipart(current_url().'/addpl/1', array('id' => 'add'));?>
	    	<div id="uploader">
				<p><?=lang('Twoja przeglądarka nie obsługuje żadnej technologii, pozwalającej na wgrywanie plików.')?></p>
			</div>
    	<?=form_close()?>
    	
    	<script type="text/javascript">
			$(function() {
				var errors = 0;
				$("#uploader").pluploadQueue({
					// General settings
					runtimes : 'html5,flash,silverlight,html4',
					url : "<?=current_url()?>/addpl/1",
					multipart_params: {
						<?=config_item('csrf_token_name')?>: $('input[name="<?=config_item('csrf_token_name')?>"]').val()
					},
					rename : false,
					dragdrop: true,
					filters : {
						prevent_duplicates: true,
						max_file_size : '20mb',
						mime_types: [
							{title : "<?=lang('Pliki graficzne')?>", extensions : "jpg,jpeg,gif,png"}
						]
					},
					resize: {
						width: <?=$this->img_width?>,
						height: <?=$this->img_height?>
					},
					flash_swf_url : '<?=base_url().config_item('gfx_a'); ?>js/plupload/Moxie.swf',
					silverlight_xap_url : '<?=base_url().config_item('gfx_a'); ?>js/plupload/Moxie.xap',
					init : {
						UploadComplete: function(up, files) {
							// Called when all files are either uploaded or failed
							if(errors == 0){
								window.location = '<?=current_url()?>';
							}
						},
						FileUploaded: function(up, file, response) {
							// Called when file has finished uploading
							response = jQuery.parseJSON(response.response);
							if (typeof response.error !== 'undefined'){
								var fileid = 'li#' + file.id;
								$(fileid).attr('class', 'plupload_failed').find('a').css('display', 'none').attr('title', response.error.message);
								file.status = plupload.FAILED;
								errors++;
							}
							else {
								$(fileid).attr('class', 'plupload_done').find('a').css('display', 'none').attr('title', 'Success');
								file.status = plupload.DONE;
							}
						},
						Error: function(up, error) {
							errors++;
						}
					}
				});
			});
		</script>
	</div>
</div>

<?
if($img !== FALSE) {
	echo form_open_multipart(current_url().'/position/1');
?>
	<div><input type="submit" value="<?=lang('Zapisz kolejność')?>" class="btn btn-default" /></div>
	
	<ul class="sortable nc-images modal-gallery">
	<?
	foreach($img as $row) {
	
		$thumb_img = thumb_name($row->img);
		$public_path = config_item('upload_path').$this->img_dir.$row->dir.'/';
		$full_path = './'.config_item('site_path').$public_path;
		
		if(file_exists($full_path.$row->img)) {
	?>
			<li class="box">
				<div class="box-body">
					<span class="fa-arrows"></span>
					<a href="<?=$public_path.$row->img?>" title="<?=htmlspecialchars($row->desc)?>">
						<img src="<?=$public_path.$thumb_img?>" alt="">
					</a>
					<input class="currentposition" type="hidden" value="<?=$row->position?>" name="position[<?=$row->id?>]">
				</div>
				<div class="box-footer nc-options">
					<a href="<?="{$this->mod_url}/edit_img/id/{$row->id}"?>" class="modal-box" data-iframe="true"><?=lang('Ustaw opis')?></a>
					<a href="<?="{$this->mod_url}/crop/id/{$row->id}"?>" class="modal-box" data-iframe="true"><?=lang('Kadruj')?></a>
					<a class="click-confirm" data-confirm="Czy na pewno usunąć?" data-href="<?=current_url()."/del/".$row->id?>"><?=lang('Kasuj')?></a>
				</div>
			</li>
	<?
		}
	}
	?>
	</ul>
<?
		echo form_close();
	} else {
		echo msg(lang('Nie znaleziono żadnych zdjęć.'), 1);
	}
?>
