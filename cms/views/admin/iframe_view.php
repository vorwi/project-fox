<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title><?=ifset($meta_title, 'NeoCMS')?></title>
		<base href="<?=base_url();?>" />
		<meta name="author" content="Artneo.pl" />
		<meta name="copyright" content="<?=lang('Wszelkie prawa zatrzeżone')?> Artneo.pl" />
		<meta name="Robots" content="noindex, nofollow" />
			
		<?=put_headers('admin')?>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="iframe">
		<section class="content">
			<?=show_messages()?>
			
			<div id="validation_errors" class="alert alert-warning" style="display: none;"></div>
	
			<? $this->load->view('admin/'.config_item('template_a').'/'.$module_template, $module_data); ?>
		</section>
	</body>
</html>
