<!DOCTYPE html>
<html>
	<head>
		<base href="<?=base_url();?>" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><?=ifset($meta_title, 'NeoCMS')?></title>
		<?=put_headers('elfinder')?>
		
		<meta name="Robots" content="noindex, nofollow" />
	</head>
	<body style="margin: 0;">
		<? $this->load->view('admin/'.config_item('template_a').'/'.$module_template, $module_data); ?>
	</body>
</html>
