<?
if($files !== FALSE) {
	echo form_open();
?>
<div class="row">
	<div class="col-md-5">
		<div class="box box-primary">
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Wybierz kopię do przywrócenia')?></label>
					<select name="backup_file" class="form-control">
						<? foreach($files as $file) {
							echo '<option value="'.$file.'">'.$file.'</option>';
						} ?>
			 		</select>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Przywróć')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
</div>
<?
	echo form_close();
} else {
	msg(lang('Nie znaleziono żadnych kopii zapasowych.'), 1);
}
?>
