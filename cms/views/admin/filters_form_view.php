<?
global $sf_count;
if(!isset($sf_count)) $sf_count = 10;

if(!isset($form_open) || $form_open == TRUE) {
	echo '<div class="box box-warning filters">';
	echo '<div class="box-header with-border"><h4 class="box-title">Filtrowanie danych</h4><div class="box-tools pull-right">
			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div></div>';
	echo '<div class="box-body no-padding">';
	echo form_open(site_url($action_url), array('method'=>'post', 'autocomplete'=>'off', 'style'=>'z-index: '.($sf_count--).';'));
}
?>
<table class="table table-condensed">
	<thead>
		<tr>
	<?
		foreach($fields as $row) {
			echo '<th class="text-center"'.($row['type'] == 'hidden' ? ' style="display:none"' : '').'>'.$row['header'].'</th>';
		}
		if(!isset($show_buttons) || $show_buttons)
			echo '<th class="text-center"></th>';
	?>
		</tr>
	</thead>
	<tbody>
		<tr>
		<?
			foreach($fields as $row) {
				filter_form_row($row, $filters);
			}
		?>
			<? if(!isset($show_buttons) || $show_buttons): ?>
			<td <? if(isset($fields2) && !empty($fields2)){ echo 'rowspan="3"'; } ?>>
				<div class="dnone">
					<?=form_hidden('mod_dir', $this->mod_dir)?>
					<?=form_hidden('mod_name', $this->mod_name.'/index')?>
					<? if(isset($id) && !empty($id)){ ?>
						<?=form_hidden('id', $id)?>
					<? } ?>	
				</div>
				<input type="submit" name="filters" class="btn btn-success btn-sm" value="<?=$button?>" />
				<input type="submit" name="clearfilters" class="btn btn-warning btn-sm" value="wyczyść filtry" />
			</td>
			<? endif; ?>
		</tr>
		<? if(isset($fields2) && !empty($fields2)){ ?>
			<tr>
			<?
			$i_f_2 = 1;
			$fields_count = count($fields);
			foreach($fields2 as $row2) {
				if(count($fields2) == 1){
					$colspan = count($fields);
				}else{
					if($i_f_2 == count($fields2)){
						$colspan = $fields_count;
					}else{
						$colspan = count($fields) / count($fields2);
						$colspan = (int)$colspan;
					}
					$fields_count -= $colspan;
				}
				echo '<th '.(count($fields) > count($fields2) ? 'colspan="'.$colspan.'"' : '').' class="tcenter"'.($row2['type'] == 'hidden' ? ' style="display:none"' : '').'>'.$row2['header'].'</th>';
				$i_f_2++;
			}
			?>
			</tr>
			<tr>
			<?
			$i_f_2 = 1;
			$fields_count = count($fields);
			foreach($fields2 as $row2) {
				if(count($fields) > count($fields2)){
					if(count($fields2) == 1){
						$row2['colspan'] = count($fields);
						$row2['td_class'] = 'tcenter';
					}else{
						if($i_f_2 == count($fields2)){
							$row2['colspan'] = $fields_count;
						}else{
							$row2['colspan'] = count($fields) / count($fields2);
							$row2['colspan'] = (int)$row2['colspan'];
						}
						$fields_count -= $row2['colspan'];
					}
				}
				filter_form_row($row2, $filters);
				$i_f_2++;
			}
			?>
			</tr>
		<? } ?>
	</tbody>
</table>
<?
if(!isset($form_close) || $form_close == TRUE) {
	echo form_close();
	echo '</div>';
	echo '</div>';
}
?>
