<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>neoCMS | <?=lang('Przypomnienie hasła')?></title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<base href="<?=base_url();?>">
		<meta name="author" content="Artneo.pl">
		<meta name="Robots" content="noindex, nofollow">
		
		<?=put_headers('admin')?>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-box-body">
				<div class="login-logo logo">
					<b>neo</b>CMS
				</div><!-- /.login-logo -->
				<h2 class="login-box-msg">
					<?=lang('Resetowanie hasła') ?>
				</h2>
				<?
					echo form_open('admin/remind');
				
					echo show_messages();
					
					if($br_count > 0 && $br_count < 3) {
						echo msg(lang('próba resetowania:').' <b>'.$br_count.'</b>, '.lang('pozostało:').' <b>'.(3-$br_count).'</b>',1);
					} else if($br_count == 3 && $br_time_left > 0) {
						echo msg(lang('Niestety 3 próba resetowania zakończona niepowodzeniem. Możliwość resetowania została zablokowana na:').' <b>'.config_item('remind_lock_time').' '.lang('sekund').'</b>.<br />'.lang('Pozostało:').' <span id="counter" style="font-weight:bold; font-size:16px;">'.$br_time_left.'</span> '.lang('sekund').'.',1);
					}
				?>
					<? if($br_lock == 0) { ?>
					<p class="callout callout-info"><?=lang('Wpisz poniżej swój adres e-mail, który jest podany w ustawieniach konta. Zostanie na niego wysłana instrukcja aktywowania nowego hasła.') ?></p>
					<div class="form-group has-feedback">
						<input name="rem_login" type="text" class="form-control" placeholder="Login" value="<?=set_value('rem_login')?>">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input name="rem_email" type="text" class="form-control" placeholder="E-mail" value="<?=set_value('rem_email')?>">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group">
						<label><?=lang('Przepisz kod z obrazka') ?></label>
						<div class="row">
							<div class="col-xs-6">
								<?='<img style="vertical-align: middle;" src="'.config_item('temp').'captcha/'.$cap['time'].'.jpg" alt="">';?>
							</div>
							<div class="col-xs-6">
								<input type="text" name="captcha" class="form-control capcha-input"> 
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-7">
							<a href="/admin/login" title="<?=lang('Przejdź do logowania') ?>"><?=lang('Zaloguj się') ?></a>
						</div>
						<div class="col-xs-5">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Wyślij</button>
						</div>
					</div>
					<? } else { ?>
						<a href="/admin/login" class="btn btn-primary btn-block btn-flat" title="<?=lang('Przejdź do logowania') ?>"><?=lang('Zaloguj się') ?></a>
					<? } ?>
				<?=form_close()?>
			</div>
			<div class="box-footer text-center">
				<a target="_blank" href="http://www.artneo.pl"><img src="<?=config_item('gfx_a').'images/logo_artneo_xs.png'?>" alt="" /></a>
			</div>
		</div>
		<? if($br_time_left > 0) { ?>
		<script type="text/javascript">
			$(document).ready(function() {
				countdown(<?=$br_time_left?>);
	 		});
	 		function countdown(b) {
				if(b > 0){
					$('#counter').text(b);
					b--;
					setTimeout("countdown("+b+")", 1000);
				}
				else location.href = 'admin';
			}
		</script>
		<? } ?>
	</body>
</html>