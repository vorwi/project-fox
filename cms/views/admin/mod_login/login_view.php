<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>neoCMS | <?=lang('Zaloguj się')?></title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<base href="<?=base_url();?>">
		<meta name="author" content="Artneo.pl">
		<meta name="Robots" content="noindex, nofollow">
		
		<?=put_headers('admin')?>
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-box-body">
				<div class="login-logo logo">
					<b>neo</b>CMS
				</div><!-- /.login-logo -->
				<p class="login-box-msg">
					<?=lang('Logowanie do systemu zarządzania treścią')?>
				</p>
				<?
					echo form_open('admin/login');
				
					echo show_messages();
					
					if($bl_count > 0 && $bl_count < 3) {
						echo msg(lang('próba logowania:').' <b>'.$bl_count.'</b>, '.lang('pozostało:').' <b>'.(3-$bl_count).'</b>',1);
					} else if($bl_count == 3 && $bl_time_left > 0) {
						echo msg(lang('Niestety 3 próba logowania zakończona niepowodzeniem. Możliwość logowania została zablokowana na:').' <b>'.config_item('login_lock_time').' '.lang('sekund').'</b>.<br />'.lang('Pozostało:').' <span id="counter" style="font-weight:bold; font-size:16px;">'.$bl_time_left.'</span> '.lang('sekund').'.',1);
					}
				?>
					<? if($bl_lock == 0) { ?>
					<div class="form-group has-feedback">
						<input name="login" type="text" class="form-control" placeholder="<?=lang('Login')?>">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input name="password" type="password" class="form-control" placeholder="<?=lang('Hasło')?>">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-7">
							<a href="/admin/remind" title="<?=lang('Przejdź do resetowania hasła') ?>"><?=lang('Nie pamiętam hasła...') ?></a>
						</div>
						<div class="col-xs-5">
							<button type="submit" class="btn btn-primary btn-block btn-flat">
								<?=lang('Zaloguj')?>
							</button>
						</div>
					</div>
					<? } ?>
				<?=form_close()?>
	
			</div>
			<div class="box-footer text-center">
				<a target="_blank" href="http://www.artneo.pl"><img src="<?=config_item('gfx_a').'images/logo_artneo_xs.png'?>" alt="" /></a>
			</div>
		</div>
		
		<? if($bl_time_left > 0) { ?>
		<script type="text/javascript">
			$(document).ready(function() {
				countdown(<?=$bl_time_left?>);
	 		});
	 		function countdown(b) {
				if(b > 0){
					$('#counter').text(b);
					b--;
					setTimeout("countdown("+b+")", 1000);
				}
				else location.href = 'admin';
			}
		</script>
		<? } ?>
	</body>
</html>

