<?php if(count($all)>0) {?>
<div class="panel-group" id="accordion">
	<?php foreach($all as $row) {?>
  	<div class="panel panel-default">
    	<div class="panel-heading">
      		<h4 class="panel-title">
        		<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$row->id?>"><?=$row->title?></a>
      		</h4>
    	</div>
    	<div id="collapse<?=$row->id?>" class="panel-collapse collapse">
      		<div class="panel-body">
      			<?php if(isset($row->children) && count($row->children)>0) {?>     			
      			<div class="panel-group" id="subaccordion<?=$row->id?>">
					<?php foreach($row->children as $sub) {?>
				  	<div class="panel panel-default">
				    	<div class="panel-heading">
				      		<h4 class="panel-title">
				        		<a data-toggle="collapse" data-parent="#subaccordion<?=$row->id?>" href="#collapse<?=$sub->id?>"><?=$sub->title?></a>
				      		</h4>
				    	</div>
				    	<div id="collapse<?=$sub->id?>" class="panel-collapse collapse">
				      		<div class="panel-body">
				      		
				      			<?php if(isset($sub->children) && count($sub->children)>0) {?>     			
				      			<div class="panel-group" id="subsubaccordion<?=$sub->id?>">
									<?php foreach($sub->children as $subsub) {?>
								  	<div class="panel panel-default">
								    	<div class="panel-heading">
								      		<h4 class="panel-title">
								        		<a data-toggle="collapse" data-parent="#subsubaccordion<?=$sub->id?>" href="#collapse<?=$subsub->id?>"><?=$subsub->title?></a>
								      		</h4>
								    	</div>
								    	<div id="collapse<?=$subsub->id?>" class="panel-collapse collapse">
								      		<div class="panel-body">
								      			<?=$subsub->content?>
								      		</div>
								    	</div>
								  	</div>
								  	<?php }?>
								</div>     			
				      			<?php } else {?>
				      			<?=$sub->content?>
				      			<?php }?>
				      		</div>
				    	</div>
				  	</div>
				  	<?php }?>
				</div>     			
      			<?php } else {?>
      			<?=$row->content?>
      			<?php }?>
      		</div>
    	</div>
  	</div>
  	<?php }?>
</div> 
<?php }?>
