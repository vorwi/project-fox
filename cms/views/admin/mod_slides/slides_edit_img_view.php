<? 
if(is_array($this->session->flashdata('msg_files'))) {
 	foreach($this->session->flashdata('msg_files') as $row){
        $msg = $this->session->flashdata('msg_files');
        echo msg($row[0], $row[1]);
 	}
} elseif(!isset($img)) {
	echo msg(lang('Wybrane zdjęcie nie istnieje lub odnośnik jest nieprawidłowy.'), 1);
} else {
		
	$thumb_img = thumb_name($img);
?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<?=form_open('', array('class' => 'box box-borders', 'id' => 'img-edit'))?>
			<div class="box-header with-border">
				<h3 class="box-title"><?=lang('Edycja zdjęcia')." #{$id}"?></h3>
           </div>
			<div class="box-body">
				<p class="text-center">
					<img src="<?=$path.$thumb_img;?>" alt="">
				</p>
				<div class="form-group">
					<label><?=lang('Opis zdjęcia')?></label>
					<input type="text" name="desc" value="<?=set_value('desc', $desc)?>" class="form-control">
				</div>
				<?=form_hidden('edit_img', 1)?>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary" style="width: 150px;">
			</div>
		<?=form_close()?>
	</div>
</div>

<script type="text/javascript">
	$('#img-edit').submit(function(e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: '<?=$this->mod_url.'/edit_img/id/'.$id?>',
			data: $(this).serialize(),
			success: function(){
				parent.location.reload();
			}
		});
	});
</script>
<? } ?>
