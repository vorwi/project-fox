<?
if($all !== FALSE ) {
	$tab = $this->uri->uri_to_assoc();
	if(@is_numeric($tab['page'])) $add = $tab['page'];
	else $add = 1;
	
	echo form_open($this->mod_url.'/edit/page/'.$add.'/check/1', array('class' => 'row'));
	
	foreach($all as $gal) {
		echo form_hidden('check[]', $gal->id);
?>
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?=lang('Galeria slajdów')?> #<?=$gal->id?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"><i class="fa fa-minus"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label><?=lang('Nazwa') ?></label>
					<input type="text" name="title_<?=$gal->id?>" value="<?=set_value('title_'.$gal->id, $gal->title)?>" class="form-control">
				</div>
				<div class="form-group" <?=(count($lang) == 1 ? 'style="display: none;"' : '')?>>
					<label><?=lang('Język')?>:</label>
					<select name="lang_<?=$gal->id?>" class="select_simple form-control">
					<? foreach($lang as $row){
						echo '<option '.set_select('lang_'.$row->id, $row->short, ($row->short == $gal->lang->short)).' value="'.$row->short.'">'.$row->lang.'</option>';
					} ?>
					</select>
				</div>
				<div class="form-group">
					<label><?=lang('Miejsce wyświetlania') ?></label>
					<select name="id_art_<?=$gal->id?>" class="select_simple form-control">
				 		<option value="0"><?=lang('wybierz artykuł')?></option>
					<? foreach($art['categories'] as $cat){
						if(!empty($cat->articles)) {
					 		echo '<optgroup label="'.$cat->name.'">';
							foreach($cat->articles as $row){
								$padd = repeater('&#x2001;', $row->tree);
				
								echo '<option value="'.$row->id.'" '.set_select('id_art_'.$row->id, $row->id, ($row->id == $gal->id_art)).'>'.$padd.' '.$row->title.'</option>';
							}
					 		echo '</optgroup>';
						}
					}
					?>
					</select>
				</div>
			</div>
			<div class="box-footer">
				<input type="submit" value="<?=lang('Zapisz')?>" class="btn btn-primary">
			</div>
		</div>
	</div>
<?
	}
	echo form_close();
} 
?>
