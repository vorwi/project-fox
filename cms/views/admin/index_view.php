<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title><?= $meta_title ?></title>
		<base href="<?= base_url(); ?>" />
		<meta name="author" content="Artneo.pl" />
		<meta name="copyright" content="<?= lang('Wszelkie prawa zatrzeżone') ?> Artneo.pl" />
		<meta name="Robots" content="noindex, nofollow" />

		<?= put_headers('admin') ?>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-green sidebar-mini fixed">

		<div class="wrapper">

			<header class="main-header">
				<!-- Logo -->
				<a href="<?= base_url() ?>" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini">CMS</span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>neo</b>CMS</span>
				</a>

				<? $this->load->view('admin/includes/header_navbar_view') ?>
			</header>

			<aside class="main-sidebar">
				<section class="sidebar">
					<? /*
					  <form action="#" method="get" class="sidebar-form">
					  <div class="input-group">
					  <input type="text" name="q" class="form-control" placeholder="Szukaj...">
					  <span class="input-group-btn">
					  <button type="submit" name="search" id="search-btn" class="btn btn-flat">
					  <i class="fa fa-search"></i>
					  </button>
					  </span>
					  </div>
					  </form>
					 */ ?>
					<h2 class="sr-only"><?= lang('Menu główne') ?></h2>
					<ul class="sidebar-menu">
						<?
						$current_com = FALSE;
						if ($components !== FALSE) {
							foreach ($components as $com) {
								$class = '';
								if ($com->selected) {
									$class = 'active';
									$current_com = $com;
								}
								echo '<li class="treeview ' . $class . '"><a href="' . $com->com_link . '">';
								if (!empty($com->icon))
									echo '<i class="fa fa-' . $com->icon . '"></i> ';
								echo '<span>' . $com->name . '</span> <i class="fa fa-angle-left pull-right"></i></a>';

								if (!empty($com->modules)) {
									echo '<ul class="treeview-menu ' . ($com->selected ? 'menu-open' : '') . '">';

									foreach ($com->modules as $mod) {
										$m_class = '';
										if (!empty($mod->label))
											echo '<li class="header">' . $mod->label . '</li>';
										$mod_link = 'admin/' . $mod->mod_dir . $mod->mod;
										if (!empty($mod->mod_function)) {
											$mod_link .= '/' . $mod->mod_function;
										}
										if (isset($mod->selected) && $mod->selected == 1) {
											$m_class = 'active';
										}

										echo '<li class="' . $m_class . '"><a href="' . site_url($mod_link) . '"><i class="fa fa-caret-right"></i> ' . $mod->title . '</a></li>';
									}
									echo '</ul>';
								}

								echo '</li>';
							}
						}
						?>
					</ul>
				</section>
			</aside>

			<!-- =============================================== -->

			<div class="content-wrapper">

				<nav class="content-header">
					<ol class="breadcrumb">
						<li><a href="<?= site_url('admin') ?>"><i class="fa fa-home"></i> <?= lang('Pulpit') ?></a></li>
						<?
						if (isset($this->mod_id)) {
							if (isset($components[$this->mod_id_component])) {
								echo '<li>' . anchor($components[$this->mod_id_component]->com_link, $this->mod_component) . '</li>';
							}
							$method = $this->router->fetch_method();
							$class = $this->router->fetch_class();
							if ($this->mod_function != $method) {
								echo '<li>' . anchor(site_url("admin/{$this->mod_dir}{$class}/{$this->mod_function}"), $this->mod_title) . '</li>';
							}
						}
						?>
						<li class="active"><?= $this->title ?></li>
					</ol>
				</nav>

				<section class="content">
					<? if (is_null($this->neocms->get_flag('no_content_wrapper'))) { ?>
						<div class="box" id="main-box">
							<div class="box-header with-border">
								<h1 class="box-title headline"><?= (isset($this->title) ? $this->title : '') ?></h1>
							</div>
							<div class="box-body">
							<? } ?>
							<?
							echo show_messages();

							if (is_array($this->session->flashdata('msg_files'))) {
								foreach ($this->session->flashdata('msg_files') as $row) {
									$msg = $this->session->flashdata('msg_files');
									echo msg($row[0], $row[1]);
								}
							}

							if ($this->session->flashdata('changed') != '') {
								$msg = null;
								$changed = $this->session->flashdata('changed');
								if (!empty($changed) && is_array($changed)) {
									foreach ($changed as $row) {
										$title = (isset($row['name']) ? $row['name'] : (isset($row['title']) ? $row['title'] : ''));
										if (!empty($title)) {
											$msg .= lang('Nazwa') . ': <b>' . $title . '</b><br />';
										}
									}
								}
								if (!empty($msg)) {
									echo msg($msg, 3);
								}
							}

							echo $module_content;
							?>
							<? if (is_null($this->neocms->get_flag('no_content_wrapper'))) { ?>
							</div>
						</div>
					<? } ?>
				</section>
			</div>

			<footer class="main-footer">
				<strong>Copyright &copy; 2017 <a target="_blank" href="http://www.artneo.pl">Artneo</a>.</strong> All rights reserved.
			</footer>

		</div>

		<? $this->load->view('admin/includes/modal_view'); ?>
	</body>
</html>
