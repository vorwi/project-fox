<div style="display: none" aria-hidden="true">
    <div class="form-group">
        <label for="<?=$js_field_name?>"><?=lang('Proszę nie zmieniać wartości tego pola')?>:</label>
        <input name="<?=$js_field_name?>" id="<?=$js_field_name?>" type="text" class="form-control" value="<?=set_value($js_field_name)?>">
    </div>
    <div class="form-group">
        <label for="fsf-botcheck"><?=lang('Proszę nie zaznaczać tego pola')?>:</label>
        <input name="bot-check" id="fsf-botcheck" type="checkbox" value="<?=$bot_check_value?>" class="form-control">
    </div>
    <div class="form-group">
        <label for="fsf-email"><?=lang('E-mail')?>:</label>
        <input name="email-relpy" id="fsf-email" class="form-control" type="email" value="" class="form-control">
    </div>

    <script type="text/javascript">
        var a = document.getElementById("<?=$js_field_name?>");
        a.value = "<?=$js_field_value?>";
    </script>
</div>