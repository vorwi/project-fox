<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Log extends CI_Log {
	
	protected $_log_files_for = 3;

	public function __construct() {
		parent::__construct();
		
		$this->_log_path .= date('Y-m').'/';

		if ( ! is_dir($this->_log_path) OR ! is_really_writable($this->_log_path)){
			mkdir($this->_log_path, 0777);
		}
		
		if (config_item('log_files_for') != ''){
			$this->_log_files_for = config_item('log_files_for');
		}
	}
	
	
	/**
	 * Write Log File
	 *
	 * Generally this function will be called using the global log_message() function
	 *
	 * @param	string	the error level
	 * @param	string	the error message
	 * @param	bool	whether the error is a native PHP error
	 * @return	bool
	 */
	public function write_log($level = 'error', $msg, $php_error = FALSE){
		parent::write_log($level, $msg, $php_error);
	
		$now_Y = date('Y');
		$now_m = date('m');
	
		$base_log_dir = str_replace($now_Y.'-'.$now_m.'/', '', $this->_log_path);
	
		$logs_subdirs = scandir($base_log_dir);
	
		$count_subdirs = count($logs_subdirs);
	
		if($count_subdirs > $this->_log_files_for+2){
			$logs_subdirs_cl = array();
	
			foreach ($logs_subdirs as $key=>$dir){
				if($key > 1 && $key < $count_subdirs-3){
					$logs_subdirs_cl[] = $dir;
				}
			}
	
			foreach ($logs_subdirs_cl as $dir_rm){
				if (is_dir($base_log_dir.$dir_rm)) {
					$objects = scandir($base_log_dir.$dir_rm);
					foreach ($objects as $object) {
						if ($object != "." && $object != "..") {
							unlink($base_log_dir.$dir_rm."/".$object);
						}
					}
					reset($objects);
					rmdir($base_log_dir.$dir_rm);
				}
			}
		}
	}
}