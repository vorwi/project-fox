<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Security extends CI_Security {
	private $form_tests_enabled = array(
		'markers_empty',
		//'CI_captcha',
		'send_time',
		'honeypot',
		'JS_enabled'
	);

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Show CSRF Error
	 *
	 * @return	void
	 */
	public function csrf_show_error()
	{
		if(session_status() !== PHP_SESSION_ACTIVE)
		{
	  		session_start();
		}

		$_SESSION['redirected_from'] = 'csrf_error';

		header('Location: ' . htmlspecialchars($_SERVER['REQUEST_URI']), TRUE, 302);
		exit();
	}

	/** Ustawienie znaczników dla zabezpieczeń robotami
	 * WAŻNE - metoda musi być wywołana po form_markers_set, żeby w ogóle dało się zweryfikować poprawność danych
	 * Ustawia dane w sesji i zwraca je, żeby dało się wykorzystać w widoku
	 * @return array
	 */
	public function form_markers_get() {
		$CI =& get_instance();

		$jsk_pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$jsk_len = 12;
		$markers = (object)array(
			'display_time' => (microtime(true) * 1000),
			'js_field_name' => 'a'.substr(str_shuffle(str_repeat($jsk_pool, ceil($jsk_len / strlen($jsk_pool)))), 0, $jsk_len),
			'js_field_value' => substr(str_shuffle(str_repeat($jsk_pool, ceil($jsk_len / strlen($jsk_pool)))), 0, $jsk_len),
			'bot_check_value' => rand(2,50)
		);
		$CI->session->set_tempdata('form_security', json_encode($markers), 3600);

		return $CI->load->view('common/security_markers_view', $markers, TRUE);
	}

	public function form_markers_check() {
		$CI =& get_instance();
		$pass = true;
		$form_time_limit = 4; //ile sekund musi upłynąć od wyświetlenia do wysłania formularza, żeby go przyjąć

		try {
			$markers = json_decode($CI->session->tempdata('form_security'));
			if(empty($markers)) {
				$markers = new stdClass();
			}

			if(isset($markers->display_time)) {
				$check_time = ((microtime(true) * 1000) - $markers->display_time) / 1000;
			} else {
				$check_time = -1;
			}
			$captcha = $CI->input->post('captcha'); //uzupełnone jeśli w HTMLu pozostała stara captcha CI i bot ją rozwiązał
			$JS_verif = $CI->input->post($markers->js_field_name);
			$hp_email = $CI->input->post('email-relpy');
			$hp_checkbox = $CI->input->post('bot-check');

			if(in_array('markers_empty', $this->form_tests_enabled) && empty($markers)) {
				//jeśli markery nie zostały uzupełnione, to BOT próbował wysłać formularz bez odwiedzenia strony na której się on wyświetla
				throw new Exception("[markers_empty] Security markers not set", 2);
			} elseif(in_array('CI_captcha', $this->form_tests_enabled) && !empty($captcha)) {
				//czy ukryta CI captcha została rozwiązana - teraz nie jest bezpieczna, ale nadaje się jako lep na boty
				throw new Exception("[CI_captcha] Old IMG captcha resolved: '{$captcha}'", 3);
			} elseif(in_array('send_time', $this->form_tests_enabled) && $check_time < $form_time_limit) {
				//czas od wyświetlenia do wysłania formularza
				throw new Exception("[send_time] Time between displaying and sending form: ".round($check_time, 2)."s < {$form_time_limit}s", 4);
			} elseif(in_array('honeypot', $this->form_tests_enabled) && !empty($hp_checkbox)) {
				//dodatkowy lep na boty - checkbox który nie powinien być zaznaczony
				throw new Exception("[honeypot] Hidden checkbox checked", 5);
			} elseif(in_array('honeypot', $this->form_tests_enabled) && !empty($hp_email)) {
				//dodatkowy lep na boty
				throw new Exception("[honeypot] Hidden e-mail field filled: '{$hp_email}'", 6);
			} elseif(in_array('JS_enabled', $this->form_tests_enabled) && $JS_verif != $markers->js_field_value) {
				//wartość pola ustawianego przez JS
				throw new Exception("[JS_enabled] Filled field value '{$JS_verif}' differs from '{$markers->js_field_value}'", 7);
			}
		}
		catch(Exception $rr) {
			$pass = false;
			//przy następnym przypływie weny, można w tym miejscy zrobić wysyłanie danych żądaniem GET do skryptu, który będzie zapisywał wszystkie nieudane próby
			//może się to przydać później do poznania które zabezpieczenia są najbardziej skuteczne, a co można wyrzucić
		}
		return $pass;
	}
}