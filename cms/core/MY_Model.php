<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model{
	private $this_mod_table = NULL;

	public function __construct($mod_table = NULL) {
		parent::__construct();
		if(!empty($mod_table)){
			$this->this_mod_table = $mod_table;
		}
	}
	
	protected function _init_module_table($file, $dir) {
		if(!empty($this->this_mod_table)){
			$this->mod_table = $this->this_mod_table;
		}else{
			$mod_name = basename($file, "_model.php");
			$mod_dir = basename($dir).'/';
			
			$mod_name = strtolower($mod_name);
			
			$this->db->select('mod_table');
			$this->db->where('mod',$mod_name);
			$this->db->where('mod_dir',$mod_dir);
			$query = $this->db->get('modules');
			if($query->num_rows()==1){
				$row = $query->row();
				$this->mod_table = $row->mod_table;
			}
		}
	}
}