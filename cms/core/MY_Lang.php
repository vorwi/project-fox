<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Language Class extension.
 *
 * Adds language fallback handling.
 *
 * When loading a language file, CodeIgniter will load first the english version,
 * if appropriate, and then the one appropriate to the language you specify.
 * This lets you define only the language settings that you wish to over-ride
 * in your idiom-specific files.
 *
 * This has the added benefit of the language facility not breaking if a new
 * language setting is added to the built-in ones (english), but not yet
 * provided for in one of the translations.
 *
 * To use this capability, transparently, copy this file (MY_Lang.php)
 * into your application/core folder.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Language
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/language.html
 */
class MY_Lang extends CI_Lang {

	/**
	 * Refactor: base language provided inside system/language
	 *
	 * @var string
	 */
	public $base_language = 'polish';
	
	private $dynamic_lang_name = '_dynamic_lang';
	private $dynamic_lang_last_mtime = NULL;
	private $dynamic_lang_lines = array();
	private $missing_lines = array();

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct(){
		parent::__construct();
	}
	
	public function __destruct(){
		try {
			$this->_update_dynamic_lang();
		}
		catch(Exception $e) {
			log_message('error', 'Dynamic lang update error: "'.$e->getMessage());
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Load a language file, with fallback to english.
	 *
	 * @param	mixed	$langfile	Language file name
	 * @param	string	$idiom		Language name (english, etc.)
	 * @param	bool	$return		Whether to return the loaded array of translations
	 * @param 	bool	$add_suffix	Whether to add suffix to $langfile
	 * @param 	string	$alt_path	Alternative path to look for the language file
	 *
	 * @return	void|string[]	Array containing translations, if $return is set to TRUE
	 */
	public function load($langfile, $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = ''){
		if (is_array($langfile)){
			foreach ($langfile as $value){
				$this->load($value, $idiom, $return, $add_suffix, $alt_path);
			}

			return;
		}

		$langfile = str_replace('.php', '', $langfile);

		if ($add_suffix === TRUE){
			$langfile = preg_replace('/_lang$/', '', $langfile) . '_lang';
		}

		$langfile .= '.php';

		if (empty($idiom) OR ! preg_match('/^[a-z_-]+$/i', $idiom)){
			$config = & get_config();
			$idiom = empty($config['language']) ? $this->base_language : $config['language'];
		}

		if ($return === FALSE && isset($this->is_loaded[$langfile]) && $this->is_loaded[$langfile] === $idiom){
			return;
		}

		// load the default language first, if necessary
		// only do this for the language files under system/
		$basepath = SYSDIR . 'language/' . $this->base_language . '/' . $langfile;
		if (($found = file_exists($basepath)) === TRUE){
			include($basepath);
		}

		// Load the base file, so any others found can override it
		$basepath = BASEPATH . 'language/' . $idiom . '/' . $langfile;
		if (($found = file_exists($basepath)) === TRUE){
			include($basepath);
		}

		// Do we have an alternative path to look in?
		if ($alt_path !== ''){
			$alt_path .= 'language/' . $idiom . '/' . $langfile;
			if (file_exists($alt_path)){
				include($alt_path);
				$found = TRUE;
			}
		} else {
			foreach (get_instance()->load->get_package_paths(TRUE) as $package_path){
				$package_path .= 'language/' . $idiom . '/' . $langfile;
				if ($basepath !== $package_path && file_exists($package_path)){
					include($package_path);
					$found = TRUE;
					break;
				}
			}
		}

		if ($found !== TRUE){
			show_error('Unable to load the requested language file: language/' . $idiom . '/' . $langfile);
		}

		if (!isset($lang) OR ! is_array($lang)){
			log_message('info', 'Language file contains no data: language/' . $idiom . '/' . $langfile);

			if ($return === TRUE){
				return array();
			}
			return;
		}

		if ($return === TRUE){
			return $lang;
		}

		$this->is_loaded[$langfile] = $idiom;
// 		$this->language = array_merge($this->language, $lang);
		$this->language = $this->language + $lang;

		log_message('info', 'Language file loaded: language/' . $idiom . '/' . $langfile);
		return TRUE;
	}

	
	private function _get_dl_filepath() {
		$CI =& get_instance();
		$dlf_path = APPPATH.'language/'.config_item('language');
		if($CI->uri->segment(1, 'none') == 'admin'){
			$dlf_path .= '/admin/'.$this->dynamic_lang_name.'.php';
		} else {
			$dlf_path .= '/frontend/'.$this->dynamic_lang_name.'.php';
		}
		return $dlf_path;
	}
	
	private function _get_dl_contents($dlf_path) {
		if(file_exists($dlf_path)) {
			ob_start();
				try {
					include($dlf_path);
					$dl_lines = $lang;
				}
				catch(Exception $e) {
					
				}
			ob_end_clean();
		}
		if(!isset($dl_lines) || !is_array($dl_lines)) {
			$dl_lines = array();
		}
		return $dl_lines;
	}
	
	private function _get_dl_line($key) {
		
		//ustalenie który plik nas interesuje, na podstawie prefixu panelu CMS
		$dlf_path = $this->_get_dl_filepath();
		
		$dlf_mtime = (file_exists($dlf_path) ? filemtime($dlf_path) : 0);
		
		//sprawdzam po dacie modyfikacji pliku, czy cokolwiek się zmieniło - nie ma sensu wczytywać w kółko tego samego pliku
		if($this->dynamic_lang_last_mtime !== $dlf_mtime) {
			$this->dynamic_lang_last_mtime = $dlf_mtime;
			$this->dynamic_lang_lines = $this->_get_dl_contents($dlf_path);
		}
		
		return (isset($this->dynamic_lang_lines[$key]) ? $this->dynamic_lang_lines[$key] : FALSE);
	}
	
	private function _generate_dl_contents($input, $in_array = 0) {
		$result = '';
		$n = 1;
		foreach($input as $k => $v) {
			if($in_array) {
				$result .= repeater("\t", $in_array)."'{$k}' => ";
			} else {
				$result .= '//'.str_replace(array("\t", "\n", "\r", "\0", "\x0B"), '', $v)."\r\n"; //oryginalna wartość w komentarzu, żeby ułatwić tłumaczenie lub szukanie w pliku
				$result .= "\$lang['{$k}'] = ";
			}

			if(is_array($v)) {
				$result .= "array(\r\n".$this->_generate_dl_contents($v, $in_array + 1).repeater("\t", $in_array).")";
			} else {
				$v = htmlspecialchars($v, ENT_QUOTES, 'UTF-8', TRUE);
				$v = htmlspecialchars_decode($v, ENT_COMPAT);
				$result .= "'{$v}'";
			}

			if($in_array) {
				$result .= ($n < count($input) ? "," : "")."\r\n";
			} else {
				$result .= ";\r\n";
			}
			$n++;
		}
		return $result;
	}
	
	/**
	 * Funkcja tworzy najpierw plik tymczasowy i dopiero jeśli jego zapis się powiedzie, podmienia go z dotychczasowym
	 */
	private function _update_dynamic_lang() {
		//jeśli nie ma nowych linii do dopisania, to nic w pliku nie zmieniamy
		if(empty($this->missing_lines)) {
			return TRUE;
		}
		$dlf_path = $this->_get_dl_filepath();
		
		//TODO dobrze by było robić aktualizację dla każdego języka - żeby pliki dynamic były spójne
		
		//blokada pliku przed zapisem
		if(file_exists($dlf_path)) {
			//próba otwarcia/utworzenia obecnego pliku
			if(!$fp = @fopen($dlf_path, 'r')) {
				//nie udało się otworzyć oryginalnego (obecnego) pliku dynamic
				throw new Exception("Couldn't open '{$dlf_path}' for reading.", 1);
			}
			
			if(!flock($fp, LOCK_EX)) {
				//nie udało się zablokować oryginalnego pliku przed zapisem
				throw new Exception("Couldn't lock '{$dlf_path}' for reading/writing.", 1);
			}
		}
		
		//odczyt aktualnych danych - jeśli pliku nie ma, zwróci pustą tablicę
		$dl_lines = $this->_get_dl_contents($dlf_path);
		
		//wygenerowanie nowej zawartości
		$merged_lines = array_merge($dl_lines, $this->missing_lines);
		
		$output = "<?php defined('BASEPATH') OR exit('No direct script access allowed'); \n\n";
		$output .= $this->_generate_dl_contents($merged_lines);
		
		$CI =& get_instance();
		$CI->load->helper('file');
		
		//zapis do pliku tymczasowego
		$mts = floor(microtime(TRUE) * 1000);
		$dlf_tmp = str_replace('_lang.php', '_tmp-'.$mts.'.php', $dlf_path);
		
		if(!write_file($dlf_tmp, $output)) {
			//nie udało się zapisać tymczasowego pliku z nowymi frazami
			throw new Exception("Couldn't write '{$dlf_tmp}' with new missing lines.", 1);
		}
		
		//zdjęcie blokady
		if(!empty($fp)) {
			
			flock($fp, LOCK_UN);
			fclose($fp);
			
			//podmieniam nazwę oryginalnego pliku - zostawiam kopię
			rename($dlf_path, $dlf_path.'_prev.php');
		}
		
		//zmiana nazwy tymczasowego pliku na właściwy
		if(!rename($dlf_tmp, $dlf_path)) {
			//nie udało się podmienić pliku TMP na właściwy
			throw new Exception("Couldn't rename '$dlf_tmp' to '$dlf_path'.", 1);
		}
		return TRUE;
	}
	
	function line($line, $log_errors = TRUE){
		$key_to_value = $line;
		$value = ($line == '' OR ! isset($this->language[$line])) ? FALSE : $this->language[$line];

		if ($value === FALSE) {
			$remove = array('.', ' ', "\t", "\n", "\r", "\0", "\x0B");
			$uniq_key = str_replace($remove, '', $line);
			if(!empty($line)) {
				$uniq_key = md5($line);
				$value = (!isset($this->language[$uniq_key])) ? FALSE : $this->language[$uniq_key];
			}
		}

		// Because killer robots like unicorns!
		if ($value === FALSE){
			
			//sprawdzenie, czy fraza jest w pliku generowanym dynamicznie
			$value = $this->_get_dl_line($uniq_key);
			
			if($value === FALSE) {
				$this->missing_lines[$uniq_key] = $key_to_value;
				$value = $key_to_value;
			}

			//przy korzystaniu z dynamic_lang ten warunek nie powinien nigdy przejść, ale zostawiam gdyby gdzieś DL trzeba było wyłączyć
			if($value === FALSE && $log_errors === TRUE){
				$e = new Exception();
				$trace = explode("\n", $e->getTraceAsString());
				$trace = array_reverse($trace);
				array_shift($trace);
				array_pop($trace);
				$length = count($trace);
				$result = array();
	
				for ($i = 0; $i < $length; $i++){
					$result[] = /*($i + 1)  . ')' . */substr($trace[$i], strpos($trace[$i], ' '));
				}
	
				$result_trace = "\t" . end($result);
				
				log_message('error', 'Could not find the language line "'.$line.'": '."\n".$result_trace);
			}
		}

		return ($value !== FALSE ? $value : $key_to_value);
	}
}