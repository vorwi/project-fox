<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	private $is_frontend = 1;
	public $id;
	public $id_parent = 0;
	public $id_grandparent = 0;
	public $id_grandgrand = 0;
	public $id_news = false;
	public $content_type;
	public $settings;
	public $class;
	public $is_mobile = false;
	public $site_name = '';
	public $dir = '';
	public $languages;
	public $msg = array();
	public $user = false;
	
	public $title = '';
	public $mod_id = null;
	public $mod_name = '';
	public $mod_dir = '';
	public $mod_table = '';
	public $mod_desc = '';
	public $mod_referer = '';
	public $default_url = '';
	public $redirect_url = '';
	public $params;
	public $model;
	
	public $meta_title = '';
	public $meta_description = '';
	public $meta_keywords = '';
	public $og_title = '';
	public $og_description = '';
	public $og_image = '';
	public $og_url = '';
	public $og_site_name = '';

	public function __construct() {
		parent::__construct();
		$this->neocms->start();
	}
	
	public function generate_view($module, $data, $static = 0, $get_source = 0, $view = 'index') {
		$data['is_mainpage'] = ($this->id == config_item('main_art') && $this->uri->rsegment(1) == 'articles');

		$this->dir = $this->is_mobile ? 'mobile/' : config_item('template_f');
		$views_path = 'frontend/'.$this->dir;
		$data['is_news'] = ($this->id_news !== false);

		if($static == 0){
			if(!isset($data['header'])) $data['header'] = '';

			$data['menu_top'] = $this->frontend->get_menu(array('menu_type'=>'top'));
			$data['menu_langs'] = $this->load->view($views_path.'includes/menu_langs_view', array(), true);
			if(config_item('breadcrumbs_enabled')){
				$data['breadcrumbs'] = $this->frontend->get_breadcrumbs();
			}
			$data['header'] = $this->frontend->get_header($data['header']);
			$data['footer'] = $this->load->view('frontend/'.$this->dir.'includes/footer_view', array(), true);
			$data['content'] = $this->frontend->get_content($module, $data);
			$data['cookie_alert'] = $this->frontend->get_cookie_alert();


			$slides = $this->frontend->get_slides($this->id);
			if($slides !== FALSE){
				$data['slider'] = $this->load->view($views_path.'includes/slider_view', array('slides' => $slides), true);
			}
			$data['popup'] = $this->frontend->get_popup();
		}
		
		if($get_source == 0){
			$data['meta'] = $this->frontend->get_meta();
			$data['stats_script'] = $this->frontend->get_stats_script();

			if ($data['is_mainpage']){
				return $this->load->view($views_path.'intro_view', $data);
			}

			if($static == 0) $this->load->view($views_path.$view.'_view', $data);
			else $this->load->view($views_path.$module, $data);
		} else {
			if($static == 0) return $this->load->view($views_path.$view.'_view', $data, true);
			else return $this->load->view($views_path.$module, $data, true);
		}
	}

	public function generate_view_admin($module, $module_data, $static = 0, $get_source = 0, $view = 'index'){
		if($static == 0){
			$this->admin->save_mod_referer();
			
			$components = $this->admin->get_components();

			$title = '';
			if(isset($this->title)) $title .= $this->title;
			$title = 'CMS'.(!empty($title) ? ' : ' : '').$title;
			
			
			$data = array(
				'meta_title' => strip_tags($title),
				'components' => $components,
				'module_template' => $module,
				'module_data' => $module_data,
			);
			
			//nie wyświetlamy treści modułu, jeśli nie został on dodany w CMS
			if(isset($this->mod_id) || $this->neocms->get_flag('error_404') === TRUE) {
				$data['module_content'] = $this->load->view('admin/'.config_item('template_a').'/'.$module, $module_data, TRUE);
			} else {
				$data['module_content'] = '';
			}
		}

		$views_path = 'admin/'.config_item('template_a');
		if($get_source == 0){
			if($static == 0) $this->load->view($views_path.$view.'_view', $data);
			else $this->load->view($views_path.$module, $module_data);
		}else {
			if($static == 0) return $this->load->view($views_path.$view.'_view', $data, true);
			else return $this->load->view($views_path.$module, $module_data, true);
		}
	}
	
	/**
	 * Zmiana statusu
	 * 
	 * Ta funkcja nie może mieć parametrów, jedyna możliwość to przypisanie do $this->params
	 */
	public function vswitch() {
		$id = null;
		$status = null;
		$column = null;
	
		if(@is_numeric($this->params->id)) {
			$id = $this->params->id;
		}
		if(@is_numeric($this->params->set)) {
			$status = $this->params->set;
		}
		if(empty($this->params->column)){
			$this->params->column = 'pub';
		}
		$column = $this->params->column;
	
		if($this->neocms->pub($id,$status,$this->mod_table,$column)) {
			$msg = array(lang("Zmiany zostały zapisane."), 0);
		} else {
			$msg = array(lang("Zmiany nie zostały zapisane."), 1);
		}
	
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
	/**
	 * Zmiana kolejności
	 *
	 * Ta funkcja nie może mieć parametrów, jedyna możliwość to przypisanie do $this->params
	 */
	public function position()
    {
    	$output = $this->neocms->positions($this->mod_table);
    	if($output > 0){
    		$msg = array(lang("Kolejność została zaktualizowana."),0);
    	}elseif($output == 0){
    		$msg = array(lang("Nie znaleziono wpisów wymagających zapisanie pozycji."),1);
    	} else{
    		$msg = array(lang("Wystąpił błąd podczas zapisywania pozycji."),1);
    	}
        
        $this->neocms->forward_msg($msg);
        redirect($this->admin->get_redirect_url());
    }

	private function _router() {
		if($this->uri->segment(1, 'none') == 'admin'  && !in_array($this->uri->segment(2, 'none'), array('login', 'remind', 'confirm'))) {
			if(!$this->admin->is_logged()) {
				$this->admin->save_mod_referer('after_login');
				redirect('admin/login');
			} else {
				$module = @$this->mod_name;
				$module_dir = @$this->mod_dir;
				
				if(empty($module)) redirect('admin');

				if(!empty($module) && $module!='com') {
					$id_m = @$this->mod_id;

					if(!$this->admin->check_rights($id_m)) {
						$this->neocms->forward_msg(lang("Nie posiadasz uprawnień do wybranego modułu."), 2);
						redirect('admin');
					}
				}
			}
		}
		else if($this->uri->segment(1, 'none') == 'frontend') redirect(base_url());
		else if(($this->uri->segment(2, 'none') == 'login')
			 && $this->uri->segment(3, 'none') != 'logout'
			 && $this->admin->is_logged()) redirect('admin');
	}

	protected function _init_module($file, $dir) {
		$this->mod_name = strtolower(basename($file, ".php"));
		$this->mod_dir = basename($dir).'/';
		$path = $this->mod_dir.$this->mod_name;
		
		$this->db->select('m.id, m.id_c, m.mod_function, mc.title, mc.description, m.mod_table, cc.name as component');
		$this->db->from('modules as m');
		$this->db->join('modules_content as mc', "m.id = mc.id_m AND mc.lang = '".$this->admin->get_session_lang()."'", 'left');
		$this->db->join('components_content as cc', "m.id_c = cc.id_c AND cc.lang = '".$this->admin->get_session_lang()."'", 'left');
		$this->db->where('m.mod', $this->mod_name);
		$this->db->where('m.mod_dir', $this->mod_dir);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$row = $query->row();
			if(!empty($row->title)){
				$this->title = $row->title;
			} else {
				$this->title = $this->mod_name;
			}
			$this->mod_id = $row->id;
			$this->mod_title = $row->title; //potrzebne do breadcrumbs, a $this->title jest nadpisywane w kontrolerach
			$this->mod_table = $row->mod_table;
			$this->mod_desc = $row->description;
			$this->mod_function = $row->mod_function;
			$this->mod_id_component = $row->id_c;
			$this->mod_component = $row->component;
			$this->mod_url = 'admin/'.$path;
			
			$this->admin->set_default_url($this->mod_url.'/'.$row->mod_function);
			$this->admin->set_mod_referer($this->mod_id);
			$this->admin->set_redirect_url($this->mod_id);
			
			$this->params = (object)$this->uri->uri_to_assoc();
			
			try {
				$model_name = $this->mod_name.'_model';
				$this->load->model($this->mod_url.'_model');
				if(isset($this->{$model_name})) {
					$this->model = $this->{$model_name};
				} else {
					throw new Exception("{$model_name} was loaded, but is not available through CI object");
				}
			}catch(RuntimeException $e) {}
		} else {
			$this->neocms->set_msg(sprintf(lang('Moduł <b>%s</b> nie został skonfigurowany w CMS i może nie działać poprawnie. Przejdź do zarządzania modułami, aby go dodać.'), $path), 1);
		}
		$this->_router();
	}
}