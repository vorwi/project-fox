<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Output extends CI_Output {

	public function __construct(){
		parent::__construct();
	}
	
	public function display_json($data) {
		$this->enable_profiler(FALSE);
		$this->set_header('Content-type: application/json; charset=UTF-8');
		try {
			$json = json_encode($data, JSON_NUMERIC_CHECK);
		} catch(Exception $e) {
			log_message('Output::display_json - '.$e->getMessage());
		}
		$this->set_output($json);
		$this->_display();
		exit();
	}
}