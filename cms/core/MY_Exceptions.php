<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions{
	function log_exception($severity, $message, $filepath, $line){
		$current_reporting = error_reporting();
		$should_report = $current_reporting & $severity;

		if($should_report){
			// call the original implementation if we should report the error
			parent::log_exception($severity, $message, $filepath, $line);
		}
	}
}