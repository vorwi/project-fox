<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Router extends CI_Router {

	public function __construct(){
		parent::__construct();
	}
	
	protected function _set_default_controller(){
		$segments = explode('/', $this->default_controller);
		$c = count($segments);
		$directory_override = isset($this->directory);
		
		while ($c-- > 0){
			$test = $this->directory
				.ucfirst($this->translate_uri_dashes === TRUE ? str_replace('-', '_', $segments[0]) : $segments[0]);

			if ( ! file_exists(APPPATH.'controllers/'.$test.'.php')
				&& $directory_override === FALSE
				&& is_dir(APPPATH.'controllers/'.$this->directory.$segments[0])
			){
				$this->set_directory(array_shift($segments), TRUE);
				continue;
			}
			$this->default_controller = strtolower($segments[0]).(isset($segments[1]) ? '/'.$segments[1] : '');
		}
		parent::_set_default_controller();
	}
}