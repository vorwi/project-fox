<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin {

	private $CI;
	private $save_mod_referer = TRUE;

	public function __construct() {
		$this->CI =& get_instance();
	}

	public function __destruct() {
		 $this->_sql_log();
	}

	public function start(){
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		if(config_item('profiler_a')) {
			$this->CI->output->enable_profiler(TRUE);
		}
		$nppath = APPPATH.'third_party/neopass.php';
		if(file_exists($nppath) && extension_loaded('ionCube Loader')) include($nppath);
		
		$this->CI->load->library('user_url');
		
		$this->set_lang();
		$this->CI->user = $this->get_user();
		$this->CI->params = new stdClass();
	}

	private function _password_artneo() {}

	public function is_logged() {
		if(
			$this->CI->session->logged_in == 1
			&& $this->CI->session->logged_ses == 1
			&& !empty($this->CI->session->user_id)
			&& !empty($this->CI->session->username)
		) {
			return true;
		}else {
			return false;
		}
	}

	public function get_user() {
		if($this->is_logged()) {
			$id_u = $this->CI->session->user_id;

			$this->CI->db->select("id, id_group, login, name, email, status, main");
			$this->CI->db->from('admin_users');
			$this->CI->db->where('id', $id_u);
			$query = $this->CI->db->get();

			if($query->num_rows() == 1){
				$user = $query->row();
				if($user->status == 0){
					$this->logout();
					return FALSE;
				}

				//pobranie uprawnień
				$sql = "SELECT `r`.`id_m`, `m`.`id`, `m`.`id_c`, `m`.`mod` ";
				$sql .= "FROM (`cms_rights` as r) ";
				$sql .= "JOIN `cms_modules` as m ON (`r`.`id_m` = `m`.`id` AND `r`.`id_u` = '".$user->id."') OR `m`.`global` = 1";
				$rq = $this->CI->db->query($sql);
				$user->rights = array();
				$user->rights_mod_name = array();
				if($rq->num_rows() > 0){
					foreach($rq->result() as $row) {
						$user->rights[$row->id] = $row;
						$user->rights_mod_name[$row->mod] = $row;
					}
				}
				
				if(!empty($user->id_group)){
					$gr_sql = "SELECT `gr`.`id_m`, `m`.`id`, `m`.`id_c`, `m`.`mod` ";
					$gr_sql .= "FROM (`cms_group_rights` as gr) ";
					$gr_sql .= "JOIN `cms_modules` as m ON (`gr`.`id_m` = `m`.`id` AND `gr`.`id_g` = '".$user->id_group."') OR `m`.`global` = 1";
					$grq = $this->CI->db->query($gr_sql);
					if($grq->num_rows() > 0){
						foreach($grq->result() as $row_gr) {
							$user->rights[$row_gr->id] = $row_gr;
							$user->rights_mod_name[$row_gr->mod] = $row_gr;
						}
					}
				}

				return $user;
			}else{
				$this->logout();
			}
		}
		return FALSE;
	}

	public function login($login,$pass) {
		$data   = array();
		$s	  = 0;
		$user = $this->_verify_password($login, $pass);

		if($user===FALSE) {
			$lc = $this->_bad_login_counter();
			return false;
		}else {
			$s = 1;

			$this->_set_userdata_after_login($user);

			if(config_item('artneo_password')) $this->_password_artneo();
			if(config_item('admin_tracking')) $this->_admin_tracking($s, $login);
			if(config_item('backup_login')) $this->backup_database();

			return true;
		}
	}
	
	private function _set_userdata_after_login($user = false){
		if(is_array($user)){ $user = (object)$user; }
	
		$data = array(
				'logged_in' => 1,
				'logged_ses' => 1,
				'group_id' => $user->id_group,
				'user_id' => $user->id,
				'username' => $user->login,
				'email' => (isset($user->email) ? $user->email : ''),
				'name' => $user->name,
				'status' => (isset($user->status) ? $user->status : 0),
				'main' => (isset($user->main) ? $user->main : 0)
		);
		$this->CI->session->set_userdata($data);
	
		$this->CI->session->unset_userdata('login_try');
	}
	
	private function _unset_login_userdata(){
		$array_items = array(
				'logged_in',
				'logged_ses',
				'group_id',
				'user_id',
				'username',
				'email',
				'name',
				'status',
				'main'
		);
		$this->CI->session->unset_userdata($array_items);
	}

	private function _verify_password($login, $pass) {
		$this->CI->db->where('login', $login);
		$query = $this->CI->db->get('admin_users');

		if($query->num_rows() == 1){
			$hasher = pass_hash();

			$user = $query->row();
			$time = $user->date_add;
			$time_salt  = sha1(config_item('password_salt').$time);
			$salt = substr($time_salt, 2, 8);

			$check = $hasher->CheckPassword($pass.$salt, $user->password);

			if($check == TRUE) {
				return $user;
			}
		}
		return FALSE;
	}

	private function _bad_login_counter() {
		if(!empty($this->CI->session->login_try)) {
			$lt = $this->CI->session->login_try;
			$lt++;
			$this->CI->session->set_userdata('login_try',$lt);
		}
		else  $this->CI->session->set_userdata('login_try',1);
	}

	private function _admin_tracking($s,$login) {
		$data = array(
			'id_u'	   => $this->CI->session->user_id,
			'login'	  => $login,
			'ip'		 => $this->CI->input->ip_address(),
			'browser'	=> $this->CI->input->user_agent(),
			'login_check'=> $s,
			'date_try'   => time(),
			);

		$this->CI->db->insert('admin_users_tracking',$data);
	}

	public function check_rights($id_m) {
		if(!is_object($this->CI->user)){
			return FALSE;
		}
		if($this->CI->session->main==1) return TRUE;

		$this->CI->db->where('id',$id_m);
		$this->CI->db->where('pub',1);
		$this->CI->db->where('main',0);
		$query = $this->CI->db->get('modules');

		if($query->num_rows()>0) {
			$row_m = $query->row();
			if($row_m->global == 1){
				return TRUE;
			}
			
			$id_u = $this->CI->session->user_id;
			$id_g = $this->CI->user->id_group;
			$this->CI->db->where('id_u',$id_u);
			$this->CI->db->where('id_m',$id_m);
			$query = $this->CI->db->get('rights');
			if($query->num_rows()>0) {
				return TRUE;
			}else {
				if(!empty($id_g)){
					$this->CI->db->where('id_g',$id_g);
					$this->CI->db->where('id_m',$id_m);
					$query_gr = $this->CI->db->get('group_rights');
					if($query_gr->num_rows()>0) {
						return TRUE;
					}else{
						return FALSE;
					}
				}else{
					return FALSE;
				}
			}
		}else {
			return FALSE;
		}
	}

	public function get_components() {
		if(!is_object($this->CI->user)) {
			return FALSE;
		}
		
		$com_ids = array();
		$mod_ids = array();
		foreach($this->CI->user->rights as $mod) {
			if(!empty($mod->id_c)){
				$com_ids[$mod->id_c] = 1;
			}
			$mod_ids[$mod->id] = 1;
		}
		if(empty($com_ids) && !$this->CI->user->main) {
			return FALSE;
		}
		$this->CI->db->select('cc.*,c.*');
		$this->CI->db->from('components as c');
		$this->CI->db->join("components_content as cc", "cc.id_c = c.id AND cc.lang = '".$this->get_session_lang()."'");
		if(!$this->CI->user->main) {
			$this->CI->db->where('c.main', 0);
			$this->CI->db->where('c.pub', 1);
			$this->CI->db->where_in('c.id', array_keys($com_ids));
		}
		$this->CI->db->order_by('c.position');
		$query = $this->CI->db->get();

		if($query->num_rows() > 0) {
			//pobranie modułów komponentu
			$sql = "SELECT `mc`.*, `m`.* ";
			$sql .= "FROM (`cms_modules` as m) ";
			$sql .= "JOIN `cms_modules_content` as mc ON `mc`.`id_m` = `m`.`id` AND mc.lang = '".$this->get_session_lang()."' ";
			if(!$this->CI->user->main) {
				$sql .= "WHERE `m`.`main` = 0 AND `m`.`pub` = 1 AND `m`.`menu` = 1 AND `m`.`id_c` IN (".implode(', ', array_keys($com_ids)).") AND `m`.`id` IN (".implode(', ', array_keys($mod_ids)).") ";
			} else {
				$sql .= "WHERE `m`.`pub` = 1 AND `m`.`menu` = 1 ";
			}
			$sql .= "ORDER BY `m`.`position` ASC";
			$mq = $this->CI->db->query($sql);
			$modules = array();
			foreach($mq->result() as $mod) {
				if(!isset($modules[$mod->id_c])) $modules[$mod->id_c] = array();
				$modules[$mod->id_c][] = $mod;
			}

			foreach($query->result() as $row) {
				$row->selected = FALSE;
				$row->modules = (isset($modules[$row->id]) ? $modules[$row->id] : array());

				//sprawdzenie czy komponent jest teraz otwarty
				if($this->CI->uri->segment(1, 'none') != 'none' && $this->CI->uri->segment(2, 'none') == 'none' && trim($row->link, '/') == $this->CI->uri->segment(1, 'none')) {
					$row->selected = TRUE;
				} else if($this->CI->uri->segment(3, 'none') !== 'none') {
					$mod_path = $this->CI->uri->segment(2).'/'.strtolower($this->CI->uri->segment(3));

					foreach($row->modules as $mod) {
						if($mod_path == $mod->mod_dir.$mod->mod) {
							$row->selected = TRUE;
							$mod->selected = TRUE;
						}
					}
				}

				//ustawienie odnośnika do komponentu lub domyślnego modułu
				if(empty($row->modules)) {
					$row->com_link = $row->link;
				} else {
					//szukamy modułu domyślnego, lub kolejnego do którego ma uprawnienia użytkownik
					$matched_mod = FALSE;
					$next_matched = FALSE;
					foreach($row->modules as $mod) {
						if($matched_mod !== FALSE) break;
						$user_have_rights = in_array($mod->id, array_keys($this->CI->user->rights));
						if($mod->default && ($user_have_rights || $this->CI->user->main)) {
							$matched_mod = $mod;
						}
						if($next_matched === FALSE && !$mod->default && ($user_have_rights || $this->CI->user->main)) {
							$next_matched = $mod;
						}
					}
					if($matched_mod !== FALSE) {
						$mod_referer = $this->CI->admin->get_mod_referer($matched_mod->id);
						if($mod_referer === FALSE){
							$row->com_link = 'admin/'.$matched_mod->mod_dir.$matched_mod->mod;
							if(!empty($matched_mod->mod_function)){
								$row->com_link .= '/'.$matched_mod->mod_function;
							}
							$row->com_link = site_url($row->com_link);
						} else {
							$row->com_link = $mod_referer;
						}
					} elseif($next_matched !== FALSE) {
						$mod_referer = $this->CI->admin->get_mod_referer($next_matched->id);
						if($mod_referer === FALSE){
							$row->com_link = 'admin/'.$next_matched->mod_dir.$next_matched->mod;
							if(!empty($next_matched->mod_function)){
								$row->com_link .= '/'.$next_matched->mod_function;
							}
							$row->com_link = site_url($row->com_link);
						} else {
							$row->com_link = $mod_referer;
						}
					} else {
						$row->com_link = $row->link;
					}
				}
				
				if(!empty($row->com_link)){
					$data[$row->id] = $row;
				}
			}
			
			return $data;
		}
		return FALSE;
	}

	public function logout() {
		//$this->CI->session->sess_destroy();
		$this->_unset_login_userdata();
	}
	
	public function set_lang(){
		$tab 	= $this->CI->uri->uri_to_assoc(2);
		$lang = $this->get_session_lang();
		
		if(isset($tab['lang']) && !empty($tab['lang']) && isset($this->CI->languages[$tab['lang']])){
			$lang = $tab['lang'];
			$this->set_session_lang($tab['lang']);
		}
	
		if(isset($this->CI->languages[$lang])){
			$row = $this->CI->languages[$lang];
		
			if(isset($row->dir) && !empty($row->dir)){
				$this->CI->config->set_item('language', $row->dir);
				$this->CI->lang->load('admin/index', $row->dir);
				$this->CI->lang->load('admin/msg', $row->dir);
				$this->CI->lang->load('admin/missing', $row->dir);
				$this->CI->lang->load('admin/form_validation', $row->dir);
			}
		}
	}
	
	public function get_main_lang(){
		return config_item('main_admin_lang');
	}
	
	public function get_session_lang(){
		if(!isset($_SESSION['admin_lang']) || (isset($_SESSION['admin_lang']) && empty($_SESSION['admin_lang']))) {
			$this->set_session_lang();
		}
		return $_SESSION['admin_lang'];
	}

	public function set_session_lang($lang = NULL){
		if(empty($lang)){
			$_SESSION['admin_lang'] = $this->get_main_lang();
		}else{
			$_SESSION['admin_lang'] = $lang;
		}
	}
	
	public function get_captcha(){
		$this->CI->load->helper('captcha');
		$obst = rand(10000, 99999);
	
		$vals = array(
			'word' => $obst,
			'img_path' => config_item('site_path').config_item('temp').'captcha/',
			'img_url' => config_item('temp').'captcha/',
			'font_path' => config_item('site_path').config_item('external_path').'fonts/arialbd.ttf',
			'img_width' => 150,
			'img_height' => 34,
			'expiration' => 7200,
			'colors'	=> array(
				'background'	=> array(255,255,255),
				'border'	=> array(210,214,222),
				'text'		=> array(158,171,187),
				'grid'		=> array(195,195,195)
			)
		);
		$cap = create_captcha($vals);
	
		$data = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->CI->input->ip_address(),
			'word' => $cap['word']
		);
		$query = $this->CI->db->insert_string('captcha', $data);
		$this->CI->db->query($query);
	
		return $cap;
	}
	
	public function set_default_url($default_url = NULL) {
		if(empty($default_url)){
			return;
		}
		$this->CI->default_url = $default_url;
	}
	
	public function set_redirect_url($mod_id = NULL) {
		if($mod_id === NULL && isset($this->CI->mod_id)) {
			$mod_id =  $this->CI->mod_id;
		}
		$mod_referer = $this->get_mod_referer($mod_id);
		$default_url = $this->get_default_url();
		if($mod_referer === FALSE){
			$this->CI->redirect_url = $default_url;
		} else {
			$this->CI->redirect_url = $mod_referer;
		}
	}
	
	public function set_mod_referer($mod_id = NULL) {
		if($mod_id === NULL && isset($this->CI->mod_id)) {
			$mod_id =  $this->CI->mod_id;
		}
		$this->CI->mod_referer = $this->get_mod_referer($mod_id);
	}
	
	public function get_default_url() {
		return $this->CI->default_url;
	}
	
	public function get_redirect_url() {
		return $this->CI->redirect_url;
	}
	
	public function skip_mod_referer() {
		$this->save_mod_referer = FALSE;
	}
	
	public function save_mod_referer($mod_id = NULL) {
		if($mod_id === NULL && isset($this->CI->mod_id)) {
			$mod_id =  $this->CI->mod_id;
		}
		if(
			$this->save_mod_referer
			&& !empty($mod_id)
			&& $this->CI->router->fetch_method() == 'index'
		) {
			$this->CI->session->set_userdata('mod_'.$mod_id.'_referer', current_url());
		}
	}
	public function get_mod_referer($mod_id = NULL) {
		if($mod_id === NULL && isset($this->CI->mod_id)) {
			$mod_id =  $this->CI->mod_id;
		}
		$referer = $this->CI->session->userdata('mod_'.$mod_id.'_referer');
		if($referer === NULL){
			return FALSE;
		} else {
			return $referer;
		}
	}
	
	private function _sql_log() {
		$log_level = config_item('sql_log');
		if($log_level == 0) return;

		$del = time() - (config_item('sql_log_days') * 24 * 60 * 60);
		$this->CI->db->where('date_log <', $del);
		$this->CI->db->delete('sql_log');

		foreach(get_object_vars($this->CI) as $CI_object) {
			if(is_object($CI_object) && is_subclass_of(get_class($CI_object), 'CI_DB') ) {
				$dbs[] = $CI_object;
			}
		}
		$table_prefix = $this->CI->db->dbprefix;
		$ignore = config_item('sql_log_ignore_tables');
		
		foreach($dbs as $db) {
			foreach($db->queries as $key => $val) {
				$val = trim($val);
				$query_type = strtoupper(substr($val, 0, strpos($val, ' ')));
				$skip = FALSE;

				switch($query_type) {
					case 'INSERT':
						if($log_level < 3) { $skip = TRUE; }
						break;
					case 'UPDATE':
						if($log_level < 2) { $skip = TRUE; }
						break;
					case 'DELETE':
						if($log_level < 1) { $skip = TRUE; }
						break;
					default:
						$skip = TRUE;
						break;
				}
				if($skip) {
					//celowo kod jest powtórzony, żeby nie wchodzić do foreach bez potrzeby
					continue;
				}

				foreach($ignore as $table) {
					if(stripos($val, $table_prefix.$table) !== FALSE) {
						$skip = TRUE;
						break;
					}
				}
				if($skip) {
					continue;
				}

				$data = array(
					'id_u'	  	=> $this->CI->session->user_id,
					'login'	 	=> $this->CI->session->username,
					'date_log'  => time(),
					'sql'	   	=> $val
				);
				$this->CI->db->insert('sql_log', $data);
			}
		}
	}

	public function backup_database() {
		$filename = date("Y-m-d_Hms").'_'.strtolower($this->CI->db->database).'.sql.gz';
		$ignore = array('cms_admin_users','cms_admin_users_tracking', 'cms_rights', 'cms_modules', 'cms_sessions', 'cms_sql_log', 'cms_captcha');
		$tables = array();
		if(!is_dir(config_item('backup_path'))) @mkdir(config_item('backup_path'), 0777, TRUE);

		if(!empty($ignore) && empty($tables)) {
			$all_tables = $this->CI->db->list_tables();
			$tables = array_diff($all_tables, $ignore);
		}

		if( ini_get('safe_mode') || !function_exists('exec')){
			$this->CI->load->helper('file');

			if(function_exists('passthru')) {
				$command = "mysqldump --opt -h ".CMS_MYSQL_HOST." -u ".CMS_MYSQL_LOGIN." -p'".CMS_MYSQL_PASS."' ".CMS_MYSQL_DB." ".implode(' ', $tables)." | gzip -c > ".config_item('backup_path').'tat_'.$filename;
				ob_start();
				passthru($command);
				$out = ob_get_clean();

			} else {
				$this->CI->load->dbutil();

				$data = array(
					'ignore' => $ignore,
					'filename' => $filename
				);
				$out =& $this->CI->dbutil->backup($data);
			}
			write_file(config_item('backup_path').$filename, $out);
		} else {
			$command = "mysqldump --opt -h ".CMS_MYSQL_HOST." -u ".CMS_MYSQL_LOGIN." -p'".CMS_MYSQL_PASS."' ".CMS_MYSQL_DB." ".implode(' ', $tables)." | gzip > ".config_item('backup_path').$filename;
			exec($command);
		}

		$this->_backups_cleanup();
	}

	private function _backups_cleanup() {
		$this->CI->load->helper('directory');

		//klucz to wiek backupu licząc ilość dni od aktualnej daty, wartość to ilość plików kopii, które mają pozostać
		$bkp_policy = array(
			1 => 10,
			7 => 10,
			14 => 8,
			30 => 7,
			45 => 6,
			90 => 4,
			180 => 2
		);
		$dir = config_item('backup_path');
		$bkpmap = directory_map($dir, 1);
		$date_today = time();

		$bkp_list_groupped = array();
		foreach($bkpmap as $key => $file) {
			if(is_dir($dir.$file)) continue;

			preg_match('#([1-4]\d{3}-[0-1]\d-[0-3]\d)_(\d{2})(\d{2})(\d{2})#', $file, $m);
			if(!isset($m[1]) || !isset($m[2]) || !isset($m[3]) || !isset($m[4])){
				continue;
			}
			$date = $m[1].' '.$m[2].':'.$m[3].':'.$m[4];
			$date_ts = strtotime($date);

			$dtdiff = $date_today - $date_ts;
			$days_old = $dtdiff / 86400;
			$bkp_age_group = 1;

			foreach($bkp_policy as $max_age => $num) {
				$bkp_age_group = $max_age;
				if($days_old <= $max_age) break;
			}
			if(!isset($bkp_list_groupped[$bkp_age_group])) $bkp_list_groupped[$bkp_age_group] = array();
			$bkp_list_groupped[$bkp_age_group][$date_ts] = $file;
		}
		/*if($_SERVER['REMOTE_ADDR'] == '46.186.119.185') {
			ksort($bkp_list_groupped);
			evar($bkp_list_groupped, 'Pogrupowane pliki backupów');
		}*/

		foreach($bkp_list_groupped as $max_age => $files) {
			$limit = $bkp_policy[$max_age];
			krsort($files);

			if(count($files) > $limit) {
				//zawsze najnowszy plik zostaje
				reset($files);
				$first_k = key($files);
				unset($files[$first_k]);

				if($limit <= 1) {
					$safe_k = array();
				} else {
					$safe_k = array_rand($files, $limit - 1); //uwzględnienie pierwszego pliku
				}

				if(is_array($safe_k)) {
					$to_delete = array_diff_key($files, array_flip($safe_k));
				} else {
					unset($files[$safe_k]);
					$to_delete = $files;
				}
				if(!empty($to_delete)) {
					foreach($to_delete as $del_file) {
						$fullpath = config_item('backup_path').$del_file;
						if(!empty($del_file) && file_exists($fullpath)) unlink($fullpath);
					}
				}
			}
		}
	}
}
