<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Neocms {

	private $CI;
	private $admin_flags = array();

	public function __construct() {
		$this->CI =& get_instance();
	}

	public function start(){
		$tab = config_item('error_delimiters');
		$this->CI->form_validation->set_error_delimiters($tab['start'], $tab['end']);

		$pattern = '/^www\.(.*?)/';
		if(CMS_REDIRECT == 'bez' && preg_match($pattern, $_SERVER['HTTP_HOST'])) {
		  redirect('http://'.preg_replace($pattern, '$1', $_SERVER['HTTP_HOST']).$_SERVER['REQUEST_URI'], 'location', 301);
		} elseif(CMS_REDIRECT == 'www' && !preg_match($pattern, $_SERVER['HTTP_HOST'])) {
		  redirect('http://www.'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], 'location', 301);
		}
		
		$this->CI->languages = $this->get_languages();		
		
		if($this->CI->uri->segment(1) == 'admin'){
			$this->CI->is_frontend = 0;
			$this->CI->load->library('admin');
			$this->CI->admin->start();
		} else {
			$this->CI->is_frontend = 1;
			$this->CI->load->library('frontend');
			$this->CI->frontend->start();
		}

		$this->CI->settings = $this->get_settings();

		if(config_item('mobile_site_enabled')) {
			if (preg_match('#^(www\.)?m\.#i', $_SERVER['HTTP_HOST'])) {
				$this->CI->is_mobile = true;
				$this->CI->site_name = preg_replace('/^(www\.)?m\.(.+)/i', '$2', $_SERVER['HTTP_HOST']);
			} else {
				$this->CI->site_name = $_SERVER['HTTP_HOST'];
			}

			include_once('external/Mobile_Detect.php');
			$detect = new Mobile_Detect;

			if ($detect->isMobile() && !isset($_SESSION['mobile_redir']) && !$detect->isTablet()) {
				$_SESSION['mobile_redir'] = true;
				redirect('http://m.' . $this->CI->site_name . $_SERVER['REQUEST_URI']);
			}
		}
	}

	public function get_settings(){
		$lang = (isset($_SESSION['lang']) ? $_SESSION['lang'] : config_item('main_lang'));

		$this->CI->db->group_start();
		$this->CI->db->where('lang', $lang);
		$this->CI->db->or_where('lang IS NULL', NULL, FALSE);
		$this->CI->db->group_end();
		if($this->CI->is_frontend) $this->CI->db->where('pub', 1);
		$query = $this->CI->db->get('settings');

		if($query->num_rows()>0){
			$data = new stdClass();
			foreach($query->result() as $row){
				$data->{$row->name} = $row->value;
			}
			return $data;

		}
		else return FALSE;
	}

	public function get_languages() {
		$this->CI->db->where('pub',1);
		$this->CI->db->order_by('position');
		$query = $this->CI->db->get('lang');

		if($query->num_rows()>0){
			$data = array();
			foreach ($query->result() as $row) {
				$data[$row->short] = $row;
			}
			return $data;
		}
		return FALSE;
	}
	
	public function get_language($short) {
		if(isset($this->CI->languages[$short])){
			return $this->CI->languages[$short];
		}else {
			return FALSE;
		}
	}

	public function positions($table) {
		$post = $this->CI->input->post('position');
		$rows = array();
	
		if(!empty($post)){
			foreach($post as $key => $row){
				$rows[] = array(
						'id' => $key,
						'position' => $row
				);
			}
		}
	
		if(!empty($rows)){
			$affected_rows = $this->CI->db->update_batch($table, $rows, 'id');
			return $affected_rows;
		}else{
			return 0;
		}
	}

	public function pub($id,$status,$table,$column) {
		if(!is_numeric($id)) $post = $this->CI->input->post('check');
		else $post = $id;
		if(empty($post)) return FALSE;

		$this->CI->db->set($column, $status);
		$this->CI->db->where_in('id', $post);
		$query = $this->CI->db->update($table);

		if($this->CI->db->affected_rows()>0) return TRUE;
		return FALSE;
	}
	
	public function insert_lang_content($table, $data) {
		$languages = $this->CI->languages;
		$affected_rows = 0;
		foreach ($languages as $lang){
			$data['lang'] = $lang->short;
			$this->CI->db->insert($table, $data);
			if($this->CI->db->affected_rows()>0){
				$affected_rows++;
			}
		}
		if($affected_rows>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function get_all_categories($cat=null) {
		if(is_numeric($cat)) $this->CI->db->where('id', $cat);
		$this->CI->db->order_by('position');
		$query = $this->CI->db->get('articles_category');

		if($query->num_rows()>0){
			$data = array();
			foreach($query->result() as $row){
				$data['categories'][$row->id] = $row;
				$data['categories'][$row->id]->articles = $this->get_all_articles($row->id);
			}

			return $data;
		}
		return FALSE;
	}

	public function get_all_articles($id_cat = FALSE, $max_depth = 4) {
		$articles_tree = $this->get_articles_tree($id_cat, $max_depth);

		if($articles_tree !== FALSE && !empty($articles_tree[0])) {
			$data = array();
			foreach($articles_tree[0] as $row) {
				$data[] = $row;
				if(isset($articles_tree[1][$row->id])) {
					foreach($articles_tree[1][$row->id] as $row_l2) {
						$data[] = $row_l2;
						if(isset($articles_tree[2][$row_l2->id])) {
							foreach($articles_tree[2][$row_l2->id] as $row_l3) {
								$data[] = $row_l3;
								if(isset($articles_tree[3][$row_l3->id])) {
									$data = array_merge($data, $articles_tree[3][$row_l3->id]);
								}
							}
						}
					}
				}
			}

			return $data;
		}
		return FALSE;
	}

	private function get_articles_tree($id_cat, $max_depth, $id_tree = 0, $level = 0) {
		static $result = array();

		$this->CI->db->select('ac.*, a.*'); //celowo w odwrotnej kolejności, żeby ID z articles nadpisało articles_content
		$this->CI->db->from('articles as a');
		$this->CI->db->join('articles_content as ac', 'a.id = ac.id_art');
		if(is_array($id_tree)) {
			$this->CI->db->where_in('a.id_tree', $id_tree);
		} else {
			$this->CI->db->where('a.id_tree', 0);
		}
		if(is_numeric($id_cat)) {
			$this->CI->db->where('a.id_cat', $id_cat);
		}
		$this->CI->db->where('ac.lang', config_item('main_lang'));
		$this->CI->db->order_by('a.id_cat');
		$this->CI->db->order_by('a.position');
		$query = $this->CI->db->get();

		if($query->num_rows() > 0) {
			$data[$level] = array();
			$ids = array();
			foreach($query->result() as $row){
				$ids[] = $row->id;
				$row->tree = $level;
				if($id_tree == 0) {
					$data[$level][] = $row;
				} else {
					if(!isset($data[$level][$row->id_tree])) $data[$level][$row->id_tree] = array();
					$data[$level][$row->id_tree][] = $row;
				}
			}
			$query->free_result();

			$level++;
			if($level < $max_depth) {
				$children = $this->get_articles_tree($id_cat, $max_depth, $ids, $level);
				if($children !== FALSE) {
					$data = array_merge($data, $children);
				}
			}

			return $data;
		}
		return FALSE;
	}

	public function set_msg($msg, $type = NULL) {
		if(!is_array($msg)) {
			$msg = array($msg, ifempty($type, 0));
		}
		$this->CI->msg[] = $msg;
	}

	public function forward_msg($msg = FALSE, $type = NULL) {
		if($msg !== FALSE) {
			$this->set_msg($msg, $type);
		}
		if(!empty($this->CI->msg)) {
			$this->CI->session->set_flashdata('msg', $this->CI->msg);
		}
	}
	
	public function set_flag($name, $value) {
		$this->cms_flags[$name] = $value;
	}
	
	public function get_flag($name) {
		if(isset($this->cms_flags[$name])) {
			return $this->cms_flags[$name];
		} else {
			return NULL;
		}
	}
}