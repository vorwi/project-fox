<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Pagination extends CI_Pagination {

	public function __construct() {

		parent::__construct();
	}
	
	public function init($config = array()) {

		if(!isset($config['full_tag_open'])) 	$config['full_tag_open'] 	= '<ul class="pagination">';
		if(!isset($config['full_tag_close'])) 	$config['full_tag_close'] 	= '</ul>';
		if(!isset($config['first_link'])) 		$config['first_link'] 		= lang('pagination_first_link');
		if(!isset($config['first_tag_open'])) 	$config['first_tag_open']   = '<li>';
		if(!isset($config['first_tag_close'])) 	$config['first_tag_close']  = '</li>';
		if(!isset($config['last_link'])) 		$config['last_link']		= lang('pagination_last_link');
		if(!isset($config['last_tag_open'])) 	$config['last_tag_open']	= '<li>';
		if(!isset($config['last_tag_close'])) 	$config['last_tag_close']   = '</li>';
		if(!isset($config['next_link'])) 		$config['next_link']		= lang('pagination_next_link');
		if(!isset($config['next_tag_open'])) 	$config['next_tag_open']	= '<li>';
		if(!isset($config['next_tag_close'])) 	$config['next_tag_close']   = '</li>';
		if(!isset($config['prev_link'])) 		$config['prev_link']		= lang('pagination_prev_link');
		if(!isset($config['prev_tag_open'])) 	$config['prev_tag_open']	= ' <li> ';
		if(!isset($config['prev_tag_close'])) 	$config['prev_tag_close']   = '</li>';
		if(!isset($config['cur_tag_open'])) 	$config['cur_tag_open']	 	= '<li class="active"><a>';
		if(!isset($config['cur_tag_close'])) 	$config['cur_tag_close']	= '</a></li>';
		if(!isset($config['num_tag_open'])) 	$config['num_tag_open']	 	= '<li>';
		if(!isset($config['num_tag_close'])) 	$config['num_tag_close']	= '</li>';
				
		$this->initialize($config); 
	}

	public function get_total_rows() {
		return $this->total_rows;
	}
}
