<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

	public function __construct() {

		parent::__construct();
	}
	
	public function ckeditor_text_max_length($str, $val){
		if (preg_match("/[^0-9]/", $val))
		{
			return FALSE;
		}
	
		$str = trim(strip_tags($str));
	
		if (function_exists('mb_strlen'))
		{
			return (mb_strlen($str) > $val) ? FALSE : TRUE;
		}
	
		return (strlen($str) > $val) ? FALSE : TRUE;
	}
	
 	public function contact_title_check($str){
		
		if (trim($str) == 'Tytuł')
		{
			//$this->form_validation->set_message('_contact_title_check', '%s nie został podany');
			return FALSE;
		}
		else return TRUE;
	}
	
	public function contact_captcha_check($str) {
		
   		$expiration = time()-7200; // Two hour limit
		$this->CI->db->where('captcha_time < '.$expiration);
		$this->CI->db->delete('captcha');
		
		$this->CI->db->select('COUNT(*) AS count');
		$this->CI->db->from('captcha');
		$this->CI->db->where("word = ".$this->CI->db->escape($str)." AND ip_address = '".$this->CI->input->ip_address()."' AND captcha_time > '".$expiration."'");
		$query = $this->CI->db->get();
		
		$row = $query->row();

		if ($row->count == 0)
		{
			//$this->form_validation->set_message('_contact_captcha_check', '%s jest niepoprawny.');
			return FALSE;
		}
		else return TRUE;
	}
	
	public function duplicate_login_check($login) {
	
		$this->CI->db->where('login',$login);
		$query = $this->CI->db->get('admin_users');
		return $query->num_rows() == 0;
	}
	
	public function not($str, $compare_to)
	{
		if($str == $compare_to) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
