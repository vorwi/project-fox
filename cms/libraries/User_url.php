<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_url {

	private $CI;

	public function __construct() {
		$this->CI =& get_instance();
	}

	public function save_url($update = false, $new_url, $type, $dest_id, $lang) {
		$url = iso_clear(trim($new_url));
		if(empty($url)){
			if($update){
				$this->CI->db->where_in('dest_id', $dest_id);
				$this->CI->db->where('type', $type);
				$this->CI->db->where('lang', $lang);
				$query_uu = $this->CI->db->delete('user_url');
				$this->update_routes();
			}
			return false;
		}
		$urls = array();

		$this->CI->db->from('user_url');
		$this->CI->db->group_start();
			$this->CI->db->group_start();
				$this->CI->db->where('type', $type);
				$this->CI->db->where('dest_id !=', $dest_id);
			$this->CI->db->group_end();
			$this->CI->db->or_group_start();
				$this->CI->db->where('type !=', $type);
			$this->CI->db->group_end();
		$this->CI->db->group_end();

		$this->CI->db->order_by('lang, type, dest_id');
		$this->CI->db->group_by('url');
		$query = $this->CI->db->get();

		if($query->num_rows() > 0){
			foreach($query->result() as $row){ $urls[] = $row->url; }
		}
		$collision = 0;
		while (in_array($url, $urls)) {
			$url .= '_';
			$collision++;
		}
		if($collision > 0) {
			$this->CI->neocms->forward_msg(lang("Adres URL '{$new_url}' był już przypisany do innego elementu. Spróbuj dobrać inny ciąg znaków."), 3);
		}

		if($update) {
			$this->CI->db->from('user_url');
			$this->CI->db->where('type', $type);
			$this->CI->db->where('lang', $lang);
			$this->CI->db->where('dest_id', $dest_id);
			$query = $this->CI->db->get();
			if($query->num_rows() > 0){
				$data = array('url'=>$url);
				$data['mod_path'] = $this->_get_mod_path($type);
				$this->CI->db->where('type', $type);
				$this->CI->db->where('lang', $lang);
				$this->CI->db->where('dest_id', $dest_id);
				$this->CI->db->update('user_url', $data);
			} else {
				$data = array('url'=>$url,'dest_id'=>$dest_id,'type'=>$type,'lang'=>$lang);
				$data['mod_path'] = $this->_get_mod_path($type);
				$this->CI->db->insert('user_url', $data);
			}
		} else {
			$data = array('url'=>$url,'dest_id'=>$dest_id,'type'=>$type,'lang'=>$lang);
			$data['mod_path'] = $this->_get_mod_path($type);

			$this->CI->db->insert('user_url', $data);
		}
	}

	public function update_routes() {
		$this->CI->db->from('user_url');
		$this->CI->db->order_by('lang, type, dest_id');
		$this->CI->db->group_by('url');
		$query = $this->CI->db->get();
		if($query->num_rows() > 0){
			$data = array();
			foreach($query->result() as $row){
				if(empty($row->url)){ continue; }

				switch($row->type) {
					case 'estatedev_investments':
					case 'estatedev_estates':
					case 'estatedev_parameters':
					case 'estatedev_types':
						$data[] = '$route["' . $row->url . '"] = "'.$row->mod_path.$row->type.'/'.$row->dest_id.'";';
						$data[] = '$route["' . $row->url . '/(:any)"] = "'.$row->mod_path. $row->type.'/'.$row->dest_id.'/filters/$1";';
						$data[] = '$route["' . $row->url . '/(:any)/(:any)"] = "'.$row->mod_path.$row->type.'/'.$row->dest_id.'/filters/$1/subfilters/$2";';
						break;
					case 'products':
					case 'products_category':
					case 'articles':
					case 'news':
						$data[] = '$route["' . $row->url . '"] = "'.$row->mod_path.$row->dest_id.'";';
						break;
				}
			}
			$query->free_result();
			$output = '<?php'."\n".implode("\n", $data)."\n".'?>';

			$this->CI->load->helper('file');

			write_file(APPPATH . "cache/routes.php", $output);
		}
	}

	private function _get_mod_path($type = NULL){
		if(empty($type)){ return FALSE; }
		$mod_path = FALSE;
		switch($type) {
			case 'estatedev_investments':
			case 'estatedev_estates':
			case 'estatedev_parameters':
			case 'estatedev_types':
				$mod_path = 'frontend/estatedev/index/';
				break;
			case 'products':
				$mod_path = 'frontend/products/index/';
				break;
			case 'products_category':
				$mod_path = 'frontend/products/category/';
				break;
			case 'articles':
				$mod_path = 'frontend/articles/index/id/';
				break;
			case 'news':
				$mod_path = 'frontend/news/single/';
				break;
		}
		return $mod_path;
	}
}