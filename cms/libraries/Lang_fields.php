<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lang_fields {
	private $lang_fields;
	private $field_type;
	private $field_name;
	private $options;

	private $field_defaults = array(
		'class' => 'form-control'
	);

	public function __construct() {
		$this->CI =& get_instance();
		if (!function_exists('_parse_form_attributes')) {
			$this->CI->load->helper('form');
		}
		/*[pl] => stdClass Object
        (
            [id] => 1
            [short] => pl
            [lang] => Polski
            [dir] => polish
        )*/
	}

	public function render(&$obj, $field_type = 'text', $field_name = '', $options = array()) {
		//drobny myk, żeby wykorzystać jeden generator dla wielu typów pól <input>
		$input_types = array('number', 'date', 'datetime', 'time', 'email', 'url');
		if(in_array($field_type, $input_types)) {
			$this->orig_field_type = $field_type;
			$field_type = 'text';
			$this->options['type'] = $field_type;
		}

		$ret = '<div class="lang-fields lf-'.$field_type.'">';
		if(!empty($obj->lang) && is_array($obj->lang)) {
			//ustawienie zmiennych wywołania, żeby mieć je pod ręką i nie przekazywać oddzielnie do każdej metody
			$this->lang_fields = $obj->lang;
			$this->field_type = $field_type;
			$this->field_name = $field_name;
			$this->options = $options;

			//generowanie "menu" z zakładkami do przełączania języka
			$ret .= $this->_tabs();

			//generowanie kodu pól językowych, opakowanych w .tab-content
			$ret .= $this->_fields();

			//czyszczenie parametrów wywołania, żeby zwolnić pamięć
			$this->_clear_params();
		}
		$ret .= '</div>';
		return $ret;
	}

	public function global_switcher(&$obj) {
		$html = '<div class="lang-switcher">'.lang('Edytuj wersję: ').'<ul>';
		if(!empty($obj->lang) && is_array($obj->lang)) {
			foreach($obj->lang as $lang_short => $fields) {
				$html .= '<li data-lang="'.$lang_short.'">'.lang_flag($lang_short).'</li>';
			}
		}
		return $html.'</ul></div>';
	}


	private function _tabs() {
		$tabs_add = (isset($this->options['data-tabs-hidden']) ? ' style="display: none"' : '');
		$ret = '<ul class="nav nav-tabs" role="tablist"'.$tabs_add.'>';
		$i = 1;
		foreach($this->lang_fields as $lang => $fields){
			$fid = $this->_get_field_id($lang);
			$ret .= '<li role="presentation" class="'.(($i == 1) ? 'active' : '').'">';
			$ret .= '<a href="#'.$fid.'" class="lang-button-'.$lang.'" aria-controls="'.$fid.'" role="tab" data-toggle="tab">';
			$ret .= lang_flag($lang);
			$ret .= '</a></li>';
			$i++;
		}
		$ret .= '</ul>';
		return $ret;
	}

	private function _fields() {
		$ret = '<div class="tab-content">';
		$i = 1;
		foreach($this->lang_fields as $lang => $fields){
			$fid = $this->_get_field_id($lang);
			$value = ifset($fields->{$this->field_name});
			$ret .= '<div role="tabpanel" class="tab-pane'.(($i == 1) ? ' active' : '').'" id="'.$fid.'">';

			//wywołanie metody generującej HTML pola, zależnie od podanego typu
			$item_fn_name = "generate_{$this->field_type}_item";
			if(method_exists($this, $item_fn_name)) {
				$ret .= $this->$item_fn_name($lang, $value);
			} else {
				$ret .= "[missing generator for `$this->field_type` field ($fid)]";
			}

			$ret .= '</div>';
			$i++;
		}
		$ret .= '</div>';
		return $ret;
	}

	private function _get_field_id($lang) {
		return "{$this->field_name}_{$lang}";
	}

	private function _get_field_name($lang) {
		return "lang[{$lang}][{$this->field_name}]";
	}

	private function _clear_params() {
		$params = array('lang_fields', 'field_type', 'field_name', 'options');
		foreach($params as $key) {
			$this->$key = NULL;
		}
	}

	/*Poniżej są metody generujące poszczególne typy pól*/
	protected function generate_text_item($lang, $value) {
		$fname = $this->_get_field_name($lang);
		$attr = array(
			'type' => 'text',
			'name' => $fname,
			'value' => set_value($fname, $value)
		);
		return '<input '._parse_form_attributes(array_merge($attr, $this->options), $this->field_defaults)." />\n";
	}
	
	protected function generate_textarea_item($lang, $value) {
		$fname = $this->_get_field_name($lang);
		$attr = array(
				'name' => $fname,
				'style' => 'height:100px',
		);
		return '<textarea '._parse_form_attributes(array_merge($attr, $this->options), $this->field_defaults).">".set_value($fname, $value)."</textarea>\n";
	}

	protected function generate_select_item($lang, $value) {
		$fname = $this->_get_field_name($lang);
		$attr = array_merge(array('name' => $fname), $this->options);
		unset($attr['values']);

		$opts = '';
		if(!empty($this->options['values'])) {
			foreach($this->options['values'] as $opt_key => $opt_label) {
				$opts .= '<option value="'.$opt_key.'" '.set_select($fname, $opt_key, $value == $opt_key).'>'.$opt_label."</option>\n";
			}
		}
		return '<select '._parse_form_attributes($attr, $this->field_defaults).'>'.$opts."</select>\n";
	}

	protected function generate_ckeditor_item($lang, $value) {
		$fname = $this->_get_field_name($lang);
		return ckeditor($fname, set_value($fname, $value, FALSE), $this->options);
	}

	protected function generate_value_item($lang, $value) {
		return $value;
	}
}