<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Image_lib extends CI_Image_lib
{

	public $errors = array();

	public function __construct()
	{

		parent::__construct();
	}

	public function crop_thumb($path, $img, $path_save = null, $img_save = null, array $coords)
	{
		$scale = ifset($coords['scale'], 1);
		$path_big = $path . $img;

		if ($path_save === null) $path_save = $path;
		if ($img_save === null) $img_save = $img;

		$path_small = $path_save . $img_save;

		$config = array();
		$config['source_image'] = $path_big;
		$config['maintain_ratio'] = false;
		$config['x_axis'] = (int)$coords['x_axis'] * $scale;
		$config['y_axis'] = (int)$coords['y_axis'] * $scale;
		$config['width'] = (int)$coords['width'] * $scale;
		$config['height'] = (int)$coords['height'] * $scale;
		$config['new_image'] = $path_small;

		$this->initialize($config);

		if (!$this->crop()) {

			$this->error[] = 'Miniaturka nie została zmieniona. Spróbuj ponownie.';
			return false;
		}

		$config = array();
		$config['source_image'] = $path_small;
		$config['maintain_ratio'] = false;
		$config['width'] = (int)$coords['thumb_width'];
		$config['height'] = (int)$coords['thumb_height'];
		$config['new_image'] = $path_small;

		$this->initialize($config);

		if (!$this->resize()) {

			$this->error[] = 'Miniaturka nie została zmniejszona. Spróbuj ponownie.';
			return false;
		}

		return true;
	}
}
