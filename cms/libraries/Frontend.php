<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Frontend {

	private $CI;
	public $menu_childrens = array();
	public $breadcrumbs = array();
 
	public function __construct() {
		$this->CI =& get_instance();
	}

	public function start(){
		if(config_item('profiler_f')) { $this->CI->output->enable_profiler(TRUE); }
		$this->CI->config->set_item('global_xss_filtering', TRUE);
		
		$this->set_lang();
		$this->set_page_id();
	}
	
	public function set_lang(){
		$tab 	= $this->CI->uri->uri_to_assoc(1);	
		$domain = $_SERVER['HTTP_HOST'];
		$dm_to_lang = config_item('dm_to_lang');
		
		if(!isset($_SESSION['lang']) || empty($_SESSION['lang'])) { $_SESSION['lang'] = config_item('main_lang'); };
			
		if(isset($tab['lang']) && !empty($tab['lang']) && isset($this->CI->languages[$tab['lang']])){
			$_SESSION['lang'] = $tab['lang'];
		} elseif (isset($dm_to_lang[$domain])) {
            $_SESSION['lang'] = $dm_to_lang[$domain];
        }
		
		$row = $this->CI->languages[$_SESSION['lang']];
		
		if(!empty($row->dir)){
			$this->CI->config->set_item('language', $row->dir);
			$this->CI->lang->load('frontend/trs', $row->dir);
			$this->CI->lang->load('frontend/form_validation', $row->dir);
			
			$lang_path = APPPATH.'language/';
			$file = $row->dir.'/frontend/missing_lang.php';
			if(file_exists($lang_path.$file)) { $this->CI->lang->load('frontend/missing', $row->dir); }
		}
	}
	
	public function set_page_id(){
		$tab 	= $this->CI->uri->uri_to_assoc();
		$rtab 	= $this->CI->uri->ruri_to_assoc(-3); 
		
		$this->CI->content_type = $this->CI->router->fetch_method();
		$this->CI->class = $this->CI->router->fetch_class();
		
		if(!isset($rtab['id'])) {
			$id = ($this->CI->uri->rsegment(1) == 'articles' && $this->CI->content_type!='cookie_info' ? config_item('main_art') : 0);
		} 
		else $id = $rtab['id'];
		
		if($this->CI->uri->rsegment(1) == 'news') {
			$news = $this->get_news($this->CI->uri->rsegment(3,0));
			$id = $news->id_art;
		}
		
		if($this->CI->uri->rsegment(1) == 'articles' || isset($id_art)) {
			$art = $this->get_article($id);
			if($art !== FALSE) {
				$this->CI->id_parent = $art->id_tree;
				$art_parent = $this->get_article($art->id_tree);
				if($art_parent !== FALSE && $art_parent->id_tree != 0) {
					$this->CI->id_grandparent = $art_parent->id_tree;
					$art_grandparent = $this->get_article($art_parent->id_tree);
					if($art_grandparent !== FALSE && $art_grandparent->id_tree != 0) {
						$this->CI->id_grandgrand = $art_grandparent->id_tree;
					}
				}
			}
		}
		
		$_SESSION['id'] = $id;
		$this->CI->id = $id;
	}
	
	public function get_popup(){
	
		$view_data = array();
		$view_data['popup_image'] 			= (isset($this->CI->settings->popup_image) ? $this->CI->settings->popup_image : '');
		$view_data['popup_width'] 	= (isset($this->CI->settings->popup_width) ? $this->CI->settings->popup_width : '800');
		$view_data['popup_date_start'] 		= (isset($this->CI->settings->popup_date_start) ? strtotime($this->CI->settings->popup_date_start) : FALSE);
		$view_data['popup_date_end'] 			= (isset($this->CI->settings->popup_date_end) ? strtotime($this->CI->settings->popup_date_end) : FALSE);
		$view_data['popup_link'] 		= (isset($this->CI->settings->popup_link) ? $this->CI->settings->popup_link : '');
	
		return $this->CI->load->view('frontend/'.$this->CI->dir.'includes/popup_view', $view_data, true);
	}
	
	public function get_meta(){
		
		$tab = array();
		$tab['meta_title'] 			= (isset($this->CI->settings->meta_title) ? $this->CI->settings->meta_title : '');
		$tab['meta_description'] 	= (isset($this->CI->settings->meta_description) ? $this->CI->settings->meta_description : '');
		$tab['meta_keywords'] 		= (isset($this->CI->settings->meta_keywords) ? $this->CI->settings->meta_keywords : '');
		$tab['og_title'] 			= (isset($this->CI->settings->og_title) ? $this->CI->settings->og_title : '');
		$tab['og_description'] 		= (isset($this->CI->settings->og_description) ? $this->CI->settings->og_description : '');
		$tab['og_image'] 			= (isset($this->CI->settings->og_image) ? $this->CI->settings->og_image : '');
		$tab['og_url'] 				= (isset($this->CI->settings->og_url) ? $this->CI->settings->og_url : '');
		$tab['og_site_name']		= (isset($this->CI->settings->og_site_name) ? $this->CI->settings->og_site_name : '');
		
		if($this->CI->meta_title!='') $tab['meta_title'] = $this->CI->meta_title;
		if($this->CI->meta_description!='') $tab['meta_description'] = $this->CI->meta_description;
		if($this->CI->meta_keywords!='') $tab['meta_keywords'] = $this->CI->meta_keywords;
		if($this->CI->og_title!='') $tab['og_title'] = $this->CI->og_title;
		if($this->CI->og_description!='') $tab['og_description'] = $this->CI->og_description;
		if($this->CI->og_image!='') $tab['og_image'] = $this->CI->og_image;
		if($this->CI->og_url!='') $tab['og_url'] = $this->CI->og_url;
		if($this->CI->og_site_name!='') $tab['og_site_name'] = $this->CI->og_site_name;
		
		return $this->CI->load->view('frontend/'.$this->CI->dir.'includes/meta_view', array('meta'=>$tab), true);	
	}
	
	public function get_stats_script(){
		if(isset($this->CI->settings->google_analytics) && !empty($this->CI->settings->google_analytics)){
			return $this->CI->settings->google_analytics;
		}
		return '';
	}

	public function get_content($module, $data){
		$module = $this->CI->load->view('frontend/'.$this->CI->dir.$module, $data, true);
		
		if(isset($data['addons'])) $addons = $data['addons'];
		else $addons = '';
		
		$module = secure_emails($module);
		
		$content = array(
			'content'	=> $module,
			'addons'	=> $addons,
		);
	
		return $this->CI->load->view('frontend/'.$this->CI->dir.'includes/content_view', $content, true);
	}
	
	public function get_article($id){
		$this->CI->load->model('frontend/articles_model');
		$data = $this->CI->articles_model->get_article($id);
		return $data;
	}
	
	public function get_news($id){
		$this->CI->load->model('frontend/news_model');
		$data = $this->CI->news_model->get_single_news($id);
		return $data;
	}

	public function add_breadcrumb($title, $url = NULL){
		if(!empty($url) && !preg_match('#^\w+://#i', $url)) $url = site_url($url);
		$crumb = new stdClass();
		$crumb->title = $title;
		$crumb->url = $url;
		$this->breadcrumbs[] = $crumb;
	}
	
	public function get_breadcrumbs(){
		$data = $this->breadcrumbs;
		return $this->CI->load->view('frontend/'.$this->CI->dir.'includes/breadcrumbs_view', array('breadcrumbs' => $data), true);
	}
	
	public function get_header($header){
		return $this->CI->load->view('frontend/'.$this->CI->dir.'includes/header_view', array('header'=>@$header), true);
	}
	public function get_cookie_alert(){
		if(is_object($this->CI->settings) && $this->CI->settings->cookie_alert_on == 1) {
			return $this->CI->load->view('frontend/'.$this->CI->dir.'includes/cookie_alert_view', array('settings'=>$this->CI->settings), true);
		} 
		return '';
	}

	public function get_captcha(){
		$this->CI->load->helper('captcha');
	
		$obst = rand(1000, 9999);
		
		$vals = array(
			'word' => $obst,
			'img_path' => config_item('site_path').config_item('temp').'captcha/',
			'img_url' => config_item('temp').'captcha/',
		 	'font_path' => config_item('site_path').config_item('gfx_c').'calibri.ttf',
			'img_width' => 150,
			'img_height' => 40,
			'font_size' => 16,
			'expiration' => 7200,
			'colors' => array(
				'background' => array(255, 255, 255),
				'border' => array(204, 204, 204),
				'text' => array(128, 149, 183),
				'grid' => array(204, 204, 204)
			)
		);
		$cap = create_captcha($vals);
		
		$data = array(
			'captcha_time' => $cap['time'],
			'ip_address' => $this->CI->input->ip_address(),
			'word' => $cap['word']
		);
		$query = $this->CI->db->insert_string('captcha', $data);
		$this->CI->db->query($query);
		
		return $cap;
	}
	
	public function get_slides($id_tree) {
		$this->CI->load->model('frontend/slides_model');
		$slides = $this->CI->slides_model->get_slides($id_tree);
		return $slides;
	}
	
	public function get_menu($params = FALSE){
		if(is_array($params) && $params !== FALSE) {
			$params = (object)$params;
		}elseif($params === FALSE){
			$params = new stdClass();
		}
		
		if(!isset($params->id_cat)){ $params->id_cat = config_item('main_menu'); }
		if(!isset($params->id_tree)){ $params->id_tree = FALSE; }
		if(!isset($params->return_source)){ $params->return_source = TRUE; }
		if(!isset($params->exclude)){ $params->exclude = FALSE; }
		if(!isset($params->with_children)){ $params->with_children = TRUE; }
		if(!isset($params->menu_type)){ $params->menu_type = 'top'; }
		
		if(!empty($this->CI->id_grandgrand)) {
			$tree_id = $this->CI->id_grandgrand;
		} elseif(!empty($this->CI->id_grandparent)) {
			$tree_id = $this->CI->id_grandparent;
		} elseif(!empty($this->CI->id_parent)) {
			$tree_id = $this->CI->id_parent;
		} else {
			$tree_id = $this->CI->id;
		}
		
		$articles = $this->get_all_articles($params);
		
		if($params->return_source){
			return $this->_menu(array('view' => 'menu_'.$params->menu_type.'_view', 'rows' => $articles[$params->id_cat]));
		}else{
			return $articles;
		}
	}
	
	public function get_all_articles($params = FALSE) {
		if(is_array($params)) $params = (object)$params;
		
		if(isset($params->get_childrens) && $params->get_childrens && isset($params->id)){
			$child_result = array();
			if(isset($this->menu_childrens[$params->id])){
				foreach ($this->menu_childrens[$params->id] as $row){
					if(isset($this->menu_childrens[$row->id_art])){
						$row->childrens = $this->get_all_articles(array('get_childrens' => TRUE, 'id' => $row->id_art));
					}
					$child_result[] = $row;
				}
			}
			
			return $child_result;
		}
		
		$this->menu_childrens = array();
		
		$this->CI->db->select('tk1.id,tk1.id_cat,tk1.id_tree,tk2.id_art,tk2.title,tk2.short_title,tk2.url,tk2.target,tk3.url as user_url, tk1.pub_menu', false);
		$this->CI->db->from('articles as tk1');
		$this->CI->db->join('articles_content as tk2', 'tk1.id = tk2.id_art', 'left');
		$this->CI->db->join('user_url as tk3', "tk1.id = tk3.dest_id AND tk3.type='articles' AND tk3.lang= '".$_SESSION['lang']."'",'left');
		$this->CI->db->where('tk1.pub', 1);
		$this->CI->db->where('tk2.pub', 1);
		if(isset($params->exclude) && is_array($params->exclude) && !empty($params->exclude)) {
			$this->CI->db->where_not_in('tk1.id', $params->exclude);
		}
		if(isset($params->id_tree) && is_array($params->id_tree)) {
			$this->CI->db->where_in('tk1.id_tree', $params->id_tree);
		} elseif(isset($params->id_tree) && !is_array($params->id_tree) && $params->id_tree !== FALSE) {
			$this->CI->db->where('tk1.id_tree', $params->id_tree);
		}
		if(isset($params->id_cat) && $params->id_cat !== FALSE){
			$this->CI->db->where('tk1.id_cat', $params->id_cat);
		}
		$this->CI->db->where('tk2.lang', $_SESSION['lang']);
		$this->CI->db->order_by('tk1.id_cat, tk1.id_tree, tk1.position');
		$this->CI->db->order_by('tk1.id');
		$query = $this->CI->db->get();
		
		if($query->num_rows() > 0){
			$data = array();
			foreach($query->result() as $row) {
				if($row->id_tree != 0){
					if(!isset($this->menu_childrens[$row->id_tree])){ $this->menu_childrens[$row->id_tree] = array(); }
					$this->menu_childrens[$row->id_tree][] = $row;
				}else{
					if(!isset($data[$row->id_cat])){ $data[$row->id_cat] = array(); }
					$data[$row->id_cat][] = $row;
				}
			}
			
			foreach ($data as $id_cat => $rows){
				foreach ($rows as $key_row => $row){
					if(isset($params->with_children) && $params->with_children && isset($this->menu_childrens[$row->id_art])){
						$row->childrens = $this->get_all_articles(array('get_childrens' => TRUE, 'id' => $row->id_art));
						$data[$id_cat][$key_row] = $row;
					}
				}
			}
			return $data;
		}
		return array();
	}
	
	private function _menu($params = FALSE){
		if(is_array($params)) $params = (object)$params;
	
		return $this->CI->load->view('frontend/'.$this->CI->dir.'includes/'.$params->view, array('menu'=>$params->rows), true);
	}
}
