<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_DB_mysqli_driver extends CI_DB_mysqli_driver
{
	/**
	 * Loads a SQL string and executes it... good for bigger data dumps
	 *
	 * @access	public
	 * @param	string	The path to a SQL file
	 * @param	boolean	If the contents being passed in parameter 1 is a path or a SQL string
	 * @return	void
	 */
	public function load_sql($sql_path, $is_path = TRUE){
		$CI =& get_instance();
		// check first to see if it is a path to a file
		if (file_exists($sql_path) AND $is_path){
			$sql = file_get_contents($sql_path);
		}
		// if not, assume it is a string
		else if(!$is_path){
			$sql = $sql_path;
		}
		else{
			return FALSE;
		}
		
		$sql = preg_replace('#/\*(.+)\*/#Us', '', $sql);
		$sql = preg_replace('/^(#|--)(.+)$/U', '', $sql);
		
		$sql_arr = explode(";\n", str_replace("\r\n", "\n", $sql));
		foreach($sql_arr as $s){
			$s = trim($s);
			if (!empty($s)){
				$CI->db->query($s);
			}
		}
	}
}
