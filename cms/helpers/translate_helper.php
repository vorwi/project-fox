<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function list_from_tree($tree, $result = FALSE, $prefix = '') {

	if($result === FALSE) $result = array();
	if(is_array($tree)) {
		foreach($tree as $k => $v) {
			if(is_array($v)) {
				$result = array_merge($result, list_from_tree($v, $result, $prefix.$k.'/'));
			} else {
				$result[] = $prefix.$v;
			}
		}
	}
	return $result;
}

function draw_dir_tree($files, $url_base, $path_prefix = '') {

	if(empty($path_prefix)) echo '<ul class="dir_tree list-unstyled">';

	foreach($files as $k => $v) {
		if(is_array($v)) {
			draw_dir_tree($v, $url_base, $path_prefix.rtrim($k, '\/\\').'::');
		} else {
			if(!preg_match('#_lang\.php$#', $v)) continue;

			$file_path = $path_prefix.$v;
			$path_regular = str_replace('::', '/', $file_path);
			echo '<li>► '.anchor("{$url_base}{$file_path}", $path_regular).'</li>';
		}
	}

	if(empty($path_prefix)) echo '</ul>';
}

function lang_input($key, $value, $disabled = FALSE) {
	return '<textarea class="form-control" '.(!$disabled ? 'name="'.$key.'"' : 'readonly="readonly" disabled="disabled"').'>'.htmlspecialchars($value, ENT_QUOTES, 'UTF-8', FALSE).'</textarea>';
}

function inputs_table($pkey, $array, $recursive = TRUE, $main_lang = FALSE) {
	if($array !== FALSE && !empty($array)) {
		if(is_array($array)) $array = (object)$array;
?>
<table class="table table-bordered">
	<tbody>
		<? foreach($array as $key => $val): ?>
			<tr>
				<td><?=$key?></td>
			<?
				echo '<td class="padding-xs'.($val === FALSE ? ' not_exists' : '').'">';
				if(is_array($val) || is_object($val)) inputs_table($pkey.'['.$key.']', $val, $main_lang);
				else echo lang_input($pkey.'['.$key.']', $val, $main_lang);
				echo '</td>';
			?>
			</tr>
		<? endforeach; ?>
	</tbody>
</table>
<?
	}
}

function generate_lang($input, $in_array = 0) {
	$result = '';
	$n = 1;
	foreach($input as $k => $v) {
		if($in_array) {
			$result .= repeater("\t", $in_array)."'{$k}' => ";
		} else {
			$result .= '$lang["'.$k.'"] = ';
		}

		if(is_array($v)) {
			$result .= "array(\r\n".generate_lang($v, $in_array + 1).repeater("\t", $in_array).")";
		} else {
			$v = htmlspecialchars($v, ENT_QUOTES, 'UTF-8', TRUE);
			$v = htmlspecialchars_decode($v, ENT_COMPAT);
			$result .= "'".$v."'";
		}

		if($in_array) {
			$result .= ($n < count($input) ? "," : "")."\r\n";
		} else {
			$result .= ";\r\n";
		}
		$n++;
	}
	return $result;
}