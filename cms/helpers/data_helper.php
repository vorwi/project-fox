<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function ifset(&$var, $default = NULL) {
	return (isset($var) ? $var : ($default !== NULL ? $default : ''));
}

function ifempty(&$var, $default = FALSE) {
	return (!empty($var) && $var != '0000-00-00' && $var != '0000-00-00 00:00:00' ? $var : ($default !== FALSE ? $default : NULL));
}

function base64_url_encode($input) {
	return strtr(base64_encode($input), '+/=', '-_,');
}

function base64_url_decode($input) {
	return base64_decode(strtr($input, '-_,', '+/='));
}

function object_to_array($data){
	if (is_array($data) || is_object($data)){
		$result = array();
		foreach ($data as $key => $value){
			$result[$key] = object_to_array($value);
		}
		return $result;
	}
	return $data;
}

function array_to_obj($array, &$obj){
	foreach ($array as $key => $value){
		if (is_array($value)){
			$obj->$key = new stdClass();
			array_to_obj($value, $obj->$key);
		}else{
			$obj->$key = $value;
		}
	}
	return $obj;
}

function extract_youtube_id($link) {
	if(empty($link)){
		return FALSE;
	}
	preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $link, $matches);
	
	if(isset($matches[1]) && !empty($matches[1])){
		return $matches[1];
	} else {
		return FALSE;
	}
}