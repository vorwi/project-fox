<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

function msg($msg, $a, $b=0, $frontend=0) {
	if($a == 0) {
		$m_class = 'success';
		$i_class = 'check';
	} elseif($a == 1) {
		$m_class = 'warning';
		$i_class = 'warning';
	} elseif($a == 2) {
		$m_class = 'danger';
		$i_class = 'ban';
	} elseif($a == 3) {
		$m_class = 'info';
		$i_class = 'info';
	} else {
		$m_class = 'action';
		$i_class = '';
	}
	//<i class="icon fa fa-'.$i_class.'"></i>
	$str = '<div class="alert alert-'.$m_class.' alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			'.$msg.'
		</div>';
	return $str;
}

function collect_messages() {
	$CI = &get_instance();
	$all_messages = array();
	$curr_msg = $CI->msg;

	$verr = validation_errors();
	if(!empty($verr)) {
		$all_messages[] = array($verr, 1);
	}

	$fwd_msg = (isset($CI->session) ? $CI->session->flashdata('msg') : FALSE);
	if($fwd_msg !== FALSE && is_array($fwd_msg)) {
		if(is_array($fwd_msg[0])) {
			foreach(array_reverse($fwd_msg) as $m) {
				$all_messages[] = $m;
			}
		} else {
			$all_messages[] = $fwd_msg;
		}
	}
	if(!empty($curr_msg)) {
		if(is_array($curr_msg[0])) {
			foreach(array_reverse($curr_msg) as $m) {
				$all_messages[] = $m;
			}
		} else {
			$all_messages[] = $curr_msg;
		}
	}
	return $all_messages;
}

function show_messages() {
	$messages = collect_messages();
	$return = '';
	
	if(!empty($messages)) {
		foreach($messages as $msg) {
			$return .= msg($msg[0], ifset($msg[1], 0), ifset($msg[2]), ifset($msg[3], 1));
		}
	}
	return $return;
}

function password_generator($pass = null, $time = null) {

	$hasher = pass_hash();

	if($pass == null){
		$pass	   = time();
		
		sleep(rand(1,3));
		
		for($i=1; $i<rand(5,10); $i++) {
	
			switch(rand(1,3)) {
	
				case 1: $pass.=chr(rand(48,57)); break;
				case 2: $pass.=chr(rand(65,90)); break;
				case 3: $pass.=chr(rand(97,122)); break;
			}
		}
	
		$pass = md5($pass);
		$pass = substr($pass, 0, 10);
	}
	if($time == null) $time = time();
	elseif(!is_numeric($time)) $time = strtotime($time);

	$time_salt  = sha1(config_item('password_salt').$time);

	$salt = substr($time_salt, 2, 8);
			
	$pass_crypt = $hasher->HashPassword($pass.$salt);

	$data = array(
		'time' 			=> $time,
		'pass' 			=> $pass,
		'pass_crypt' 	=> $pass_crypt
	);	
	return $data;
}

function setting($name) {
	$CI =& get_instance();
	if(isset($CI->settings->$name)) return $CI->settings->$name;
	return FALSE;
}

function lang_flag($short, $lang = '') {
	if(is_object($short)) {
		$lang = $short->lang;
		$short = $short->short;
	}
	return '<img alt="'.$short.'" title="'.$lang.'" src="'.config_item('gfx_c').'img/flags/'.$short.'.png" />';
}

function jqueryValidateCommonVars(){
	return '
		errorClass: "has-warning",
		errorPlacement: function(error, element) {
			if(typeof toastr !== "undefined") {
				toastr.warning(error.text());
			} else {
				console.log("brak biblioteki toastr do wyświetlenia błędu: " + error.text());
			}
		},
		onfocusout: function(element) {
			$(element).valid();
		},
		onkeyup: function(element) {
			$(element).valid();
		},
		invalidHandler: function(event, validator) {
			if(!$(this).data("submitted")) {
				$(this).data("submitted", true);
				if(confirm("'.lang('Czy na pewno wysłać formularz z błędami?').'")) {
					validator.currentForm.submit();
				} else {
					event.preventDefault();
					event.stopImmediatePropagation();
				}
			}
		},
	';
}

function echo_article_select_options($items, $field_name, $add = ''){
	foreach ($items as $row){
		echo '<option value="'.$row->id.'" '.set_select($field_name, $row->id).'>'.$add.' '.$row->title.'</option>';
		if(isset($row->children) && !empty($row->children)){
			echo_article_select_options($row->children, $field_name, $add.'&#x2001;');
		}
	}
}

function filter_form_row($row, $filters) {
	$class = '';
	if(isset($row['class']) && !empty($row['class'])){ $class = $row['class']; }
	echo '<td'.(isset($row['td_class']) ? ' class="'.$row['td_class'].'"' : '').(isset($row['colspan']) ? ' colspan="'.$row['colspan'].'"' : '').($row['type'] == 'hidden' ? ' style="display:none"' : '').'>';
	if($row['type'] == 'select') {
		if(isset($row['values']) && !empty($row['values']) && $row['values'] !== FALSE) {
			$multiple = '';
			if(isset($row['multiple']) && $row['multiple']){ $multiple = 'multiple="multiple"'; }
			if(isset($row['style']) && !empty($row['style'])){ $style = $row['style']; }else{ $style = 'width: auto;'; }
			echo '<select class="'.$class.'" name="'.$row['name'].($multiple != '' ? '[]' : '').'" '.$multiple.' style="'.$style.'">';
			echo '<option value="">dowolne</option>';

			foreach($row['values'] as $k => $v){
				$rid = isset($v->id) ? $v->id : $k;
				$rname = isset($v->name_pl) ? $v->name_pl : (isset($v->name) ? $v->name : (isset($v->title) ? $v->title : $v));
				if(isset($v->lvl)) $rname = ($v->lvl > 0 ? repeater('&#x2001;', $v->lvl) : '').$rname;
				$set = false;
				if(isset($filters->{$row['name']}) && $filters->{$row['name']} != ''){
					if(is_array($filters->{$row['name']})){
						$set = in_array($rid, $filters->{$row['name']}) ? true : false ;
					}else{
						$set = $filters->{$row['name']} == $rid ? true : false;
					}
				}
				echo '<option value="'.$rid.'" '.set_select($row['name'], $rid, $set).'>'.$rname.'</option>';
			}
			echo '</select>';
		}
	}if($row['type'] == 'select_with_optgroups') {
		if(isset($row['values']) && !empty($row['values']) && $row['values'] !== FALSE) {
			$multiple = '';
			if(isset($row['multiple']) && $row['multiple']){ $multiple = 'multiple="multiple"'; }
			if(isset($row['style']) && !empty($row['style'])){ $style = $row['style']; }else{ $style = 'width: auto;'; }
			echo '<select class="'.$class.'" name="'.$row['name'].($multiple != '' ? '[]' : '').'" '.$multiple.' style="'.$style.'">';
			echo '<option value="">dowolne</option>';

			foreach($row['values'] as $k => $v){
				if(isset($v->options) && !empty($v->options)){
					$rname = isset($v->name_pl) ? $v->name_pl : (isset($v->name) ? $v->name : (isset($v->title) ? $v->title : $v));
					echo '<optgroup label="'.$rname.'">';
					foreach ($v->options as $option){
						$set = false;
						if(isset($filters->{$row['name']}) && !empty($filters->{$row['name']})){
							if(is_array($filters->{$row['name']})){
								$set = in_array($option->id, $filters->{$row['name']}) ? true : false ;
							}else{
								$set = $filters->{$row['name']} == $option->id;
							}
						}
						echo '<option class="group_'.$v->id.'" value="'.$option->id.'" '.set_select($row['name'], $option->id, $set).'>'.$option->name.'</option>';
					}
					echo '</optgroup>';
				}
			}
			echo '</select>';
		}
	} elseif($row['type'] == 'date') {
		echo '<input type="text" name="'.$row['name'].'" value="'.set_value($row['name'], ifset($filters->{$row['name']})).'" class="datepicker" style="width: 80px;" />';
	} elseif($row['type'] == 'time') {
		if($row['values'] !== FALSE) {
			echo '<select name="'.$row['name'].'" style="width: 85px">';
			echo '<option value="0">dowolny</option>';
			foreach($row['values'] as $v){
				$set = (ifset($filters->{$row['name']}) == $v->timestamp ? TRUE : FALSE);
				echo '<option value="'.$v->timestamp.'" '.set_select($row['name'], $v->timestamp, $set).'>'.$v->time.'</option>';
			}
			echo '</select>';
		} else {
			echo '<input type="text" name="'.$row['name'].'" value="'.set_value($row['name'], sec_to_time(ifset($filters->{$row['name']}))).'" class="timepicker" style="width: 50px;" />';
		}
	}elseif($row['type'] == 'daydate') {
		echo '<input type="text" name="'.$row['name'].'" value="'.set_value($row['name'], ifset($filters->{$row['name']})).'" class="daydatepicker" style="width: 40px;" />';
	}elseif($row['type'] == 'datetime') {
		echo '<input type="text" name="'.$row['name'].'" value="'.set_value($row['name'], ifset($filters->{$row['name']})).'" class="datetimepicker" style="width: 100px;" />';
	}elseif($row['type'] == 'year') {
		echo '<input type="text" name="'.$row['name'].'" value="'.set_value($row['name'], ifset($filters->{$row['name']})).'" style="width: 30px; text-align: center;" />';
	}elseif($row['type'] == 'checkbox') {
		echo '<input type="checkbox" name="'.$row['name'].'" value="'.$row['values'].'" '.set_checkbox($row['name'], 1, isset($filters->{$row['name']})).' style="width: auto; display: block; margin: 0 auto;" />';
	} elseif($row['type'] == 'hidden') {
		echo '<input type="checkbox" name="'.$row['name'].'" value="'.$row['values'].'" '.set_checkbox($row['name'], 1, isset($filters->{$row['name']})).' style="display: none" />';
	} elseif($row['type'] == 'text') {
		echo '<input type="text" name="'.$row['name'].'" value="'.set_value($row['name'], (isset($filters->{$row['name']}) ? rawurldecode($filters->{$row['name']}) : $row['values']) ).'" />';
	}
	echo '</td>';
}

function form_field_tooltip($text) {
	return '<span class="fa fa-info-circle info-tooltip" title="'.htmlspecialchars($text).'"></span>';
}

function form_fileinput($name, $value, $elfinder_url = NULL, $params = array()) {
	if(empty($elfinder_url)){
		$elfinder_url = '/admin/mod_elfinder/elfinder_init/input_view';
	}
	if(array_key_exists('id',$params)) $input_id = $params['id'];
	else $input_id = 'f'.$name;
	
	return '
		<div class="input-group">
			<input id="'.$input_id.'" type="text" name="'.$name.'" value="'.$value.'" class="form-control">
			<div class="input-group-btn">
				<button class="btn btn-info" id="select-'.$input_id.'">'.lang('Wybierz_plik').'</button>
				<button class="btn btn-danger" id="clear-'.$input_id.'">'.lang('Wyczyść').'</button>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#select-'.$input_id.'").popupWindow({
					windowURL: "'.$elfinder_url.'/functionReturn/processf'.$input_id.'",
					windowName: "'.lang('Menadżer plików').'",
					height: 552,
					width: 950,
					centerScreen: 1
				});
				$("#clear-'.$input_id.'").click(function(e){
					e.preventDefault();
					$("#'.$input_id.'").val("");
				});
			});
			function processf'.$input_id.'(file){
				$("#'.$input_id.'").val(file);
			}
		</script>
		';
}

function lang_versions($lang, $obj, $url_prefix) {
	$html = '<span class="nc-options lang-ver">';
	if(count($lang) > 1) {
		$html .= lang('Języki').': ';
		foreach($lang as $l) {
			$html .= '<a href="'.site_url($url_prefix.'/lang/'.$l->short.'/id/'.$obj->id).'">'.lang_flag($l).'</a>';
		}
	} 
	$html .= '</span>';
	return $html;
}
