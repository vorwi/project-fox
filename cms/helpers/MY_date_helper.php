<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function dateV($format, $timestamp = null){
	$to_convert = array(
		'S'=>array('dat'=>'N','str'=>array('pn','wt','sr','cz','pt','so','nd')),
		'l'=>array('dat'=>'N','str'=>array('poniedziałek','wtorek','środa','czwartek','piątek','sobota','niedziela')),
		'F'=>array('dat'=>'n','str'=>array('sty','lut','mar','kwi','maj','cze','lip','sie','wrz','paz','lis','gru')),
		'f'=>array('dat'=>'n','str'=>array('stycznia','lutego','marca','kwietnia','maja','czerwca','lipca','sierpnia','września','października','listopada','grudnia'))
	);
	if($pieces = preg_split('#[:/.\-,\s+]#', $format, NULL, PREG_SPLIT_NO_EMPTY)){
		if ($timestamp === null) { $timestamp = time(); }
		foreach($pieces as $datepart){
			if(array_key_exists($datepart, $to_convert)) {
				$replace[] = $to_convert[$datepart]['str'][(date($to_convert[$datepart]['dat'], $timestamp)-1)];
			} else {
				$replace[] = date($datepart, $timestamp);
			}
		}
		$result = strtr($format, array_combine($pieces, $replace));
		return $result;
	}
}
