<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function substr_full_word($string, $length, $end_string = '') {
	if(strlen($string) > $length) {
		$return = wordwrap($string, $length, "#|#", FALSE);
		$return = substr($return, 0, strpos($return, "#|#"));
		return closetags($return).$end_string;
	} else {
		return $string;
	}
}

function wrap_html($content, $title = '') {
	$html = <<<HTML
<!DOCTYPE html>
<html>
	<head>
		<title>{$title}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body>
		{$content}
	</body>
</html>
HTML;
	return $html;
}

function closetags($html) {
	// Put all opened tags into an array
	preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
	$openedtags = $result[1];   #put all closed tags into an array
	preg_match_all('#</([a-z]+)>#iU', $html, $result);
	$closedtags = $result[1];
	$len_opened = count($openedtags);
	# Check if all tags are closed
	if (count($closedtags) == $len_opened){
		return $html;
	}
	$openedtags = array_reverse($openedtags);
	# close tags
	for ($i=0; $i < $len_opened; $i++) {
		if (!in_array($openedtags[$i], $closedtags)){
			if(strtolower($openedtags[$i]) != 'br'){
				// Ignores <br> tags to avoid unnessary spacing at the end of the string
				$html .= '</'.$openedtags[$i].'>';
			}
		} else {
			unset($closedtags[array_search($openedtags[$i], $closedtags)]);
		}
	}
	return $html;
}

function strip_selected_tags($text, $tags = array())
{
	$args = func_get_args();
	$text = array_shift($args);
	$tags = func_num_args() > 2 ? array_diff($args,array($text))  : (array)$tags;
	foreach ($tags as $tag){
		while(preg_match('/<'.$tag.'(|\W[^>]*)>(.*)<\/'. $tag .'>/iusU', $text, $found)){
			$text = str_replace($found[0],$found[2],$text);
		}
		/*if(preg_match_all('/<'.$tag.'[^>]*>(.*)<\/'.$tag.'>/iU', $text, $found)){
			$text = str_replace($found[0],$found[1],$text);
		}*/
	}

	return $text;
}