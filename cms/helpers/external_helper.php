<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function load_ext_class($name, $dir) {
	if(!class_exists($name) && file_exists($dir)) {
		include($dir);
	}
	if(class_exists($name)) return TRUE;
	return FALSE;
}

function pass_hash($iterations = 8, $portable_hashes = TRUE) {
	if(load_ext_class('PasswordHash', 'external/PasswordHash.php')) {
		return new PasswordHash($iterations, $portable_hashes);
	}
}

function ckeditor($name, $value, $width, $toolbar = 'Full', $height = 400, $enterMode = 'p') {
	static $initialized = FALSE;
	include_once(config_item('site_path').config_item('external_path')."ckeditor/ckeditor.php") ;
	$enterModes = array(
		'p' => 'CKEDITOR.ENTER_P',
		'br' => 'CKEDITOR.ENTER_BR',
		'div' => 'CKEDITOR.ENTER_DIV'
	);

	$CKEditor = new CKEditor();
	$CKEditor->basePath 			= '/'.config_item('external_path').'ckeditor/';
	$CKEditor->returnOutput 		= true;
	$CKEditor->config['width']		= $width;
	$CKEditor->config['height']		= $height;
	$CKEditor->config['toolbar'] 	= $toolbar;
	if(isset($enterModes[$enterMode])){ $CKEditor->config['enterMode'] 	= $enterModes[$enterMode]; }

	$initialValue = $value;
	$code = $CKEditor->editor($name, $initialValue);

	if(!$initialized && function_exists('add_js')) {
		add_js(config_item('external_path').'ckeditor/ckeditor.js', 'admin');
		$initialized = TRUE;
	}

	return $code;
}

function elfinder_common_attr($pattern = NULL){
	if(!is_array($pattern)){ $pattern = array($pattern); }
	$ret = array();
	foreach ($pattern as $p){
		if(empty($p)){ continue; }
		$ret[] = array(
			'pattern' => $p,
			'hidden' => true,
			'read'    => true,
			'write'   => false,
			'locked'  => true
		);
	}
	return $ret;
}

function initialize_elfinder($value=''){
	$CI =& get_instance();
	$CI->load->helper('path');

	$opts = array(
		//'debug' => true,
		'roots' => array(
			array(
		 		'driver' => 'LocalFileSystem',
				'path'   => config_item('base_path').config_item('site_path').config_item('upload_path'),
				'URL'	=> site_url(config_item('upload_path')),
				'attributes' => elfinder_common_attr(array(
					'/^\/?.htaccess$/',
					'/^\/?.quarantine$/',
					'/^\/?.tmb$/',
					'/^\/?gallery$/',
					'/^\/?news$/',
					'/^\/?products$/',
					'/^\/?settings$/',
					'/^\/?slides$/'
				))
				// more elFinder options here
			)
		)
	);
	return $opts;
}

if(!function_exists('add_js')) {
	function add_js($file = '', $type = 'admin') {
		if(!in_array($type, array('frontend', 'admin', 'elfinder'))){ return; }

		$str = '';
		$ci = &get_instance();
		$header_js = $ci->config->item($type.'_header_js');

		if(empty($file)) { return; }

		if(is_array($file) && isset($file[0])) {
			foreach($file as $item) {
				if(in_array($item, $header_js) || !isset($item['file'])){ continue; }
				$header_js[] = $item;
			}
			$ci->config->set_item($type.'_header_js', $header_js);
		} elseif(is_array($file) && isset($file['file'])) {
			$str = $file;
			if(in_array($str, $header_js)){ return; }
			$header_js[] = $str;
			$ci->config->set_item($type.'_header_js', $header_js);
		} else {
			if(!is_array($file)){
				$str = array('file' => $file);
				if(in_array($str, $header_js)){ return; }
				$header_js[] = $str;
				$ci->config->set_item($type.'_header_js', $header_js);
			}
		}
	}
}

if(!function_exists('add_css')) {
	function add_css($file = '', $type = 'admin') {
		if(!in_array($type, array('frontend', 'admin', 'elfinder'))){ return; }

		$str = '';
		$ci = &get_instance();
		$header_css = $ci->config->item($type.'_header_css');

		if(empty($file)) { return; }

		if(is_array($file) && isset($file[0])) {
			foreach($file as $item) {
				if(in_array($item, $header_css) || !isset($item['file'])){ continue; }
				$header_css[] = $item;
			}
			$ci->config->set_item($type.'_header_css', $header_css);
		} elseif(is_array($file) && isset($file['file'])) {
			$str = $file;
			if(in_array($str, $header_css)){ return; }
			$header_css[] = $str;
			$ci->config->set_item($type.'_header_css', $header_css);
		} else {
			if(!is_array($file)){
				$str = array('file' => $file);
				if(in_array($str, $header_css)){ return; }
				$header_css[] = $str;
				$ci->config->set_item($type.'_header_css', $header_css);
			}
		}
	}
}

if(!function_exists('put_headers')) {
	function put_headers($type = 'admin') {
		if(!in_array($type, array('frontend', 'admin', 'elfinder'))){ return; }

		$str = '';
		$ci = &get_instance();
		$header_css = $ci->config->item($type.'_header_css');
		$header_js = $ci->config->item($type.'_header_js');
		$site_path = './'.$ci->config->item('site_path');

		$css_first = "";
		$css_middle = "";
		$css_last = "";
		foreach($header_css as $item) {
			// taka metoda obsługuje nie tylko pliki zdalne, ale też dynamicznie generowane, z lokalnymi adresami
			$mtime =(file_exists($site_path.$item['file']) ? filemtime($site_path.$item['file']) : FALSE);
			$css_m_time =($mtime !== FALSE ? '?'.filemtime($site_path.$item['file']) : '');
			if(isset($item['position']) && $item['position'] == 'first'){
				$css_first .= '<link rel="stylesheet" href="'.$item['file'].$css_m_time.'" type="text/css" />'."\n";
			}elseif(isset($item['position']) && $item['position'] == 'last'){
				$css_last .= '<link rel="stylesheet" href="'.$item['file'].$css_m_time.'" type="text/css" />'."\n";
			}else{
				$css_middle .= '<link rel="stylesheet" href="'.$item['file'].$css_m_time.'" type="text/css" />'."\n";
			}
		}
		$str .= $css_first.$css_middle.$css_last;

		$js_first = "";
		$js_middle = "";
		$js_last = "";
		foreach($header_js as $item) {
			$mtime =(file_exists($site_path.$item['file']) ? filemtime($site_path.$item['file']) : FALSE);
			$js_m_time =($mtime !== FALSE ? '?'.filemtime($site_path.$item['file']) : '');

			if(isset($item['position']) && $item['position'] == 'first') {
				$js_first .= '<script type="text/javascript" src="'.$item['file'].$js_m_time.'"></script>'."\n";
			} elseif(isset($item['position']) && $item['position'] == 'last') {
				$js_last .= '<script type="text/javascript" src="'.$item['file'].$js_m_time.'"></script>'."\n";
			} else {
				$js_middle .= '<script type="text/javascript" src="'.$item['file'].$js_m_time.'"></script>'."\n";
			}
		}
		$str .= $js_first.$js_middle.$js_last;

		return $str;
	}
}

if(!function_exists('put_footer_scripts')) {
	function put_footer_scripts() {

		$str = '';
		$ci = &get_instance();
		$footer_js = $ci->config->item('footer_js');
		$site_path = './'.$ci->config->item('site_path');

		$js_first = "";
		$js_middle = "";
		$js_last = "";
		foreach($footer_js as $item) {
			$mtime =(file_exists($site_path.$item['file']) ? filemtime($site_path.$item['file']) : FALSE);
			$js_m_time =($mtime !== FALSE ? '?'.filemtime($site_path.$item['file']) : '');

			if(isset($item['position']) && $item['position'] == 'first') {
				$js_first .= '<script type="text/javascript" src="'.$item['file'].$js_m_time.'"></script>'."\n";
			} elseif(isset($item['position']) && $item['position'] == 'last') {
				$js_last .= '<script type="text/javascript" src="'.$item['file'].$js_m_time.'"></script>'."\n";
			} else {
				$js_middle .= '<script type="text/javascript" src="'.$item['file'].$js_m_time.'"></script>'."\n";
			}
		}
		$str .= $js_first.$js_middle.$js_last;

		return $str;
	}
}