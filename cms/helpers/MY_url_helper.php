<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function iso_clear($text) {
	$text = strip_tags(htmlspecialchars_decode($text, ENT_QUOTES)); //zmiana encji na znaki, usunięcie znaczników HTML
	//przeliterowanie wszelkich znaków na ASCII
	if(function_exists('transliterator_transliterate')) {
		$text = transliterator_transliterate("Any-Latin; Latin-ASCII; Lower()", $text);
	} else {
		$text = strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $text));
	}
	$text = str_replace(array(' ', '_'), '-', $text); 		//zmiana spacji i podkreśleń na minusy
	$text = preg_replace('#[^a-z0-9:_\/\-]+#', '', $text); 	//wyrzucenie wszystkich znaków spoza zakresu bezpiecznego dla URL
    $text = preg_replace('/[-\s]+/', '-', $text); 			//usunięcie powtórzonych minusów
    return trim($text, '-');								//minus na początku i końcu nie jest potrzebny
}

function article_url($art, $absolute = TRUE) {
	$ci = &get_instance();
	$px_articles = $ci->config->item('px_articles');
	if(isset($art->title) && !empty($art->title)) {
		$title = $art->title;
	} elseif(isset($art->name) && !empty($art->name)) {
		$title = $art->name;
	}

	if(isset($art->url) && !empty($art->url)) {
		$url = preg_match('#^http(s)?:\/\/#i', $art->url) ? $art->url : site_url($art->url);
	} elseif(isset($art->user_url) && !empty($art->user_url)) {
		$url = site_url($art->user_url);
	} else {
		$url = site_url($px_articles . '/' . (isset($art->id_art) ? $art->id_art : $art->id) . '/' . iso_clear($title));
	}
	return ($absolute ? $url : str_replace(base_url(), '', $url));
}

function news_url($news, $absolute = TRUE) {
	$ci = &get_instance();
	$px_news = $ci->config->item('px_news');
	if(isset($news->title) && !empty($news->title)) {
		$title = $news->title;
	} elseif(isset($news->name) && !empty($news->name)) {
		$title = $news->name;
	}

	if(isset($news->url) && !empty($news->url)) {
		$url = preg_match('#^http(s)?:\/\/#i', $news->url) ? $news->url : site_url($news->url);
	} elseif(isset($news->user_url) && !empty($news->user_url)) {
		$url = site_url($news->user_url);
	} else {
		$url = site_url($px_news . '/' . $news->id . '/' . iso_clear($title));
	}
	return ($absolute ? $url : str_replace(base_url(), '', $url));
}

function gallery_url($gallery, $absolute = TRUE) {
	$ci = &get_instance();
	$px_gallery = $ci->config->item('px_gallery');
	if(isset($gallery->title) && !empty($gallery->title)) {
		$title = $gallery->title;
	} elseif(isset($gallery->name) && !empty($gallery->name)) {
		$title = $gallery->name;
	}

	if(isset($gallery->url) && !empty($gallery->url)) {
		$url = preg_match('#^http(s)?:\/\/#i', $gallery->url) ? $gallery->url : site_url($gallery->url);
	} else {
		$url = site_url($px_gallery . '/' . $gallery->id . '/' . iso_clear($title));
	}
	return ($absolute ? $url : str_replace(base_url(), '', $url));
}

function products_category_url($cat, $absolute = TRUE) {
	$ci = &get_instance();
	$px_products_category = $ci->config->item('px_products_category');
	if(isset($cat->title) && !empty($cat->title)) {
		$title = $cat->title;
	} elseif(isset($cat->name) && !empty($cat->name)) {
		$title = $cat->name;
	}

	if(isset($cat->url) && !empty($cat->url)) {
		$url = preg_match('#^http(s)?:\/\/#i', $cat->url) ? $cat->url : site_url($cat->url);
	} elseif(isset($cat->user_url) && !empty($cat->user_url)) {
		$url = site_url($cat->user_url);
	} else {
		$url = site_url($px_products_category . '/' . (isset($cat->id_cat) ? $cat->id_cat : $cat->id) . '/' . iso_clear($title));
	}
	return ($absolute ? $url : str_replace(base_url(), '', $url));
}

function product_url($prod, $absolute = TRUE) {
	$ci = &get_instance();
	$px_product = $ci->config->item('px_product');
	if(isset($prod->title) && !empty($prod->title)) {
		$title = $prod->title;
	} elseif(isset($prod->name) && !empty($prod->name)) {
		$title = $prod->name;
	}

	if(isset($prod->url) && !empty($prod->url)) {
		$url = preg_match('#^http(s)?:\/\/#i', $prod->url) ? $prod->url : site_url($prod->url);
	} elseif(isset($prod->user_url) && !empty($prod->user_url)) {
		$url = site_url($prod->user_url);
	} else {
		$url = site_url($px_product . '/' . (isset($prod->id_pr) ? $prod->id_pr : $prod->id) . '/' . iso_clear($title));
	}
	return ($absolute ? $url : str_replace(base_url(), '', $url));
}

function sort_url($base, $curr, $col, $title) {
	$base = preg_replace('#(.*?)sort/[^/]+/order/[^/]+/(.*?)#', '$1$2', $base);
	$order = 'asc';
	$ico_order = $order;
	$act = '';
	if(isset($curr->sort) && $curr->sort == $col) {
		if($curr->order == 'desc') $ico_order = 'desc';
		if($curr->order == 'asc') $order = 'desc';
		$act = ' active';
	}
	$title .= '<span class="fa fa-sort-amount-' . $ico_order . '"></span>';
	$attr = array('class' => 'sort_th' . $act);
	return anchor($base . "sort/{$col}/order/{$order}/pstart/0", $title, $attr);
}

function thumb_name($full_img, $suffix = '_thumb') {
	if(!empty($full_img)) {
		return preg_replace('#^(.*)(\.[a-z0-9]{3,4})$#i', '$1' . $suffix . '$2', $full_img);
	}
	return FALSE;
}

function lang_url($lang) {
	$lang_to_dm = array_flip(config_item('dm_to_lang'));
	if(array_key_exists($lang,$lang_to_dm)) $link = prep_url($lang_to_dm[$lang]);
	else $link = site_url('lang/'.$lang);
	return $link;
}
