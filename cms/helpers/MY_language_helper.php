<?php defined('BASEPATH') OR exit('No direct script access allowed.');

function lang_field(&$obj, $field_type = 'text', $field_name = '', $options = array()) {
	$CI =& get_instance();
	if(!class_exists('Lang_fields', FALSE)) {
		$CI->load->library('lang_fields');
	}
	return $CI->lang_fields->render($obj, $field_type, $field_name, $options);
}

function lang_value(&$obj, $field_name) {
	$CI =& get_instance();
	if(!class_exists('Lang_fields', FALSE)) {
		$CI->load->library('lang_fields');
	}
	return $CI->lang_fields->render($obj, 'value', $field_name, array('data-tabs-hidden' => true));
}
