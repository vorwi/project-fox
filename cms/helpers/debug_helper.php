<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function parray($tab, $hidden = FALSE){
	if(ENVIRONMENT != 'development'){ return ''; }
	
	$output = '<br />';
	if($hidden) $output .= '<div style="display: none">';
	$output .= '<pre>'.htmlspecialchars(print_r($tab, TRUE),ENT_QUOTES).'</pre><br />';
	if($hidden) $output .= '</div>';

	return $output;
}

function evar($var, $title = '') {
	if(ENVIRONMENT != 'development'){ return ''; }
	
	if(isset($var)) {
		if(is_array($var) || is_object($var)) {
			if($title != '') echo "<h3>### zawartość $title</h3>";
			echo '<pre>';
			print_r($var);
			echo '</pre>';
		} else {
			$var = ($var === FALSE ? 'FALSE' : $var);
			$var = ($var === TRUE ? 'TRUE' : $var);
			if($title != '') echo "<h3>### zawartość $title: $var</h3>";
			else echo "<h3>### $var</h3>";
		}
	} else {
		if($title != '') echo "<h3>### $title nie istnieje</h3>";
		else echo "<h3>### zmienna nie istnieje.</h3>";
	}
}