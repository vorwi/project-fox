<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Error_404 extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$error_string = lang('Błąd 404');
		$header_string = lang('Strona nie została odnaleziona');
        if (is_cli()) {
            $this->output->set_output(PHP_EOL.$error_string.PHP_EOL);
            echo $this->output->get_output();
            exit(EXIT_ERROR);
        }
        set_status_header(404);
        if ($this->input->is_ajax_request()) {
            $this->output->set_output($error_string);
            return;
        }
		
		$data = array(
			'header' => $header_string
		);
		$this->neocms->set_flag('error_404', TRUE);
		
		if($this->uri->segment(1) == 'admin'){
			$this->title = $header_string;
			$this->generate_view_admin('../errors/html/error_404_view', $data);
		} else {
			$this->generate_view('../../errors/html/error_404_view', $data);
		}
	} 
}
