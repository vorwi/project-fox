<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
	private $bl_count 		= 0;
	private $bl_lock 		= 0;
	private $bl_time_left 	= 0;
	
	private $br_count 		= 0;
	private $br_lock 		= 0;
	private $br_time_left 	= 0;

	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
		$this->admin->set_default_url('admin');
		$this->admin->set_redirect_url('after_login');
		
		$this->_bad_login();
		$this->_bad_remind();
	}
	
	private function _bad_login() {
		$this->bl_count = $this->model->bad_login_count();

		if($this->model->lock_login($this->bl_count)) {
			$this->bl_time_left = $this->model->lock_login_left();
			
			if($this->bl_time_left==0) {
				$this->model->remove_login_lock();
				$this->bl_count=0;
				$this->bl_lock=0;
				$this->bl_time_left=0;
			}
			else $this->bl_lock=1;
		}
	}
	
	private function _bad_remind() {
		$this->br_count = $this->model->bad_remind_count();
	
		if($this->model->lock_remind($this->br_count)) {
			$this->br_time_left = $this->model->lock_remind_left();
	
			if($this->br_time_left==0) {
				$this->model->remove_remind_lock();
				$this->br_count=0;
				$this->br_lock=0;
				$this->br_time_left=0;
			}
			else $this->br_lock=1;
		}
	}
	
	public function index() {
		$this->admin->skip_mod_referer();
		
		//jakby była potrzeba wygenerować hash do znanego hasła, to wystarczy odkomentować
		//jako drugi argument można podać date_add, żeby nie aktualizować go w bazie
		// if ($_SERVER['REMOTE_ADDR'] == '46.186.119.130' OR $_SERVER['REMOTE_ADDR'] == '46.186.119.185' OR $_SERVER['REMOTE_ADDR'] == '127.0.0.1') { evar(password_generator('asdf')); die(); }; 
		$data = array();

		$login = $this->input->post('login');
		$pass = $this->input->post('password');

		if($this->bl_lock==0){
			if(!empty($login) or !empty($pass)) {
				$this->form_validation->set_rules('login', lang('Login'), 'required');
				$this->form_validation->set_rules('password', lang('Hasło'), 'required');
				if($this->form_validation->run()===FALSE){
					$msg = array(lang("Login i hasło są wymagane."), 1);
					$this->neocms->forward_msg($msg);		
					redirect('admin/login');
				}

				if($this->admin->login($login,$pass)) {
					redirect($this->admin->get_redirect_url());
				}else {
					$msg = array(lang("Login lub hasło nie są poprawne."), 2);
					$this->neocms->forward_msg($msg);		
					redirect('admin/login');
				}
			}
		}

		$data = array(
			'bl_lock'   	=> $this->bl_lock,
			'bl_time_left'  => $this->bl_time_left,
			'bl_count'	  => $this->bl_count
		);

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data, 1);
	}
	
	public function logout() {
		$this->admin->logout();
		redirect('admin/login');
	}
	
	public function remind() {
		$data = array();
		if($this->br_lock==0){
			$data['cap'] = $this->admin->get_captcha();
		
			if($this->input->post('send') !== FALSE){
				if($this->model->check_rem()) {
					if($this->model->remind()) {
						$msg = array(lang('Potwierdzenie resetowania hasła zostało wysłane na Twój adres e-mail.'), 0);
					} else {
						$this->model->bad_remind_counter();
						$msg = array(lang('Podane konto nie istnieje, lub podano nieprawidłowy adres e-mail.'), 1);
					}
					$this->neocms->forward_msg($msg);
					redirect('admin/remind');
				}
			}
		}
		
		$data['br_lock'] = $this->br_lock;
		$data['br_time_left'] = $this->br_time_left;
		$data['br_count'] = $this->br_count;
	
		$this->generate_view_admin($this->mod_dir.'remind_password_view', $data, 1);
	}
	
	public function confirm() {
		$tab = $this->uri->ruri_to_assoc(-3);
	
		if(isset($tab['hash']) && !empty($tab['hash'])){
			if($this->model->confirmExists($tab['hash'])){
				if($this->model->confirm($tab['hash'])){
					$msg = array(lang('Resetowanie hasła zostało potwierdzone, nowe hasło zostanie wysłane na twój adres email.'), 0, 1);				
				}else {
					$msg = array(lang('Resetowanie hasła nie zostało potwierdzone. Spróbuj ponownie.'), 2, 1);
				}
				$this->neocms->forward_msg($msg);
				redirect('admin/login');
			} else {
				$msg = array(lang('Podane zgłoszenie nie zostało znalezione w bazie.'), 2, 1);
				$this->neocms->forward_msg($msg);
				redirect('admin/remind');
			}
		} else {
			redirect('admin/remind');
		}
	}
}
