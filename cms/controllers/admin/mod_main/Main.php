<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index() {
		$this->neocms->set_flag('no_content_wrapper', TRUE);

		$data = array(
			'last_logins' => $this->model->get_logins(),
			'last_articles' => $this->model->get_articles(),
			//'last_news' => $this->model->get_news()
		);

		$this->generate_view_admin('mod_main/main_view', $data);
	}

	public function compile_less() {
		if($this->user->main != 1) {
			show_error('Brak dostępu!', 401);
			return;
		}

		echo "<b>kompilator LESS => CSS</b><br>";

		require FCPATH.config_item('external_path')."less.php/Less.php";

		$public_path = config_item('external_path')."AdminLTE/";
		$base_path = './'.config_item('site_path').$public_path;

		$less_file = $base_path."less/AdminLTE.less";
		$css_file = $base_path."css/AdminLTE.min.css";

		$options = array(
			'compress' 			=> true,
			'sourceMap'         => true,
			'sourceMapWriteTo'  => $base_path."css/AdminLTE.map",
			'sourceMapURL'      => site_url($public_path."css/AdminLTE.map")
		);
		$less = new Less_Parser($options);
		try
		{
			$less->parseFile($less_file, $css_file);

			//TODO: trzeba robić kopię poprzedniej wersji pliku CSS - może jakiś globalny katalog backup w temp na takie sytuacje?
			copy($css_file, $css_file.'_bk'.date('ymd'));
			file_put_contents($css_file, $less->getCss());
			echo "<br>Gotowe!";
		}
		catch (Exception $e)
		{
			echo "Błąd kompilacji: ".$e->getMessage();
		}
	}
}