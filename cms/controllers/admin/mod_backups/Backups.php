<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backups extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
		
		$this->load->helper('file');
		$this->load->helper('directory');
	}

	public function index(){
		
		$data = array();
		
		if($this->input->post('backup_file')) {
			if($this->model->restore_backup()) $msg = array(lang('Kopia zapasowa została przywrócona'), 0);
			else $msg = array(lang('Nie udało się przywrócić kopii zapasowej, spróbuj ponownie'), 1);
			$this->neocms->forward_msg($msg);
			redirect(current_url());
		}

		$data['files'] = $this->model->get_all();
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);  
	}
  
}