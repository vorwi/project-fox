<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller {
	public $pr_dir			= 'products';
	public $gal_img_width	= '1920';
	public $gal_img_height	= '1080';
	public $gal_thumb_width	= '82';
	public $gal_thumb_height= '64';
	public $gal_thumb_list_width	= '200';
	public $gal_thumb_list_height= '164';

	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
		
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('zip');
		
		$this->load->helper('file');
		$this->load->helper('directory');
		$this->load->model('admin/'.$this->mod_dir.'categories_model');
	}

	public function index() {
		$data = array();

		$data['lang'] = $this->languages;
		$data['categories'] = $this->categories_model->get_all_categories();
		$data['all'] = $this->model->get_all_categories_with_products();
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

 	public function add() {
 		$response = new stdClass();
		if(!$this->model->add()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Produkt został dodany.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {
		
		$id = @is_numeric($this->params->del) ? $this->params->del : null;

		if($this->model->delete($id)) {
			$msg = array(lang("Zaznaczone produkty zostały usunięte."), 0);
		} else {
			$msg = array(lang("Nie został zaznaczony żaden produkt."), 1);
		}

		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title .= lang(': edycja');

		$data = array();

		if(@!is_numeric($this->params->id) || !$this->model->prodExists($this->params->id)){

			$msg = array(lang("Produkt nie istnieje."), 2);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		if (!isset($this->params->lang) || (isset($this->params->lang) && empty($this->params->lang))) {
			$this->params->lang = $this->admin->get_main_lang();
		}

		if(isset($this->params->check) && $this->params->check==1){
			if($this->input->post()){
				if($this->model->save($this->params->id,$this->params->lang)){
					$msg = array(lang("Produkt został zaktualizowany."), 0);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
				}
				else {
					$msg = array(lang("Produkt nie został zaktualizowany. Spróbuj ponownie."), 1);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
				}
			} else {
				redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
			}
		}

		$data['lang'] = $this->languages;
		$data['prod'] = $this->model->edit($this->params->id, $this->params->lang);
		$data['categories'] = $this->categories_model->get_all_categories();

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);

	}
	
	public function images() {
		$data = array ();
		
		add_js(config_item('gfx_a').'js/plupload/plupload.full.min.js', 'admin');
		add_js(config_item('gfx_a').'js/plupload/jquery.plupload.queue/jquery.plupload.queue.min.js', 'admin');
		add_js(config_item('gfx_a').'js/plupload/i18n/pl.js', 'admin');
		add_css(config_item('gfx_a').'js/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css', 'admin');
	
		$this->title .= lang(': galeria');
	
		if (@!is_numeric($this->params->id)) {
			$msg = array (lang("Produkt nie istnieje."),1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
	
		$product = $this->model->prExists($this->params->id);
	
		if ($product === FALSE) {
			$msg = array (lang("Produkt nie istnieje."),1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
	
		$this->title .= lang(': galeria: ').$product->title;
	
		if (isset($this->params->check) && $this->params->check == 1) {
			if ($this->model->save_images_pl($this->params->id)) {
				$msg = array (lang("Produkt został zaktualizowany."),0);
			} else {
				$msg = array (lang("Produkt nie został zaktualizowany. Spróbuj ponownie."),1);			
			}
			$this->neocms->forward_msg($msg);
			redirect($this->mod_url.'/images/id/'.$this->params->id);
		}
		if (isset($this->params->position) && $this->params->position == 1) {
			if ($this->model->position_images($this->params->id)) {
				$msg = array (lang("Kolejność została zaktualizowana."),0);
			} else {
				$msg = array (lang("Kolejność nie została zaktualizowana. Spróbuj ponownie."),1);			
			}
			$this->neocms->forward_msg($msg);
			redirect($this->mod_url.'/images/id/'.$this->params->id);
		}
		if (isset($this->params->del) && is_numeric($this->params->del)) {
			if ($this->model->del_images($this->params->del)) {
				$msg = array (lang("Zdjęcie zostało usunięte."),0);				
			} else {
				$msg = array (lang("Zdjęcie nie zostało usunięte. Spróbuj ponownie."),1);
			}
			$this->neocms->forward_msg($msg);
			redirect($this->mod_url.'/images/id/'.$this->params->id);
		}
		
		if (isset($this->params->crop) && is_numeric($this->params->crop)) {
			$data_crop = array();
			
			if ($this->input->post('crop')) {
				if($this->model->crop($this->params->crop)){
					$msg = array(lang("Miniaturka została zmieniona"), 0);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/images/id/'.$this->params->id);
				}
			}
			 
			$data_crop = $this->model->get_image($this->params->crop);
			 
			if($data_crop) {
				$data_crop['form_url'] 	= $this->mod_url.'/images/id/'.$data_crop['id_pr'].'/crop/'.$this->params->crop;
				$data_crop['width'] 	= $this->gal_thumb_width;
				$data_crop['height'] 	= $this->gal_thumb_height;
			} else {
				$msg = array(lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url.'/images/id/'.$this->params->id);
			}
			
			$this->generate_view_admin('crop_view', $data_crop, TRUE);
		}
		
		if (isset($this->params->crop_list) && is_numeric($this->params->crop_list)) {
			$data_crop = array();
		
			if ($this->input->post('crop')) {
				if($this->model->crop_list($this->params->crop_list)){
					$msg = array(lang("Miniaturka została zmieniona"), 0);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/images/id/'.$this->params->id);
				}
			}
			 
			$data_crop = $this->model->get_image($this->params->crop_list);
			 
			if($data_crop) {
				$data_crop['form_url'] 	= $this->mod_url.'/images/id/'.$data_crop['id_pr'].'/crop_list/'.$this->params->crop_list;
				$data_crop['width'] 	= $this->gal_thumb_list_width;
				$data_crop['height'] 	= $this->gal_thumb_list_height;
			} else {
				$msg = array(lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url.'/images/id/'.$this->params->id);
			}
		
			$this->generate_view_admin('crop_view', $data_crop, 1);
		}
	
		$img = $this->model->get_product_images($this->params->id);
	
		$data['img'] = $img;
	
		if(!isset($this->params->crop) && !isset($this->params->crop_list)){
			$this->generate_view_admin($this->mod_dir.$this->mod_name.'_images_view', $data);
		}
	}
}
