<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help extends MY_Controller {

	public function __construct(){	
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index() {		

		$data = array('all' => $this->model->get_all());
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

}
