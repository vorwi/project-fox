<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instructions extends MY_Controller {

	public function __construct(){	
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index() {		

		$data = array(
			'lang' => $this->languages,
			'all' => $this->model->get_all()
		);
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

 	public function add() {
		$response = new stdClass();
		if(!$this->model->verify_form('add')) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Artykuł został dodany'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function position() {
		$response = new stdClass();
		$response->code = 'ERROR';
		$data = $this->input->post('items');
		if(!empty($data)){
			$output = $this->model->save_positions($data);
			if($output > 0){
				$this->neocms->set_msg(lang('Pozycje zostały zapisane pomyślnie'), 0);
				$response->code = 'OK';
			}elseif($output == 0){
				$this->neocms->set_msg(lang('Nie znaleziono wpisów wymagających zapisanie pozycji'), 1);
			} else{
				$this->neocms->set_msg(lang('Wystąpił błąd podczas zapisywania pozycji'), 1);
			}
		} else {
			$this->neocms->set_msg(lang('Nie znaleziono wpisów wymagających zapisanie pozycji'), 1);
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}
	
	public function delete() {
		$id = @is_numeric($this->params->del) ? $this->params->del : null;
		
		if($this->model->delete($id)) {
			$msg = array(lang('Zaznaczone artykuły zostały usunięte'), 0);
		} else {
			$msg = array(lang('Nie został zaznaczony żaden artykuł'), 1);
		}

		$this->neocms->forward_msg($msg);

		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title .= lang(': edycja');

		$data = array();

		if(@!is_numeric($this->params->id) || !$this->model->artExists($this->params->id)){
			$msg = array(lang('Artykuł nie istnieje'), 0);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		if(empty($this->params->lang)) $this->params->lang = $this->admin->get_main_lang();

		if(isset($this->params->check) && $this->params->check==1){
			if($this->input->post()){
				
				if($this->model->verify_form('edit',$this->params->id)) {
					if($this->model->save($this->params->id,$this->params->lang)){
						$msg = array(lang('Artykuł został zaktualizowany'), 0);
					}else {
						$msg = array(lang('Artykuł nie został zaktualizowany. Spróbuj ponownie'), 1);
					}
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
				}		
			}
			else redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
		}

		$data['lang'] = $this->languages;
		$data['art'] = $this->model->edit($this->params->id, $this->params->lang);
		$data['all'] = $this->model->get_all();

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
}
