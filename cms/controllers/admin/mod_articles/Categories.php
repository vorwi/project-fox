<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MY_Controller {
 	
	public function __construct(){		
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index(){		
		$data = array('all'=>$this->model->get_all());

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);  
	}
	
	public function add() {		
		$response = new stdClass();
		
		if(!$this->model->verify_form()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Kategoria została dodana'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}
	
	public function delete() {
		$id = @is_numeric($this->params->del) ? $this->params->del : null;

		if($this->model->delete($id)) {
			$msg = array(lang('Zaznaczone kategorie zostały usunięte'), 0);
		}else {
			$msg = array(lang('Nie została zaznaczona żadna kategoria'), 1);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
	public function edit() {		
 		$this->title .= lang(': edycja');
 		
		$id = @is_numeric($this->params->id) ? $this->params->id : null;
		
		if(isset($this->params->check) && $this->params->check==1){
			
			$changed = $this->model->save();
		
			$msg = array(lang('Zaznaczone kategorie zostały zaktualizowane'), 0);
	   		$this->neocms->forward_msg($msg);
			$this->session->set_flashdata('changed', $changed);
			redirect($this->admin->get_redirect_url());
		}	
		
		$data = array('all'=>$this->model->edit($id));

		if($data['all']===FALSE) {
			
			$msg = array(lang('Nie znaleziono kategorii lub nie została zaznaczona żadna kategoria'), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
	   	$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
}