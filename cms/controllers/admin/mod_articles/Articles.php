<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends MY_Controller {
    
    public $cat_id = null;

    public function __construct(){	
        parent::__construct();
        $this->_init_module(__FILE__, __DIR__);
    }

    public function index() {		
        if(isset($this->params->cat) && is_numeric($this->params->cat)) $this->cat_id = $this->params->cat;

        $data = array('lang'=>$this->languages,'all'=>$this->model->get_all_categories());
        
        $this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
    }

     public function add() {
        $response = new stdClass();
        if(!$this->model->verify_form('add')) {
            $response->status = 'ERROR';
        } else {
            if($this->model->insert()) {
                $response->status = 'OK';
                $this->neocms->forward_msg(lang('Artykuł został dodany.'), 0);
                $response->redirect = $this->admin->get_redirect_url();
            }
        }
        $response->messages = collect_messages();
        $this->output->display_json($response);
    }

    public function position() {
        $response = new stdClass();
        $response->code = 'ERROR';
        $data = $this->input->post('items');
        if(!empty($data)){
            $output = $this->model->save_positions($data);
            if($output > 0){
                $this->neocms->set_msg(lang('Pozycje zostały zapisane pomyślnie'), 0);
                $response->code = 'OK';
            }elseif($output == 0){
                $this->neocms->set_msg(lang('Nie znaleziono wpisów wymagających zapisanie pozycji'), 1);
            } else{
                $this->neocms->set_msg(lang('Wystąpił błąd podczas zapisywania pozycji'), 1);
            }
        } else {
            $this->neocms->set_msg(lang('Nie znaleziono wpisów wymagających zapisanie pozycji'), 1);
        }
        $response->messages = collect_messages();
        $this->output->display_json($response);
    }
    
    public function delete() {
        $id = @is_numeric($this->params->del) ? $this->params->del : null;
        
        if($id != config_item('main_art')) {
            if($this->model->delete($id)) {
                $msg = array(lang('Zaznaczone artykuły zostały usunięte'), 0);
            } else {
                $msg = array(lang('Nie został zaznaczony żaden artykuł'), 1);
            }
        } else {
            $msg = array(lang('Nie można usunąć strony głównej'), 1);
        }

        $this->neocms->forward_msg($msg);

        redirect($this->admin->get_redirect_url());
    }

    public function pub() {
        $id = @is_numeric($this->params->id) ? $this->params->id : null;
        $status = @is_numeric($this->params->set) ? $this->params->set : null;

        if($id != config_item('main_art')) {
            if($this->neocms->pub($id,$status,$this->mod_table,'pub')) {
                $msg = array(lang('Zmiany zostały zapisane'), 0);
            } else {
                $msg = array(lang('Zmiany nie zostały zapisane'), 1);
            }
        } else {
            $msg = array(lang('Nie można publikować/odpublikować strony głównej'), 1);
        }

        $this->neocms->forward_msg($msg);
        redirect($this->admin->get_redirect_url());
    }

    public function edit() {
        
        $this->load->library('lang_fields');
        
        $this->title .= lang(': edycja');

        if(@!is_numeric($this->params->id) || !$this->model->artExists($this->params->id)){
            $msg = array(lang('Artykuł nie istnieje'), 0);
            $this->neocms->forward_msg($msg);
            redirect($this->admin->get_redirect_url());
        }

        $data = array();
        $data['art'] = $this->model->edit($this->params->id);
        $data['cat'] = $this->neocms->get_all_categories($data['art']->id_cat);
        
        if(isset($this->params->check) && $this->params->check==1){
            if($this->input->post()){
                
                if($this->model->verify_form('edit',$this->params->id)) {
                    if($this->model->save($data['art'])){
                        $msg = array(lang('Artykuł został zaktualizowany'), 0);
                    }else {
                        $msg = array(lang('Artykuł nie został zaktualizowany. Spróbuj ponownie'), 2);
                    }
                    $this->neocms->forward_msg($msg);
                    redirect($this->mod_url.'/edit/id/'.$this->params->id);
                }		
            }
            else redirect($this->mod_url.'/edit/id/'.$this->params->id);
        }

        $data['numbers'] = $this->model->get_all_numbers();
        $data['offers'] = $this->model->get_all_offers();
        $data['skills'] = $this->model->get_all_skills();
        
        $this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
    }

    public function add_number() {
        $this->model->add_number();

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function save_numbers() {
        $this->model->save_numbers();

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function delete_number() {
        $id = $this->params->number_id;

        $this->model->delete_number($id);

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function add_offer() {
        $this->model->add_offer();

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function save_offers() {
        $this->model->save_offers();

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function delete_offer() {
        $id = $this->params->offer_id;

        $this->model->delete_offer($id);

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function add_skill() {
        $this->model->add_skill();

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function save_skills() {
        $this->model->save_skills();

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }

    public function delete_skill() {
        $id = $this->params->skill_id;

        $this->model->delete_skill($id);

        redirect($this->mod_url.'/edit/id/'.$this->params->id);
    }
}
