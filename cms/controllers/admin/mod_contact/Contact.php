<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {
	public $img_dir			= 'contact/';
	
	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index(){
		$data = array();

		$data['art'] = $this->neocms->get_all_categories();
		$data['all'] = $this->model->get_all();
		$data['lang'] = $this->languages;
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);  
	}
	
	public function add() {
		$response = new stdClass();
		if(!$this->model->verify_form('add')) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Formularz został dodany.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}
	
	public function delete() {
		$id = @is_numeric($this->params->del) ? $this->params->del : null;

		if($this->model->delete($id)) {
			$msg = array(lang("Formularz został usunięty."), 0);
		}else {
			$msg = array(lang("Nie został zaznaczony żaden formularz."), 1);			
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
	public function edit() {
 		$this->title = lang('Formularze: edycja');
 		
		$data = array();
		$id = null;
		
		if(isset($this->params->check) && $this->params->check==1){
			if($this->model->verify_form('edit')) {
				$changed = $this->model->save();
				
				$msg = array(lang("Zaznaczone formularze zostały zaktualizowane."), 0);
				$this->neocms->forward_msg($msg);
				$this->session->set_flashdata('changed', $changed);
				redirect($this->admin->get_redirect_url());
			}		
		}
		
		if(@is_numeric($this->params->id)) $id = $this->params->id;
		
		$data['art'] = $this->neocms->get_all_categories();
		$data['all'] = $this->model->edit($id);
		$data['lang'] = $this->languages;

		if($data['all']===FALSE) {
			$msg = array(lang("Nie znaleziono formularza lub nie został zaznaczony żaden formularz."), 2);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
	   	$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
  
}