<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends MY_Controller {
	public $img_dir = 'gallery/';
	public $img_width = '1920';
	public $img_height = '1080';
	public $thumb_width = '154';
	public $thumb_height = '116';

	public function __construct() {
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);

		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('zip');

		$this->load->helper('file');
		$this->load->helper('directory');
	}

	public function index() {
		$data = array();

		$data['art'] = $this->neocms->get_all_categories();
		$data['news'] = $this->model->get_all_news();
		$data['galleries'] = $this->model->get_all();
		$data['lang'] = $this->languages;

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

	public function add() {
		$response = new stdClass();
		if(!$this->model->add()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Galeria została dodana.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {
		$id = @is_numeric($this->params->del) ? $this->params->del : null;

		if($this->model->delete($id)) {
			$msg = array(lang("Galeria została usunięta."), 0);
		}else {
			$msg = array(lang("Nie została zaznaczona żadna galeria."), 1);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title .= lang(': edycja');

		$data = array();
		$id = @is_numeric($this->params->id) ? $this->params->id : null;

		if(isset($this->params->check) && $this->params->check == 1) {
			$changed = $this->model->save();

			$msg = array(lang('Zaznaczone galerie zostały zaktualizowane.'),0);
			$this->neocms->forward_msg($msg);
			$this->session->set_flashdata('changed', $changed);
			redirect($this->admin->get_redirect_url());
		}

		$data['art'] = $this->neocms->get_all_categories();
		$data['news'] = $this->model->get_all_news();
		$data['all'] = $this->model->edit($id);
		$data['lang'] = $this->languages;

		if($data['all'] === FALSE) {

			$msg = array(lang("Nie znaleziono galerii lub nie została zaznaczona żadna galeria."),1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}

	public function galleries() {
		add_js(config_item('gfx_a').'js/plupload/plupload.full.min.js', 'admin');
		add_js(config_item('gfx_a').'js/plupload/jquery.plupload.queue/jquery.plupload.queue.min.js', 'admin');
		add_js(config_item('gfx_a').'js/plupload/i18n/pl.js', 'admin');
		add_css(config_item('gfx_a').'js/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css', 'admin');

		$this->title .= lang(': zarządzaj zdjęciami');

		$data = array();

		if(is_numeric($this->params->id)) {
			$id = $this->params->id;

			$gal = $this->model->edit($id);
			$gal = current($gal);
			$this->title = $gal->name.': '.lang('zarządzanie zdjęciami');

			if(isset($this->params->add) && $this->params->add == 1) {
				if($this->model->add_img($id)) {
					$msg = array(lang("Zdjęcie zostało dodane."),0);					
				} else {
					$msg = array(lang("Zdjecie nie zostało dodane. Sprobuj ponownie."),2);
				}
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url.'/galleries/id/'.$this->params->id);
			}

			if(isset($this->params->addpl) && $this->params->addpl == 1) {
				header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
				header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Cache-Control: post-check=0, pre-check=0", false);
				header("Pragma: no-cache");
				if($this->model->add_img_pl($id)) {
					die('{"jsonrpc" : "2.0", "success" : {"code": 200, "message": "File upload success."}, "id" : "id"}');
				}
			}

			if(isset($this->params->del) && is_numeric($this->params->del)) {
				if($this->model->del_img($this->params->del)) {
					$msg = array(lang("Zdjecie zostało usunięte."),0);					
				} else {
					$msg = array(lang("Zdjęcie nie zostało usunięte. Sprobuj ponownie."),2);
				}
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url.'/galleries/id/'.$this->params->id);
			}

			if(isset($this->params->position) && is_numeric($this->params->position)) {
				if($this->neocms->positions('gallery_img')) {
					$msg = array(lang("Kolejność została zaktualizowana."),0);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/galleries/id/'.$this->params->id);
				}
			}

			if(isset($this->params->save) && is_numeric($this->params->save)) {
				if($this->model->save_img($this->params->save)) {
					$msg = array(lang("Galeria został zaktualizowana."),0);
				} else {
					$msg = array(lang("Galeria nie została zaktualizowana. Sprobuj ponownie."),2);
				}
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url.'/galleries/id/'.$this->params->id);
			}

			$data['img'] = $this->model->get_all_images($id);
		}

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_img_view', $data);
	}

	public function crop() {
		$data = array();

		if(isset($this->params->id) && is_numeric($this->params->id)) {
			$id = $this->params->id;

			if($this->input->post('crop')) {
				if($this->model->crop($id)) {
					$msg = array(lang("Miniaturka została zmieniona"),0);
					$this->neocms->forward_msg($msg);
				}
			}
		} else {
			$msg = array(lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."),1);
			$this->neocms->forward_msg($msg);
		}

		$data = $this->model->get_image($id);

		if($data) {
			$imgsize = getimagesize('./'.config_item('site_path').$data['path'].$data['img']);

			$data['form_url'] = $this->mod_url.'/crop/id/'.$id;
			$data['width'] = $this->thumb_width;
			$data['height'] = $this->thumb_height;
		} else {
			$msg = array(lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."),1);
			$this->neocms->forward_msg($msg);
		}

		$this->generate_view_admin('crop_view', $data, TRUE);
	}

	public function edit_img() {
		$data = array();

		if(isset($this->params->id) && is_numeric($this->params->id)) {

			$id = $this->params->id;

			if($this->input->post('edit_img')) {
				if($this->model->update_img($id)) {
					$msg = array(lang("Opis zdjęcia został zmieniony."),0);
				} else {
					$msg = array(lang("Nie udało się zapisać zmian."),1);
				}
				$this->neocms->forward_msg($msg);
			}
		} else {
			$msg = array(lang("Zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."),1);
			$this->neocms->forward_msg($msg);
		}

		$data = $this->model->get_image($id);

		if($data) {
			$data['form_url'] = $this->mod_url.'/edit_img/id/'.$id;
		} else {
			$msg = array(lang("Zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."),1);
			$this->neocms->forward_msg($msg);
		}

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_img_view', $data, FALSE, FALSE, 'iframe');
	}

}
