<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails extends MY_Controller {
	
	public function __construct(){

		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}
	
	public function index(){
	
		$data = array();
		
		$limit = 100;
		
		$count = $this->db->count_all($this->mod_table.'_emails');
		$base_url = "{$this->mod_url}/index/page/";
		
		$pconfig = array(
			'base_url' => $base_url,
			'total_rows' => $count,
			'per_page' => $limit,
			'uri_segment' => $this->uri->total_segments(),
			'first_url' => $base_url.'0',
		);
		$this->pagination->init($pconfig);
		$data['pagination'] = $this->pagination->create_links();
		
		$start = $this->uri->segment($this->uri->total_segments());
		
		$data['start'] = $start;
		$data['groups'] = $this->model->getGroups();
		$data['emails'] = $this->model->getEmails($start, $limit, FALSE);
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}
	
	public function add() {
		
		$response = new stdClass();
		if(!$this->model->checkFormAdd()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->addEmails()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Adresy zostały dodane.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			} else {
				$response->status = 'ERROR';
				$this->neocms->forward_msg(lang('Pole musi zawierać poprawne adresy email.'), 2);
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}
	
	public function delete() {
		
		$id = (isset($this->params->id) && is_numeric($this->params->id)) ? $this->params->id : null;
			
		if($this->model->delEmails($id)){
			$msg = array('Adresy zostały usunięte.', 0);
		} else {
			$msg = array('Adresy nie zostały usunięte. Spróbuj ponownie.', 2);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
	public function confirm() {
		
		$id = (isset($this->params->id) && is_numeric($this->params->id)) ? $this->params->id : null;
			
		if($this->model->confirmEmails($id)){
			$msg = array('Adresy zostały zatwierdzone.', 0);
		}else {
			$msg = array('Adresy nie zostały zatwierdzone. Spróbuj ponownie.', 2);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	

	public function assignGroups() {
	
		if($this->model->assignGroups()) {
	
			$msg = array("Przypisania zostały zaktualizowane.", 0);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
	
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}
	
}