<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends MY_Controller {
	
	public function __construct(){

		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}
	
	public function index(){
	
		$data = array();
		
		$curr_lang = isset($this->params->lang) ? $this->params->lang : $this->admin->get_main_lang();

		$data['curr_lang'] = $curr_lang;
    	$data['lang'] = $this->languages;
    	$data['projects'] = $this->model->getProjects($curr_lang);
	
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}
	
	public function add() {
		$response = new stdClass();
		if(!$this->model->checkProjectAdd()) {
			$response->status = 'ERROR';
		} else {
			if($id = $this->model->addProject()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Projekt został dodany.'), 0);
				$response->redirect = site_url($this->mod_url.'/edit/id/'.$id);
			} else {
				$response->status = 'ERROR';
				$this->neocms->forward_msg(lang('Projekt nie został dodany.'), 2);
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}
	
	public function delete() {
		$id = (isset($this->params->id) && is_numeric($this->params->id)) ? $this->params->id : null;
		
		if($this->model->delProjects($id)) {
			$msg = array("Zaznaczone wpisy zostały usunięte.", 0);
		}
		else {
			$msg = array("Nie został zaznaczony żaden wpis.", 1);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
	
		$this->title = 'Projekt: edycja';
		 
		add_js(config_item('gfx_a').'js/newsletter.js');
		
		$tab = $this->uri->uri_to_assoc();
		
		$id = (isset($this->params->id) && is_numeric($this->params->id)) ? $this->params->id : null;
		 
		if(@!is_numeric($id) || !$this->model->projectExists($id)){
	
			$msg = array("Projekt nie istnieje.", 0);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
		if(isset($this->params->check) && $this->params->check==1){
			
			if($this->model->checkProjectAdd()){
				if($this->model->saveProject($id)) {
					$msg = array("Wpis zaktualizowany.", 0);
					$this->neocms->forward_msg($msg);
					$this->session->set_flashdata('changed', $changed);
					redirect($this->admin->get_redirect_url());
				}
			}
		}
		 
		$data = array();
		$data['lang'] 		= $this->languages;
		$data['project'] 	= $this->model->editProject($id);
		$data['newsletter'] = trim($data['project']->text)!='' ? $data['project']->text : setting('newsletter');
		if(trim($data['project']->content) != '') {
			$data['newsletter_content'] = $data['project']->content;
		} else {
			$data['newsletter_content'] = $this->load->view('admin/'.$this->mod_dir.'content_tpl_view', array(), TRUE);
		}
			
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
}