<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Send extends MY_Controller {
	
	public function __construct(){

		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}
	
	public function index(){
		$data = array();
		
		add_js(config_item('gfx_a').'js/newsletter.js');
		
		$curr_lang = isset($this->params->lang) ? $this->params->lang : $this->admin->get_main_lang();
		
		$data['numer'] = $this->model->getNumber();
		$data['emails'] = $this->model->getEmails();
		$data['groups'] = $this->model->getGroups(null,null,1);
		$data['lang'] = $curr_lang;
		$data['projects'] = $this->model->getProjects($curr_lang);
		
		if($this->input->post('project')) {
			$project = $this->model->getProject($this->input->post('project'));
			$data['newsletter'] = $project->text;
			$data['newsletter_content'] = $project->content;
		}
		else {
			$data['newsletter'] = setting('newsletter');
			$data['newsletter_content'] = $this->load->view('admin/'.$this->mod_dir.'content_tpl_view', array(), TRUE);
		}

		if(isset($this->params->check)){
			if($this->model->checkForm()){

				if($this->model->save_queue()) {
					$msg = array('Newsletter został zapisany do wysyłki. Kolejne e-maile będą wysyłane w tle, w zależności od obciążenia i limitów serwera.', 0);
				} else {
					$msg = array('Newsletter nie został zapisany. Spróbuj ponownie.', 2);
				}
				$this->neocms->forward_msg($msg);
				redirect($this->admin->get_redirect_url());

			}
		}
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}
}