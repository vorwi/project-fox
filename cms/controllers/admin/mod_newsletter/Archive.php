<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Archive extends MY_Controller {
	
	public function __construct(){

		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}
	
	public function index(){
		$data = array();
		$limit = 25;
		
		$count = $this->db->count_all($this->mod_table);
		$base_url = "{$this->mod_url}/index/page/";
		
		$pconfig = array(
				'base_url' => $base_url,
				'total_rows' => $count,
				'per_page' => $limit,
				'uri_segment' => $this->uri->total_segments(),
				'first_url' => $base_url.'0',
		);
		$this->pagination->init($pconfig);
		$data['pagination'] = $this->pagination->create_links();
		
		$start = (isset($this->params->page) && is_numeric($this->params->page)) ? ($this->params->page*$limit)-$limit : 0;
		
		$data['newsletters'] = $this->model->getNewsletters($start, $limit, FALSE);
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}
	
	public function delete() {
		$id = (isset($this->params->id) && is_numeric($this->params->id)) ? $this->params->id : null;
			
		if($this->model->delNewsletters($id)){
			$msg = array('Newsletter został usunięty.', 0);
		} else {
			$msg = array('Newsletter nie został usunięty. Spróbuj ponownie.', 2);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
	public function edit() {
		
		if(@!is_numeric($this->params->id)) {
			$msg = array("Wpis nie istnieje.",0);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
		$id = $this->params->id;
		$show = $this->model->getNewsletter($id);
		
		if($show===FALSE) {
			$msg = array("Wpis nie istnieje.",0);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
	
		if(isset($this->params->check) && $this->params->check==1){
			if($this->model->saveNewsletter($id)){
				$msg = array('Newsletter został zaktualizowany.', 0);
				$this->neocms->forward_msg($msg);
	
				redirect(site_url($this->mod_url.'/edit/id/'.$id));
			}
		}
	
		$data['show'] = $show;
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
		
		
	}
}