<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends MY_Controller {
	
	public function __construct(){

		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}
	
	public function index(){
		$data = array();
		$limit = 100;
		
		$count = $this->db->count_all($this->mod_table.'_groups');
		$base_url = "{$this->mod_url}/index/page/";
		
		$pconfig = array(
				'base_url' => $base_url,
				'total_rows' => $count,
				'per_page' => $limit,
				'uri_segment' => $this->uri->total_segments(),
				'first_url' => $base_url.'0',
		);
		$this->pagination->init($pconfig);
		$data['pagination'] = $this->pagination->create_links();
		
		$start = (isset($this->params->page) && is_numeric($this->params->page)) ? ($this->params->page*$limit)-$limit : 0;
	
		$data['start'] = $start;
		$data['groups'] = $this->model->getGroups($start, $limit);
	
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}
	
	public function add() {
		$response = new stdClass();
		if(!$this->model->checkGroupAdd()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->addGroup()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Grupa została dodana.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			} else {
				$response->status = 'ERROR';
				$this->neocms->forward_msg(lang('Grupa nie została dodana.'), 2);
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}
	
	public function delete() {
	
		$id = (isset($this->params->id) && is_numeric($this->params->id)) ? $this->params->id : null;
			
		if($this->model->delGroups($id)){
			$msg = array('Grupy zostały usunięte.', 0);
		}
		else {
			$msg = array('Grupy nie zostały usunięte. Spróbuj ponownie.', 2);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
	public function edit() {
	
		$this->title = 'Grupy adresatów: edycja';
			
		$data = array();
		$id = (isset($this->params->id) && is_numeric($this->params->id)) ? $this->params->id : null;
	
		if(isset($this->params->check) && $this->params->check==1){
	
			$changed = $this->model->saveGroups();
	
			$msg = array("Zaznaczone grupy zostały zaktualizowane.", 0);
			$this->neocms->forward_msg($msg);
			$this->session->set_flashdata('changed', $changed);
			redirect($this->admin->get_redirect_url());
		}
	
		$data['all'] = $this->model->editGroups($id);
	
		if($data['all']===FALSE) {
	
			$msg = array("Nie znaleziono grupy lub nie została zaznaczona żadna grupa.", 2);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
			
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
}