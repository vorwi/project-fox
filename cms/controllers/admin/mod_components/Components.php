<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Components extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index() {
		$data = array('lang'=>$this->languages,'rows'=>$this->model->get_rows());
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

 	public function add() {
 		$response = new stdClass();
		if(!$this->model->add()) {
			$response->status = 'ERROR';
		}else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Komponent został dodany'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {
		$id = @is_numeric($this->params->del) ? $this->params->del : null;
		
		if($this->model->delete($id)) {
			$msg = array(lang('Zaznaczone wpisy zostały usunięte'), 0);
		} else {
			$msg = array(lang('Nie został zaznaczony żaden wpis'), 1);
		}

		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title .= lang(': edycja');

		if(@!is_numeric($this->params->id)){
			$msg = array(lang("Wpis nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		if (!isset($this->params->lang) || (isset($this->params->lang) && empty($this->params->lang))) {
			$this->params->lang = $this->admin->get_main_lang();
		}

		if(!$row = $this->model->edit($this->params->id, $this->params->lang)){
			$msg = array(lang("Wpis nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}


		if(isset($this->params->check) && $this->params->check==1){
			if($this->input->post()){
				if($this->model->save($this->params->id,$this->params->lang)){
					$msg = array(lang("Wpis został zaktualizowany."), 0);
					$this->neocms->forward_msg($msg);
					//redirect('admin/'.$this->mod_dir.$this->mod_name.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
					redirect($this->admin->get_redirect_url());
				}else {
					$msg = array(lang("Wpis nie został zaktualizowany. Spróbuj ponownie."), 2);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
				}
			}else {
				redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
			}
		}
		$data = array('lang'=>$this->languages,'row'=>$row);
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
}
