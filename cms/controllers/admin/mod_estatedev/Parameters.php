<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parameters extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
		
		$this->config->load('estatedev', TRUE);
		
		$config['estatedev_custom_parameters'] = array();
		$param = new stdClass();
		$config['estatedev_custom_parameters'][] = $param;
	}

	public function index() {
		$data = array();

		$data['items'] = $this->model->get_all();
		$data['lang'] = $this->languages;

		$this->generate_view_admin($this->mod_dir .$this->mod_name. '_view', $data);
	}

	public function add() {
		$response = new stdClass();
		if(!$this->model->insert_validate()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Parametr został dodany.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {
		if (!isset($this->params->id) && is_array($this->input->post('check'))) {
			$id = $this->input->post('check');
		} else {
			$id = $this->params->id;
		}

		if ($this->model->delete($id)) {
			$msg = array(lang("Zaznaczone atrybuty nieruchomości zostały usunięte."), 0);
		} else {
			$msg = array(lang("Nie został zaznaczony żaden atrybut nieruchomości."), 1);
		}

		$this->neocms->forward_msg($msg);

		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title .= lang(': edycja');

		$data = array();

		if (@!is_numeric($this->params->id)) {
			$msg = array(lang("Parametr nieruchomości nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		if (!$this->model->exists($this->params->id)) {
			$msg = array(lang("Parametr nieruchomości nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		if (!isset($this->params->lang) || (isset($this->params->lang) && empty($this->params->lang))) {
			$this->params->lang = $this->admin->get_main_lang();
		}

		if ($this->input->post()) {
			if ($this->model->save($this->params->id, $this->params->lang, $this->languages)) {
				$msg = array(lang("Parametr nieruchomości został zaktualizowany."), 0);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/edit/id/' . $this->params->id . '/lang/' . $this->params->lang);
			} else {
				$msg = array(lang("Parametr nieruchomości nie został zaktualizowany. Spróbuj ponownie."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/edit/id/' . $this->params->id . '/lang/' . $this->params->lang);
			}
		}

		$data['lang'] = $this->params->lang;
		$data['item'] = $this->model->edit($this->params->id, $this->params->lang);

		$this->generate_view_admin($this->mod_dir . $this->mod_name . '_edit_view', $data);
	}

}
