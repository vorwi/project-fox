<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Investments extends MY_Controller {
	
	public $gal_dir = 'estatedev/';
	public $gal_img_width = '1920';
	public $gal_img_height = '650';
	public $gal_thumb_width = '82';
	public $gal_thumb_height = '64';
	public $gal_thumb_list_width = '263';
	public $gal_thumb_list_height = '300';

	public function __construct() {
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);

		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->library('zip');
		$this->load->helper('file');
		$this->load->helper('directory');

		$dir = './' . config_item('site_path') . config_item('upload_path') . $this->gal_dir;
		if (!is_dir($dir)) {
			mkdir($dir);
		}
	}

	public function index() {
		$data = array();

		$data['items'] = $this->model->get_all();
		$data['lang'] = $this->languages;

		$this->generate_view_admin($this->mod_dir .$this->mod_name. '_view', $data);
	}
	
	public function add() {
		$response = new stdClass();
		if(!$this->model->insert_validate()) {
			$response->status = 'ERROR';
		} else {
			if($id = $this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Inwestycja została dodana. Uzupełnij poniższe dane.'), 0);
				$response->redirect = $this->mod_url . '/edit/id/' . $id . '/lang/' . $this->admin->get_main_lang();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {
		if (!isset($this->params->id) && is_array($this->input->post('check'))) {
			$id = $this->input->post('check');
		} else {
			$id = $this->params->id;
		}

		if ($this->model->delete($id)) {
			$msg = array(lang("Zaznaczone inwestycje zostały usunięte."), 0);
		} else {
			$msg = array(lang("Nie została zaznaczona żadena inwestycja."), 1);
		}

		$this->neocms->forward_msg($msg);

		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title .= lang(': edycja');

		$data = array();

		if (@!is_numeric($this->params->id)) {
			$msg = array(lang("Inwestycja nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		if (!$this->model->exists($this->params->id)) {
			$msg = array(lang("Inwestycja nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		if (!isset($this->params->lang) || (isset($this->params->lang) && empty($this->params->lang))) {
			$this->params->lang = $this->admin->get_main_lang();
		}

		if ($this->input->post()) {
			if ($this->model->save($this->params->id, $this->params->lang)) {
				$msg = array(lang("Inwestycja została zaktualizowana."), 0);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/edit/id/' . $this->params->id . '/lang/' . $this->params->lang);
			} else {
				$msg = array(lang("Inwestycja nie została zaktualizowana. Spróbuj ponownie."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/edit/id/' . $this->params->id . '/lang/' . $this->params->lang);
			}
		}

		$data['lang'] = $this->languages;
		$data['item'] = $this->model->edit($this->params->id, $this->params->lang);

		$this->generate_view_admin($this->mod_dir . $this->mod_name . '_edit_view', $data);
	}

	public function images() {
		$data = array();
		
		add_js(config_item('gfx_a').'js/plupload/plupload.full.min.js', 'admin');
		add_js(config_item('gfx_a').'js/plupload/jquery.plupload.queue/jquery.plupload.queue.min.js', 'admin');
		add_js(config_item('gfx_a').'js/plupload/i18n/pl.js', 'admin');
		add_css(config_item('gfx_a').'js/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css', 'admin');

		$investment = $this->model->get($this->params->id);
		$data['investment'] = $investment;

		if ($investment === FALSE) {
			$msg = array(lang("Inwestycja nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		$this->title = lang('Zdjęcia inwestycji: ') . $investment->name;

		if (isset($this->params->check) && $this->params->check == 1) {
			if ($this->model->save_images($this->params->id)) {
				$msg = array(lang("Inwestycja została zaktualizowana."), 0);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			} else {
				$msg = array(lang("Inwestycja nie została zaktualizowana. Spróbuj ponownie."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			}
		}
		if (isset($this->params->position) && $this->params->position == 1) {
			if ($this->model->position_images($this->params->id)) {
				$msg = array(lang("Kolejność została zaktualizowana."), 0);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			} else {
				$msg = array(lang("Kolejność nie została zaktualizowana. Spróbuj ponownie."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			}
		}
		if (isset($this->params->del) && is_numeric($this->params->del)) {
			if ($this->model->del_images($this->params->del)) {
				$msg = array(lang("Zdjęcie zostało usunięte."), 0);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			} else {
				$msg = array(lang("Zdjęcie nie zostało usunięte. Spróbuj ponownie."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			}
		}

		if (isset($this->params->crop) && is_numeric($this->params->crop)) {
			$data_crop = array();

			if ($this->input->post('crop')) {
				if ($this->model->crop($this->params->crop)) {
					$msg = array(lang("Miniaturka została zmieniona"), 0);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url . '/images/id/' . $this->params->id);
				}
			}

			$to_crop = $this->model->get_image($this->params->crop);

			$data_crop['img'] = $to_crop->image;
			$data_crop['path'] = config_item('upload_path') . $this->gal_dir . $this->params->id . '/';

			if ($to_crop) {
				$imgsize = getimagesize('./' . config_item('site_path') . config_item('upload_path') . $this->gal_dir . $this->params->id . '/' . $to_crop->image);
				$scaled_width = 800;
				if ($imgsize[0] > $scaled_width) {
					$scale = $imgsize[0] / $scaled_width;
				} else {
					$scale = 1;
				}
				$data_crop['form_url'] = $this->mod_url . '/images/id/' . $this->params->id . '/crop/' . $to_crop->id;
				$data_crop['width'] = $this->gal_thumb_width * (1 / $scale);
				$data_crop['height'] = $this->gal_thumb_height * (1 / $scale);
				$data_crop['aspect'] = $this->gal_thumb_width . ':' . $this->gal_thumb_height;
				$data_crop['scale'] = $scale;
			} else {
				$msg = array(lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			}

			$this->generate_view_admin('crop_view', $data_crop, TRUE);
		}

		if (isset($this->params->crop_list) && is_numeric($this->params->crop_list)) {
			$data_crop = array();

			if ($this->input->post('crop')) {
				if ($this->model->crop_list($this->params->crop_list)) {
					$msg = array(lang("Miniaturka została zmieniona"), 0);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url . '/images/id/' . $this->params->id);
				}
			}

			$to_crop = $this->model->get_image($this->params->crop_list);

			$data_crop['img'] = $to_crop->image;
			$data_crop['path'] = config_item('upload_path') . $this->gal_dir . $this->params->id . '/';

			if ($data_crop) {
				$imgsize = getimagesize('./' . config_item('site_path') . config_item('upload_path') . $this->gal_dir . $this->params->id . '/' . $to_crop->image);
				$scaled_width = 800;
				if ($imgsize[0] > $scaled_width) {
					$scale = $imgsize[0] / $scaled_width;
				} else {
					$scale = 1;
				}

				$data_crop['form_url'] = $this->mod_url . '/images/id/' . $this->params->id . '/crop_list/' . $this->params->crop_list;
				$data_crop['width'] = $this->gal_thumb_list_width * (1 / $scale);
				$data_crop['height'] = $this->gal_thumb_list_height * (1 / $scale);
				$data_crop['aspect'] = $this->gal_thumb_list_width . ':' . $this->gal_thumb_list_height;
				$data_crop['scale'] = $scale;
			} else {
				$msg = array(lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			}

			$this->generate_view_admin('crop_view', $data_crop, TRUE);
		}
		
		if (isset($this->params->desc) && is_numeric($this->params->desc)) {
			$this->output->enable_profiler(FALSE);
			
			$data_desc = array();

			if ($this->input->post('desc')) {
				if ($this->model->save_images_desc($this->params->desc)) {
					$msg = array(lang("Opis został zmieniony"), 0);
					$this->neocms->forward_msg($msg);
					// przekierowanie jest wykonane w widoku ustawiania opisu zamiast poniższej linii
					//redirect($this->mod_url . '/images/id/' . $this->params->id . '/desc/' . $this->params->desc);
				}
			}

			$to_do = $this->model->get_image($this->params->desc);

			$data_desc['form_url'] = $this->mod_url . '/images/id/' . $this->params->id . '/desc/' . $to_do->id;
			$data_desc['img'] = $to_do;
			$data_desc['path'] = config_item('upload_path') . $this->gal_dir . $this->params->id . '/';
			$data_desc['languages'] = $this->languages;
			if ($to_do) {
				
			} else {
				$msg = array(lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."), 1);
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url . '/images/id/' . $this->params->id);
			}

			$this->generate_view_admin('desc_view', $data_desc, 1);
		}

		$data['img'] = $this->model->get_images($this->params->id);

		if (!isset($this->params->crop) && !isset($this->params->crop_list) && !isset($this->params->desc)) {
			$this->generate_view_admin($this->mod_dir . $this->mod_name . '_images_view', $data);
		}
	}

	public function buildings() {
		$data = array();

		$investment = $this->model->get($this->params->id);
		$data['investment'] = $investment;

		if ($investment === FALSE) {
			$msg = array(lang("Inwestycja nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		$this->title = lang('Budynki inwestycji: ') . $investment->name;

		if (isset($this->params->action) && $this->params->action == 'edit' && isset($this->params->edib)) {
			if ($this->input->post()) {
				if ($this->model->buildings_save($this->params->edib)) {
					$msg = array(lang("Budynek został zaktualizowany."), 0);
					$this->neocms->forward_msg($msg);
					if ($this->model->buildings_save_face_maps($this->params->edib)) {
						$msg = array(lang("Kondygnacje zostały zapisane."), 0);
						$this->neocms->forward_msg($msg);
					}
				} else {
					$msg = array(lang("Budynek nie został zaktualizowany. Spróbuj ponownie."), 1);
					$this->neocms->forward_msg($msg);
				}
				redirect($this->mod_url . '/buildings/id/' . $this->params->id . '/action/edit/edib/' . $this->params->edib);
			}

			$data['item'] = $this->model->get_buildings(null, $this->params->edib);
			$data['facemaps'] = $this->model->get_buildings_face_maps($this->params->edib);
			$this->generate_view_admin($this->mod_dir . $this->mod_name . '_buildings_edit_view', $data);
		}

		if (isset($this->params->action) && $this->params->action == 'delete' && isset($this->params->edib)) {
			if ($this->model->buildings_delete($this->params->edib)) {
				$msg = array(lang("Zmiany zostały zapisane."), 0);
			} else {
				$msg = array(lang("Zmiany nie zostały zapisane."), 1);
			}
			$this->neocms->forward_msg($msg);
			redirect($this->mod_url . '/buildings/id/' . $this->params->id);
		}

		if (isset($this->params->action) && $this->params->action == 'pub' && isset($this->params->edib)) {
			if ($this->model->buildings_pub($this->params->set, $this->params->edib)) {
				$msg = array(lang("Zmiany zostały zapisane."), 0);
			} else {
				$msg = array(lang("Zmiany nie zostały zapisane."), 1);
			}
			$this->neocms->forward_msg($msg);
			redirect($this->mod_url . '/buildings/id/' . $this->params->id);
		}

		if (isset($this->params->action) && $this->params->action == 'add') {
			$response = new stdClass();
			if($id = $this->model->add_buildings($this->params->id)) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Budynek został dodany. Uzupełnij kondygnacje.'), 0);
				$response->redirect = $this->mod_url . '/buildings/id/' . $this->params->id . '/action/edit/edib/' . $id;
			} else {
				$response->status = 'ERROR';
				$this->neocms->forward_msg(lang('Budynek nie został dodany. Spróbuj ponownie.'), 1);
				$response->redirect = $this->mod_url . '/buildings/id/' . $this->params->id;
			}
			$response->messages = collect_messages();
			$this->output->display_json($response);
		}

		if (isset($this->params->action) && $this->params->action == 'save_position') {
			$this->output->enable_profiler(FALSE);
			$data = new stdClass();
			$data->id = $this->model->buildings_save_position();
			$this->output->set_content_type('application/json')->set_output(json_encode($data));
		}


		$data['lang'] = $this->languages;
		$data['items'] = $this->model->get_buildings($this->params->id);

		if (!isset($this->params->action) || ($this->params->action !== 'edit')) {
			$this->generate_view_admin($this->mod_dir . $this->mod_name . '_buildings_view', $data);
		}
	}

}
