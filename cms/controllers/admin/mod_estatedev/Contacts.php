<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacts extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index() {
		$data = array();

		$data['items'] = $this->model->get_all();
		$data['types'] = $this->model->get_all_types();
		$data['lang'] = $this->languages;

		$this->generate_view_admin($this->mod_dir .$this->mod_name. '_view', $data);
	}

	public function add() {
		$response = new stdClass();
		if(!$this->model->verify_form()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Osoba została dodana.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {
		$id = @is_numeric($this->params->id) ? $this->params->id : null;

		if ($this->model->delete($id)) {
			$msg = array(lang('Zaznaczone kontakty zostały usunięte.'), 0);
		} else {
			$msg = array(lang('Nie został zaznaczony żaden kontakt.'), 1);
		}

		$this->neocms->forward_msg($msg);

		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title .= lang(': edycja');

		$data = array();

		if (@!is_numeric($this->params->id) || !$this->model->exists($this->params->id)) {
			$msg = array(lang('Kontakt nie istnieje.'), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
		if (!isset($this->params->lang) || (isset($this->params->lang) && empty($this->params->lang))) {
			$this->params->lang = $this->admin->get_main_lang();
		}

		if(isset($this->params->check) && $this->params->check == 1) {
			if($this->model->verify_form()) {
				if ($this->model->save($this->params->id, $this->params->lang)) {
					$msg = array(lang('Kontakt został zaktualizowany.'), 0);
				} else {
					$msg = array(lang('Kontakt nie został zaktualizowany. Spróbuj ponownie.'), 1);
				}
				$this->neocms->forward_msg($msg);
				redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
			}
		}

		$data['lang'] = $this->languages;
		$data['item'] = $this->model->edit($this->params->id, $this->params->lang);
		$data['types'] = $this->model->get_all_types($this->params->id);

		$this->generate_view_admin($this->mod_dir . $this->mod_name . '_edit_view', $data);
	}

}
