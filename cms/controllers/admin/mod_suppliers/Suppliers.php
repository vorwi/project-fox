<?php if(!defined('BASEPATH')) { exit('No direct script access allowed');
}

class Suppliers extends MY_Controller
{
    public $img_dir = 'suppliers/';
    public $img_width = '800';
    public $img_height = '600';
    public $thumb_width = '180';
    public $thumb_height = '120';

    public $per_page = null;

    public function __construct()
    {
        parent::__construct();
        $this->_init_module(__FILE__, __DIR__);

        $this->load->library('upload');
        $this->load->library('image_lib');
    }

    public function index()
    {
        $data['offset'] = 0;

        $count = $this->model->get_rows('count');
        $base_url = "{$this->mod_url}/index/page/";
        
        $pconfig = array(
        'base_url' => $base_url,
        'total_rows' => $count,
        'per_page' => $this->per_page,
        'uri_segment' => $this->uri->total_segments(),
        'first_url' => $base_url.'0',
        );
        $this->pagination->init($pconfig);
        $data['pagination'] = $this->pagination->create_links();

        $offset = $this->uri->segment($this->uri->total_segments());

        $data['suppliers'] = $this->model->get_rows();

        $this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
    }
    
    public function add()
    {
        $response = new stdClass();
        if(!$this->model->verify_form('add')) {
            $response->status = 'ERROR';
        } else {
            if($id = $this->model->insert()) {
                $response->status = 'OK';
                $this->neocms->forward_msg(lang('Wpis został dodany.'), 0);
                $response->redirect = site_url($this->mod_url.'/edit/id/'.$id);
            }
        }
        $response->messages = collect_messages();
        $this->output->display_json($response);
    }

    public function delete()
    {
        $id = @is_numeric($this->params->del) ? $this->params->del : null;

        if($this->model->delete($id)) {
            $msg = array(
            lang("Zaznaczone wpisy zostały usunięte."),
            0
            );
        } else {
            $msg = array(
            lang("Nie został zaznaczony żaden wpis."),
            1
            );
        }

        $this->neocms->forward_msg($msg);
        redirect($this->admin->get_redirect_url());
    }

    public function edit()
    {
        
        $this->load->library('lang_fields');
        
        $this->title .= lang(': edycja');

        if(@!is_numeric($this->params->id) || !$this->model->rowExists($this->params->id)) {
            $msg = array(lang("Aktualność nie istnieje."),1);
            $this->neocms->forward_msg($msg);
            redirect($this->admin->get_redirect_url());
        }
        
        $data = array();
        $data['supplier'] = $this->model->edit($this->params->id);

        if(isset($this->params->check) && $this->params->check == 1) {
            
            if($this->input->post()) {
            
                if($this->model->verify_form('edit', $this->params->id)) {
                    if($this->model->save($data['supplier'])) {
                        $msg = array(lang('Wpis został zaktualizowany'), 0);
                    }else {
                        $msg = array(lang('Wpis nie został zaktualizowany. Spróbuj ponownie'), 2);
                    }
                    $this->neocms->forward_msg($msg);
                    redirect($this->mod_url.'/edit/id/'.$this->params->id);
                }
            }
            else { redirect($this->mod_url.'/edit/id/'.$this->params->id);
            }
        }
        
        

        $this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
    }

    public function crop()
    {
        $data = array();

        if(isset($this->params->id) && is_numeric($this->params->id)) {
            $id = $this->params->id;

            if($this->input->post('crop')) {
                if($this->model->crop($id)) {
                    $msg = array(
                    lang("Miniaturka została zmieniona"),
                    0
                    );
                    $this->neocms->forward_msg($msg);
                }
            }
        } else {
            $msg = array(
            lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."),
            1
            );
            $this->neocms->forward_msg($msg);
        }

        $data = $this->model->get_rows($id);

        if($data) {
            //w wersji orginalnej kodu row jest arrayem
            $data = (array)$data;
            $data['path'] = config_item('upload_path').$this->img_dir;

            $imgsize = getimagesize('./'.config_item('site_path').$data['path'].$data['img']);
            $scaled_width = 800;
            if($imgsize[0] > $scaled_width) {
                $scale = $imgsize[0] / $scaled_width;
            } else {
                $scale = 1;
            }

            $data['form_url'] = $this->mod_url.'/crop/id/'.$id;
            $data['width'] = $this->thumb_width * (1 / $scale);
            $data['height'] = $this->thumb_height * (1 / $scale);
            $data['aspect'] = $this->thumb_width.':'.$this->thumb_height;
            $data['scale'] = $scale;
        } else {
            $msg = array(
            lang("Żadne zdjęcie nie zostało znalezione. Spróbuj jeszcze raz."),
            1
            );
            $this->neocms->forward_msg($msg);
        }

        $this->generate_view_admin('crop_view', $data, 1);
    }

}
