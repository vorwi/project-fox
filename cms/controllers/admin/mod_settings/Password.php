<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Password extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index() {
		$data = array();

		if(isset($this->params->change) && $this->params->change == 1) {
			if($this->model->form_check()) {
				if($this->model->password_change()) {
					$msg = array(
						lang("Zmiany zostały zapisane."),
						0
					);
					$this->neocms->forward_msg($msg);
					redirect($this->admin->get_redirect_url());
				} else {
					$msg = array(
						lang("Zmiany nie zostały zapisane. Spróbuj ponownie."),
						1
					);
					$this->neocms->forward_msg($msg);
					redirect($this->admin->get_redirect_url());
				}
			}
		}

		if(isset($this->params->change)) {
			$this->admin->skip_mod_referer();
		}
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

	public function _old_pass_check() {
		$pass = $this->input->post('old_pass');

		$this->db->where('id', $this->session->userdata('user_id'));
		$query = $this->db->get('admin_users');

		if($query->num_rows() == 1) {
			$hasher = pass_hash();

			$user = $query->row();

			$time = $user->date_add;
			$time_salt = sha1(config_item('password_salt').$time);
			$salt = substr($time_salt, 2, 8);

			$check = $hasher->CheckPassword($pass.$salt, $user->password);

			if($check != TRUE) {
				$this->form_validation->set_message('_old_pass_check', lang('Stare hasło jest niepoprawne. Sprobuj ponownie.'));
				return FALSE;
			} else
				return TRUE;

		} else
			return FALSE;
	}

	public function _new_pass_check() {
		$pass1 = $this->input->post('new_pass1');
		$pass2 = $this->input->post('new_pass2');

		if($pass1 != $pass2) {
			$this->form_validation->set_message('_new_pass_check', lang('Podane hasła nie są zgodne. Spróbuj ponownie.'));
			return FALSE;
		} else
			return TRUE;
	}
}
