<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);

		$this->load->library('upload');
	}

	public function index() {
		$data = array();

		if(isset($this->params->lang))
			$lang = $this->params->lang;
		else
			$lang = $this->admin->get_main_lang();

		if(isset($this->params->change) && $this->params->change == 1) {
			if($this->model->settings_change()) {
				$msg = array(
					lang("Ustawienia zostały zapisane."),
					0
				);
			} else {
				$msg = array(
					lang("Ustawienie nie zostały zapisane. Spróbuj ponownie."),
					1
				);
			}
			$hash = $this->input->post('tab_hash');
			$this->neocms->forward_msg($msg);
			redirect($this->mod_url.'/index/lang/'.$lang.$hash);
		}

		$data['lang'] = $this->languages;
		$data['curr_lang'] = $lang;

		$data['settings_tree'] = $this->model->get_settings_tree($lang);

		$data['categories'] = $this->model->get_categories();

		if(isset($this->params->change)) {
			$this->admin->skip_mod_referer();
		}
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

	public function add() {
		$response = new stdClass();
		if(!$this->model->add()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Ustawienie zostało dodane.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function add_category() {
		$response = new stdClass();
		if(!$this->model->add_category()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert_category()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Kategoria ustawień została dodana.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {

		$id = @is_numeric($this->params->del) ? $this->params->del : null;
		if($this->session->userdata('main') == 1) {
			if($this->model->delete($id)) {
				$msg = array(
					lang("Zaznaczone ustawienia zostały usunięte."),
					0
				);
			} else {
				$msg = array(
					lang("Nie zostało zaznaczone żadne ustawienie."),
					1
				);
			}
		} else {
			$msg = array(
				lang("Nie masz odpowiednich uprawnień."),
				1
			);
		}

		$this->neocms->forward_msg($msg);

		redirect($this->admin->get_redirect_url());
	}
	
	public function vswitch() {
		if($this->session->userdata('main') == 1) {
			parent::vswitch();
		}else{
			$msg = array(lang("Nie masz odpowiednich uprawnień."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
	}

}
