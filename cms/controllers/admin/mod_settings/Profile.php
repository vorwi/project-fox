<?  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index(){
		$this->title = $this->title;
		$data = array();
		
		if(isset($this->params->check) && $this->params->check==1){
			if($this->input->post()){
				if($this->model->check()){
					if($this->model->save()) {
						$msg = array(lang("Zmiany zostały zapisane."), 0);				
					}else {
						$msg = array(lang("Zmiany nie zostały zapisane. Spróbuj ponownie."), 2);
					}
					$this->neocms->forward_msg($msg);
					redirect($this->admin->get_redirect_url());
				}
			}else{
				redirect($this->admin->get_redirect_url());
			}
		}
		
		$data['user'] = $this->model->edit();
		
		if(isset($this->params->check)){
			$this->admin->skip_mod_referer();
		}
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);  
	}
	
	
}