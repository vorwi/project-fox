<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Elfinder_init extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}
		
	function index(){
		$this->admin->skip_mod_referer();
		$this->output->enable_profiler(FALSE);
		
		$opts = initialize_elfinder();
		$this->load->library('elfinder_lib', $opts);
	}
	
	function ckeditor_view(){
		$this->admin->skip_mod_referer();
		$this->output->enable_profiler(FALSE);
		
		$data = array();
		$this->generate_view_admin($this->mod_dir.'elfinder_ckeditor_view', $data, 0, FALSE, 'elfinder');
	}
	
	function input_view(){
		$this->admin->skip_mod_referer();
		$this->output->enable_profiler(FALSE);
		
		$data = array();
		$data['functionReturn'] = (isset($this->params->functionReturn) ? $this->params->functionReturn : 'processfile');
		$this->generate_view_admin($this->mod_dir.'elfinder_input_view', $data, 0, FALSE, 'elfinder');
	}

	function standalone_view(){
		$this->admin->skip_mod_referer();
		$this->output->enable_profiler(FALSE);
		
		$data = array();
		$this->generate_view_admin($this->mod_dir.'elfinder_standalone_view', $data, 0, FALSE, 'elfinder');
	}
}