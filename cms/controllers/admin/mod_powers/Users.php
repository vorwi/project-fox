<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {
	private $per_page		= 100;

	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
		
		$this->load->model('admin/'.$this->mod_dir.'groups_model');
	}

	public function index() {
		$data = array();
		
		$this->params->count = TRUE;
		$count_return = $this->model->get_rows($this->params);
		$count = $count_return['count'];
		unset($count_return);
		
		preg_match('#^(.*?)(?<idx>index)?(?<ps>\/pstart(?:\/[0-9]+)?)?$#i', current_url(), $url_parts);
		if(!isset($url_parts['ps'])) {
			redirect(current_url().(isset($url_parts['idx']) ? '' : '/index').'/pstart/0');
		}
		
		$base_stripped = preg_replace('#(.*?)\/pstart\/\d+#i', '$1', current_url());
		
		$pconfig = array(
			'base_url' => $base_stripped.'/pstart',
			'total_rows' => $count,
			'per_page' => $this->per_page,
			'uri_segment' => $this->uri->total_segments(),
			'first_url' => $base_stripped.'/pstart/0',
			'num_links' => 10
		);
		$this->pagination->init($pconfig);
		$data['pagination'] = $this->pagination->create_links();
		
		unset($this->params->count);
		$this->params->offset = $this->uri->segment($this->uri->total_segments());
		$this->params->limit = $this->per_page;
		
		$data['params'] = $this->params;
		$data['count'] = $count;
		$data['rows'] = $this->model->get_rows($this->params);
		$groups = $this->groups_model->get_rows();
		$data['groups'] = $groups;
		
		$groups_with_users = $this->model->get_groups_with_users();
		$data['groups_with_users'] = $groups_with_users;
		
		$sf_data = array(
				'action_url' => 'admin/'.$this->mod_dir.$this->mod_name.'/prepare_filters',
				'fields' => array(
						array('header' => lang('Grupa'),		'type' => 'select', 	'name' => 'group', 		'values' => $groups, 'class' => 'select_simple', 'style' => 'width: 200px;'),
						array('header' => lang('Aktywne'), 		'type' => 'checkbox', 	'name' => 'status', 	'values' => 1)
				),
				'button' => lang('filtruj'),
				'filters' => $this->params
		);
		$data['filters_form'] 	= $this->load->view('admin/'.config_item('template_a').'filters_form_view', $sf_data, TRUE);
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

	public function add() {
		$response = new stdClass();
		if(!$this->model->add()) {
			$response->status = 'ERROR';
		} else {
			$add = $this->model->insert();
			if($add !== FALSE) {
				$response->status = 'OK';
				$msg = sprintf(lang("Użytkownik został dodany. Hasło <b>%s</b> zostało wysłane na podany email."), $add);
				
				$this->neocms->forward_msg($msg, 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}
	
 	public function edit() {
 		$this->title .= lang(': edycja');
 		
		$data = array();
		$id = @is_numeric($this->params->id) ? $this->params->id : null;
		
		if(isset($this->params->check) && $this->params->check==1){
			$changed = $this->model->save();
			$msg = array(lang("Zmiany zostały zapisane."), 0);
				$this->neocms->forward_msg($msg);
				if(!empty($changed)){
				$this->session->set_flashdata('changed', $changed);
				}
			redirect($this->admin->get_redirect_url());
		}
		
		$data['groups'] = $this->groups_model->get_rows();
		$data['all'] = $this->model->edit($id);
		
		$groups_with_users = $this->model->get_groups_with_users();
		$data['groups_with_users'] = $groups_with_users;

		if($data['all']===FALSE) {
			$msg = array(lang("Nie został zaznaczony żaden wpis."), 2);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
			$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
	
	public function delete() {

		$id = @is_numeric($this->params->del) ? $this->params->del : null;

		if($this->model->delete($id)) {
			$msg = array(lang("Zaznaczone wpisy zostały usunięte."), 0);
		} else {
			$msg = array(lang("Nie został zaznaczony żaden wpis."), 1);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
	public function active() {
		$id = @is_numeric($this->params->id) ? $this->params->id : null;
		$status = @is_numeric($this->params->set) ? $this->params->set : null;
		
		if($this->neocms->pub($id,$status,$this->mod_table,'status')) {
			$msg = array(lang("Zmiany zostały zapisane."), 0);
		} else {
			$msg = array(lang("Zaminy nie zostały zapisane."), 1);
		}
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}
	
 	public function powers() {
 		$this->title = $this->title.lang(': uprawnienia');
 		
		$data = array();
		$id = @is_numeric($this->params->id) ? $this->params->id : null;
		
		if(isset($this->params->check) && $this->params->check==1){
			$changed = $this->model->powers_save();
		
			$msg = array(lang("Zmiany zostały zapisane."), 0);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

		$data['all'] = $this->model->powers($id);
		
		if($data['all']===FALSE) {
			$msg = array(lang("Nie został zaznaczony żaden wpis."), 2);
			$this->neocms->forward_msg($msg);
	 		redirect($this->admin->get_redirect_url());
		}
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_powers_view', $data);
	}
	
	//funkcja przerabia dane POST z formularza filtrującego na odpowiedni URL i robi przekierowanie
	public function prepare_filters() {
		$url = 'admin/';
	
		if($this->input->post('mod_dir') !== NULL && $this->input->post('mod_name') !== NULL) {
			$url .= $this->input->post('mod_dir').$this->input->post('mod_name').'/';
		}
		if($this->input->post('clearfilters') === NULL) {
			if($this->input->post('id') != 0) $url .= "id/".$this->input->post('id')."/";
			if($this->input->post('group') != 0) $url .= "group/".$this->input->post('group')."/";
			if($this->input->post('status') != 0) $url .= "status/1/";
		}
	
		redirect(site_url($url.'pstart/0'));
	}
	
}
