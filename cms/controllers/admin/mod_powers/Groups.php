<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);
	}

	public function index() {
		$data = array();

		$data['lang'] = $this->languages;
		$data['rows'] = $this->model->get_rows();
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

 	public function add() {
 		$response = new stdClass();
		if(!$this->model->add()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Grupa została dodana.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function position() {
		if($this->neocms->positions($this->mod_table)) {
			$msg = array(lang("Kolejność została zaktualizowana."), 0);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}

 		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

	public function delete() {
		
		$id = @is_numeric($this->params->del) ? $this->params->del : null;
		
		if($this->model->delete($id)) {
			$msg = array(lang("Zaznaczone wpisy zostały usunięte."), 0);
		} else {
			$msg = array(lang("Nie został zaznaczony żaden wpis."), 1);
		}

		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}

	public function edit() {
		$this->title = $this->title.lang(': edycja');

		$data = array();

		if(@!is_numeric($this->params->id)){
			$msg = array(lang("Wpis nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
		if(empty($this->params->lang)) { $this->params->lang = $this->admin->get_main_lang(); }

		if(!$row = $this->model->edit($this->params->id, $this->params->lang)){
			$msg = array(lang("Wpis nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
		if(isset($this->params->check) && $this->params->check==1){
			if($this->input->post()){
				if($this->model->save($this->params->id,$this->params->lang)){
					$msg = array(lang("Wpis został zaktualizowany."), 0);
					$this->neocms->forward_msg($msg);
					redirect($this->admin->get_redirect_url());
				}else {
					$msg = array(lang("Wpis nie został zaktualizowany. Spróbuj ponownie."), 2);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
				}
			}else {
				redirect($this->mod_url.'/edit/lang/'.$this->params->lang.'/id/'.$this->params->id);
			}
		}

		$data['lang'] = $this->languages;
		$data['row'] = $row;
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
	
	public function powers() {
		$this->title .= lang(': uprawnienia');
			
		$data = array();
		$lang = $_SESSION['admin_lang'];
		
		if(@!is_numeric($this->params->id)){
			$msg = array(lang("Wpis nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		
		if(!$group = $this->model->edit($this->params->id, $lang)){
			$msg = array(lang("Wpis nie istnieje."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
		}
		 
		if(isset($this->params->check) && $this->params->check==1){
			if($this->input->post()){
				if($this->model->powers_save($this->params->id)){
					$msg = array(lang("Zmiany zostały zapisane."), 0);
					$this->neocms->forward_msg($msg);
					redirect($this->admin->get_redirect_url());
				}else {
					$msg = array(lang("Zmiany nie zostały zapisane. Spróbuj ponownie."), 2);
					$this->neocms->forward_msg($msg);
					redirect($this->mod_url.'/powers/id/'.$this->params->id);
				}
			}else {
				redirect($this->mod_url.'/powers/id/'.$this->params->id);
			}
		}
	
		$data['modules'] = $this->model->power_models();
		$data['rights'] = $this->model->rights($this->params->id);
		$data['group'] = $group;
		 
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_powers_view', $data);
	}
}
