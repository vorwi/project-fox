<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Languages extends MY_Controller {
	public $lang_path	   = '';

	public function __construct(){
		parent::__construct();
		$this->_init_module(__FILE__, __DIR__);

		$this->lang_path = APPPATH.'language/';
		$this->load->helper('directory');
	}

	public function index() {
		$data = array();

		if(isset($this->params->edit)) {
			$this->admin->skip_mod_referer();
			$this->_edit($this->params->edit);
			return;
		}

		$data['flags'] = directory_map('./'.config_item('site_path').config_item('gfx_c').'img/flags/', FALSE);
		asort($data['flags']);
		$data['lang'] = $this->model->get_languages();

		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_view', $data);
	}

	private function _edit($lang_short) {
		
		$lang = $this->model->get_lang($lang_short);
		if($lang !== FALSE) {
			$data = array(
				'edit' => $lang
			);
		} else {
			$this->msg = array(lang("Nie znaleziono wybranego języka."), 1);
		}

		if($this->input->post('save') !== FALSE && $this->model->add()){

			if($this->model->save($lang->id)) {
				$msg = array(lang("Zmiany zostały zapisane."), 0);
			} else {
				$msg = array(lang("Zmiany nie zostały zapisane. Spróbuj ponownie."), 2);
			}
	   		$this->neocms->forward_msg($msg);
	   		redirect($this->admin->get_redirect_url());
		}
		$data['flags'] = directory_map('./'.config_item('site_path').config_item('gfx_c').'img/flags/', FALSE);
		
		$this->generate_view_admin($this->mod_dir.$this->mod_name.'_edit_view', $data);
	}
	
	public function add() {
		$response = new stdClass();
		if(!$this->model->add()) {
			$response->status = 'ERROR';
		} else {
			if($this->model->insert()) {
				$response->status = 'OK';
				$this->neocms->forward_msg(lang('Język został dodany.'), 0);
				$response->redirect = $this->admin->get_redirect_url();
			}
		}
		$response->messages = collect_messages();
		$this->output->display_json($response);
	}

	public function delete() {
		$id = @is_numeric($this->params->del) ? $this->params->del : null;
	
		if($id != config_item('main_lang')) {
			if($this->model->delete($id)) {
				$msg = array(lang('Zaznaczone języki zostały usunięte'), 0);
			} else {
				$msg = array(lang('Nie został zaznaczony żaden język'), 1);
			}
		} else {
			$msg = array(lang('Nie można usunąć głównego języka'), 1);
		}
	
		$this->neocms->forward_msg($msg);
	
		redirect($this->admin->get_redirect_url());
	}
	
	public function pub() {
		$id = @is_numeric($this->params->id) ? $this->params->id : null;
		$status = @is_numeric($this->params->set) ? $this->params->set : null;
	
		if($id != config_item('main_lang')) {
			if($this->neocms->pub($id,$status,$this->mod_table,'pub')) {
				$msg = array(lang('Zmiany zostały zapisane'), 0);
			} else {
				$msg = array(lang('Zmiany nie zostały zapisane'), 1);
			}
		} else {
			$msg = array(lang('Nie można publikować/odpublikować głównego języka'), 1);
		}
	
		$this->neocms->forward_msg($msg);
		redirect($this->admin->get_redirect_url());
	}

	public function translate($lang, $file = FALSE) {
		$this->load->helper('translate');
		$this->load->helper('string');
		$lang = $this->model->get_lang($lang);
		if($lang !== FALSE) {
			$data['language'] = $lang;
		} else {
			$msg = array(lang("Nie znaleziono wybranego języka."), 1);
			$this->neocms->forward_msg($msg);
			redirect($this->admin->get_redirect_url());
			return;
		}
		$data['main_language'] = $this->model->get_lang(config_item('main_lang'));

		$open_file = str_replace('::', '/', $file);

		if($this->input->post('save') !== NULL) {
			if($this->_save_file($lang->dir.'/'.$open_file)) {
				$msg = array(sprintf(lang("Plik %s został poprawnie zapisany."), $open_file), 0);
			} else {
				$msg = array(sprintf(lang("Nie udało się zapisać pliku %s."), $open_file), 2);
			}
			$this->neocms->forward_msg($msg);
			redirect(current_url());
		}

		$base_path = $this->lang_path.$lang->dir.'/';
		$files = directory_map($base_path, FALSE);

		//przygotowanie plików języka na bazie wersji polskiej
		if(config_item('main_lang') != $lang->short) {
			$main_base_path = $this->lang_path.$data['main_language']->dir.'/';
			$main_files = list_from_tree(directory_map($main_base_path, FALSE));

			foreach($main_files as $path) {
				if(preg_match('#\.php_backup\.php$#', $path)) continue;
				if(!file_exists($base_path.$path)) {
					@mkdir(dirname($base_path.$path), 0755, TRUE);
					copy($main_base_path.$path, $base_path.$path);
				}
			}
		}

		$this->title = $lang->lang.' - '.(empty($open_file) ? lang('edycja tłumaczeń') : $open_file);
		$data['file'] = $open_file;
		$data['lang_path'] = $this->lang_path;
		$data['files'] = $files;

		$this->generate_view_admin($this->mod_dir.'edit_translation_view', $data);
	}

	private function _save_file($file) {
		$lines = $this->input->post();
		unset($lines[config_item('csrf_token_name')]);
		unset($lines['save']);

		if(is_file($this->lang_path.$file)) {
			copy($this->lang_path.$file, $this->lang_path.$file.'_backup.php');

			$fp = fopen($this->lang_path.$file, 'w');
			fwrite($fp, "<?php defined('BASEPATH') OR exit('No direct script access allowed');\n\n");

			fwrite($fp, generate_lang($lines));

			fwrite($fp, "\r\n\r\n/* End of file {$file} */");
			fwrite($fp, "\r\n/* Location: {$this->lang_path}{$file} */");
			fclose($fp);
			return TRUE;
		}
		return FALSE;
	}
}
