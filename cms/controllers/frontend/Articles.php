<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends MY_Controller {

    public $mod_name = 'articles';
    public $mod_table = 'articles';

    public function __construct() {
        parent::__construct();

        $this->id_art = config_item('main_id');
        $this->load->model('frontend/'.$this->mod_name.'_model');
        $this->articles_model->checkConfig();
    }

    public function index() {
        $data = array();

        $art = $this->articles_model->get_article($this->id);

        if($art === FALSE) {
            show_404();
            return;
        }
        
        $data['header'] = $art->title;

        if(config_item('breadcrumbs_enabled')) {
            if($art->parent_id !== NULL) {
                $art_parent = $this->articles_model->get_article($art->parent_id);
                if($art_parent !== FALSE && $art_parent->parent_id !== NULL) {
                    $art_parent_parent = $this->articles_model->get_article($art_parent->parent_id);
                    if($art_parent_parent !== FALSE) {
                        $this->frontend->add_breadcrumb($art_parent_parent->title, article_url($art_parent_parent));
                    }
                }
                if($art_parent !== FALSE) {
                    $this->frontend->add_breadcrumb($art_parent->title, article_url($art_parent));
                }
            }
            $this->frontend->add_breadcrumb($art->title);
        }

        $data['article'] = $art;
        $data['addons'] = $this->addons($this->id);
        
        if(!empty($art->meta_title)) $this->meta_title = $art->meta_title;
        if(!empty($art->meta_description)) $this->meta_description = $art->meta_description;
        if(!empty($art->meta_keywords)) $this->meta_keywords = $art->meta_keywords;
        if(!empty($art->og_title)) $this->og_title = $art->og_title;
        if(!empty($art->og_description)) $this->og_description = $art->og_description;
        if(!empty($art->og_image)) $this->og_image = $art->og_image;
        if(!empty($art->og_url)) $this->og_url = $art->og_url;

        $this->load->model('frontend/suppliers_model');

        $data['clients'] = $this->suppliers_model->get_suppliers();

        if ($art->id == 1) {
            $data['main_section'] = $this->articles_model->get_article('4');
            $data['about'] = $this->articles_model->get_article('5');
            $data['clients'] = $this->articles_model->get_article('6');
            $data['offer'] = $this->articles_model->get_article('7');
            $data['collaboration'] = $this->articles_model->get_article('8');
            $data['distinguish'] = $this->articles_model->get_article('9');
            $data['contact'] = $this->articles_model->get_article('10');
            $data['numbers'] = $this->articles_model->get_all_numbers();
            $data['offers'] = $this->articles_model->get_all_offers();
            $data['skills'] = $this->articles_model->get_all_skills();
        }

        $this->generate_view('mod_articles/'.$this->mod_name.'_view', $data);
    }

    public function page_404() {
        $this->generate_view('mod_articles/404_view', array());
    } 

    public function cookie_info() {
        //$this->output->enable_profiler(FALSE);
        $cp = $this->settings->cookie_policy;

        $this->meta_title = lang('Polityka plików cookies');
        
        $data = array(
            'header' => lang('Polityka plików cookies'),
            'cookie_info' => str_replace('{%adres%}', $_SERVER['HTTP_HOST'], $cp)
        );
        
        if(config_item('breadcrumbs_enabled')) {

            $this->frontend->add_breadcrumb(lang('Polityka plików cookies'));
        }

        //$this->generate_view('mod_articles/cookie_info_view', $data, 0, 0, 'plain');
        $this->generate_view('mod_articles/cookie_info_view', $data);
    }

    public function coordinates() {
        $this->output->enable_profiler(FALSE);

        $coordinates = new stdClass();
        $coordinates->type = "FeatureCollection";
        $coordinates->features = [];

        $point = new stdClass();
        $point->type = "Feature";
        $point->properties = new stdClass();
        $point->properties->title = "Project Fox";

        $point->geometry = new stdClass();
        $point->geometry->type = "Point";
        $point->geometry->coordinates = [$this->settings->latitude,$this->settings->longitude];

        $coordinates->features = [$point];

        echo json_encode($coordinates);
    }

    public function addons($id_art) {
        $data['add_top'] = array();
        $data['add_bottom'] = array();

        $this->load->model('frontend/contact_model');
        $contact = $this->contact_model->addon($id_art);
        if($contact !== FALSE) {
            $data['add_top'][] = $contact;
        }

        $this->load->model('frontend/gallery_model');
        $gallery = $this->gallery_model->addon($id_art, $perpage = 10, $limit_img = 4);
        if($gallery !== FALSE) {
            $data['add_bottom'][] = $gallery;
        }

        $this->load->model('frontend/news_model');
        $news = $this->news_model->addon($id_art, $perpage = 10);
        if($news !== FALSE) {
            $data['add_bottom'][] = $news;
        }

        $tab = array(
            'top' => (!empty($data['add_top']) ? $this->generate_view('includes/addons_view', array('top' => $data['add_top']), 1, 1) : NULL),
            'bottom' => (!empty($data['add_bottom']) ? $this->generate_view('includes/addons_view', array('bottom' => $data['add_bottom']), 1, 1) : NULL),
        );

        return $tab;
    }

}
