<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class News extends MY_Controller {

	public $mod_name = 'news';
	public $img_dir = 'news/';
	public $header;
	public $per_page = 10;
	public $uri_segment = 2;

	public function __construct() {
		parent::__construct();

		$this->load->model('frontend/news_model');
	}

	public function index() {
		$data = array();

		$news = $this->news_model->get_news();
		
		$pconfig = array(
				'base_url' => base_url().config_item('px_news'),
				'total_rows' => count($news),
				'per_page' => $this->per_page,
				'uri_segment' => $this->uri_segment
		);
		$this->pagination->init($pconfig);
		$data['pagination'] = $this->pagination->create_links();

		if(@is_numeric($this->uri->segment($this->uri_segment)))
			$start = $this->uri->segment($this->uri_segment);
		else
			$start = 0;

		$news = $this->news_model->get_news($this->per_page, $start);

		if(config_item('breadcrumbs_enabled')) {
			$this->frontend->add_breadcrumb(lang('Aktualności'));
		}

		$data['header'] = lang('Aktualności');
		$data['news'] = $news;
		$data['echo_pag'] = TRUE;
		$data['read'] = 0;

		$this->generate_view('mod_news/news_list_view', $data);
	}

	public function single($id) {
		$news = $this->news_model->get_single_news($id);
		
		$data = array(
			'news' => $news
		);
		
		if($news === FALSE) {
			$this->neocms->set_msg(lang('Wybrana aktualność nie istnieje.'), 2);
		} else {
			$data['header'] = $news->title;
			$data['addons'] = $this->addons($news->id);
			
			if(config_item('breadcrumbs_enabled')) {
				$art = $this->frontend->get_article($news->id_art);
				$this->frontend->add_breadcrumb($art->title, article_url($art));
				$this->frontend->add_breadcrumb($news->title);
			}
			
			if(!empty($news->meta_title)) $this->meta_title = $news->meta_title;
			if(!empty($news->meta_description)) $this->meta_description = $news->meta_description;
			if(!empty($news->meta_keywords)) $this->meta_keywords = $news->meta_keywords;
		}

		$this->generate_view('mod_news/single_view', $data);
	}

	public function addons($id_news) {
		$data['add_top'] = array();
		$data['add_bottom'] = array();

		$this->load->model('frontend/gallery_model');
		$gallery = $this->gallery_model->addon($id_news, 10, 0, 'news');
		if($gallery !== FALSE) {
			$data['add_bottom'][] = $gallery;
		}

		$tab = array(
			'top' => (!empty($data['add_top']) ? $this->generate_view('includes/addons_view', array('top' => $data['add_top']), 1, 1) : NULL),
			'bottom' => (!empty($data['add_bottom']) ? $this->generate_view('includes/addons_view', array('bottom' => $data['add_bottom']), 1, 1) : NULL),
		);

		return $tab;
	}

}
