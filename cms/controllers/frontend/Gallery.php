<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends MY_Controller {

	public $mod_name = 'gallery';
	public $img_dir = 'gallery/';
	private $per_page = 10;
	public $uri_segment = 2;

	public function __construct() {
		parent::__construct();

		$this->load->model('frontend/gallery_model');
	}

	public function index() {
		$data = array();
		$galleries = $this->gallery_model->get_galleries('', '');
		
		$pconfig = array(
				'base_url' => base_url().config_item('px_gallery'),
				'total_rows' => count($galleries),
				'per_page' => $this->per_page,
				'uri_segment' => $this->uri_segment
		);
		$this->pagination->init($pconfig);
		$data['pagination'] = $this->pagination->create_links();

		if(@is_numeric($this->uri->segment($this->uri_segment))) {
			$start = $this->uri->segment($this->uri_segment);
		} else {
			$start = 0;
		}

		$galleries = $this->gallery_model->get_galleries($this->per_page, $start);

		$data['header'] = lang('Galerie zdjęć');
		$data['echo_pag'] = TRUE;
		$data['galleries'] = $galleries;

		if(config_item('breadcrumbs_enabled')) {
			$this->frontend->add_breadcrumb(lang('Galerie'));
		}

		$this->generate_view('mod_gallery/gallery_list_view', $data);
	}

	public function single($id) {
		$gallery = $this->gallery_model->get_gallery($id);
		
		$data = array(
			'gallery' => $gallery
		);
		
		if($gallery === FALSE) {
			$this->neocms->set_msg(lang('Wybrana galeria nie istnieje.'), 2);
		} else {
			if(config_item('breadcrumbs_enabled')) {
				if(!empty($gallery->id_art)) {
					$this->load->model('frontend/articles_model');
					$art = $this->articles_model->get_article($gallery->id_art);
	
					if($art !== FALSE) {
						if($art->parent_id !== NULL) {
							$art_parent = $this->articles_model->get_article($art->parent_id);
							$this->frontend->add_breadcrumb($art_parent->title, article_url($art_parent));
						}
						$this->frontend->add_breadcrumb($art->title, article_url($art));
					}
				}
				$this->frontend->add_breadcrumb($gallery->name);
			}
			
			$data['header'] = $gallery->name;
		}

		$this->generate_view('mod_gallery/single_view', $data);
	}
}
