<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Intro extends MY_Controller {
	public $mod_name = 'intro';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index() {
		$this->generate_view($this->mod_name.'_view', '', 1);
	}
}
