<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MY_Controller {

	public $mod_name		= 'search';
	public $header;
	public $per_page 	= 10;
	public $uri_segment = 3;
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('frontend/search_model');
	}
	
	public function index($query = FALSE) {
		$data = array();
		
		if($this->input->post('query')) {
			if($this->search_model->check()) {
				$query = rawurlencode($this->input->post('query'));
				redirect(config_item('px_search').'/'.$query);			
			}
		}
		
		$results_count = 0;
	
		if(config_item('breadcrumbs_enabled')){
			$this->frontend->add_breadcrumb(lang('Szukaj'), config_item('px_search'));
		}
		
		$results = NULL;
		if(!empty($query)) {
			
			if(config_item('breadcrumbs_enabled')){
				$this->frontend->add_breadcrumb(htmlspecialchars($query));
			}
			
			$query = rawurldecode($query);						
			
			//wyszukiwanie wstępne, dla sprawdzenia ilości wyników do paginacji
			$results = $this->search_model->search($query);
			
			if(!empty($results)) {
				$results_count = count($results);
				
				$pconfig = array(
					'base_url' => base_url().config_item('px_search').'/'.rawurlencode($query),
					'total_rows' => $results_count,
					'per_page' => $this->per_page,
					'uri_segment' => $this->uri_segment
				);
				$this->pagination->init($pconfig);
				
				$start = $this->uri->segment($this->uri_segment, 0);//((int)$this->uri->segment($this->uri_segment, 0) * $this->per_page) - $this->per_page;
						
				$results = $this->search_model->search($query, $this->per_page, $start);
			}
		}
		
		$data['header']			= lang('Szukaj');
		$data['search_string']	= htmlspecialchars($query);
		$data['results']		= $results;
		$data['count']			= $results_count;
		
		$this->generate_view('mod_search/'.$this->mod_name.'_results_view', $data);
	}
}
