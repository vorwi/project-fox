<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {

	public $mod_name = 'contact';

	public function __construct() {
		parent::__construct();

		$this->load->model('frontend/contact_model');

		$this->load->helper('file');
		$this->load->helper('directory');
	}

	public function index() {
		$data = array();

		$cap = $this->frontend->get_captcha();
		$email = $this->contact_model->getEmail();

		$data['email'] = $email;
		$data['cap'] = $cap;

		if($this->input->post('contact-form-send')) {
			if($this->contact_model->check()) {
				if($this->contact_model->send($email)) {
					$msg = array(
						lang('Dziękujemy. Email został wysłany.'),
						0,
						1
					);
					$this->session->set_flashdata('msg', $msg);

					redirect(current_url());
				} else {
					$msg = array(
						lang('Email nie został wysłany, spróbuj ponownie.'),
						1,
						1
					);
					$this->session->set_flashdata('msg', $msg);

					redirect(current_url());
				}
			}
		}
		$this->generate_view('mod_contact/'.$this->mod_name.'_view', $data);
	}

}
