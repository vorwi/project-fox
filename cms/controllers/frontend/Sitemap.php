<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends MY_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('frontend/articles_model');
		$this->output->enable_profiler(FALSE);
	}

	public function index() {
		header("content-type: text/xml");

		$data = array();

		$data['articles'] = $this->articles_model->getAllArticles2(0);

		$this->generate_view('mod_sitemap/sitemap_view', $data, 1);
	}

	public function sitemapPage() {
		$data = array();

		$data['header'] = lang('Mapa strony');
		$data['sitemap']['articles'] = $this->articles_model->getAllArticles2(0);
		
		if(config_item('breadcrumbs_enabled')) {
			$this->frontend->add_breadcrumb(lang('Mapa strony'));
		}

		$this->generate_view('mod_sitemap/sitemap_page_view', $data);
	}
	
	public function robots()
	{
	    $this->output->set_header('HTTP/1.0 200 OK');
	    $this->output->set_header('HTTP/1.1 200 OK');	    
	    $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
	    $this->output->set_header('Cache-Control: post-check=0, pre-check=0');
	    $this->output->set_header('Pragma: no-cache');
	    $this->output->set_content_type('text/plain','UTF-8');
	    $data = $this->settings->robots_txt;
	    $this->output->set_output($data);
	}
}
