<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends MY_Controller {

	public $mod_name	= 'newsletter';
	private $queue_limit = 20;
	
	public function __construct(){
		parent::__construct();

		$this->load->model('frontend/newsletter_model');
	}
	
	public function index() {
		$data = array();
		
		$data['header'] = 'Newsletter';
		if(config_item('breadcrumbs_enabled')){
			$this->frontend->add_breadcrumb(lang('Newsletter'));
		}

		$this->generate_view('mod_newsletter/'.$this->mod_name.'_view', $data);
	}
	
	public function newsletter_new(){
		$data = array();
		
		if(config_item('breadcrumbs_enabled')){
			$this->frontend->add_breadcrumb(lang('Newsletter'));
		}
		
		$tab = $this->uri->ruri_to_assoc(-7);
		
		if (empty($tab['d'])
		or empty($tab['m'])
		or empty($tab['y'])
		or empty($tab['nr'])) return FALSE;
		
		$link = '/'.config_item('px_newsletter').'/'.$tab['d'].'/'.$tab['m'].'/'.$tab['y'].'/'.$tab['nr'].'.html';
		$data['newsletter'] = $this->newsletter_model->get_newsletter($link);
	
		if(empty($data['newsletter'])) {
			redirect();
		}
	
		$this->generate_view('mod_newsletter/newsletter_read_view', $data);
	}
	
	public function add() {
		$this->load->helper('email');
	
		$data = array();
		 
		$data['header'] = 'Newsletter';
		
		if($this->newsletter_model->add()){
			$email = $this->input->post('email');
			$msg = array(lang('Email został dodany do bazy. Na podany adres została wysłana wiadomość z linkiem. Kliknij w niego aby potwierdzić rejestrację.'), 0, 1);
			$this->session->set_flashdata('msg', $msg);
		}
		/*else {
			$msg = array('Email nie został dodany. Spróbuj ponownie.', 2, 1);
			$this->session->set_flashdata('msg', $msg);
		}*/
		
		redirect(config_item('px_newsletter'));
	}

	public function remove() {
		$data = array();
	
		$data['header'] = 'Newsletter';
		
		if($this->newsletter_model->remove()){
			$email = $this->input->post('email');
			$msg = array(lang('Email został usunięty z bazy.'), 0, 1);
			$this->session->set_flashdata('msg', $msg);
		}
		/*else {
			$msg = array('Email nie został usunięty. Spróbuj ponownie.', 2, 1);
			$this->session->set_flashdata('msg', $msg);
		}*/
		 
		redirect(config_item('px_newsletter'));
	}
	
	public function confirm() {
		$data = array();
	
		$data['header'] = 'Newsletter';
		
		$tab = $this->uri->uri_to_assoc(-1);
		
		if(isset($tab['confirm']) && !empty($tab['confirm'])){
			if($this->newsletter_model->confirm($tab['confirm'])){
				$msg = array(lang('Email został potwierdzony.'), 0, 1);
				$this->session->set_flashdata('msg', $msg);
			}else {
				$msg = array(lang('Email nie został potwierdzony. Spróbuj ponownie.'), 2, 1);
				$this->session->set_flashdata('msg', $msg);
			}
			
			redirect(config_item('px_newsletter'));
		}
	
		$this->generate_view('mod_newsletter/'.$this->mod_name.'_view', $data);
	}
	
	public function unsubscribe() {
		$data = array();
	
		$data['header'] = 'Newsletter';
		 
		$tab = $this->uri->uri_to_assoc(-1);
		 
		if(isset($tab['unsubscribe']) && !empty($tab['unsubscribe'])){
			if($this->newsletter_model->unsubscribe($tab['unsubscribe'])){
				$msg = array(lang('Email został usunięty z bazy.'), 0, 1);
				$this->session->set_flashdata('msg', $msg);
			}
			/*else {
				$msg = array(lang('Email nie został usunięty z bazy. Spróbuj ponownie.'), 2, 1);
				$this->session->set_flashdata('msg', $msg);
			}*/
	
			redirect(config_item('px_newsletter'));
		}
	
		$this->generate_view('mod_newsletter/'.$this->mod_name.'_view', $data);
	}
	
	public function send_from_queue() {
		//if($_SERVER['REMOTE_ADDR'] != '94.152.152.35' && $_SERVER['REMOTE_ADDR'] != '46.186.119.185') exit();
		$this->load->helper('file');
		$newsletters = $this->newsletter_model->get_queue($this->queue_limit);
		
		foreach($newsletters as $newsletter) {
	
			$ok = 0;
			$error = 0;
			$content = FALSE;
			$emails_hashes = array();
	
			$phrase = lang('newsletter_unsubcribe');
	
			//pobranie hashy e-maili
			$this->db->select("email, hash");
			$this->db->where_in('email', $newsletter->emails);
			$emquery = $this->db->get('newsletter_emails');
			if($emquery->num_rows() > 0) {
				$emails_hashes = array();
				foreach($emquery->result() as $emd) $emails_hashes[$emd->email] = $emd->hash;
			}
	
			foreach($newsletter->emails as $ide => $email) {
				if(isset($newsletter->text) && !empty($newsletter->text)) {
					$this->email->subject($newsletter->title);
					$this->email->from("no-reply@{$_SERVER['HTTP_HOST']}", "no-reply@{$_SERVER['HTTP_HOST']}");
					$this->email->to($email);
					
					$mail_b = $newsletter->text;
					preg_match_all('/(img\=|src\=|url\()(\"|\')[^\"\'\>]+/i', $mail_b, $media);
					$data = preg_replace('/(img|src|url\()(\"|\'|\=\"|\=\')(.*)/i',"$3",$media[0]);
					 
					foreach($data as $row){
						if(strpos($row, 'http://') === FALSE){
							
							$tab_f = pathinfo($row);
							$mime = get_mime_by_extension($tab_f['basename']);
							$file_content = file_get_contents(site_url($row));
							
							$this->email->attach($file_content, 'inline', md5($row).$tab_f['extension'], $mime, true);
							$cid = $this->email->get_attachment_cid(md5($row).$tab_f['extension']);
							 
							$mail_b = str_replace('..'.$row, 'cid:'.$cid, $mail_b);
							$mail_b = str_replace($row, 'cid:'.$cid, $mail_b);
						}
					}
				}
				 
				if($mail_b !== FALSE) {
					$content_add = '';
					if(isset($emails_hashes[$email])) {
						$url = "http://{$_SERVER['HTTP_HOST']}/newsletter/unsubscribe/{$emails_hashes[$email]}";
						$content_add = "<p style=\"font-size:11px;text-align:center; color: #1b3542;\">{$phrase}:
						<br /><a style=\"color: #add8e6; text-decoration: none;\" href=\"{$url}\">{$url}</a></span></p>";
					}
	
					$body = $this->email->full_html('Newsletter', $mail_b.$content_add);
					$this->email->message($body);
	
					$result = $this->email->send();
				} else {
					$result = "Brak treści newslettera.";
				}
	
				if($result === TRUE) {
					$ok++;
					$esdata = array(
						'date_sent' => date("Y-m-d H:i:s"),
						'sent' => 1
					);
				} else {
					$error++;
					$esdata = array(
						'error' => $this->email->print_debugger(),
						'sent' => -1
					);
	
					echo date('d-m-Y H:i:s')." nie udało się wysłać: ".$email."\n";
				}
	
				$this->db->where('id', $ide);
				$this->db->update('newsletter_queue', $esdata);
	
				$this->email->clear(TRUE);
			}
	
			if(isset($newsletter->id)) {
				$this->db->set("date_sent", 'NOW()', FALSE);
				$this->db->set("sent_ok", "(sent_ok + {$ok})", FALSE);
				$this->db->set("errors", "(errors + {$error})", FALSE);
				$this->db->where('id', $newsletter->id);
				$this->db->update('newsletter');
			}
		}
	}
}