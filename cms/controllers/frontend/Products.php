<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller {

	public $mod_name = 'products';
	public $mod_table = 'products';

	public $id_pr = null;
	public $id_cat = null;

	public $pr_dir = 'products/';

	public function __construct() {
		parent::__construct();
		$this->load->model('frontend/'.$this->mod_name.'_model');
	}

	public function index($id) {
		$data = array();

		if(!$product = $this->products_model->get_product($id)) {
			show_404();
		}

		if($product) {
			$this->id_pr = $id;
			$this->id_cat = $product->id_cat;

			if(isset($product->meta_title) && !empty($product->meta_title)) {
				$this->meta_title = $product->meta_title;
			} else {
				$this->meta_title = $product->title;
			}
			if(isset($product->meta_keywords) && !empty($product->meta_keywords)) {
				$this->meta_keywords = $product->meta_keywords;
			}
			if(isset($product->meta_description) && !empty($product->meta_description)) {
				$this->meta_description = $product->meta_description;
			}

			$category = $this->products_model->get_category($product->id_cat);

			$data['header'] = $product->title;
			$data['product'] = $product;
			$data['category'] = $category;
		}
		$this->generate_view('mod_products/product_view', $data);
	}

	public function category($id) {
		$data = array();

		if(!$category = $this->products_model->get_category($id)) {
			show_404();
		}

		if($category) {
			$this->id_cat = $id;

			if(isset($category->meta_title) && !empty($category->meta_title)) {
				$this->meta_title = $category->meta_title;
			} else {
				$this->meta_title = $category->title;
			}
			if(isset($category->meta_keywords) && !empty($category->meta_keywords)) {
				$this->meta_keywords = $category->meta_keywords;
			}
			if(isset($category->meta_description) && !empty($category->meta_description)) {
				$this->meta_description = $category->meta_description;
			}

			$subcategories = $this->products_model->get_categories(array('id_tree' => $id));
			$products = $this->products_model->get_products(array('id_cat' => $id));

			$data['header'] = $category->title;
			$data['category'] = $category;
			$data['subcategories'] = $subcategories;
			$data['products'] = $products;
		}
		$this->generate_view('mod_products/category_view', $data);

	}
}