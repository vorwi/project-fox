<?

class Estatedev extends MY_Controller {

	public $mod_name = 'estatedev';
	public $tab;
	public $params = false;
	public $subparams = false;

	public function __construct() {

		parent::__construct();

		$this->config->load('estatedev', TRUE);

		$this->load->model('frontend/' . $this->mod_name . '_model');

		$this->tab = $this->uri->ruri_to_assoc();
		
		$this->wrapper = false;
	}

	public function investment() {
		$data = array();

		$data['investment'] = $this->estatedev_model->get_single_investment($this->tab['estatedev_investments']);
		$data['investments'] = $this->estatedev_model->get_all_investments();
		$data['types'] = $this->estatedev_model->get_investment_estates_types($this->tab['estatedev_investments']);
		
		$data['building_param'] = $this->estatedev_model->get_single_parameter(17);

		if ($this->params) {
			//var_dump($this->params);
			//var_dump($data['building_param'], $this->params[$data['building_param']->url]);
			
			$data['bulding_url'] = $data['investment']->url;
			if (isset($data['types']) && is_array($data['types'])) {
				foreach ($data['types'] as $type) {
					if (isset($this->params[$type->url]) && in_array($type->id, $this->params[$type->url])) {
						$data['bulding_url'] .= '/'.$type->url.'_'.$type->id;
						break;
					}
				}
			}
			
			$data['bulidings'] = $this->estatedev_model->get_investment_buildings($this->tab['estatedev_investments']);
			if(is_array($data['bulidings']) && !empty($data['building_param'])){
				foreach($data['bulidings'] as $b){
					if(isset($this->params[$data['building_param']->url]) && in_array($b->id, $this->params[$data['building_param']->url])){
						$data['building'] = $b;
						break;
					}
				}
			}
			if(isset($data['building'])){
				$data['selected_buliding'] = $this->estatedev_model->get_investment_single_building($this->tab['estatedev_investments'], $data['building']->id);
			}

			$type = reset($this->params);
			$data['estates'] = $this->estatedev_model->get_estates($this->params, $type[0]);
			$data['investments'] = $this->estatedev_model->get_all_investments();
			$data['search_parameters'] = $this->estatedev_model->get_search_parameters();
			$data['selected_search_parameters'] = $this->params;
			$data['table_parameters'] = $this->estatedev_model->get_table_parameters();
			$data['search_link'] = current_url();
			$data['content'] = $this->generate_view('mod_estatedev/' . $this->mod_name . '_investment_estates_view', $data, 1, 1);
		} else {
			$data['images'] = $this->estatedev_model->get_investment_images($this->tab['estatedev_investments']);
			$data['content'] = $this->generate_view('mod_estatedev/' . $this->mod_name . '_investment_desc_view', $data, 1, 1);
		}

		$this->generate_view('mod_estatedev/' . $this->mod_name . '_investment_view', $data);
	}

	public function estates_list() {
		$data = array();

		// tworzenie zapytania pobierającego nieruchomości
		$data['estates'] = $this->estatedev_model->get_estates($this->params, $this->tab['estatedev_types']);
		// tworzenie zapytania pobierającego nieruchomości

		$data['estate_type'] = $this->estatedev_model->get_type($this->tab['estatedev_types']);
		$data['investments'] = $this->estatedev_model->get_all_investments();
		$data['search_parameters'] = $this->estatedev_model->get_search_parameters();
		$data['selected_search_parameters'] = $this->params;
		$data['table_parameters'] = $this->estatedev_model->get_table_parameters();
		$data['search_link'] = $this->estatedev_model->get_search_link($this->tab['estatedev_types']);
		
		$this->frontend->add_breadcrumb($data['estate_type']->name_plural, $data['search_link']);

		$this->generate_view('mod_estatedev/' . $this->mod_name . '_estates_list_view', $data);
	}

	public function estate() {
		$data = array();
		$data['tab'] = $this->tab;

		$data['investments'] = $this->estatedev_model->get_all_investments();
		$data['estate'] = $this->estatedev_model->get_single_estate($this->tab['estatedev_estates']);
		if ($data['estate']) {
			$data['estate_type'] = $this->estatedev_model->get_type($data['estate']->id_edt, false);
			$data['contact'] = $this->estatedev_model->get_contact($data['estate']->id_edt);
			
			$investment = $this->estatedev_model->get_single_investment($data['estate']->id_edi);
			$this->frontend->add_breadcrumb($investment->name, $investment->url);
			$this->frontend->add_breadcrumb($data['estate_type']->name . ' ' . $data['estate']->name, $data['estate']->url);

			$this->generate_view('mod_estatedev/' . $this->mod_name . '_estate_view', $data);
		}
	}

	public function index() {

		$data = array();
		$data['tab'] = $this->tab;

		// ustalenie id strony w zależności od typu nieruchomości
		if (isset($this->tab['estatedev_types'])) {
			switch ($this->tab['estatedev_types']) {
				case '1': $this->id = 3;
					break; // mieszkania
				//case '2': $this->id = 0; break; //garaże - aktualnie nieobecne w menu strony
				case '3': $this->id = 4;
					break; // lokale użytkowe
				case '4': $this->id = 5;
					break; // domy
				case '5': $this->id = 6;
					break; // tereny inwestycyjne
			}
		}
		// ustalenie id strony w zależności od typu nieruchomości
		// adres wyników wyszukiwania (dodanie na końcu parametrów i wartości)
		$search_parameters = $this->estatedev_model->get_search_parameters();

		$url = '';
		if (is_array($this->input->post('param'))) {
			foreach ($this->input->post('param') as $param_key => $param_values) {
				if (isset($search_parameters[$param_key]) && !empty($param_values)) {
					if ((isset($param_values['from']) && !empty($param_values['from'])) || (isset($param_values['to']) && !empty($param_values['to']))) {
						$url .= (($url !== '') ? '__' : '') . $search_parameters[$param_key]->url;
						if (isset($param_values['from']) && !empty($param_values['from'])) {
							$url .= '_min_' . $param_values['from'];
						}
						if (isset($param_values['to']) && !empty($param_values['to'])) {
							$url .= '_max_' . $param_values['to'];
						}
					} else if (!isset($param_values['from']) && !isset($param_values['to'])) {
						$url .= (($url !== '') ? '__' : '') . $search_parameters[$param_key]->url;
						foreach ($param_values as $param_value_key => $param_value) {
							$url .= '_' . $param_value;
						}
					}
				}
			}
		}
		if (!empty($url)) {
			redirect(current_url() . '/' . $url);
		}
		// adres wyników wyszukiwania (dodanie na końcu parametrów i wartości)
		// przetworzenie kryteriów wyszukiwania z linku do tablicy
		$this->params = false;
		if (isset($this->tab['filters'])) {
			$params_ex = explode('__', $this->tab['filters']);
			if (is_array($params_ex)) {
				$this->params = array();
				foreach ($params_ex as $pex) {
					$values = explode('_', $pex);
					$pk = array_shift($values);
					$this->params[$pk] = $values;
					if (in_array('min', $values) || in_array('max', $values)) {
						$this->params[$pk] = array('min' => false, 'max' => false);
						if ($values[0] === 'min') {
							$this->params[$pk]['min'] = (int) $values[1];
						}
						if ($values[0] === 'max') {
							$this->params[$pk]['max'] = (int) $values[1];
						} elseif (isset($values[2]) && $values[2] === 'max') {
							$this->params[$pk]['max'] = (int) $values[3];
						}
					}
				}
			}
		}
		// przetworzenie kryteriów wyszukiwania z linku do tablicy
		// przetwarzanie dodatkowych parametrów
		if (isset($this->tab['subfilters'])) {
			// dodatkowe filtry w linku
			$subparams_ex = explode('__', $this->tab['subfilters']);
			foreach ($subparams_ex as $pex) {
				$values = explode('_', $pex);
				$pk = array_shift($values);
				$this->subparams[$pk] = $values;
			}
		}

		if (isset($this->tab['estatedev_investments'])) {
			$this->investment();
		} elseif (isset($this->tab['estatedev_estates'])) {
			$this->estate();
		} else {
			$this->estates_list();
		}


		//show_404();
	}
	
	public function get_estate_ajax($id) {
		$this->output->enable_profiler(FALSE);
		$data['estate'] = $this->estatedev_model->get_single_estate($id);
		if ($data['estate']) {
			$data['estate_type'] = $this->estatedev_model->get_type($data['estate']->id_edt, false);
		} else {
			$data['error'] = 'Nie można pobrać nieruchomości '.$id;
		}
		
		$html = $this->load->view('frontend/' . config_item('template_f') . 'mod_estatedev/' . $this->mod_name . '_estate_ajax_view', $data, true);
		
		$this->output->set_header('Cache-Control: no-cache, must-revalidate');
		$this->output->set_header('Expires: ' . gmdate(DATE_RFC1123));
		$this->output->set_header('Content-type: text/html');
		
		$this->output->set_content_type('text/html')->set_output($html);
		return true;
	}

}
