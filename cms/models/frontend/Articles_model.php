<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Articles_model extends CI_Model {
	public $mod_table = 'articles';

	public function __construct() {
		parent::__construct();
	}

	public function checkConfig() {
		$this->db->where('id', config_item('main_art'));
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() == 0) {
			$data = array('id' => $main_id, 'id_cat' => 1, 'id_tree' => 0, 'position' => 0, 'pub' => 1, );

			$this->db->insert($this->mod_table, $data);

			if($this->db->affected_rows() == 1) {
				$stamp = uniqid().mt_rand();

				$data = array('id_art' => $main_id, 'stamp' => $stamp, 'lang' => config_item('main_lang'), 'title' => 'Strona główna', 'pub' => 1, );

				$this->db->set('date_modified', 'NOW()', FALSE);
				$this->db->insert($this->mod_table.'_content', $data);
			}
		}
	}

	public function get_article($id = null) {
		$data = array();

		if($id == null)
			$id = config_item('main_id');

		$this->db->select('tk2.*, tk1.*, tk3.id_art as parent_id, tk3.title as parent_title, tk4.url as user_url');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_art');
		$this->db->join($this->mod_table.'_content as tk3', 'tk1.id_tree = tk3.id_art and tk2.lang = tk3.lang', 'left');
		$this->db->join('user_url tk4', "tk4.dest_id=tk1.id AND tk4.type='articles' AND tk4.lang= '".$_SESSION['lang']."'", 'left');
		$this->db->where('tk1.id', $id);
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			$data = $query->row();
		}else{
			$data = FALSE;
		}

		return $data;
	}

	public function getAllArticles() {
		$data = array();

		$this->db->select('tk1.id_tree, tk2.*, tk3.url as user_url');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_art');
		$this->db->join('user_url tk3', "tk3.dest_id=tk1.id AND tk3.type='articles' AND tk3.lang= '".$_SESSION['lang']."'", 'left');
		$this->db->where('tk1.id_cat !=', 3);
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			$all = $query->result();
			$subs = array();
			foreach($all as $row) {
				if($row->id_tree == 0) {
					$data[$row->id_art] = $row;
				} else {
					$subs[$row->id_art] = $row;
				}
			}
			//poziom 2
			foreach($subs as $row) {
				if(isset($data[$row->id_tree])) {
					if(!isset($data[$row->id_tree]->children))
						$data[$row->id_tree]->children = array();
					$data[$row->id_tree]->children[$row->id_art] = $row;
					unset($subs[$row->id_art]);
				}
			}
		}

		return $data;
	}

	public function getAllArticles2($id_tree = 0) {
		$this->db->select('tk1.id, tk2.title, tk2.short_title, tk2.url, tk1.id_tree, tk3.url as user_url');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_art', 'left');
		$this->db->join('user_url tk3', "tk3.dest_id=tk1.id AND tk3.type='articles' AND tk3.lang= '".$_SESSION['lang']."'", 'left');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		if(is_array($id_tree)) {
			$this->db->where_in('tk1.id_tree', $id_tree);
		} else {
			$this->db->where('tk1.id_tree', $id_tree);
		}
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			$data = array();
			$parent_ids = array();

			foreach($query->result() as $row) {
				$parent_ids[] = $row->id;
				$data[$row->id] = $row;
			}
			$query->free_result();
			$children = $this->getAllArticles2($parent_ids);

			foreach($children as $row) {
				if(!isset($data[$row->id_tree]->children))
					$data[$row->id_tree]->children = array();
				$data[$row->id_tree]->children[] = $row;
			}

			return $data;
		}
		return array();
	}

	public function article_exists($id) {
		if(!is_numeric($id)){ return FALSE; }

		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function tabs($id) {
		$this->db->select('tk2.id_art, tk2.title, tk2.content');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_art');
		$this->db->where('tk1.id_tree', $id);
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $tab) {
				$this->db->where('id_tree', $tab->id_art);
				$query = $this->db->get($this->mod_table);
				if($query->num_rows() == 0) {
					$data[] = array('id_art' => $tab->id_art, 'title' => $tab->title, 'content' => $tab->content);
				}
			}

			return $data;
		} else{
			return FALSE;
		}
	}

	public function get_all_numbers()
    {
        $this->db->select('*');
        $this->db->from('numbers');
        
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
	}
	
	public function get_all_offers()
    {
        $this->db->select('*');
        $this->db->from('offers');
        
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
	}
	
	public function get_all_skills()
    {
        $this->db->select('*');
        $this->db->from('skills');
        
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }
}
