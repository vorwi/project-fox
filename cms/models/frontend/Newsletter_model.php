<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function add(){
		$min 	= 6;
		$max 	= 12;

		$email = trim(strtolower($this->input->post('email')));

		if(!valid_email($email)){
			$msg = array(lang('Podany adres jest niepoprawny. Spróbuj ponownie.'), 2, 1);
			$this->session->set_flashdata('msg_error', $msg);
			return FALSE;
		}

		$this->db->where('email',$email);
		$query = $this->db->get('newsletter_emails');

		if($query->num_rows()>0){
			$msg = array(lang('Podany adres znajduje się już w bazie.'), 2, 1);
			$this->session->set_flashdata('msg_error', $msg);
			return FALSE;
		}

		$pass = '';
		for($i=0;$i<rand($min,$max);$i++){
			$char=chr(rand(48,122));
			if(preg_match("/[^0-9A-Za-z]/",$char)) $pass .= $char;
			else $i--;
		}

		$pass = $pass.time();
		$hash = md5($pass);

		$data = array(
			'email'		=>$email,
			'hash'		=>$hash,
			'confirm'	=>0,
		);

		$this->db->set('date_add', 'NOW()', FALSE);
		$query = $this->db->insert('newsletter_emails', $data);

		if($query){
			$format = 'Adres email: <b>%s</b> został zgłoszony do newslettera.<br />';
			$format .= ' Jeżeli chcesz dostawać wiadomości z <a href="http://%s">%s</a>, kliknij na poniższy link:';
			$format .= ' <br /><a href="http://%s/newsletter/confirm/%s">http://%s/newsletter/confirm/%s</a><br /><br />';
			$format .= 'W przeciwnym wypadku zignoruj ten e-mail. Adres zostanie usunięty z bazy w ciągu 48h.';
			
			$html = sprintf(lang($format), $email, $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST'], $hash, $_SERVER['HTTP_HOST'], $hash);
			
			$body = $this->email->full_html(lang('Newsletter - potwierdzenie'), $html);
			$this->email->from('automat@'.$_SERVER['HTTP_HOST'], 'automat@'.$_SERVER['HTTP_HOST']);
			$this->email->to($email);
	
			$this->email->subject(lang('Newsletter - potwierdzenie'));
			$this->email->message($body);

			if($this->email->send()) return TRUE;

			return FALSE;
		}
		else return FALSE;
	}

	public function confirm($hash=null){
		if($hash===null) return FALSE;

		$hash = trim($hash);

		$this->db->where('hash',$hash);
		$query = $this->db->get('newsletter_emails');

		if($query->num_rows()==0){
			$msg = array(lang('Podany adres nie został znaleziony w bazie.'), 2, 1);
			$this->session->set_flashdata('msg_error', $msg);
			return FALSE;
		}

		$this->db->where('hash',$hash);
		$query = $this->db->update('newsletter_emails', array('confirm'=>1));

		if($query) return TRUE;
		else return FALSE;
	}

	public function remove(){
		$email = trim(strtolower($this->input->post('email')));

		$this->db->where('email',$email);
		$query = $this->db->get('newsletter_emails');

		if($query->num_rows()==0){
			$msg = array(lang('Podany adres nie znajduje się w bazie.'), 2, 1);
			$this->session->set_flashdata('msg_error', $msg);
			return FALSE;
		}

		$this->db->where('email',$email);
		$query = $this->db->delete('newsletter_emails');

		if($query) return TRUE;
		else return FALSE;
	}

	public function unsubscribe($hash=null){
		if($hash===null) return FALSE;

		$hash = trim($hash);

		$this->db->where('hash',$hash);
		$query = $this->db->get('newsletter_emails');

		if($query->num_rows()==0){
			$msg = array(lang('Podany adres nie został znaleziony w bazie.'), 2, 1);
			$this->session->set_flashdata('msg_error', $msg);
			return FALSE;
		}

		$this->db->where('hash',$hash);
		$query = $this->db->delete('newsletter_emails');

		if($query) return TRUE;
		else return FALSE;

	}

	public function get_newsletter($link = null){
		if(is_null($link)) return FALSE;

		$query = $this->db
			->where('link',$link)
			->get('newsletter');

		return ($query->num_rows()>0) ? $query->row() : FALSE;
	}
	
	public function get_queue($queue_limit) {
	
		$this->db->select('id, id_newsletter, email');
		$this->db->where('sent', 0);
		$this->db->order_by('id', 'asc');
		$this->db->limit($queue_limit);
		$query = $this->db->get('newsletter_queue');
	
		$newsletters = array();
		$nl_ids = array();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$idn = $row->id_newsletter;
				$nl_ids[$idn] = $idn;
	
				if(!isset($newsletters[$idn])) {
	
					$this->db->where('id', $idn);
					$nlquery = $this->db->get('newsletter');
	
					$newsletters[$idn] = $nlquery->row();
					$newsletters[$idn]->emails = array($row->id => $row->email);
	
				} else {
					$newsletters[$idn]->emails[$row->id] = $row->email;
				}
	
			}
			return $newsletters;
		}
		return array();
	}
}