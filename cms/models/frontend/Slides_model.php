<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Slides_model extends CI_Model {
	
	public $mod_table = 'slides';
	public $img_dir = 'slides/';

	public function __construct() {
		parent::__construct();
	}
	
	public function getSlidesImages($ids, $limit = 0) {
		$this->db->select('si.*, s.dir');
		$this->db->from($this->mod_table.'_img as si');
		$this->db->join($this->mod_table.' as s', 'si.id_gal = s.id');
		$this->db->where_in('si.id_gal', $ids);
		$this->db->order_by('si.id_gal, si.position');
		$this->db->group_by('si.id');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $row) {
				if (!isset($data[$row->id_gal])) { $data[$row->id_gal] = array(); }
				if ($limit > 0 && count($data[$row->id_gal]) == $limit) { continue; }

				$tab = explode('.', $row->img);
				$row->img_site_path = './' . config_item('site_path') . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $row->img;
				$row->thumb_site_path = './' . config_item('site_path') . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $tab[0] . '_thumb.' . $tab[1];
				$row->img_path = '/' . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $row->img;
				$row->thumb_path = '/' . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $tab[0] . '_thumb.' . $tab[1];
				if (file_exists($row->img_site_path) && file_exists($row->thumb_site_path)) {
					$data[$row->id_gal][] = $row;
				}
			}
			return $data;
		} else {
			return NULL;
		}
	}

	public function get_slides($id_art) {
		$dir = 'slides/';
		/*
		 $this->db->where('id', $id_art);
		 $query2 = $this->db->get('articles');

		 $row = $query2->row();
		 $tree_id = $row->id_tree;
		 */

		$this->db->where('pub', 1);
		$this->db->order_by('position');
		$this->db->where('lang', $_SESSION['lang']);
		$this->db->where('id_art', $id_art);
		$query = $this->db->get($this->mod_table);

		//jesli dziecko nie ma przypisanych slajdów to pobiera od rodzica
		/*
		 if($query->num_rows() == 0) {
		 if($tree_id != 0){$id_art = $tree_id;}
		 $this->db->where_in('pub', 1);
		 $this->db->order_by('position');
		 $this->db->where('id_art', $id_art);
		 $query = $this->db->get($this->mod_table);

		 } */

		if($query->num_rows() > 0) {
			$data = array();
			$ids = array();
			foreach ($query->result() as $row) { $ids[] = $row->id; }
			$images = $this->getSlidesImages($ids);
			
			foreach($query->result() as $row) {
				if (!isset($images[$row->id])) { continue; }
				$row->img = $images[$row->id];
				$data[] = $row;
			}

			return $data;
		}
		return FALSE;
	}

}
