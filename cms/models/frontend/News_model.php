<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model {
	public $mod_table = 'news';
	public $img_dir = 'news';

	public function __construct() {
		parent::__construct();
	}

	public function get_single_news($id) {
		$this->db->select('nc.*, n.*, uu.url as user_url');
		$this->db->from($this->mod_table.' n');
		$this->db->join($this->mod_table.'_content as nc', 'n.id = nc.id_news');
		$this->db->join('user_url uu', "uu.dest_id=n.id AND uu.type = 'news' AND uu.lang  = '".$_SESSION['lang']."'", 'left');
		$this->db->where('n.pub', 1);
		$this->db->where('nc.pub', 1);
		$this->db->where('nc.lang', $_SESSION['lang']);
		$this->db->where('n.id', $id);
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->row();
		}
		return FALSE;
	}

	public function get_news($num = FALSE, $offset = FALSE) {
		$this->db->select('nc.*, n.*, uu.url as user_url');
		$this->db->from($this->mod_table.' n');
		$this->db->join($this->mod_table.'_content as nc', 'n.id = nc.id_news');
		$this->db->join('user_url uu', "uu.dest_id=n.id AND uu.type = 'news' AND uu.lang  = '".$_SESSION['lang']."'", 'left');
		$this->db->where('n.pub', 1);
		$this->db->where('nc.pub', 1);
		$this->db->where('nc.lang', $_SESSION['lang']);
		$this->db->order_by('nc.date_add', 'desc');
		if($num !== FALSE && $offset !== FALSE) {
			$this->db->limit($num, $offset);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result();
		}
		return FALSE;
	}

	public function addon($id_art, $perpage = 10) {
		if($perpage > 0) {
			
			$this->db->select('nc.*, n.*');
			$this->db->from($this->mod_table.' n');
			$this->db->join($this->mod_table.'_content as nc', 'n.id = nc.id_news');
			$this->db->where('n.id_art', $id_art);
			$this->db->where('n.pub', 1);
			$this->db->where('nc.pub', 1);
			$this->db->where('nc.lang', $_SESSION['lang']);	
			$this->db->order_by('nc.date_add', 'desc');	
			$query = $this->db->get();
			$numrows = $query->num_rows();

			if($numrows == 0)
				return FALSE;

			$pconfig = array(
					'base_url' => base_url() . config_item('px_articles_pag') . '/' . $id_art . '/',
					'total_rows' => $numrows,
					'per_page' => $perpage,
					'uri_segment' => 3
			);
			$this->pagination->init($pconfig);
			
			if(@is_numeric($this->uri->segment(3))) {
				$start = $this->uri->segment(3);
			} else {
				$start = 0;
			}
		}

		$this->db->select('nc.*, n.*, uu.url as user_url');
		$this->db->from($this->mod_table.' n');
		$this->db->join($this->mod_table.'_content as nc', 'n.id = nc.id_news');
		$this->db->join('user_url uu', "uu.dest_id=n.id AND uu.type = 'news' AND uu.lang  = '".$_SESSION['lang']."'", 'left');
		$this->db->where('n.id_art', $id_art);
		$this->db->where('n.pub', 1);
		$this->db->where('nc.pub', 1);
		$this->db->where('nc.lang', $_SESSION['lang']);
		$this->db->order_by('nc.date_add', 'desc');
		if($perpage > 0) { $this->db->limit($perpage, $start); }
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			$view_data = array('news' => $query->result(), 'img_dir' => 'news/');

			return $this->load->view('frontend/'.config_item('template_f').'mod_news/news_list_view', $view_data, true);
		}
		return FALSE;
	}

}
