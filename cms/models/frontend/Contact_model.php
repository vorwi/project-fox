<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends CI_Model {
	public $mod_table = 'contact';

	public function __construct() {
		parent::__construct();
	}

	public function get_forms($id_art) {
		$this->db->where('id_art', $id_art);
		$this->db->where('lang', $_SESSION['lang']);
		$query = $this->db->get($this->mod_table);

		$forms = array();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$forms[$row->id] = $row;
			}
		}
		return $forms;
	}

	public function check($id) {
		if($id==11){
			$this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email');
			$this->form_validation->set_rules('country', lang('Kraj'), 'trim|required');
			$this->form_validation->set_rules('city', lang('Miejscowość'), 'trim|required');
			$this->form_validation->set_rules('name', lang('Imie'), 'trim|required');
			$this->form_validation->set_rules('surname', lang('Nazwisko'), 'trim|required');
			$this->form_validation->set_rules('tel', lang('Numer telefonu'), 'trim|required');
		}
		else{
			$this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email');
			$this->form_validation->set_rules('message', lang('Treść wiadomości'), 'trim|required');
			$this->form_validation->set_rules('name', lang('Imie'), 'trim|required');
			$this->form_validation->set_rules('surname', lang('Nazwisko'), 'trim|required');
			$this->form_validation->set_rules('tel', lang('Numer telefonu'), 'trim|required');
		}
		return ($this->form_validation->run() === TRUE && $this->security->form_markers_check());
		
	}

	public function send($addreesee, $id) {
		if($id==11){
			$title = lang('Wiadomość z formularza w sprawie pracy');
			$sender = $this->input->post('email');

			$html = '<b>'.lang('E-mail').':</b> <a href="mailto:'.$sender.'">'.$sender.'</a><br />';
			$html .= '<b>'.lang('Imie').':</b><br />'.$this->input->post('name').'<br />';
			$html .= '<b>'.lang('Nazwisko').':</b><br />'.$this->input->post('surname').'<br />';
			$html .= '<b>'.lang('Numer telefonu').':</b><br />'.$this->input->post('tel').'<br />';
			$html .= '<b>'.lang('Kraj').':</b><br />'.$this->input->post('country').'<br />';
			$html .= '<b>'.lang('Miejscowość').':</b><br />'.$this->input->post('city').'<br />';
		}
		else{
			$title = lang('Wiadomość z formularza kontaktowego');
			$sender = $this->input->post('email');

			$html = '<b>'.lang('E-mail').':</b> <a href="mailto:'.$sender.'">'.$sender.'</a><br />';
			$html .= '<b>'.lang('Imie').':</b><br />'.$this->input->post('name').'<br />';
			$html .= '<b>'.lang('Nazwisko').':</b><br />'.$this->input->post('surname').'<br />';
			$html .= '<b>'.lang('Numer telefonu').':</b><br />'.$this->input->post('tel').'<br />';
			$html .= '<b>'.lang('Treść wiadomości').':</b><br />'.$this->input->post('message').'<br />';
		}
		

		$body = $this->email->full_html($title, $html);
		$this->email->from('automat@'.$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
		$this->email->to($addreesee);
		$this->email->reply_to($sender);
		//$this->email->to('admin@artneo.pl');

		$this->email->subject($title);
		$this->email->message($body);

		if(isset($_FILES['cv']) || isset($_FILES['lm'])){
			$cv_dir=$this->attach_file('cv');
			$lm_dir=$this->attach_file('lm');
		}
		if($this->email->send()) {
			if(isset($cv_dir)){
				if(is_dir($cv_dir)){
					$files=glob($cv_dir.'*');
					foreach($files as $file){
						unlink($file);
					}
					rmdir($cv_dir);
				}
			}
			if(isset($lm_dir)){
				if(is_dir($lm_dir)){
					$files=glob($lm_dir.'*');
					foreach($files as $file){
						unlink($file);
					}
					rmdir($lm_dir);
				}
			}
			return TRUE;
		} else {
			//echo $this->email->print_debugger();
			return FALSE;
		}
	}
	
	public function attach_file($file_name)
		{
			if(is_uploaded_file($_FILES[$file_name]['tmp_name'])) {
				$dir_tmp = './'.config_item('temp').'attach_'.time().'/';
				@mkdir($dir_tmp, 0777);
				if(!is_dir($dir_tmp)) return FALSE;
		
				$config = array(
					'upload_path' => $dir_tmp,
					'allowed_types' => 'doc|pdf|docx|odt', 
					'max_size' => '2120',
					'file_name' => 'attach_'.time()
				);
	
				$this->load->library('upload', $config);
	
				if($this->upload->do_upload($file_name)) {
					$tab = $this->upload->data();

					$this->email->attach($tab['full_path'], '', $file_name);
				} else {
					$msg[] = array(lang("Załącznik nie został dodany poprawnie: ").$this->upload->display_errors('', ''), 2);
					$this->neocms->forward_msg(lang('CV i LM musi być w formacie '.str_replace('|', ', ', $config['allowed_types']).' i mieć rozmiar mniejszy niż '.number_format((float)$config['max_size']/1024, 2, '.', '').' MB.'), 1);
					redirect(current_url());

					if(count($msg) > 0) {
						$this->session->set_flashdata('msg_files', $msg);
					}
					return FALSE;
				}
			}
			return $dir_tmp;
		}

	public function addon($id) {
		$forms = $this->get_forms($id);

		if(!empty($forms)) {
			$view_data = array(
				'forms' => $forms
			);

			if($this->input->post('contact-form-send')) {
				$id_form = $this->input->post('id');
				$chosen_form = (isset($forms[$id_form]) ? $forms[$id_form] : null);

				if($this->check($id) && is_object($chosen_form)) {
					$email = $chosen_form->email;

					if($this->send($email, $id)) {
						$this->neocms->forward_msg(lang('Dziękujemy. Email został wysłany.'), 0);
						redirect(current_url());
					} else {
						$this->neocms->set_msg(lang('Email nie został wysłany, spróbuj ponownie.'), 1);
					}
				}
			}
			$view_data['security_markers'] = $this->security->form_markers_get();
			return $this->load->view('frontend/'.config_item('template_f').'mod_contact/contact_view', $view_data, true);
		}
		return FALSE;
	}
}
