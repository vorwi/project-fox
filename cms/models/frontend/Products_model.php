<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model {
	public $product_dir = 'products/';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function get_product($id = null){
		$data = array();
		 
		if($id==null) {
			return FALSE;
		}
		 
		$this->db->select('tk2.*, tk1.*, tk3.url as user_url');
		$this->db->from('products as tk1');
		$this->db->join('products_content as tk2', 'tk1.id = tk2.id_pr');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'products' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk1.id', $id);
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$query = $this->db->get();
	
		if($query->num_rows()>0) {
			$data = $query->row();
			$images = $this->getProductsImages(array($data->id));
			if (isset($images[$data->id])) {
				$data->images = $images[$data->id];
				$data->main_image = current($images[$data->id]);
			}
		}else {
			$data = FALSE;
		}
	
		return $data;
	}

	public function getProductsImages($ids) {
		$this->db->select('pi.*, p.dir');
		$this->db->from('products_content_img as pi');
		$this->db->join('products as p', 'pi.id_pr = p.id');
		$this->db->where_in('pi.id_pr', $ids);
		$this->db->order_by('pi.id_pr, pi.position');
		$this->db->group_by('pi.id');
		$query = $this->db->get();
	
		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $row) {
				if (!isset($data[$row->id_pr])) { $data[$row->id_pr] = array(); }
	
				$tab = explode('.', $row->img);
				$row->img_site_path = './' . config_item('site_path') . config_item('upload_path') . $this->product_dir . $row->dir . '/' . $row->img;
				$row->thumb_site_path = './' . config_item('site_path') . config_item('upload_path') . $this->product_dir . $row->dir . '/' . $tab[0] . '_thumb.' . $tab[1];
				$row->thumb_list_site_path = './' . config_item('site_path') . config_item('upload_path') . $this->product_dir . $row->dir . '/' . $tab[0] . '_thumb_list.' . $tab[1];
				$row->img_path = '/' . config_item('upload_path') . $this->product_dir . $row->dir . '/' . $row->img;
				$row->thumb_path = '/' . config_item('upload_path') . $this->product_dir . $row->dir . '/' . $tab[0] . '_thumb.' . $tab[1];
				$row->thumb_list_path = '/' . config_item('upload_path') . $this->product_dir . $row->dir . '/' . $tab[0] . '_thumb_list.' . $tab[1];
				if (file_exists($row->img_site_path) && file_exists($row->thumb_list_site_path)) {
					$data[$row->id_pr][] = $row;
				}
			}
			return $data;
		} else {
			return NULL;
		}
	}
	
	public function get_category($id = null){
		if($id==null) {
			return FALSE;
		}
		 
		$this->db->select('tk2.*, tk1.*, tk3.id_cat as parent_id, tk3.title as parent_title, tk4.url as user_url');
		$this->db->from('products_category as tk1');
		$this->db->join('products_category_content as tk2', 'tk1.id = tk2.id_cat');
		$this->db->join('products_category_content as tk3', 'tk1.id_tree = tk3.id_cat', 'left');
		$this->db->join('user_url tk4',"tk4.dest_id=tk1.id AND tk4.type = 'products_category' AND tk4.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk1.id', $id);
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$query = $this->db->get();
	
		if($query->num_rows()>0) {
			return $query->row();
		}else {
			return FALSE;
		}
	}
	
	public function get_products($params = FALSE){
		if(is_array($params)) $params = (object)$params;
		
		if(!isset($params->id_cat)){
			$params->id_cat = FALSE;
		}
		
		$this->db->select('tk2.*, tk1.*, tk3.url as user_url');
		$this->db->from('products as tk1');
		$this->db->join('products_content as tk2', 'tk1.id = tk2.id_pr');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'products' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		if($params->id_cat !== FALSE){
			$this->db->where('tk1.id_cat', $params->id_cat);
		}
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->group_by('tk1.id');
		$this->db->order_by('tk1.position');
		if(isset($params->offset)) {
			$this->db->limit($params->limit, $params->offset);
		}
		if(isset($params->count)) {
			$query = $this->db->get();
			return $query->num_rows();
		} else {
			$query = $this->db->get();
		}
	
		if($query->num_rows()>0) {
			$data = array();
			$ids = array();
			foreach ($query->result() as $row) { $ids[] = $row->id; }
			$images = $this->getProductsImages($ids);
			
			foreach ($query->result() as $row){
				if (isset($images[$row->id])) { 
					$row->images = $images[$row->id];
					$row->main_image = current($images[$row->id]);
				}
				$data[] = $row;
			}
			return $data;
		}else {
			return FALSE;
		}
	}
	
	public function get_categories($params = false){
		if(is_array($params)) $params = (object)$params;
			
		$this->db->select('tk2.*, tk1.*, tk3.url as user_url');
		$this->db->from('products_category as tk1');
		$this->db->join('products_category_content as tk2', 'tk1.id = tk2.id_cat');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'products_category' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		if(isset($params->id_tree)){
			$this->db->where('tk1.id_tree', $params->id_tree);
		}
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->group_by('tk1.id');
		$this->db->order_by('tk1.position');
		if(isset($params->offset)) {
			$this->db->limit($params->limit, $params->offset);
		}
		if(isset($params->count)) {
			$query = $this->db->get();
			return $query->num_rows();
		} else {
			$query = $this->db->get();
		}
	
		if($query->num_rows()>0) {
			$data = array();
			foreach ($query->result() as $row){
				$data[] = $row;
			}
			return $data;
		}else {
			return FALSE;
		}
	}

}