<? if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends CI_Model {
	public $mod_table = 'gallery';
	public $img_dir = 'gallery/';

	public function __construct() {
		parent::__construct();
	}
	
	public function get_gallery($id, $limit = 0){
 		
 		$data = array();
 		$dir = 'gallery/';
 		
 		$this->db->where('id', $id);
 		$this->db->order_by('position');
 		$query = $this->db->get($this->mod_table);
 		
 		if($query->num_rows() > 0){
 			
			$gallery = $query->row();
			$gallery->images = $this->get_gallery_images($id, $limit);

 			return $gallery;
 		}
 		return FALSE;
 	}

	public function get_gallery_images($ids, $limit = 0) {
		$this->db->select('gi.*, g.dir');
		$this->db->from($this->mod_table.'_img as gi');
		$this->db->join($this->mod_table.' as g', 'gi.id_gal = g.id');
		$this->db->where_in('gi.id_gal', $ids);
		$this->db->order_by('gi.id_gal, gi.position');
		$this->db->group_by('gi.id');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $row) {
				if (!isset($data[$row->id_gal])) { $data[$row->id_gal] = array(); }
				if ($limit > 0 && count($data[$row->id_gal]) == $limit) { continue; }

				$tab = explode('.', $row->img);
				$row->img_site_path = './' . config_item('site_path') . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $row->img;
				$row->thumb_site_path = './' . config_item('site_path') . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $tab[0] . '_thumb.' . $tab[1];
				$row->img_path = '/' . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $row->img;
				$row->thumb_path = '/' . config_item('upload_path') . $this->img_dir . $row->dir . '/' . $tab[0] . '_thumb.' . $tab[1];
				if (file_exists($row->img_site_path) && file_exists($row->thumb_site_path)) {
					$data[$row->id_gal][] = $row;
				}
			}
			return (is_numeric($ids) ? $data[$ids] : $data);
		} else {
			return NULL;
		}
	}

	public function get_galleries($num = '', $offset = '') {
		$this->db->where('pub', 1);
		$this->db->where('lang', $_SESSION['lang']);
		$this->db->order_by('position');
		$query = $this->db->get($this->mod_table, $num, $offset);

		if ($query->num_rows() > 0) {
			$data = array();
			$ids = array();
			foreach ($query->result() as $row) { $ids[] = $row->id; }

			$images = $this->get_gallery_images($ids);
			
			foreach ($query->result() as $row) {
				if (!isset($images[$row->id])) { continue; }
				
				$row->images = $images[$row->id];
				
				$vdata = array(
					'id_gal' => $row->id,
					'gallery' => $row,
					'addons' => 1
				);
				$data[] = $this->load->view('frontend/' . config_item('template_f') . 'mod_gallery/single_view', $vdata, true);
			}
			return $data;
		} else
			return FALSE;
	}

	public function addon($id, $perpage = 10, $limit = 4, $type = 'art') {
		if (!in_array($type, array('art', 'news'))) { return FALSE; }

		if ($perpage > 0) {
			if ($type == 'art') {
				$this->db->where('id_art', $id);
			} elseif ($type == 'news') {
				$this->db->where('id_news', $id);
			}
			$this->db->where('pub', 1);
			$this->db->where('lang', $_SESSION['lang']);
			$query = $this->db->get($this->mod_table);
			$numrows = $query->num_rows();

			if ($numrows == 0)
				return FALSE;
			
			$pconfig = array(
					'base_url' => base_url() . config_item('px_articles_pag') . '/' . $id . '/',
					'total_rows' => $numrows,
					'per_page' => $perpage,
					'uri_segment' => 3
			);
			$this->pagination->init($pconfig);

			if (@is_numeric($this->uri->segment(3))) {
				$start = $this->uri->segment(3);
			} else {
				$start = 0;
			}
		}

		$this->db->from($this->mod_table);
		if ($type == 'art') {
			$this->db->where('id_art', $id);
		} else {
			$this->db->where('id_news', $id);
		}
		$this->db->where('pub', 1);
		$this->db->where('lang', $_SESSION['lang']);
		$this->db->order_by('position');
		if ($perpage > 0) { $this->db->limit($perpage, $start); }
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			$ids = array();
			foreach ($query->result() as $row) { $ids[] = $row->id; }

			$images = $this->get_gallery_images($ids);
			foreach ($query->result() as $row) {
				if (!isset($images[$row->id])) { continue; }
				
				$row->images = $images[$row->id];

				$view_data = array(
					'id_gal' => $row->id,
					'gallery' => $row,
					'addons' => 1
				);

				$data[] = $this->load->view('frontend/' . config_item('template_f') . 'mod_gallery/single_view', $view_data, true);
			}
			return $data;
		} else
			return FALSE;
	}

}
