<?

class Estatedev_model extends CI_Model {

	public $mod_table = 'estatedev';

	public function __construct() {

		parent::__construct();
	}

	function get_contact($estate_type = null) {
		$ret = false;
		$this->db->select('tk1.id, tk1.image, tk2.name, tk2.surname, tk2.email, tk2.phone, tk2.office');
		$this->db->from($this->mod_table . '_contacts AS tk1');
		$this->db->join($this->mod_table . '_contacts_content AS tk2', 'tk1.id = tk2.id_edc', 'inner');
		$this->db->join($this->mod_table . '_contacts_join_types AS tk3', 'tk1.id = tk3.id_edc', 'inner');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->where('tk3.id_edt', $estate_type);
		$sql_contacts = $this->db->get();
		if ($sql_contacts->num_rows() > 0) {
			$ret = array();
			foreach ($sql_contacts->result() as $contact) {
				if (!is_file(config_item('base_path') . config_item('site_path') . $contact->image)) {
					$contact->image = false;
				}
				$ret[] = $contact;
			}
		}
		return $ret;
	}

	public function get_single_estate($estate = null) {
		$data = false;
		if (is_numeric($estate)) {
			$this->db->select('tk1.id, tk1.id_edt, tk1.id_edi, tk4.url, tk2.admission, tk2.description, tk3.name AS investment_name, tk3.admission AS investment_admission, tk3.description AS investment_description, tk5.url AS investment_url');
			$this->db->from($this->mod_table . '_estates AS tk1');
			$this->db->join($this->mod_table . '_estates_content AS tk2', 'tk1.id = tk2.id_ede', 'inner');
			$this->db->join($this->mod_table . '_investments_content AS tk3', 'tk1.id_edi = tk3.id_edi', 'left');
			$this->db->join('user_url tk4',"tk4.dest_id=tk1.id AND tk4.type = 'estatedev_estates' AND tk4.lang  = '".$_SESSION['lang']."'",'left');
			$this->db->join('user_url tk5',"tk5.dest_id=tk3.id_edi AND tk5.type = 'estatedev_investments' AND tk5.lang  = '".$_SESSION['lang']."'",'left');
			$this->db->where('tk1.pub', 1);
			$this->db->where('tk1.id', $estate);
			$this->db->where('tk2.lang', $_SESSION['lang']);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				$data = $query->row();
				$data->params = $this->get_estate_parameters($estate);

				$data->name = '';
				if ($data->params) {
					foreach ($data->params as $estate_param) {
						if (isset($estate_param->in_name) && $estate_param->in_name === '1') {
							if (isset($estate_param->type) && $estate_param->type === 'file') {
								
							} elseif (isset($estate_param->type) && $estate_param->type === 'text') {
								// parametr tekstowy (np. cena)
								if (isset($estate_param->text_value)) {
									$data->name .= (($data->name !== '') ? ', ' : '') . (($estate_param->in_name_show === '1') ? $estate_param->name . ' ' : '') . $estate_param->text_value . ' ' . $estate_param->unit;
								}
							} elseif (isset($estate_param->type) && $estate_param->type === 'predefined_parameter') {
								if (isset($estate_param->value)) {
									$data->name .= (($data->name !== '') ? ', ' : '') . (($estate_param->in_name_show === '1') ? $estate_param->name . ' ' : '') . $estate_param->value;
								}
							} else {
								// parametr predefiniowany (np. status mieszkania)
								$data->name .= (($data->name !== '') ? ', ' : '') . (($estate_param->in_name_show === '1') ? $estate_param->name . ' ' : '');
								foreach ($estate_param as $estate_param_value) {
									$data->name .= ' ' . $estate_param_value->value;
								}
							}
						}
					}
				}

				//$data->name = strtolower($data->name);
			}
		}
		return $data;
	}

	public function get_investment_estates_types($investment = null, $limit = null) {
		$data = false;

		$this->db->select('tk1.id, tk2.name, tk2.name_plural, tk4.url, tk1.icon_inactive, tk1.icon_active');
		$this->db->from($this->mod_table . '_types AS tk1');
		$this->db->join($this->mod_table . '_types_content AS tk2', 'tk1.id = tk2.id_edt', 'inner');
		$this->db->join($this->mod_table . '_estates AS tk3', 'tk1.id = tk3.id_edt AND tk3.id_edi = \'' . $investment . '\'', 'inner');
		$this->db->join('user_url tk4',"tk4.dest_id=tk1.id AND tk4.type = 'estatedev_types' AND tk4.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->group_by('tk1.id');
		if (is_numeric($limit)) {
			$this->db->limit($limit);
		}
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			if ($limit === 1) {
				$data = $query->row();
			} else {
				$data = $query->result();
			}
		}
		return $data;
	}
	
	public function get_investment_single_building($investment = null, $building = null) {
		$data = false;

		$this->db->select('tk1.id, tk1.name, tk1.face_image');
		$this->db->from($this->mod_table . '_investments_buildings AS tk1');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk1.id_edi', $investment);
		$this->db->where('tk1.id', $building);
		$this->db->group_by('tk1.id');
		$query_buildings = $this->db->get();

		if ($query_buildings->num_rows() > 0) {
			$data = array();
			foreach ($query_buildings->result() as $building) {
				$this->db->from($this->mod_table . '_investments_face_map AS tk1');
				$this->db->where('tk1.id_edib', $building->id);
				$this->db->order_by('tk1.floor');
				$query_faces = $this->db->get();
				$building->face_maps = false;
				if(!is_file(config_item('base_path').config_item('site_path').$building->face_image)){
					$building->face_image = false;
				}
				if ($query_faces->num_rows() > 0) {
					$building->face_maps = array();
					foreach ($query_faces->result() as $face_map) {
						$face_map->floor_skeches = false;
						$this->db->from($this->mod_table . '_investments_sketches_floors AS tk1');
						$this->db->where('tk1.id_edifm', $face_map->id);
						$query_floor_skeches = $this->db->get();
						if ($query_floor_skeches->num_rows() > 0) {
							$face_map->floor_skeches = array();
							foreach ($query_floor_skeches->result() as $floor_skech) {
								if(!is_file(config_item('base_path').config_item('site_path').$floor_skech->image)){
									$floor_skech->image = false;
								}
								$floor_skech->floor_skeches_maps = false;
								$this->db->from($this->mod_table . '_investments_sketches_floors_maps AS tk1');
								$this->db->where('tk1.id_edisf', $floor_skech->id);
								$query_floor_skeches_maps = $this->db->get();
								if ($query_floor_skeches_maps->num_rows() > 0) {
									$floor_skech->floor_skeches_maps = $query_floor_skeches_maps->result();
								}
								$face_map->floor_skeches[] = $floor_skech;
							}
						}
						$building->face_maps[] = $face_map;
					}
				}
				$data[] = $building;
			}
		}
		return $data;
	}

	public function get_investment_buildings($investment = null) {
		$data = false;

		$this->db->select('tk1.id, tk1.name, tk1.face_image');
		$this->db->from($this->mod_table . '_investments_buildings AS tk1');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk1.id_edi', $investment);
		$this->db->group_by('tk1.id');
		$query_buildings = $this->db->get();

		if ($query_buildings->num_rows() > 0) {
			$data = array();
			foreach ($query_buildings->result() as $building) {
				$data[] = $building;
			}
		}
		return $data;
	}

	public function get_type($type = null, $to_lower = true) {
		$data = false;

		$this->db->select('tk1.id, tk2.name, tk3.url, tk2.name_plural');
		$this->db->from($this->mod_table . '_types AS tk1');
		$this->db->join($this->mod_table . '_types_content AS tk2', 'tk1.id = tk2.id_edt', 'inner');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'estatedev_types' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk1.pub', 1);
		if (is_numeric($type)) {
			$this->db->where('tk1.id', $type);
		}
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->group_by('tk1.id');
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->row();
			if ($to_lower === true) {
				$data->name = strtolower($data->name);
			}
		}
		return $data;
	}

	public function get_all_investments() {
		$data = false;

		$this->db->select('tk1.id, tk2.name, tk3.url, tk2.meta_title, (SELECT image FROM ' . $this->db->dbprefix($this->mod_table . '_investments_img') . ' WHERE id_edi = tk1.id ORDER BY position ASC LIMIT 1) AS image');
		$this->db->from($this->mod_table . '_investments AS tk1');
		$this->db->join($this->mod_table . '_investments_content AS tk2', 'tk1.id = tk2.id_edi', 'inner');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'estatedev_investments' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $investment) {
				$type = $this->get_investment_estates_types($investment->id, 1);

				if ($type) {
					$investment->url_estates = $investment->url . '/' . $type->url . '_' . $type->id;
				} else {
					$investment->url_estates = false;
				}

				if (!empty($investment->meta_title)) {
					$investment->title = $investment->meta_title;
				} else {
					$investment->title = $investment->name;
				}

				$investment->boxname->l1 = '';
				$investment->boxname->l2 = '';
				$investment->boxname->l3 = '';
				$name = explode(' ', $investment->name, 3);
				if (is_array($name)) {
					if (isset($name[0])) {
						$investment->boxname->l1 = $name[0];
					}
					if (isset($name[1])) {
						$investment->boxname->l2 = $name[1];
					}
					if (isset($name[2])) {
						$investment->boxname->l3 = $name[2];
					}
				}

				if (!empty($investment->image)) {
					if (is_file(config_item('base_path') . config_item('site_path') . config_item('upload_path') . 'estatedev/' . $investment->id . '/' . str_replace('.', '_thumb_list.', $investment->image))) {
						$investment->image = config_item('upload_path') . 'estatedev/' . $investment->id . '/' . str_replace('.', '_thumb_list.', $investment->image);
					}
				}
				//$this->db->join($this->mod_table . '_investments_img AS tk3', 'tk1.id = tk3.id_edi', 'left');
				//$this->db->join($this->mod_table . '_investments_img_content AS tk4', 'tk1.id_edi = tk4.id_edi AND tk4.lang = \'' . config_item('main_lang') . '\'', 'left');
				$data[] = $investment;
			}
		}

		return $data;
	}

	public function get_single_investment($investment = null) {
		$data = false;

		$this->db->select('tk1.id, tk2.name, tk3.url, tk2.meta_title, tk2.admission, tk2.description, tk1.map_coords, tk1.address');
		$this->db->from($this->mod_table . '_investments AS tk1');
		$this->db->join($this->mod_table . '_investments_content AS tk2', 'tk1.id = tk2.id_edi', 'inner');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'estatedev_investments' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk1.id', $investment);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->row();

			$data->url_estates = $data->url . '/mieszkania';
			if (!empty($data->meta_title)) {
				$data->title = $data->meta_title;
			} else {
				$data->title = $data->name;
			}
		}

		return $data;
	}

	public function get_investment_images($investment = null) {
		$data = false;

		$this->db->select('tk1.id, CONCAT(tk1.id_edi, \'/\', tk1.image) AS image, tk2.description', false);
		$this->db->from($this->mod_table . '_investments_img AS tk1');
		$this->db->join($this->mod_table . '_investments_img_content AS tk2', 'tk1.id = tk2.id_edii AND tk2.lang = \'' . $_SESSION['lang'] . '\'', 'left');
		$this->db->where('tk1.id_edi', $investment);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $image) {
				if (!is_file(config_item('base_path') . config_item('site_path') . config_item('upload_path') . $image->image)) {
					$image->image = config_item('upload_path') . 'estatedev/' . $image->image;
					$data[] = $image;
				}
			}
		}

		return $data;
	}

	public function get_values_estate_floor() {
		$data = false;

		$this->db->select('tk1.id, tk1.floor AS value, tk1.floor AS label');
		$this->db->from($this->mod_table . '_investments_face_map AS tk1');
		$this->db->order_by('tk1.floor');
		$this->db->where('tk1.floor !=', '');
		$this->db->group_by('value');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $floor) {
				$data[] = $floor;
			}
		}
		return $data;
	}

	public function get_values_investment_name() {
		$data = false;

		$this->db->select('tk1.id, tk1.id AS value, tk2.name AS label');
		$this->db->from($this->mod_table . '_investments AS tk1');
		$this->db->join($this->mod_table . '_investments_content AS tk2', 'tk1.id = tk2.id_edi', 'inner');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $investment) {
				$name = explode(' ', $investment->label, 2);
				$investment->label = '<span>' . $name[0] . '</span><span>' . $name[1] . '</span>';
				$data[] = $investment;
			}
		}
		return $data;
	}

	public function get_values_building() {
		$data = false;

		$this->db->select('tk1.id, tk1.id AS value, tk1.name AS label');
		$this->db->from($this->mod_table . '_investments_buildings AS tk1');
		$this->db->where('tk1.pub', 1);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $investment) {
				$name = explode(' ', $investment->label, 2);
				$data[] = $investment;
			}
		}
		return $data;
	}

	public function filter_values_investment_name($param = null, $db, $i) {
		if (is_array($param)) {
			$db->where_in('tk1.id_edi', $param);
		}
		return true;
	}

	public function filter_values_building($param = null, $db, $i) {
		if (is_array($param)) {
			$db->where_in('tk1.id_edib', $param);
		}
		return true;
	}

	public function filter_values_estate_floor($param = null, $db, $i) {
		$db->join($this->mod_table . '_investments_sketches_floors_maps AS tks' . $i . 'a', 'tk1.id = tks' . $i . 'a.id_ede', 'inner');
		$db->join($this->mod_table . '_investments_sketches_floors AS tks' . $i . 'b', 'tks' . $i . 'a.id_edisf = tks' . $i . 'b.id', 'inner');
		$db->join($this->mod_table . '_investments_face_map AS tks' . $i . 'c', 'tks' . $i . 'c.id = tks' . $i . 'b.id_edifm', 'inner');
		$db->where_in('tks' . $i . 'c.floor', $param);
		return true;
	}

	public function view_values_investment_name($estate) {
		$data = false;

		$this->db->select('tk2.name');
		$this->db->from($this->mod_table . '_investments AS tk1');
		$this->db->join($this->mod_table . '_investments_content AS tk2', 'tk1.id = tk2.id_edi', 'inner');
		$this->db->join($this->mod_table . '_estates AS tk3', 'tk1.id = tk3.id_edi AND tk3.id = \'' . $estate . '\'', 'inner');
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->row()->name;
		}
		return $data;
	}

	public function view_values_estate_floor($estate) {
		$data = false;

		$this->db->select('tk3.floor');
		$this->db->from($this->mod_table . '_investments_sketches_floors_maps AS tk1');
		$this->db->join($this->mod_table . '_investments_sketches_floors AS tk2', 'tk1.id_edisf = tk2.id', 'inner');
		$this->db->join($this->mod_table . '_investments_face_map AS tk3', 'tk3.id = tk2.id_edifm', 'inner');
		$this->db->where('tk1.id_ede', $estate);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->row()->floor;
		}
		return $data;
	}

	public function view_values_building($estate) {
		$data = false;

		$this->db->select('tk1.name');
		$this->db->from($this->mod_table . '_investments_buildings AS tk1');
		$this->db->join($this->mod_table . '_estates AS tk2', 'tk1.id = tk2.id_edib AND tk2.id = \'' . $estate . '\'', 'inner');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->row()->name;
		}
		return $data;
	}

	public function get_search_parameters() {
		$data = false;

		$this->db->select('tk1.id, tk1.type, tk1.unit, tk1.icon, tk1.predefined_parameter, tk3.url, tk2.name');
		$this->db->from($this->mod_table . '_parameters AS tk1');
		$this->db->join($this->mod_table . '_parameters_content AS tk2', 'tk1.id = tk2.id_edp', 'inner');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'estatedev_parameters' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk1.searchable', 1);
		//$this->db->where('tk1.home_search', 1);
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $param) {
				if ($param->type === 'text') {
					$this->db->select('MAX(tk1.text_value * 1) + 1 AS max_val, MIN(tk1.text_value * 1) - 1 AS min_val');
					$this->db->from($this->mod_table . '_parameters_join_estates AS tk1');
					$this->db->where('tk1.id_edp', $param->id);
					$this->db->group_by('tk1.id_edp');
					$query_pv = $this->db->get();
					if ($query_pv->num_rows() > 0) {
						$v = $query_pv->row();
						$param->max = $v->max_val;
						$param->min = $v->min_val;
					} else {
						$param->max = false;
						$param->min = false;
					}
				} elseif ($param->type === 'predefined_parameter') {
					if (method_exists($this, 'get_values_' . $param->predefined_parameter)) {
						$param->values = $pv = $this->{'get_values_' . $param->predefined_parameter}();
					}
				} else {
					$this->db->select('DISTINCT tk1.id, tk2.value, tk2.value AS label', false);
					$this->db->from($this->mod_table . '_parameters_values AS tk1');
					$this->db->join($this->mod_table . '_parameters_values_content AS tk2', 'tk1.id = tk2.id_edepv', 'inner');
					$this->db->where('tk1.id_edep', $param->id);
					$this->db->where('tk2.lang', $_SESSION['lang']);
					$this->db->where('tk2.value !=', '');
					$this->db->order_by('tk1.position');
					$query_pv = $this->db->get();
					if ($query_pv->num_rows() > 0) {
						$param->values = $query_pv->result();
					} else {
						$param->values = false;
					}
				}
				if (!is_file(config_item('base_path') . config_item('site_path') . $param->icon)) {
					$param->icon = false;
				}
				$data[$param->id] = $param;
			}
		}

		return $data;
	}

	public function get_search_link($estate_type = null) {
		$this->db->select('tk1.id, tk2.url');
		$this->db->from($this->mod_table . '_types AS tk1');
		$this->db->join('user_url tk2',"tk2.dest_id=tk1.id AND tk2.type = 'estatedev_types' AND tk2.lang  = '".$_SESSION['lang']."'",'left');
		if (is_null($estate_type)) {
			$this->db->where('tk1.id', 1);
		} elseif (is_numeric($estate_type)) {
			$this->db->where('tk1.id', $estate_type);
		}
		//$this->db->where('tk1.home_search', 1);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return '/' . $query->row()->url;
		} else {
			return false;
		}
	}

	public function get_table_parameters() {
		$ret = false;
		$this->db->select('tk1.id, tk1.position, tk1.type, tk1.icon, tk2.name, tk3.url');
		$this->db->from($this->mod_table . '_parameters AS tk1');
		$this->db->join($this->mod_table . '_parameters_content AS tk2', 'tk1.id = tk2.id_edp', 'inner');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'estatedev_parameters' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->where('tk1.in_table', 1);
		$this->db->order_by('tk1.position');
		$sql_params = $this->db->get();
		if ($sql_params->num_rows() > 0) {
			$ret = array();
			foreach ($sql_params->result() as $param) {
				$ret[] = $param;
			}
		}
		return $ret;
	}

	public function get_single_parameter($id) {
		$ret = false;
		$this->db->select('tk1.id, tk1.position, tk1.type, tk1.icon, tk2.name, tk3.url');
		$this->db->from($this->mod_table . '_parameters AS tk1');
		$this->db->join($this->mod_table . '_parameters_content AS tk2', 'tk1.id = tk2.id_edp', 'inner');
		$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'estatedev_parameters' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->where('tk1.id', $id);
		$sql_params = $this->db->get();
		if ($sql_params->num_rows() > 0) {
			$ret = $sql_params->row();
		}
		return $ret;
	}

	public function get_estate_parameters($estate = null, $in_table = null) {
		$ret = false;
		if (is_numeric($estate)) {
			// parametry ogólne
			$this->db->select('tk1.id, tk1.type, tk1.icon, tk1.icon_button, tk1.predefined_parameter, tk1.presentation, tk1.in_name, tk1.in_name_show, tk2.name, uu.url, tk1.unit, tk3.text_value, tk5.value, tk1.position');
			$this->db->from($this->mod_table . '_parameters AS tk1');
			$this->db->join($this->mod_table . '_parameters_content AS tk2', 'tk1.id = tk2.id_edp', 'inner');
			$this->db->join($this->mod_table . '_parameters_join_estates AS tk3', 'tk1.id = tk3.id_edp', 'inner');
			$this->db->join($this->mod_table . '_parameters_values AS tk4', 'tk1.id = tk4.id_edep AND tk4.id = tk3.id_edepv', 'left');
			$this->db->join($this->mod_table . '_parameters_values_content AS tk5', 'tk4.id = tk5.id_edepv AND tk5.lang = \'' . $_SESSION['lang'] . '\'', 'left');
			$this->db->join('user_url uu',"uu.dest_id=tk1.id AND uu.type = 'estatedev_parameters' AND uu.lang  = '".$_SESSION['lang']."'",'left');
			$this->db->where('tk3.id_ede', $estate);
			$this->db->where('tk2.lang', $_SESSION['lang']);
			if (is_numeric($in_table)) {
				$this->db->where('tk1.in_table', 1);
			}
			$this->db->order_by('tk1.position');
			$sql_params = $this->db->get();
			if ($sql_params->num_rows() > 0) {
				$ret = array();
				foreach ($sql_params->result() as $param) {
					if (!is_file(config_item('base_path') . config_item('site_path') . $param->icon)) {
						$param->icon = false;
					}
					if (!is_file(config_item('base_path') . config_item('site_path') . $param->icon_button)) {
						$param->icon_button = false;
					}
					if ($param->type === 'text') {
						$ret[$param->position] = $param;
					} else if ($param->type === 'file') {
						if (!is_file(config_item('base_path') . config_item('site_path') . $param->text_value)) {
							$param->text_value = false;
						}
						$ret[$param->position] = $param;
					} else if ($param->type === 'predefined_parameter') {

						$ret[$param->position] = $param;
					} else {
						$ret[$param->position][] = $param;
					}
				}
			}

			// parametry predefiniowane
			$this->db->select('tk1.id, tk1.type, tk1.icon, tk1.icon_button, tk1.predefined_parameter, tk1.presentation, tk1.in_name, tk1.in_name_show, tk3.url, tk2.name, tk1.position');
			$this->db->from($this->mod_table . '_parameters AS tk1');
			$this->db->join($this->mod_table . '_parameters_content AS tk2', 'tk1.id = tk2.id_edp', 'inner');
			$this->db->join('user_url tk3',"tk3.dest_id=tk1.id AND tk3.type = 'estatedev_parameters' AND tk3.lang  = '".$_SESSION['lang']."'",'left');
			$this->db->where('tk2.lang', $_SESSION['lang']);
			$this->db->where('tk1.in_table', 1);
			$this->db->order_by('tk1.position');
			$this->db->where('tk1.type', 'predefined_parameter');
			$sql_params = $this->db->get();
			if ($sql_params->num_rows() > 0) {
				foreach ($sql_params->result() as $param) {
					if (!is_file(config_item('base_path') . config_item('site_path') . $param->icon)) {
						$param->icon = false;
					}
					if (!is_file(config_item('base_path') . config_item('site_path') . $param->icon_button)) {
						$param->icon_button = false;
					}
					if (method_exists($this, 'view_values_' . $param->predefined_parameter)) {
						$param->value = $this->{'view_values_' . $param->predefined_parameter}($estate);
					} else {
						$param->value = '&nbsp;';
					}
					$ret[$param->position] = $param;
				}
			}
		}
		return $ret;
	}

	public function get_estates($params = false, $estate_type = null) {
		$ret = false;

		$check_params = array();
		$this->db->select('tk1.id, tk2.url, tk1.type, tk1.predefined_parameter');
		$this->db->from($this->mod_table . '_parameters AS tk1');
		$this->db->join('user_url tk2',"tk2.dest_id=tk1.id AND tk2.type = 'estatedev_parameters' AND tk2.lang  = '".$_SESSION['lang']."'",'left');
		//$this->db->where('tk1.searchable', 1);
		$sql_params = $this->db->get();
		if ($sql_params->num_rows() > 0) {
			foreach ($sql_params->result() as $p) {
				$check_params[$p->url] = $p;
			}
		}


		// podstawowa część zapytania
		$this->db->select('tk1.id, tk5.url, tk2.admission, tk4.id_edifm AS in_building_floor, tk3.id AS in_floor_position');
		$this->db->from($this->mod_table . '_estates AS tk1');
		$this->db->join($this->mod_table . '_estates_content AS tk2', 'tk1.id = tk2.id_ede', 'inner');
		$this->db->join($this->mod_table . '_investments_sketches_floors_maps AS tk3', 'tk1.id = tk3.id_ede', 'left');
		$this->db->join($this->mod_table . '_investments_sketches_floors AS tk4', 'tk3.id_edisf = tk4.id', 'left');
		$this->db->join('user_url tk5',"tk5.dest_id=tk1.id AND tk5.type = 'estatedev_estates' AND tk5.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where('tk2.lang', $_SESSION['lang']);
		$this->db->where('tk1.pub', 1);
		$this->db->group_by('tk1.id');

		// rozwinięcie zapytania o parametry
		if (is_array($params) && count($params) > 0 && is_array($check_params)) {
			$i = 0;
			foreach ($params as $param_key => $param) {
				if (isset($check_params[$param_key])) {
					if ($check_params[$param_key]->type !== 'predefined_parameter') {
						// dodanie do zapytania parametrów zwykłych
						if (isset($param['min']) || isset($param['max'])) {
							if (isset($param['min']) && !empty($param['min']) && isset($param['max']) && !empty($param['max'])) {
								// podany jest pełny przedział
								$this->db->join($this->mod_table . '_parameters_join_estates AS tks' . $i . '', 'tk1.id = tks' . $i . '.id_ede AND (tks' . $i . '.text_value * 1 BETWEEN ' . (int) $param['min'] . ' AND ' . (int) $param['max'] . ')', 'inner');
							} elseif (isset($param['min']) && !empty($param['min']) && (!isset($param['max']) || empty($param['max']))) {
								// podana jest tylko wartość minimalna
								$this->db->join($this->mod_table . '_parameters_join_estates AS tks' . $i . '', 'tk1.id = tks' . $i . '.id_ede AND tks' . $i . '.text_value * 1 >= ' . (int) $param['min'] . '', 'inner');
							} elseif (isset($param['max']) && !empty($param['max']) && (!isset($param['min']) || empty($param['min']))) {
								// podana jest tylko wartość maksymalna
								$this->db->join($this->mod_table . '_parameters_join_estates AS tks' . $i . '', 'tk1.id = tks' . $i . '.id_ede AND tks' . $i . '.text_value * 1 <= ' . (int) $param['max'] . '', 'inner');
							}
						} else {
							if (is_array($param)) {
								$param_statement = '';
								foreach ($param as $v) {
									$param_statement .= ((!empty($param_statement)) ? ' OR' : '') . ' tks' . $i . 'b.value = ' . $v;
								}
								if (!empty($param_statement)) {
									$this->db->join($this->mod_table . '_parameters_join_estates AS tks' . $i . 'a', 'tk1.id = tks' . $i . 'a.id_ede', 'inner');
									$this->db->join($this->mod_table . '_parameters_values_content AS tks' . $i . 'b', 'tks' . $i . 'a.id_edepv = tks' . $i . 'b.id_edepv AND (' . $param_statement . ')', 'inner');
								}
							}
						}
					} else {

						// dodanie do zapytania parametrów predefiniowanych
						if (method_exists($this, 'filter_values_' . $check_params[$param_key]->predefined_parameter)) {
							$this->{'filter_values_' . $check_params[$param_key]->predefined_parameter}($param, $this->db, $i);
						}
					}
				}
				$i++;
			}
		}

		// dodanie do zapytania typu nieruchomości
		if (is_numeric($estate_type)) {
			$this->db->where('tk1.id_edt', $estate_type);
		}

		// wykonanie zapytania
		$query = $this->db->get();

		//var_dump($this->db->last_query());

		if ($query->num_rows() > 0) {
			$ret = array();
			foreach ($query->result() as $row) {
				$row->params = $this->get_estate_parameters($row->id, 1);
				$ret[] = $row;
			}
		} else {
			return false;
		}
		return $ret;
	}

}
