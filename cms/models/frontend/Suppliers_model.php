<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers_model extends CI_Model {
	public $mod_table = 'suppliers';
	public $img_dir = 'suppliers';

	public function __construct() {
		parent::__construct();
	}

	public function get_suppliers($num = FALSE, $offset = FALSE) {
		$this->db->select('nc.*, n.*');
		$this->db->from($this->mod_table.' n');
		$this->db->join($this->mod_table.'_content as nc', 'n.id = nc.id_supplier');
		// $this->db->join('user_url uu', "uu.dest_id=n.id AND uu.type = 'supplier' AND uu.lang  = '".$_SESSION['lang']."'", 'left');
		$this->db->where('n.pub', 1);
		// $this->db->where('nc.pub', 1);
		$this->db->where('nc.lang', $_SESSION['lang']);
		$this->db->order_by('n.name', 'asc');
		$this->db->order_by('n.id', 'asc');
		if($num !== FALSE && $offset !== FALSE) {
			$this->db->limit($num, $offset);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result();
		}
		return FALSE;
	}
}
