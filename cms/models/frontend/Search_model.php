<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_model extends CI_Model {
	private $count_art = 0;
	private $count_news = 0;

	public function __construct(){

		parent::__construct();
	}

	public function check() {
		$this->form_validation->set_rules('query', lang('Szukaj'), 'trim|required|min_length[3]|strip_tags');

		return $this->form_validation->run();
	}
	
	public function search($string, $num = null, $offset = null) {

		$data = array();

		if($num !== null && $offset !== null) {
			$limit = new stdClass();
			$count_all = $this->count_art + $this->count_news;
			if ($count_all == 0) {
				return;
			}

//echo "art: {$this->count_art}, news: {$this->count_news}, prod: {$this->count_prod}, all: {$count_all}<br>";
			//limit dla poszczególnego typu wyników jest zależny
			//od stosunku znalezionych wyników tego typu, do całej liczby wyników
			if(!isset($limit->art)) $limit->art = new stdClass();
			$limit->art->num = ceil($num * ($this->count_art / $count_all));
			$limit->art->offset = ceil($offset * ($this->count_art / $count_all));
//echo "art_num: ".($num * ($this->count_art / $count_all)).", offset: ".($offset * ($this->count_art / $count_all));

			if(!isset($limit->news)) $limit->news = new stdClass();
			$limit->news->num = round($num * ($this->count_news / $count_all));
			$limit->news->offset = round($offset * ($this->count_news / $count_all));
//echo "<br>news_num: ".($num * ($this->count_news / $count_all)).", offset: ".($offset * ($this->count_news / $count_all));
		}

		//podstrony
		$this->db->distinct();
		$this->db->select('ac.id_art as id, ac.title as name, ac.content, uu.url as user_url, ac.url');
		$this->db->from('articles_content as ac');
		$this->db->join('articles a',"ac.id_art=a.id");
		$this->db->join('user_url uu',"uu.dest_id=ac.id_art AND uu.type='articles' AND uu.lang= '".$_SESSION['lang']."'",'left');
		$this->db->where("ac.`url` = '' AND ac.`pub` = 1 AND a.`pub` = 1 AND (ac.title LIKE '%".$this->db->escape_like_str($string)."%' OR ac.content LIKE '%".$this->db->escape_like_str($string)."%') AND ac.lang = '".$_SESSION['lang']."'", NULL, FALSE);
		$this->db->group_by('ac.id_art');
		if($num !== null && $offset !== null)
			$this->db->limit($limit->art->num, $limit->art->offset);
		$query = $this->db->get();

		if($query->num_rows()>0) {
			$i = 0;
			foreach($query->result() as $row) {
				$row->type = 'article';
				$row->url = article_url($row);
				$data[] = $row;
				$i++;
			}
			if(empty($this->count_art)) $this->count_art = $i;
		}

		//aktualności
		$this->db->distinct();
		$this->db->select('nc.id_news as id, nc.title as name, nc.content, uu.url as user_url, nc.url');
		$this->db->from('news_content as nc');
		$this->db->join('news n',"nc.id_news=n.id");
		$this->db->join('user_url uu',"uu.dest_id=nc.id_news AND uu.type = 'news' AND uu.lang  = '".$_SESSION['lang']."'",'left');
		$this->db->where("nc.`url` = '' AND nc.`pub` = 1 AND n.`pub` = 1 AND (nc.title LIKE '%".$this->db->escape_like_str($string)."%' OR nc.content LIKE '%".$this->db->escape_like_str($string)."%') AND nc.lang = '".$_SESSION['lang']."'", NULL, FALSE);
		$this->db->group_by('nc.id_news');
		if($num !== null && $offset !== null)
			$this->db->limit($limit->news->num, $limit->news->offset);
		$query = $this->db->get();

		if($query->num_rows()>0) {
			$i = 0;
			foreach($query->result() as $row) {
				$row->type = 'news';
				$row->url = news_url($row);
				$data[] = $row;
				$i++;
			}

			if(empty($this->count_news)) $this->count_news = $i;
		}

		return $data;
	}
}