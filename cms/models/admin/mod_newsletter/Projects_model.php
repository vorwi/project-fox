<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects_model extends MY_Model {

	public function __construct(){

		parent::__construct('newsletter');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	public function getProjects($lang = null) {
	
		if ($lang!=null) $this->db->where('lang', $lang);
	
		$this->db->order_by('title');
		$query = $this->db->get('newsletter_projects');
	
		return $query->num_rows()>0 ? $query->result() : FALSE;
	}
	
	public function delProjects($id) {
			
		if(!is_numeric($id)) $post = $this->input->post('check');
		else $post = $id;
	
		if(empty($post)) return FALSE;
	
		$this->db->where_in('id', $post);
		$query = $this->db->delete('newsletter_projects');
	
		return TRUE;
	}
	
	public function checkProjectAdd() {
	
		$this->form_validation->set_rules('title', 'Tytuł','required');
		$this->form_validation->set_rules('lang');
	
		return $this->form_validation->run();
	}
	
	public function addProject() {
		 
		$data = array(
				'lang'=>$this->input->post('lang'),
				'title'=>htmlspecialchars($this->input->post('title'))
		);
		 
		$this->db->insert('newsletter_projects', $data);
		 
		return $this->db->affected_rows()==1 ? $this->db->insert_id() : FALSE;
		 
	}
	
	public function projectExists($id){
		 
		$this->db->where('id',$id);
		$query = $this->db->get('newsletter_projects');
		return $query->num_rows()>0;
	}
	
	public function editProject($id) {
		 
		$this->db->where('id',$id);
		$query = $this->db->get('newsletter_projects');
		 
		return $query->num_rows()>0 ? $query->row() : FALSE;
		 
	}
	
	public function saveProject($id) {
		 
		$mail_b =		 stripslashes($this->input->post('mail'));
		 
		$data = array(
				'lang'=>$this->input->post('lang'),
				'title'=>htmlspecialchars($this->input->post('title')),
				'text'=>$mail_b,
				'content'=>$this->input->post('newsletter'),
		);
		$this->db->where('id',$id);
		$this->db->update('newsletter_projects', $data);
	
	
		return TRUE;
		 
	}

}
