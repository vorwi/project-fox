<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups_model extends MY_Model {

	public function __construct(){

		parent::__construct('newsletter');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	public function checkGroupAdd(){
	
		$this->form_validation->set_rules('name', 'Nazwa', 'required');
	
		return $this->form_validation->run();
	}
	
	public function addGroup() {
	
		$data = array(
				'name' => $this->input->post('name')
		);
	
		$this->db->insert($this->mod_table.'_groups', $data);
	
		return $this->db->affected_rows()==1;
	}
	
	public function delGroups($id=null){
	
		if($id==null) $id = $this->input->post('check');
	
		if(empty($id)) return FALSE;
	
		$this->db->where_in('id',$id);
	
		return $this->db->delete($this->mod_table.'_groups');
	}
	
	public function getGroups($start=null, $limit=null, $with_quantities=0) {
	
		$data = array();
	
		$this->db->order_by('name');
		if($start!==null && $limit!==null) $this->db->limit($limit,$start);
		$query = $this->db->get($this->mod_table.'_groups');
		//echo $this->db->last_query();
		if($query->num_rows()>0){
	
			foreach ($query->result() as $row) {
				 
				if($with_quantities==1) {
					$this->db->where('id_group',$row->id);
					$query2 = $this->db->get($this->mod_table.'_emails_to_groups');
					$row->num_members = $query2->num_rows();
				}
				 
				$data[] = $row;
			}
			return $data;
		}
		else return FALSE;
	
	}
	
	public function saveGroups() {
	
		$data = array();
		$chenged = array();
	
		$post = $this->input->post('check');
	
		foreach($post as $row) {
	
			$data = array(
					'name'=>$this->input->post('name_'.$row),
			);
	
			$this->db->where('id', $row);
			$query = $this->db->update($this->mod_table.'_groups', $data);
			 
			$changed[] = array('name'=>$this->input->post('name_'.$row));
		}
	
		return $changed;
	}
	
	public function editGroups($id) {
	
		if(!is_numeric($id)) $post = $this->input->post('check');
		else $post = $id;
	
		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table.'_groups');
	
		return $query->num_rows()>0 ? $query->result() : FALSE;
	}

}
