<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails_model extends MY_Model {

	public function __construct(){

		parent::__construct('newsletter');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function getEmails($start=null, $limit=null, $confirm=true, $groups=false) {

		$data = array();

		if($confirm===TRUE) $this->db->where('confirm',1);
		$this->db->order_by('email');
		if($start!==null && $limit!==null) $this->db->limit($limit,$start);
		$query = $this->db->get($this->mod_table.'_emails');
		
		if($query->num_rows()>0){

			foreach($query->result() as $row) {
				
				if($groups!==false){
					$this->db->where_in('id_group',$groups);
					$this->db->where('id_email',$row->id);
					$query2 = $this->db->get('newsletter_emails_to_groups');
				
					if($query2->num_rows()==0) continue;
				}
				 
				$this->db->select('g.id');
				$this->db->from('newsletter_groups g');
				$this->db->join('newsletter_emails_to_groups etg','etg.id_group=g.id');
				$this->db->where('etg.id_email',$row->id);
				$query2 = $this->db->get();
				if(count($query2->num_rows()>0)){
					$row->groups = array();
					foreach($query2->result() as $row2) {
						$row->groups[] = $row2->id;
					}
				}
				
				$data[] = $row;
			}

			return $data;
		}
		else return FALSE;

	}

	public function checkFormAdd(){

		$this->form_validation->set_rules('add', 'Adresy', 'required');

		return $this->form_validation->run();
	}

	public function addEmails(){

		$ok 	= array();
		$error 	= array();
		$min 	= 6;
		$max 	= 12;

		$emails = $this->input->post('add');
		$groups = $this->input->post('id_groups');

		if(empty($emails)) return FALSE;

		$tab = preg_split("#[;,\r\n ]+#", $emails);

		foreach($tab as $row){

			$email = trim(strtolower($row));

			if($email!=''){

				$pass = '';
				for($i=0;$i<rand($min,$max);$i++)
				{
					$char=chr(rand(48,122));
					if(preg_match("/[^0-9A-Za-z]/",$char)) $pass .= $char;
					else $i--;
				}

				$pass = $pass.time();
				$hash = md5($pass);

				$this->db->where('email',$email);
				$query = $this->db->get($this->mod_table.'_emails');

				if($query->num_rows()==0 and valid_email($email)){

					$data = array(
						'email'		=>$email,
						'hash'		=>$hash,
						'confirm'	=>1,
					);

					$this->db->set('date_add', 'NOW()', FALSE);
					$query = $this->db->insert($this->mod_table.'_emails', $data);

					if($query) {
    					
    					$email_id = $this->db->insert_id();
    					if(isset($groups)) {
    						foreach($groups as $row) {
    							$this->db->insert($this->mod_table.'_emails_to_groups', array('id_email'=>$email_id,'id_group'=>$row));
    						}
    					}
    					
    					$ok[] = $email;
    				}
					else $error[] = $email;

				}
				else $error[] = $email;

			}
		}

		$msg = array($ok, 0);
	  	$this->session->set_flashdata('ok', $msg);

	  	$msg = array($error, 2);
	  	$this->session->set_flashdata('error', $msg);

	  	return empty($error);
	}

	public function delEmails($id=null){

		if($id==null) $id = $this->input->post('check');

		if(empty($id)) return FALSE;

		$this->db->where_in('id',$id);

		return $this->db->delete($this->mod_table.'_emails');
	}

	public function confirmEmails($id=null){

		if($id==null) $id = $this->input->post('check');

		if(empty($id)) return FALSE;

		$this->db->where_in('id',$id);

		return $this->db->update($this->mod_table.'_emails', array('confirm'=>1));
	}

	
	public function getGroups($start=null, $limit=null, $with_quantities=0) {
	
		$data = array();
	
		$this->db->order_by('name');
		if($start!==null && $limit!==null) $this->db->limit($limit,$start);
		$query = $this->db->get($this->mod_table.'_groups');
		//echo $this->db->last_query();
		if($query->num_rows()>0){
	
			foreach ($query->result() as $row) {
				 
				if($with_quantities==1) {
					$this->db->where('id_group',$row->id);
					$query2 = $this->db->get($this->mod_table.'_emails_to_groups');
					$row->num_members = $query2->num_rows();
				}
				 
				$data[] = $row;
			}
			return $data;
		}
		else return FALSE;
	
	}
	
	public function assignGroups() {
	
		$data = array();
		 
		$post = $this->input->post('id_group');
	
		if($post) {
			foreach($post as $email => $groups){
	
				$this->db->delete('newsletter_emails_to_groups',array('id_email'=>$email));
				foreach($groups as $group) {
					$data[] = array('id_email'=>$email,'id_group'=>$group);
				}
			}
		}
		 
	
		if(count($data)>0) $this->db->insert_batch('newsletter_emails_to_groups', $data);
		 
		return true;
	
	}

}
