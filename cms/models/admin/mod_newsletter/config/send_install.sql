-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 21 Lip 2016, 10:00
-- Wersja serwera: 5.6.20-log
-- Wersja PHP: 5.5.15

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Baza danych: `neocms`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_newsletter`
--

DROP TABLE IF EXISTS `cms_newsletter`;
CREATE TABLE `cms_newsletter` (
  `id` int(11) NOT NULL,
  `lang` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(1000) COLLATE utf8_polish_ci NOT NULL,
  `link` varchar(500) COLLATE utf8_polish_ci NOT NULL,
  `text` longtext COLLATE utf8_polish_ci NOT NULL,
  `content` longtext COLLATE utf8_polish_ci NOT NULL,
  `sent` int(1) NOT NULL,
  `date_sent` datetime NOT NULL,
  `sent_ok` int(11) NOT NULL,
  `errors` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_newsletter_emails`
--

DROP TABLE IF EXISTS `cms_newsletter_emails`;
CREATE TABLE `cms_newsletter_emails` (
  `id` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `email` varchar(500) COLLATE utf8_polish_ci NOT NULL,
  `hash` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `confirm` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_newsletter_emails_to_groups`
--

DROP TABLE IF EXISTS `cms_newsletter_emails_to_groups`;
CREATE TABLE `cms_newsletter_emails_to_groups` (
  `id_email` int(11) NOT NULL,
  `id_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_newsletter_groups`
--

DROP TABLE IF EXISTS `cms_newsletter_groups`;
CREATE TABLE `cms_newsletter_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_newsletter_projects`
--

DROP TABLE IF EXISTS `cms_newsletter_projects`;
CREATE TABLE `cms_newsletter_projects` (
  `id` int(11) NOT NULL,
  `lang` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `title` varchar(1000) COLLATE utf8_polish_ci NOT NULL,
  `text` longtext COLLATE utf8_polish_ci NOT NULL,
  `content` longtext COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `cms_newsletter_projects`
--

INSERT INTO `cms_newsletter_projects` (`id`, `lang`, `title`, `text`, `content`) VALUES
(1, 'pl', 'Projekt 1', '<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0; border: 0 none; border-collapse: collapse;"><tbody><tr><td id="content_td"><table border="1" cellpadding="5" cellspacing="0" class="nl1" style="width: 100%; border-collapse: collapse; border: 1px solid rgb(204, 204, 204);">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #CCC;"><img alt="" src="/userfiles/newsletter/baner.jpg" style="width: 200px; height: 47px;" /></td>\r\n			<td style="border: 1px solid #CCC; vertical-align: top;">\r\n			<h3 style="color: #022738;">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velitttt</h3>\r\n\r\n			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis faucibus pharetra. Praesent in malesuada massa. Suspendisse eget semper nibh. Etiam eget pellentesque purus. Etiam pharetra nisl at lacus lacinia fringilla. Phasellus sed volutpat lectus. Phasellus rhoncus justo ac porttitor ullamcorper. Etiam lacinia mattis feugiat. Nullam in tortor mauris. Curabitur consequat neque sed felis auctor aliquam. Donec vitae sapien nunc. Aliquam erat volutpat. Aenean ullamcorper ex libero, eu iaculis risus tempor eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sit amet consequat nulla, ut vulputate urna.</p>\r\n\r\n			<p style="text-align: right;"><a href="http://www.artneo.pl/" style="color: #6E9CAC;">zobacz więcej</a></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p> </p>\r\n\r\n<table align="center" border="1" cellpadding="5" cellspacing="0" class="nl1" style="border-collapse: collapse; border: 1px solid rgb(204, 204, 204); width: 600px;">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid rgb(204, 204, 204); text-align: center;"><img alt="" src="/userfiles/newsletter/baner2.jpg" style="width: 200px; height: 47px;" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #CCC; vertical-align: top;">\r\n			<h3 style="color: #022738;">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velitttt</h3>\r\n\r\n			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis faucibus pharetra. Praesent in malesuada massa. Suspendisse eget semper nibh. Etiam eget pellentesque purus. Etiam pharetra nisl at lacus lacinia fringilla. Phasellus sed volutpat lectus. Phasellus rhoncus justo ac porttitor ullamcorper. Etiam lacinia mattis feugiat. Nullam in tortor mauris. Curabitur consequat neque sed felis auctor aliquam. Donec vitae sapien nunc. Aliquam erat volutpat. Aenean ullamcorper ex libero, eu iaculis risus tempor eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sit amet consequat nulla, ut vulputate urna.</p>\r\n\r\n			<p> </p>\r\n\r\n			<p style="text-align: right;"><a href="http://www.artneo.pl/" style="color: #6E9CAC;">zobacz więcej</a></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</td></tr></tbody></table><p style="text-align: center;"><span style="font-size: 15px;">Zapraszamy do zapoznania się z najnowszym Newsletterem {%numer%} pod adresem:<br />{%link%}</span></p><p></p>', '<table border="1" cellpadding="5" cellspacing="0" class="nl1" style="width: 100%; border-collapse: collapse; border: 1px solid rgb(204, 204, 204);">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #CCC;"><img alt="" src="/userfiles/newsletter/baner.jpg" style="width: 200px; height: 47px;" /></td>\r\n			<td style="border: 1px solid #CCC; vertical-align: top;">\r\n			<h3 style="color: #022738;">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velitttt</h3>\r\n\r\n			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis faucibus pharetra. Praesent in malesuada massa. Suspendisse eget semper nibh. Etiam eget pellentesque purus. Etiam pharetra nisl at lacus lacinia fringilla. Phasellus sed volutpat lectus. Phasellus rhoncus justo ac porttitor ullamcorper. Etiam lacinia mattis feugiat. Nullam in tortor mauris. Curabitur consequat neque sed felis auctor aliquam. Donec vitae sapien nunc. Aliquam erat volutpat. Aenean ullamcorper ex libero, eu iaculis risus tempor eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sit amet consequat nulla, ut vulputate urna.</p>\r\n\r\n			<p style="text-align: right;"><a href="http://www.artneo.pl/" style="color: #6E9CAC;">zobacz więcej</a></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<table align="center" border="1" cellpadding="5" cellspacing="0" class="nl1" style="border-collapse: collapse; border: 1px solid rgb(204, 204, 204); width: 600px;">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid rgb(204, 204, 204); text-align: center;"><img alt="" src="/userfiles/newsletter/baner2.jpg" style="width: 200px; height: 47px;" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #CCC; vertical-align: top;">\r\n			<h3 style="color: #022738;">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velitttt</h3>\r\n\r\n			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis faucibus pharetra. Praesent in malesuada massa. Suspendisse eget semper nibh. Etiam eget pellentesque purus. Etiam pharetra nisl at lacus lacinia fringilla. Phasellus sed volutpat lectus. Phasellus rhoncus justo ac porttitor ullamcorper. Etiam lacinia mattis feugiat. Nullam in tortor mauris. Curabitur consequat neque sed felis auctor aliquam. Donec vitae sapien nunc. Aliquam erat volutpat. Aenean ullamcorper ex libero, eu iaculis risus tempor eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque sit amet consequat nulla, ut vulputate urna.</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p style="text-align: right;"><a href="http://www.artneo.pl/" style="color: #6E9CAC;">zobacz więcej</a></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_newsletter_queue`
--

DROP TABLE IF EXISTS `cms_newsletter_queue`;
CREATE TABLE `cms_newsletter_queue` (
  `id` int(11) NOT NULL,
  `id_newsletter` int(11) NOT NULL,
  `date_sent` datetime NOT NULL,
  `email` varchar(500) COLLATE utf8_polish_ci NOT NULL,
  `sent` tinyint(1) NOT NULL,
  `error` varchar(1000) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `cms_newsletter`
--
ALTER TABLE `cms_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_newsletter_emails`
--
ALTER TABLE `cms_newsletter_emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`(255));

--
-- Indexes for table `cms_newsletter_emails_to_groups`
--
ALTER TABLE `cms_newsletter_emails_to_groups`
  ADD PRIMARY KEY (`id_email`,`id_group`),
  ADD KEY `id_email` (`id_email`),
  ADD KEY `id_group` (`id_group`);

--
-- Indexes for table `cms_newsletter_groups`
--
ALTER TABLE `cms_newsletter_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_newsletter_projects`
--
ALTER TABLE `cms_newsletter_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_newsletter_queue`
--
ALTER TABLE `cms_newsletter_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_newsletter` (`id_newsletter`),
  ADD KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `cms_newsletter`
--
ALTER TABLE `cms_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_newsletter_emails`
--
ALTER TABLE `cms_newsletter_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_newsletter_groups`
--
ALTER TABLE `cms_newsletter_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_newsletter_projects`
--
ALTER TABLE `cms_newsletter_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `cms_newsletter_queue`
--
ALTER TABLE `cms_newsletter_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `cms_newsletter_emails_to_groups`
--
ALTER TABLE `cms_newsletter_emails_to_groups`
  ADD CONSTRAINT `cms_newsletter_emails_to_groups_ibfk_1` FOREIGN KEY (`id_email`) REFERENCES `cms_newsletter_emails` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cms_newsletter_emails_to_groups_ibfk_2` FOREIGN KEY (`id_group`) REFERENCES `cms_newsletter_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `cms_newsletter_queue`
--
ALTER TABLE `cms_newsletter_queue`
  ADD CONSTRAINT `cms_newsletter_queue_ibfk_1` FOREIGN KEY (`id_newsletter`) REFERENCES `cms_newsletter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS=1;
