SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `cms_newsletter`,
	`cms_newsletter_emails`,
	`cms_newsletter_emails_to_groups`,
	`cms_newsletter_groups`,
	`cms_newsletter_projects`,
	`cms_newsletter_queue`;

SET foreign_key_checks = 1;