<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Send_model extends MY_Model {

	public function __construct(){

		parent::__construct('newsletter');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function getEmails($start=null, $limit=null, $confirm=true, $groups=false) {

		$data = array();

		if($confirm===TRUE) $this->db->where('confirm',1);
		$this->db->order_by('email');
		if($start!==null && $limit!==null) $this->db->limit($limit,$start);
		$query = $this->db->get($this->mod_table.'_emails');
		
		if($query->num_rows()>0){

			foreach($query->result() as $row) {
				
				if($groups!==false){
					$this->db->where_in('id_group',$groups);
					$this->db->where('id_email',$row->id);
					$query2 = $this->db->get('newsletter_emails_to_groups');
				
					if($query2->num_rows()==0) continue;
				}
				 
				$this->db->select('g.id');
				$this->db->from('newsletter_groups g');
				$this->db->join('newsletter_emails_to_groups etg','etg.id_group=g.id');
				$this->db->where('etg.id_email',$row->id);
				$query2 = $this->db->get();
				if(count($query2->num_rows()>0)){
					$row->groups = array();
					foreach($query2->result() as $row2) {
						$row->groups[] = $row2->id;
					}
				}
				
				$data[] = $row;
			}

			return $data;
		}
		else return FALSE;

	}

	public function getNumber(){

		$query = $this->db
			->select_max('id')
			->get('newsletter');

		return ($query->num_rows()>0) ? (int)$query->row()->id+1 : 1;
	}

	public function checkForm(){

		$this->form_validation->set_rules('title', 'Tytuł', 'required');
		$this->form_validation->set_rules('newsletter', 'Treść newslettera', 'required');
		$this->form_validation->set_rules('mail', 'Mail', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');
		$this->form_validation->set_rules('sendtoall','Wyślij do wszystkich', '');
		$this->form_validation->set_rules('emails[]','Adresy', '');
		$this->form_validation->set_rules('sendtogroups[]');

		return $this->form_validation->run();
	}
	
	public function save_queue(){
		 
		$mail_b =		 stripslashes($this->input->post('mail'));
		 
		$title 			= $this->input->post('title');
		 
		$data = array(
				'lang'=>$this->input->post('lang'),
				'title'=>$title,
				'text'=>$mail_b,
				'content'=>$this->input->post('newsletter'),
				'link'=>$this->input->post('link'),
				'sent'=>1,
				'sent_ok'=>0,
		);
		 
		$query = $this->db
		->set('date_sent', 'NOW()', FALSE)
		->insert($this->mod_table, $data);
		 
		$id = $this->db->insert_id();
	
		$sendtoall 		= $this->input->post('sendtoall');
		 
		$emails 		= $this->input->post('emails');
		if(!is_array($emails)) $emails = array();
		if($sendtoall) {
			$temp = $this->getEmails();
			foreach($temp as $email) $emails[] = $email->email;
		}
		if($this->input->post('sendtogroups')) {
			$temp = $this->getEmails(null,null,true,$this->input->post('sendtogroups'));
			foreach($temp as $email) $emails[] = $email->email;
		}
	
		 
		$emails = array_unique($emails);
		 
		if(empty($emails)) return FALSE;
	
		foreach($emails as $e) {
			$queue_data = array(
					'id_newsletter' => $id,
					'email' => $e
			);
			$this->db->insert('newsletter_queue', $queue_data);
		}
	
		return TRUE;
	}
	
	public function getGroups($start=null, $limit=null, $with_quantities=0) {
	
		$data = array();
	
		$this->db->order_by('name');
		if($start!==null && $limit!==null) $this->db->limit($limit,$start);
		$query = $this->db->get($this->mod_table.'_groups');
		//echo $this->db->last_query();
		if($query->num_rows()>0){
	
			foreach ($query->result() as $row) {
				 
				if($with_quantities==1) {
					$this->db->where('id_group',$row->id);
					$query2 = $this->db->get($this->mod_table.'_emails_to_groups');
					$row->num_members = $query2->num_rows();
				}
				 
				$data[] = $row;
			}
			return $data;
		}
		else return FALSE;
	
	}
	
	public function getProjects($lang = null) {
	
		if ($lang!=null) $this->db->where('lang', $lang);
	
		$this->db->order_by('title');
		$query = $this->db->get('newsletter_projects');
	
		return $query->num_rows()>0 ? $query->result() : FALSE;
	}
	
	public function getProject($id) {
	
		$data = array();
	
		$this->db->where('id',$id);
		$query = $this->db->get('newsletter_projects');
		
		return $query->num_rows()>0 ? $query->row() : FALSE;
	
	}

}
