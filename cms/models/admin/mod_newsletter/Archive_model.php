<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Archive_model extends MY_Model {

	public function __construct(){

		parent::__construct('newsletter');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function getNewsletters($start=null, $limit=null, $sent=true) {

		if($sent===TRUE) $this->db->where('sent',1);
		$this->db->order_by('date_sent desc');

		$query = $this->db->get($this->mod_table,$limit,$start);

		return $query->num_rows()>0 ? $query->result() : FALSE;
	}

	public function delNewsletters($id=null){

		if($id==null) $id = $this->input->post('check');

		if(empty($id)) return FALSE;

		$this->db->where_in('id',$id);

		return $this->db->delete($this->mod_table);
	}

	public function getNewsletter($id=null){

		if($id===null) return FALSE;

		$this->db->where('id',$id);
		$query = $this->db->get($this->mod_table);

		return $query->num_rows()==0 ? FALSE : $query->row();
	}

	public function saveNewsletter($id){

		if(!is_numeric($id)) return FALSE;

		$data = array(
				'title'=>$this->input->post('title'),
				'content'=>$this->input->post('newsletter')
		);

		$this->db
			->where('id',$id)
			->update('newsletter',$data);

		return TRUE;
	}

}
