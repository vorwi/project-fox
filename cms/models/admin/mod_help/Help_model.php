<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Help_model extends MY_Model {

	public function __construct() {
		parent::__construct('instructions');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all($id_tree = 0, $with_children = TRUE) {
		$this->db->select('tk2.*, tk1.*');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_art', 'left');
		if(is_array($id_tree)) $this->db->where_in('tk1.id_tree', $id_tree);
		else $this->db->where('tk1.id_tree', $id_tree);
		$this->db->where('tk2.lang', $this->admin->get_main_lang());
		$this->db->where('tk1.pub', 1);
		$this->db->where('tk2.pub', 1);
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			if($with_children) {
				$data = array();
				$parent_ids = array();

				foreach($query->result() as $row) {
					$parent_ids[] = $row->id;
					$data[$row->id] = $row;
				}
				$query->free_result();
				$children = $this->get_all($parent_ids, $with_children);

				foreach($children as $row) {
					if(!isset($data[$row->id_tree]->children))
						$data[$row->id_tree]->children = array();
					$data[$row->id_tree]->children[] = $row;
				}

				return $data;
			} else {
				return $query->result();
			}
		}
		return array();
	}


}
