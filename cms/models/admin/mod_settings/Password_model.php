<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password_model extends MY_Model {

	public function __construct(){
		parent::__construct('admin_users');
		$this->_init_module_table(__FILE__, __DIR__);
	}
			
	public function form_check() {
		$this->form_validation->set_rules('old_pass', lang('Stare hasło'), 'trim|required|callback__old_pass_check');
		$this->form_validation->set_rules('new_pass1', lang('Nowe hasło'), 'trim|required|callback__new_pass_check');
		$this->form_validation->set_rules('new_pass2', lang('Nowe hasło (potwierdzenie)'), 'trim|required');
		
		return $this->form_validation->run();
	}
	
	public function password_change() {
		$pass = trim($this->input->post('new_pass1'));
		
		$pass = password_generator($pass);
		
		$data['date_add'] = $pass['time'];
		$data['password'] = $pass['pass_crypt'];

		$this->db->where('id', $this->session->userdata('user_id'));
		$query = $this->db->update($this->mod_table, $data);
			
		return $this->db->affected_rows()>0;
	}
}
