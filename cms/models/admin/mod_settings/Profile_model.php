<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends MY_Model {

	public function __construct(){
		parent::__construct('admin_users');
		$this->_init_module_table(__FILE__, __DIR__);
	}
			
	public function check() {
		$this->form_validation->set_rules('name', lang('Imię i nazwisko'), 'trim|required');
		$this->form_validation->set_rules('phone', lang('Telefon'), 'trim');
		$this->form_validation->set_rules('post_code', lang('Kod pocztowy'), 'trim');
		$this->form_validation->set_rules('city', lang('Miasto'), 'trim');
		$this->form_validation->set_rules('address', lang('Adres'), 'trim');
		
		return $this->form_validation->run();
	}

	public function edit() {
		$id = $this->session->userdata('user_id');
		if(empty($id)){ return FALSE; }
		
		$this->db->from($this->mod_table);
		$this->db->where('id', $id);
		$query = $this->db->get();
	
		return $query->num_rows()==1 ? $query->row() : FALSE;
	}
	
	public function save() {
		$id = $this->session->userdata('user_id');
		if(empty($id)){ return FALSE; }
		
		$data = array (
				'name' => $this->input->post('name'),
				'phone' => $this->input->post('phone'),
				'post_code' => $this->input->post('post_code'),
				'city' => $this->input->post('city'),
				'address' => $this->input->post('address')
		);
		$this->db->where('id', $id);
		$this->db->update($this->mod_table, $data);
	
		return TRUE;
	}
}
