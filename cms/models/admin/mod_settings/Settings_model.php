<?
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Settings_model extends MY_Model {

	public function __construct() {
		parent::__construct('settings');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_categories() {
		$this->db->order_by('position');
		$query = $this->db->get($this->mod_table.'_category');

		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function get_settings_tree($lang) {
		$data = array();
		$this->db->order_by('position');
		$query = $this->db->get($this->mod_table.'_category');

		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$row->settings = $this->get_settings($lang, $row->id);
				$data[] = $row;
			}
			return $data;
		} else {
			return FALSE;
		}
	}

	public function get_settings($lang, $group = false) {
		$main = $this->session->userdata('main');
		$data = array();
		$this->db->group_start();
		$this->db->where('lang', $lang);
		$this->db->or_where('lang IS NULL', NULL, FALSE);
		$this->db->group_end();
		if($main != 1) {
			$this->db->where('pub', 1);
		}
		if($group !== false) {
			$this->db->where('group', $group);
		} else {
			$this->db->order_by('group');
		}

		$this->db->order_by('position');
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() > 0) {
			foreach($query->result() as $key => $row) {
				$data[$key] = $row;
			}

			return $data;
		} else {
			if($main != 1) {
				$this->db->where('pub', 1);
			}
			$this->db->group_start();
			$this->db->where('lang', $this->admin->get_main_lang());
			$this->db->or_where('lang IS NULL', NULL, FALSE);
			$this->db->group_end();
			if($group !== false) {
				$this->db->where('group', $group);
			} else {
				$this->db->order_by('group');
			}

			$this->db->order_by('position');
			$query = $this->db->get($this->mod_table);

			if($query->num_rows() > 0) {
				foreach($query->result() as $key => $row) {
					$set = array(
						'lang' => $lang,
						'pub' => $row->pub,
						'name' => $row->name,
						'value' => $row->value,
						'description' => $row->description,
						'type' => $row->type,
						'group' => $row->group,
						'position' => $row->position,
					);
					$this->db->insert($this->mod_table, $set);
					$set['id'] = $this->db->insert_id();
					$data[$key] = (object)$set;
				}
				return $data;
			}
		}
		return FALSE;
	}

	public function delete($id) {
		if(empty($id))
			return FALSE;

		$this->db->where_in('id', $id);
		$query = $this->db->delete($this->mod_table);

		return TRUE;
	}

	public function add() {
		$this->form_validation->set_rules('name', 'nazwa', 'trim|required|alpha_dash');
		$this->form_validation->set_rules('value');
		$this->form_validation->set_rules('description', 'opis', 'trim|required');
		$this->form_validation->set_rules('type', 'rodzaj', 'required');
		$this->form_validation->set_rules('group', 'grupa', 'trim|required|integer');
		$this->form_validation->set_rules('position', 'pozycja', 'trim|required|integer');
		$this->form_validation->set_rules('lang_related', 'zależne od języka', 'required|integer');

		return $this->form_validation->run();
	}

	public function add_category() {
		$this->form_validation->set_rules('name', 'nazwa', 'trim|required');
		$this->form_validation->set_rules('position', 'pozycja', 'trim|required|integer');

		return $this->form_validation->run();
	}

	public function insert() {
		if($this->input->post('lang_related')) {
			$lang =  $this->input->post('lang');
		} else {
			$lang =  NULL;
		}
		$set = array(
			'lang' => $lang,
			'name' => $this->input->post('name'),
			'value' => $this->input->post('value'),
			'description' => $this->input->post('description'),
			'type' => $this->input->post('type'),
			'group' => $this->input->post('group'),
			'position' => $this->input->post('position')
		);
		$this->db->insert($this->mod_table, $set);

		return $this->db->affected_rows() == 1;
	}

	public function insert_category() {
		$set = array(
			'name' => $this->input->post('name'),
			'position' => $this->input->post('position')
		);
		$this->db->insert($this->mod_table.'_category', $set);

		return $this->db->affected_rows() == 1;
	}

	public function settings_change() {
		$data = array();
		$msg = array();
		$uploaded = array();

		$files_dir = config_item('base_path').config_item('site_path').config_item('settings_files_path');
		if(!is_dir($files_dir)) {
			mkdir($files_dir);
			@chmod($files_dir, 0777);
		}

		if(count($this->input->post()) > 0) {
			foreach($this->input->post() as $key => $row) {
				if($key == 'position') {
					continue;
				}

				if($key == 'rem_cat') {
					$rem_cat = $row;

					if(!empty($rem_cat)) {
						foreach($rem_cat as $key_rc => $rc) {
							if($rc == 'true') {
								$this->db->where_in('group', $key_rc);
								$this->db->delete($this->mod_table);

								$this->db->where_in('id', $key_rc);
								$this->db->delete($this->mod_table.'_category');
							}
						}
					}
				} else {
					if(isset($_FILES['f'.$key]) && is_uploaded_file($_FILES['f'.$key]['tmp_name'])) {
						$config['upload_path'] = $files_dir;
						$config['allowed_types'] = 'gif|jpg|jpeg|png|zip|doc|docx|pdf';
						$config['overwrite'] = TRUE;
						$config['file_name'] = $row;
						$this->upload->initialize($config);

						if(!$this->upload->do_upload('f'.$key))
							$msg[] = array(
								"Plik nie został wgrany poprawnie: ".$this->upload->display_errors('', ''),
								2
							);
						else {
							$tab = $this->upload->data();
							$row .= $tab['file_ext'];
							$uploaded[$key] = TRUE;
						}
					}
					if(count($msg) > 0) {
						$this->session->set_flashdata('msg_files', $msg);
					}
					if(!isset($_FILES['f'.$key]) || isset($uploaded[$key])) {
						$data = array('value' => $row);
						$this->db->where('id', $key);
						$this->db->update($this->mod_table, $data);
					}
				}
			}
			return TRUE;
		}
		return FALSE;
	}

}
