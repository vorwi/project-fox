<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modules_model extends MY_Model {

	public function __construct(){
		parent::__construct('modules');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	public function get_rows($id_c = FALSE) {
		$this->db->select('rc.*, r.*'); 
		$this->db->from($this->mod_table.' as r');
		$this->db->join($this->mod_table."_content as rc", "r.id = rc.id_m AND rc.lang = '".$_SESSION['admin_lang']."'", 'left');
		if($id_c !== FALSE){
			$this->db->where('r.id_c', $id_c);
		}
		$this->db->order_by('r.position');
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function get_components($with_mods = FALSE) {
		$this->db->select('cc.*, c.*');
		$this->db->from('components as c');
		$this->db->join("components_content as cc", "c.id = cc.id_c AND cc.lang = '".$_SESSION['admin_lang']."'", "left");
		$this->db->order_by('c.position');
		$query = $this->db->get();
	
		if($query->num_rows() > 0) {
			if($with_mods !== FALSE){
				$data = array();
				foreach($query->result() as $row){
					$row->modules = $this->get_rows($row->id);
					$data[] = $row;
				}
				$com_null = new stdClass();
				$com_null->name = lang('Brak');
				$com_null->modules = $this->get_rows(NULL);
				$data[] = $com_null;
				return $data;
			}else{
				return $query->result();
			}
		}else{
			return FALSE;
		}
	}

	public function delete($id) {
		if(!is_numeric($id)) {
			$post = $this->input->post('check');
		} else {
			$post = $id;
		}
		if(empty($post)) { return FALSE; }

		$result = TRUE;
		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);
		
		foreach($query->result() as $mod) {
			$this->db->trans_start();
			
			$this->db->where_in('id', $post);
			$result = $this->db->delete($this->mod_table);
			
			$uninstall_sql_path = APPPATH."models/admin/{$mod->mod_dir}/config/{$mod->mod}_uninstall.sql";
			$this->db->load_sql($uninstall_sql_path);
			
			$this->db->trans_complete();
			if($this->db->trans_status() === FALSE) {
				$result = FALSE;
				break;
			}
		}
		
		return $result;
	}

	public function add() {
		$this->form_validation->set_rules('title', lang('Tytuł'), 'trim|required');
		$this->form_validation->set_rules('mod', 'mod', 'trim|required');
		$this->form_validation->set_rules('mod_dir', 'mod_dir', 'trim|required');
		$this->form_validation->set_rules('mod_table', 'mod_table', 'trim');
		$this->form_validation->set_rules('mod_function', 'mod_function', 'trim');

		return $this->form_validation->run();
	}

	public function insert() {
		$result = FALSE;
		$id_c = $this->input->post('id_c');
		$id_c = ifempty($id_c);
		
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$this->db->where('id_c', $id_c);
		$query = $this->db->get();
		$row = $query->row();
		$position = $row->position + 1;
		
		$mod_name = $this->input->post('mod');
		$mod_dir = $this->input->post('mod_dir');
		$mod_dir = rtrim($mod_dir, '/').'/';
		$mod_function = $this->input->post('mod_function');
		if(empty($mod_function)){
			$mod_function = 'index';
		}
		
		$this->db->trans_start();
	
		$data = array(
			'id_c' => $id_c,
			'mod' => $mod_name,
			'mod_dir' => $mod_dir,
			'mod_table' => $this->input->post('mod_table'),
			'mod_function' => $mod_function,
			'position' => $position,
			'pub' => $this->input->post('pub'),
			'global' => $this->input->post('global'),
			'main' => $this->input->post('main'),
			'menu' => $this->input->post('menu')
		);

		$this->db->insert($this->mod_table, $data);

		if($this->db->affected_rows()==1) {
			 $id = $this->db->insert_id();

			 $data = array(
				'id_m' => $id,
			 	'lang' => $this->admin->get_main_lang(),
				'title' => $this->input->post('title'),
			 	'label' => $this->input->post('label'),
			 	'description' => $this->input->post('description')
			);

			if($this->neocms->insert_lang_content($this->mod_table.'_content', $data)){
				$install_sql_path = APPPATH."models/admin/{$mod_dir}/config/{$mod_name}_install.sql";
				$this->db->load_sql($install_sql_path);
				$result = TRUE;
			}
		}
		$this->db->trans_complete();
		
		return $result || $this->db->trans_status();
	}

	public function LangExists($id,$lang){
		$this->db->where('id_m',$id);
		$this->db->where('lang',$lang);
		$query = $this->db->get($this->mod_table.'_content');
		return $query->num_rows()>0;
	}

	public function edit($id,$lang) {
		if(!$this->LangExists($id, $lang)){
			$this->db->where('id_m',$id);
			$this->db->where('lang',$this->admin->get_main_lang());
			$query = $this->db->get($this->mod_table.'_content');
			if($query->num_rows()== 0) { return FALSE; }
			$row = $query->row();

			$data = (array)$row;
			unset($data['id']);
			$data['lang'] = $lang;
			$this->db->insert($this->mod_table.'_content', $data);
		}
		
		$this->db->select('rc.*, r.*');
		$this->db->from($this->mod_table.' as r');
		$this->db->join($this->mod_table.'_content as rc', 'r.id = rc.id_m');
		$this->db->where('rc.id_m',$id);
		$this->db->where('rc.lang', $lang);
		$query = $this->db->get();

		if($query->num_rows()>0){
			$row = $query->row();

			if(!empty($row->lang)){
				$row->lang = $this->neocms->get_language($row->lang);
			}

			return $row;
		}else{
			return FALSE;
		}
	}

	public function save($id,$lang) {
		$id_c = $this->input->post('id_c');
		$mod_dir = $this->input->post('mod_dir');
		$mod_dir = rtrim($mod_dir, '/').'/';
		$mod_function = $this->input->post('mod_function');
		if(empty($mod_function)){
			$mod_function = 'index';
		}
		
		$data = array (
			'id_c' => ifempty($id_c),
			'mod' => $this->input->post('mod'),
			'mod_dir' => $mod_dir,
			'mod_table' => $this->input->post('mod_table'),
			'mod_function' => $mod_function,
			'pub' => $this->input->post('pub'),
			'global' => $this->input->post('global'),
			'main' => $this->input->post('main'),
			'menu' => $this->input->post('menu')
		);
		$this->db->where('id', $id);
		$this->db->update($this->mod_table, $data);

		$data = array(
			'title' => $this->input->post('title'),
			'label' => $this->input->post('label'),
			'description' => $this->input->post('description')
		);

		$this->db->where('id_m', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table.'_content', $data);

		return TRUE;
	}
}
