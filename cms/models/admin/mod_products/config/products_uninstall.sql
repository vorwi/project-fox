SET foreign_key_checks = 0;

DROP TABLE `cms_products`,
	`cms_products_category`,
	`cms_products_category_content`,
	`cms_products_content`,
	`cms_products_content_img`;

SET foreign_key_checks = 1;