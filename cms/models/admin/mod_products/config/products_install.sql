-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 21 Lip 2016, 10:38
-- Wersja serwera: 5.6.20-log
-- Wersja PHP: 5.5.15

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Baza danych: `neocms`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_products`
--

CREATE TABLE IF NOT EXISTS `cms_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cat` int(11) NOT NULL,
  `dir` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `pub` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cat` (`id_cat`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_products_category`
--

CREATE TABLE IF NOT EXISTS `cms_products_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tree` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `pub` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_products_category_content`
--

CREATE TABLE IF NOT EXISTS `cms_products_category_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cat` int(11) NOT NULL,
  `lang` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admission` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_modified` datetime NOT NULL,
  `admission_on` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `pub` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cat` (`id_cat`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_products_content`
--

CREATE TABLE IF NOT EXISTS `cms_products_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pr` int(11) NOT NULL,
  `lang` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admission` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_modified` datetime NOT NULL,
  `admission_on` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `pub` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pr` (`id_pr`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_products_content_img`
--

CREATE TABLE IF NOT EXISTS `cms_products_content_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pr` int(11) NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_add` datetime NOT NULL,
  `position` int(11) NOT NULL,
  `pub` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pr` (`id_pr`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `cms_products`
--
ALTER TABLE `cms_products`
  ADD CONSTRAINT `cms_products_ibfk_1` FOREIGN KEY (`id_cat`) REFERENCES `cms_products_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `cms_products_category_content`
--
ALTER TABLE `cms_products_category_content`
  ADD CONSTRAINT `cms_products_category_content_ibfk_1` FOREIGN KEY (`id_cat`) REFERENCES `cms_products_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cms_products_category_content_ibfk_2` FOREIGN KEY (`lang`) REFERENCES `cms_lang` (`short`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `cms_products_content`
--
ALTER TABLE `cms_products_content`
  ADD CONSTRAINT `cms_products_content_ibfk_1` FOREIGN KEY (`id_pr`) REFERENCES `cms_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cms_products_content_ibfk_2` FOREIGN KEY (`lang`) REFERENCES `cms_lang` (`short`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `cms_products_content_img`
--
ALTER TABLE `cms_products_content_img`
  ADD CONSTRAINT `cms_products_content_img_ibfk_1` FOREIGN KEY (`id_pr`) REFERENCES `cms_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
