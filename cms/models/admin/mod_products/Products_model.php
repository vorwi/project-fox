<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends MY_Model {

	public function __construct(){
		parent::__construct('products');
		$this->_init_module_table(__FILE__, __DIR__);
		
		$path = './' . config_item('site_path') . config_item('upload_path') . $this->pr_dir.'/';
		if (!is_dir($path)) {
			@mkdir($path); 
			@chmod($path, 0777);
		}
	}
	
	public function get_all_categories_with_products() {
		$data = array();
		$categories = $this->categories_model->get_all_categories();
		
		if(!empty($categories)){
			foreach($categories as $row) {
				$row->products = $this->get_products($row->id);
				$data[] = $row;
			}
		}
		
		return $data;
	}
	
	public function get_products($id_cat = FALSE) {
		$this->db->select("pc.*, p.*", false);
		$this->db->from('products as p');
		$this->db->join('products_content as pc', 'p.id = pc.id_pr');
		
		if($id_cat !== FALSE && is_numeric($id_cat)) {
			$this->db->where('p.id_cat', $id_cat);
		}
		$this->db->where('pc.lang', $this->admin->get_main_lang());
		$this->db->group_by('p.id');
		$this->db->order_by('p.position');
		$query = $this->db->get();
	
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) return FALSE;

		$this->db->where_in('id', $post);
		$query = $this->db->get('products');
		
		foreach($query->result() as $row) {
			$dir = './'.config_item('site_path').config_item('upload_path').$this->pr_dir.'/'.$row->dir.'/';
			if(is_dir($dir)){
				rrmdir($dir);
			}
		}
		
		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);
			
		$this->db->where_in('id_pr', $post);
		$query = $this->db->delete('products_content');
			
		$this->db->where_in('id_pr', $post);
		$query = $this->db->delete('products_content_img');
		
		$this->db->where_in('dest_id', $post);
		$this->db->where('type', 'products');
		$query_uu = $this->db->delete('user_url');
		$this->user_url->update_routes();
		
		return TRUE;
	}

	public function add() {
		$this->form_validation->set_rules('id_cat', lang('Kategoria'), 'required');
		$this->form_validation->set_rules('title', lang('Tytuł'), 'trim|required');

		return $this->form_validation->run();
	}

	public function insert() {
		$id_cat = $this->input->post('id_cat');
		$this->db->select('max(position) as position');
		$this->db->from('products');
		$this->db->where('id_cat', $id_cat);
		$query = $this->db->get();

		$row = $query->row();

		$position = $row->position+1;

		$data = array(
			'position' => $position,
			'id_cat' => $id_cat
		);

		$this->db->insert($this->mod_table, $data);

		if($this->db->affected_rows()==1) {
			 $id = $this->db->insert_id();
			 
			 $dir = $this->prAssignDir($id);
			 $title = trim($this->input->post('title'));

			 $data = array(
				'id_pr' => $id,
			 	'lang' => $this->admin->get_main_lang(),
				'title' => $title,
			 	'author' => $this->session->userdata('user_id'),
				'pub' => 1,
			);
			$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert('products_content', $data);

			$s = false;
			if($this->db->affected_rows()==1){ $s = true; }
			
			$this->user_url->save_url(false,$title,'products',$id,$this->admin->get_main_lang());
			$this->user_url->update_routes();
			
			if($s === true){
				return $this->input->post('title');
			} else {
				return(false);
			}
		}
		else return FALSE;
	}

	public function prodExists($id){
		$this->db->where('id_pr',$id);
		$query = $this->db->get('products_content');
		return $query->num_rows()>0;
	}

	public function prExists($id) {
		$this->db->where('id_pr', $id);
		$this->db->where('lang',$this->admin->get_main_lang());
		$query = $this->db->get('products_content');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return FALSE;
		}
	}
	
	public function prodLangExists($id,$lang){
		$this->db->where('id_pr',$id);
		$this->db->where('lang',$lang);
		$query = $this->db->get('products_content');
		return $query->num_rows()>0;
	}
	
	public function prDirExists($id) {
		$this->db->where('id', $id);
		$this->db->where('dir !=', '');
		$query = $this->db->get($this->mod_table);
	
		if($query->num_rows() > 0) {
			$dir = './'.config_item('site_path').config_item('upload_path').$this->pr_dir.'/';
			if(!is_dir($dir.$query->row()->dir)){
				return FALSE;
			}else{
				return TRUE;
			}
		} else{
			return FALSE;
		}
	}
	
	public function prAssignDir($id) {
		$dir = time();
		$this->db->set('dir', $dir);
		$this->db->where('id', $id);
		$this->db->update($this->mod_table);
	
		$pr_dir = './'.config_item('site_path').config_item('upload_path').$this->pr_dir.'/'.$dir.'/';
		mkdir($pr_dir);
		if(!is_dir($pr_dir)){
			return FALSE;
		}
		@chmod($pr_dir, 0777);
	
		if($this->db->affected_rows() == 1) {
			return $pr_dir;
		} else{
			return FALSE;
		}
	}

	public function edit($id,$lang) {
		if(!$this->prodLangExists($id, $lang)){
			$this->db->where('id_pr',$id);
			$this->db->where('lang',$this->admin->get_main_lang());
			$query = $this->db->get('products_content');
			$row = $query->row();

			$data = array(
				'id_pr' => $id,
			 	'title' => $row->title,
			 	'short_title' => $row->short_title,
				'admission' => $row->admission,
			 	'url' => $row->url,
				'price' => $row->price,
				'target' => $row->target,
			 	'content' => $row->content,
			 	'meta_title' => $row->meta_title,
			 	'meta_keywords' => $row->meta_keywords,
			 	'meta_description' => $row->meta_description,
			 	'admission_on' => $row->admission_on,
			 	'lang' => $lang,
			 	'author' => $this->session->userdata('user_id'),
				'pub' => 1,
			);
			$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert('products_content', $data);
			
			$this->user_url->save_url(false,$row->title,'products',$id,$lang);
			$this->user_url->update_routes();
		}
		
		if(!$this->prDirExists($id)) {
			$dir = time();
			$this->db->set('dir', $dir);
			$this->db->where('id', $id);
			$this->db->update($this->mod_table);
		
			if($this->db->affected_rows() == 1) {
				$pr_dir = './'.config_item('site_path').config_item('upload_path').$this->pr_dir;
				if(!is_dir($pr_dir)) {
					mkdir($pr_dir);
					if(!is_dir($pr_dir))
						return FALSE;
					@chmod($pr_dir, 0777);
				}
		
				$dir = $pr_dir.'/'.$dir.'/';
				mkdir($dir);
				if(!is_dir($dir))
					return FALSE;
				@chmod($dir, 0777);
			}
		}

		$this->db->where('id_pr',$id);
		$this->db->where('lang',$lang);
		$query = $this->db->get('products_content');


		if($query->num_rows()>0){
			$prod = $query->row();

			$this->db->select('p.id_cat, p.pub, uu.url as user_url');
			$this->db->from('products p');
			$this->db->join('user_url uu', "uu.dest_id=p.id AND uu.type = 'products' AND uu.lang = '".$lang."'", 'left');
			$this->db->where('p.id',$id);
			$query = $this->db->get();
			$prod_stub = $query->row();
			$prod->id_cat = $prod_stub->id_cat;
			$prod->pub_all = $prod_stub->pub;
			$prod->user_url = $prod_stub->user_url;

			if(is_numeric($prod->author)){
				$this->db->select('login, name');
				$this->db->where('id',$prod->author);
				$query = $this->db->get('admin_users');
				$prod->author = $query->row();
			}

			if(!empty($prod->lang)){
				$prod->lang = $this->neocms->get_language($prod->lang);
			}

			return $prod;
		}
		return FALSE;

	}

	public function save($id,$lang) {
		$data = array (
			'id_cat' => $this->input->post('id_cat'),
			'pub' 		=> $this->input->post('pub_all')
		);
		$this->db->where('id', $id);
		$this->db->update('products', $data);
		
		$this->user_url->save_url(true,$this->input->post('user_url'),'products',$id,$lang);
		$this->user_url->update_routes();

		$data = array(
			'title' 			=> htmlspecialchars($this->input->post('title')),
			'short_title' 		=> htmlspecialchars($this->input->post('short_title')),
			'meta_title' 		=> htmlspecialchars($this->input->post('meta_title')),
			'url' 				=> htmlspecialchars($this->input->post('url')),
			'price' 			=> htmlspecialchars($this->input->post('price')),
			'target' 			=> $this->input->post('target'),
			'meta_description' 	=> $this->input->post('meta_description'),
			'meta_keywords' 	=> $this->input->post('meta_keywords'),
			'admission' 		=> $this->input->post('admission'),
			'admission_on' 		=> $this->input->post('admission_on'),
			'content'	 		=> $this->input->post('prod_content'),
			'author' 			=> $this->session->userdata('user_id'),
			'pub' 				=> $this->input->post('pub')
		);

		$this->db->set('date_modified', 'NOW()', FALSE);
		$this->db->where('id_pr', $id);
		$this->db->where('lang', $lang);
		$this->db->update('products_content', $data);

		return TRUE;
	}
	
	public function get_image($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('products_content_img');
	
		if ($query->num_rows() > 0) {
			$row = $query->row();
			$gal = $row->id_pr;
			$img = $row->img;
		}
		else return FALSE;
	
		$this->db->where('id', $gal);
		$query = $this->db->get('products');
	
		$row = $query->row();
		$dir = $row->dir;
	
		$path = config_item('upload_path').$this->pr_dir.'/'.$dir.'/';
			
		$data = array('id' => $id, 'dir' => $dir, 'img' => $img, 'path'=>$path, 'id_pr'=>$gal);
	
		return $data;
	}
	
	public function crop($id) {
		$data = $this->get_image($id);
			
		$dir = $data['dir'];
		$img = $data['img'];
			
		$img_save = thumb_name($img);
			
		$coords['x_axis'] 	= $this->input->post('x_axis');
		$coords['y_axis'] 	= $this->input->post('y_axis');
		$coords['width'] 	= $this->input->post('width');
		$coords['height']	= $this->input->post('height');
		$coords['thumb_width'] 	= $this->gal_thumb_width;
		$coords['thumb_height']	= $this->gal_thumb_height;
			
		$path = './'.config_item('site_path').config_item('upload_path').$this->pr_dir.'/'.$dir.'/';
	
		$this->image_lib->crop_thumb($path, $img, $path_save=null, $img_save,  $coords);
	}
	
	public function crop_list($id) {
		$data = $this->get_image($id);
	
		$dir = $data['dir'];
		$img = $data['img'];
	
		$img_save = thumb_name($img, '_thumb_list');
	
		$coords['x_axis'] 	= $this->input->post('x_axis');
		$coords['y_axis'] 	= $this->input->post('y_axis');
		$coords['width'] 	= $this->input->post('width');
		$coords['height']	= $this->input->post('height');
		$coords['thumb_width'] 	= $this->gal_thumb_list_width;
		$coords['thumb_height']	= $this->gal_thumb_list_height;
	
		$path = './'.config_item('site_path').config_item('upload_path').$this->pr_dir.'/'.$dir.'/';
	
		$this->image_lib->crop_thumb($path, $img, $path_save=null, $img_save,  $coords);
	}
	
	public function get_product_images($id = null) {
		if($id == null) {
			return FALSE;
		}
	
		if(!$this->prDirExists($id)) {
			$dir = $this->prAssignDir($id);
		}
	
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);
	
		$row = $query->row();
	
		$dir = $row->dir;
	
		$dir = './'.config_item('site_path').config_item('upload_path').$this->pr_dir.'/'.$dir.'/';
	
		if(!is_dir($dir)) {
			mkdir($dir);
			if(!is_dir($dir)) {
				return FALSE;
			}
			@chmod($dir, 0777);
		}
	
		$this->db->select('tk1.*, tk2.dir');
		$this->db->from('products_content_img as tk1');
		$this->db->where('tk1.id_pr', $id);
		$this->db->join('products as tk2', 'tk1.id_pr = tk2.id');
		$this->db->order_by('tk1.position');
	
		$query = $this->db->get();
	
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}
	
	public function save_images_pl($id){
		$img = md5(microtime());
	
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);
	
		$row = $query->row();
	
		$dir = $row->dir;
	
		$dir_tmp = './'.config_item('temp').'prod_'.time().'_'.$id.'/';
		@mkdir($dir_tmp);
		@chmod($dir_tmp, 0777);
		if(!is_dir($dir_tmp)){
			return FALSE;
		}
		
		$config = null;
		$config['upload_path'] = $dir_tmp;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $img;
	
		$this->upload->initialize($config);
	
		if(is_uploaded_file($_FILES['file']['tmp_name'])){
			if (!$this->upload->do_upload('file')){
				die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
			}else {
					
				$tab = $this->upload->data();
	
				$map_path = $tab['file_path'];
	
				$map = array($tab['file_name']);
				
				sort($map);
	
				if(!empty($map)) {
					if(!$this->save_thumbnails($id, $map, $map_path)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
					}
				}
			}
	
			return TRUE;
		}
		else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
		}
	}
	
	public function save_thumbnails($id, $dir = null, $path = null) {
		$msg = array();
	
		if($dir == null || $path == null || empty($dir)) return FALSE;
	
		foreach($dir as $value) {
			$file = $path.'/'.$value;
	
			$tab = pathinfo($file);
	
			$this->db->where('id', $id);
			$query = $this->db->get($this->mod_table);
	
			$row = $query->row();
			$dir = $row->dir;
	
			$dir = './'.config_item('site_path').config_item('upload_path').$this->pr_dir.'/'.$dir.'/';
	
			$name = md5(microtime());
			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $dir.$name.'.'.$tab['extension'];
			$config['create_thumb'] = FALSE;
	
			$config['maintain_ratio'] = TRUE;
			$size = getimagesize($file);
			if($size[0] < $this->gal_img_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->gal_img_width;
			if($size[1] < $this->gal_img_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->gal_img_height;
	
			// jeśli zdjęcie jest pionowe, to będzie dopasowane do wysokości; jeśli poziome, to do szerokości
			if($size[0] < $size[1]) {
				$config['master_dim'] = 'height';
			} else {
				$config['master_dim'] = 'width';
			}
	
			$config['master_dim'] = 'width';
	
			$this->image_lib->initialize($config);
			if(!$this->image_lib->resize())
				$msg[] = array(sprintf(lang("Plik <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')),1);
			$this->image_lib->clear();
				
			$thumb_file = $dir.$name.'_thumb.'.$tab['extension'];
			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $thumb_file;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
				
			$size = getimagesize($dir.$name.'.'.$tab['extension']);
			if($size[0] < $this->gal_thumb_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->gal_thumb_width;
			if($size[1] < $this->gal_thumb_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->gal_thumb_height;
	
			// sprawdzenie, czy zdjęcie trzeba przyciąć z boków, czy z góry
			if(($size[1] * $config['width']) / $size[0] < $config['height']) {
				$config['master_dim'] = 'height';
			} else {
				$config['master_dim'] = 'width';
			}
				
			$this->image_lib->initialize($config);
			if(!$this->image_lib->resize())
				$msg[] = array(sprintf(lang("Plik miniaturki <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')),1);
			$this->image_lib->clear();
	
			// przycięcie do ustalonych wymiarów miniaturek
			$config = null;
			$config['source_image'] = $thumb_file;
			$size = getimagesize($thumb_file);
			$config['maintain_ratio'] = FALSE;
			if($size[0] < $this->gal_thumb_width) {
				$config['width'] = $size[0];
			} else {
				$config['width'] = $this->gal_thumb_width;
			}
			if($size[1] < $this->gal_thumb_height) {
				$config['height'] = $size[1];
			} else {
				$config['height'] = $this->gal_thumb_height;
			}
			$config['x_axis'] = 0;
			$config['y_axis'] = 0;
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();
			
			$thumb_file = $dir.$name.'_thumb_list.'.$tab['extension'];
			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $thumb_file;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;
			 
			$size = getimagesize($dir.$name.'.'.$tab['extension']);
			if($size[0] < $this->gal_thumb_list_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->gal_thumb_list_width;
			if($size[1] < $this->gal_thumb_list_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->gal_thumb_list_height;
			
			// sprawdzenie, czy zdjęcie trzeba przyciąć z boków, czy z góry
			if(($size[1] * $config['width']) / $size[0] < $config['height']) {
				$config['master_dim'] = 'height';
			} else {
				$config['master_dim'] = 'width';
			}
			 
			$this->image_lib->initialize($config);
			if(!$this->image_lib->resize())
				$msg[] = array(sprintf(lang("Plik miniaturki <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')),1);
			$this->image_lib->clear();
			
			// przycięcie do ustalonych wymiarów miniaturek
			$config = null;
			$config['source_image'] = $thumb_file;
			$size = getimagesize($thumb_file);
			$config['maintain_ratio'] = FALSE;
			if($size[0] < $this->gal_thumb_list_width) {
				$config['width'] = $size[0];
			} else {
				$config['width'] = $this->gal_thumb_list_width;
			}
			if($size[1] < $this->gal_thumb_list_height) {
				$config['height'] = $size[1];
			} else {
				$config['height'] = $this->gal_thumb_list_height;
			}
			$config['x_axis'] = 0;
			$config['y_axis'] = 0;
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();
	
			$this->db->select('max(position) as position');
			$this->db->where('id_pr', $id);
			$query = $this->db->get('products_content_img');
	
			$row1 = $query->row();
	
			$position = $row1->position + 1;
	
			$data = array(
					'id_pr' => $id,
					'img' => $name.'.'.$tab['extension'],
					'pub' => 1,
					'position' => $position
			);
	
			$this->db->set('data_add', 'NOW()', false);
			$this->db->insert('products_content_img', $data);
	
			sleep(1);
		}
	
		delete_files($path, TRUE);
		rmdir($path);
	
		if(count($msg) > 0){
			$this->session->set_flashdata('msg_files', $msg);
		}
	
		return TRUE;
	}
	
	public function position_images($id) {
		$post = $this->input->post('position');
		if(empty($post)){
			return FALSE;
		}
	
		foreach($post as $key => $row) {
			$data = array(
					'position' => $row
			);
			$this->db->where('id', $key);
			$this->db->where('id_pr', $id);
			$this->db->update('products_content_img', $data);
		}
	
		return TRUE;
	}
	
	public function del_images($id) {
		$this->db->select('tk1.*, tk2.dir');
		$this->db->from('products_content_img as tk1');
		$this->db->where('tk1.id', $id);
		$this->db->join('products as tk2', 'tk1.id_pr = tk2.id');
		$query = $this->db->get();
	
		$row = $query->row();
	
		$dir = './'.config_item('site_path').'/'.config_item('upload_path').'/'.$this->pr_dir.'/'.$row->dir.'/';
	
		$tab = explode('.', $row->img);
	
		$path_big = $dir.$row->img;
		$path_small = $dir.$tab[0].'_thumb.'.$tab[1];
		$path_list = $dir.$tab[0].'_thumb_list.'.$tab[1];
	
		unlink($path_big);
		unlink($path_small);
		unlink($path_list);
	
		$this->db->where('id', $id);
		$this->db->delete('products_content_img');
	
		return TRUE;
	}
}
