<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends MY_Model {

	public function __construct() {
		parent::__construct('products_category');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all_categories($max_depth = 4) {
		$data = array();

		$categories_tree = $this->get_categories_tree($max_depth);

		if($categories_tree !== FALSE && !empty($categories_tree[0])) {
			foreach($categories_tree[0] as $row) {
				$data[] = $row;
				if(isset($categories_tree[1][$row->id])) {
					foreach($categories_tree[1][$row->id] as $row_l2) {
						$data[] = $row_l2;
						if(isset($categories_tree[2][$row_l2->id])) {
							foreach($categories_tree[2][$row_l2->id] as $row_l3) {
								$data[] = $row_l3;
								if(isset($categories_tree[3][$row_l3->id])) {
									$data = array_merge($data, $categories_tree[3][$row_l3->id]);
								}
							}
						}
					}
				}
			}

			return $data;
		}
		return FALSE;
	}

	private function get_categories_tree($max_depth, $id_tree = 0, $level = 0) {
		static $result = array();

		$this->db->select('pcc.*, pc.*');
		$this->db->from('products_category as pc');
		$this->db->join('products_category_content as pcc', 'pc.id = pcc.id_cat');
		if(is_array($id_tree)) {
			$this->db->where_in('pc.id_tree', $id_tree);
		} else {
			$this->db->where('pc.id_tree', 0);
		}
		$this->db->where('pcc.lang', $this->admin->get_main_lang());
		$this->db->order_by('pc.position');
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			$data[$level] = array();
			$ids = array();
			foreach($query->result() as $row) {
				$ids[] = $row->id;
				$row->tree = $level;
				if($id_tree == 0) {
					$data[$level][] = $row;
				} else {
					if(!isset($data[$level][$row->id_tree]))
						$data[$level][$row->id_tree] = array();
					$data[$level][$row->id_tree][] = $row;
				}
			}
			$query->free_result();

			$level++;
			if($level < $max_depth) {
				$children = $this->get_categories_tree($max_depth, $ids, $level);
				if($children !== FALSE) {
					$data = array_merge($data, $children);
				}
			}

			return $data;
		}
		return FALSE;
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post))
			return FALSE;

		$this->db->where_in('id', $post);
		$this->db->or_where_in('id_tree', $post);
		$query = $this->db->delete($this->mod_table);

		$query = $this->db->get('products_category');

		if($this->db->affected_rows() > 0) {
			foreach($query->result() as $row) {
				if($row->id_tree != 0) {
					$this->db->where('id', $row->id_tree);
					$query1 = $this->db->get('products_category');

					if($query1->num_rows() == 0) {
						$this->db->where('id', $row->id);
						$this->db->or_where('id_tree', $row->id);
						$query = $this->db->delete($this->mod_table);
					}
				}
			}
		}

		$this->db->where_in('dest_id', $post);
		$this->db->where('type', 'products_category');
		$query_uu = $this->db->delete('user_url');
		$this->user_url->update_routes();

		return TRUE;
	}

	public function add() {
		$this->form_validation->set_rules('title', lang('Tytuł'), 'trim|required');
		return $this->form_validation->run();

	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from('products_category');
		$this->db->where('id_tree', $this->input->post('id_tree'));
		$query = $this->db->get();

		$row = $query->row();

		$position = $row->position + 1;

		$data = array(
			'id_tree' => $this->input->post('id_tree'),
			'position' => $position
		);

		$this->db->insert($this->mod_table, $data);

		if($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();
			$title = trim($this->input->post('title'));

			$data = array(
				'id_cat' => $id,
				'lang' => $this->admin->get_main_lang(),
				'title' => $title,
				'author' => $this->session->userdata('user_id'),
				'pub' => 1,
			);
			$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert('products_category_content', $data);

			$s = false;
			if($this->db->affected_rows() == 1) { $s = true;
			}

			$this->user_url->save_url(false, $title, 'products_category', $id, $this->admin->get_main_lang());
			$this->user_url->update_routes();

			if($s === true) {
				return $this->input->post('title');
			} else {
				return (false);
			}
		} else
			return FALSE;
	}

	public function catExists($id) {
		$this->db->where('id_cat', $id);
		$query = $this->db->get('products_category_content');
		return $query->num_rows() > 0;
	}

	public function catLangExists($id, $lang) {
		$this->db->where('id_cat', $id);
		$this->db->where('lang', $lang);
		$query = $this->db->get('products_category_content');
		return $query->num_rows() > 0;
	}

	public function edit($id, $lang) {
		if(!$this->catLangExists($id, $lang)) {
			$this->db->where('id_cat', $id);
			$this->db->where('lang', $this->admin->get_main_lang());
			$query = $this->db->get('products_category_content');
			$row = $query->row();

			$data = array(
				'id_cat' => $id,
				'title' => $row->title,
				'short_title' => $row->short_title,
				'admission' => $row->admission,
				'url' => $row->url,
				'target' => $row->target,
				'content' => $row->content,
				'meta_title' => $row->meta_title,
				'meta_keywords' => $row->meta_keywords,
				'meta_description' => $row->meta_description,
				'admission_on' => $row->admission_on,
				'lang' => $lang,
				'author' => $this->session->userdata('user_id'),
				'pub' => 1,
			);
			$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert('products_category_content', $data);

			$this->user_url->save_url(false, $row->title, 'products_category', $id, $lang);
			$this->user_url->update_routes();
		}

		$this->db->where('id_cat', $id);
		$this->db->where('lang', $lang);
		$query = $this->db->get('products_category_content');

		if($query->num_rows() > 0) {
			$cat = $query->row();

			$this->db->select('pc.id_tree, pc.pub, uu.url as user_url');
			$this->db->from('products_category pc');
			$this->db->join('user_url uu', "uu.dest_id=pc.id AND uu.type = 'products_category' AND uu.lang = '".$lang."'", 'left');
			$this->db->where('pc.id', $id);
			$query = $this->db->get();
			$cat_stub = $query->row();
			$cat->id_tree = $cat_stub->id_tree;
			$cat->pub_all = $cat_stub->pub;
			$cat->user_url = $cat_stub->user_url;

			if(is_numeric($cat->author)) {
				$this->db->select('login, name');
				$this->db->where('id', $cat->author);
				$query = $this->db->get('admin_users');
				$cat->author = $query->row();
			}

			if(!empty($cat->lang)) {
				$cat->lang = $this->neocms->get_language($cat->lang);
			}

			return $cat;
		}
		return FALSE;
	}

	public function save($id, $lang) {
		$data = array(
			'id_tree' => $this->input->post('id_tree'),
			'pub' => $this->input->post('pub_all')
		);
		$this->db->where('id', $id);
		$this->db->update('products_category', $data);

		$this->user_url->save_url(true, $this->input->post('user_url'), 'products_category', $id, $lang);
		$this->user_url->update_routes();

		$data = array(
			'title' => htmlspecialchars($this->input->post('title')),
			'short_title' => htmlspecialchars($this->input->post('short_title')),
			'meta_title' => htmlspecialchars($this->input->post('meta_title')),
			'url' => htmlspecialchars($this->input->post('url')),
			'target' => $this->input->post('target'),
			'meta_description' => $this->input->post('meta_description'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'admission' => $this->input->post('admission'),
			'admission_on' => $this->input->post('admission_on'),
			'content' => $this->input->post('cat_content'),
			'author' => $this->session->userdata('user_id'),
			'pub' => $this->input->post('pub')
		);

		$this->db->set('date_modified', 'NOW()', FALSE);
		$this->db->where('id_cat', $id);
		$this->db->where('lang', $lang);
		$this->db->update('products_category_content', $data);

		return TRUE;
	}

}
