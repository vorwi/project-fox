<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Components_model extends MY_Model {

	public function __construct(){
		parent::__construct('components');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	public function get_rows($params = FALSE) {
		if(is_array($params)) $params = (object)$params;
		
		$this->db->select('rc.*, r.*'); 
		$this->db->from($this->mod_table.' as r');
		$this->db->join($this->mod_table.'_content as rc', "r.id = rc.id_c AND rc.lang = '".$_SESSION['admin_lang']."'", 'left');
		$this->db->order_by('r.position');
		$query = $this->db->get();
		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) { return FALSE; }

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);
		return TRUE;
	}

	public function add() {
		$this->form_validation->set_rules('name', lang('Nazwa'), 'trim|required');
		$this->form_validation->set_rules('link', lang('Link'), 'trim');
		$this->form_validation->set_rules('description', lang('Opis'), 'trim');

		return $this->form_validation->run();
	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();

		$position = $query->row()->position+1;

		$data = array(
			'position' => $position,
			'pub' => $this->input->post('pub'),
			'main' => $this->input->post('main')
		);

		$this->db->insert($this->mod_table, $data);

		if($this->db->affected_rows()==1) {
			 $id = $this->db->insert_id();

			 $data = array(
				'id_c' => $id,
			 	'lang' => $this->admin->get_main_lang(),
				'name' => $this->input->post('name'),
			 	'link' => $this->input->post('link'),
			 	'description' => $this->input->post('description')
			);

			return $this->neocms->insert_lang_content($this->mod_table.'_content', $data); 
		} else {
			return FALSE;
		}
	}

	public function LangExists($id,$lang){
		$this->db->where('id_c',$id);
		$this->db->where('lang',$lang);
		$query = $this->db->get($this->mod_table.'_content');
		return $query->num_rows() > 0;
	}

	public function edit($id,$lang) {
		if(!$this->LangExists($id, $lang)){
			$this->db->where('id_c',$id);
			$this->db->where('lang',$this->admin->get_main_lang());
			$query = $this->db->get($this->mod_table.'_content');
			if($query->num_rows()== 0) { return FALSE; }
			$row = $query->row();

			$data = (array)$row;
			unset($data['id']);
			$data['lang'] = $lang;
			$this->db->insert($this->mod_table.'_content', $data);
		}
		
		$this->db->select('rc.*, r.*');
		$this->db->from($this->mod_table.' as r');
		$this->db->join($this->mod_table.'_content as rc', 'r.id = rc.id_c');
		$this->db->where('rc.id_c',$id);
		$this->db->where('rc.lang', $lang);
		$query = $this->db->get();

		if($query->num_rows()>0){
			$row = $query->row();

			if(!empty($row->lang)){
				$row->lang = $this->neocms->get_language($row->lang);
			}

			return $row;
		}else{
			return FALSE;
		}
	}

	public function save($id,$lang) {
		$data = array (
			'icon' => $this->input->post('icon'),
			'pub' => $this->input->post('pub'),
			'main' => $this->input->post('main')
		);
		$this->db->where('id', $id);
		$this->db->update($this->mod_table, $data);

		$data = array(
			'name' => $this->input->post('name'),
			'link' => $this->input->post('link'),
			'description' => $this->input->post('description')
		);

		$this->db->where('id_c', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table.'_content', $data);

		return TRUE;
	}
}
