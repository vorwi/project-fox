<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends MY_Model {

	public function __construct(){
	
		parent::__construct('news_category');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	
 	public function count_all() {
			
		$query = $this->db->get($this->mod_table);   
		return $query->num_rows();
	}
	
	public function get_all() {
		
		 $data = array();   
		 
		 $this->db->order_by('position');	
		 $query = $this->db->get($this->mod_table);

		 return $query->num_rows()>0 ? $query->result() : FALSE;
	}
	
	public function delete($id) {
 		
		if(!is_numeric($id)) $post = $this->input->post('check');
		else $post = $id;

		if(empty($post)) return FALSE;
		 
		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);

		return TRUE;
	}
	
	public function add() {
	
		$this->form_validation->set_rules('name', lang('Nazwa'), 'trim|required');
		return $this->form_validation->run();
	}

	public function insert() { 
	 
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();
		
		$row = $query->row();

		$position = $row->position+1;
		
		$data = array(
				'name' => $this->input->post('name'),
			 	'position' => $position,
			);
		
		$this->db->insert($this->mod_table, $data); 
			 
		return $this->db->affected_rows()==1 ? TRUE : FALSE;
	}
	
	public function catExists($id){
		
		$this->db->where('id',$id);
		$query = $this->db->get($this->mod_table);
		return $query->num_rows()>0 ? TRUE : FALSE;
	}
	
	public function edit($id) {

		$data = array();
		
		if(!is_numeric($id)) $post = $this->input->post('check');
		else $post = $id;
		
		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		return $query->num_rows()>0 ? $query->result() : FALSE;
	}
	
	public function save() {

		$data = array();
		$chenged = array();

		$post = $this->input->post('check');

		foreach($post as $row) {
		
			$data = array(
				'name'=>$this->input->post('name_'.$row), 
			);
			
			$this->db->where('id', $row);
			$query = $this->db->update($this->mod_table, $data);
			
			$changed[] = array('name'=>$this->input->post('name_'.$row));
		}
		
		return $changed;
	}
	
}
