<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends MY_Model {

	public function __construct() {
		parent::__construct('news');
		$this->_init_module_table(__FILE__, __DIR__);

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
		if(!is_dir($path)) {
			@mkdir($path);
			@chmod($path, 0777);
		}
	}
	
	public function get_all_news($id_cat, $limit, $offset = FALSE) {
		
		$this->db->select('tk2.*, tk1.*');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_news', 'left');
		if(is_numeric($id_cat)) {
			$this->db->where('tk1.id_cat', $id_cat);
		}
		$this->db->where('tk2.lang', $this->admin->get_main_lang());
	 	$this->db->order_by('tk2.date_add', 'desc');
		if($limit == 'count') {
			return $this->db->count_all_results();
		} else {
			$this->db->limit($limit, $offset);
			$query = $this->db->get();
		}

		if($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $row){
				$row->art_path = $this->_get_art_cat($row->id_art, '');
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}

	private function _get_art_cat($id_art, $title) {
		$this->db->select('tk1.id, tk1.id_tree, tk1.id_cat, tk2.title');
		$this->db->from('articles as tk1');
		$this->db->join('articles_content as tk2', "tk1.id = tk2.id_art and tk2.lang='".$this->admin->get_main_lang()."'", 'left');
		$this->db->where('tk1.id', $id_art);

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$row = $query->row();

			if(!empty($title))
				$title = $row->title.' / '.$title;
			else
				$title = $row->title;

			if($row->id_tree == 0) {
				$this->db->where('id', $row->id_cat);
				$query = $this->db->get('articles_category');

				$row = $query->row();

				$title = $row->name.' / '.$title;
				return $title;
			} else {

				return $this->_get_art_cat($row->id_tree, $title);
			}
		} else {
			return "[usunięty artykuł nr {$id_art}]";
		}
	}

	public function get_news($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);

		$row = $query->row();

		$path = config_item('upload_path').$this->img_dir;

		$data = array(
			'id' => $id,
			'dir' => '',
			'img' => $row->img,
			'path' => $path
		);

		return $data;
	}

	public function delete($id) {

		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) return FALSE;

		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		foreach($query->result() as $row) {

			$tab = explode('.', $row->img);
			$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;

			@unlink($path.$tab[0].'_thumb.'.$tab[1]);
			@unlink($path.$tab[0].'.'.$tab[1]);

		}

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);

		$this->db->where_in('dest_id', $post);
		$this->db->where('type', 'news');
		$query_uu = $this->db->delete('user_url');
		$this->user_url->update_routes();

		return TRUE;
	}
	
	public function verify_form($type = 'add', $id_art = 0) {
		$this->form_validation->set_rules('id_art');
		
		if($type == 'add') {
			$this->form_validation->set_rules('title', lang('Tytuł'), 'trim|required|htmlspecialchars');
		}
	
		if($type == 'edit') {
			
			foreach($this->languages as $row) {
				$this->form_validation->set_rules('lang['.$row->short.'][title]', lang('Tytuł').' '.lang_flag($row), 'trim|required|htmlspecialchars');
				$this->form_validation->set_rules('lang['.$row->short.'][url]', lang('Zewnętrzny odnośnik').' '.lang_flag($row), 'trim|htmlspecialchars');
				$this->form_validation->set_rules('lang['.$row->short.'][user_url]', lang('Niestandardowy adres URL').' '.lang_flag($row), 'trim|iso_clear');
				$this->form_validation->set_rules('lang['.$row->short.'][meta_title]', lang('Meta Tytuł (title)').' '.lang_flag($row), 'max_length[60]|trim|htmlspecialchars');
				$this->form_validation->set_rules('lang['.$row->short.'][meta_description]', lang('Meta Opis (description)').' '.lang_flag($row), 'max_length[160]|trim');
				$this->form_validation->set_rules('lang['.$row->short.'][meta_keywords]', lang('Meta Słowa kluczowe (keywords)').' '.lang_flag($row), 'trim');
				$this->form_validation->set_rules('lang['.$row->short.'][admission]');
				$this->form_validation->set_rules('lang['.$row->short.'][pub]');
				$this->form_validation->set_rules('lang['.$row->short.'][target]');
				$this->form_validation->set_rules('lang['.$row->short.'][content]', lang('Treść').' '.lang_flag($row), 'trim');
			}

			$this->form_validation->set_rules('pub');
		}
		return $this->form_validation->run();
	}

	public function insert() {
		$img = time();

		$data = array(
			'id_cat' => '1',
			'id_art' => htmlspecialchars($this->input->post('id_art')),
			'img' => $img
		);
		$this->db->insert($this->mod_table, $data);
		
		if($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();
			
			$data = array(
					'id_news' => $id,
					'lang' => $this->admin->get_main_lang(),
					'title' => $this->input->post('title'),
					'author' => $this->session->userdata('user_id'),
					'pub' => 1,
			);
			$this->db->set('date_add', 'NOW()', FALSE);
			$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert($this->mod_table.'_content', $data);
			
			$this->user_url->save_url(false, $this->input->post('title'), 'news', $id, $this->admin->get_main_lang());
			$this->user_url->update_routes();
			
			return $id;
		} else {
			return FALSE;
		}
	}

	public function newsExists($id) {

		$this->db->where('id_news', $id);
		$query = $this->db->get($this->mod_table.'_content');
		return $query->num_rows() > 0;
	}
	
	public function newsLangExists($id, $lang) {
		$this->db->where('id_news', $id);
		$this->db->where('lang', $lang);
		$query = $this->db->get($this->mod_table.'_content');
		return $query->num_rows() > 0;
	}

	public function edit($id) {
		
		//pobranie informacji niezależnych od języka
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);
		
		if($query->num_rows() > 0) {
			$news = $query->row();
				
			//dodanie pól językowych
			$news->lang = array();
				
			$this->db->select('nc.*, l.short as lang, l.main, uu.url as user_url, au.login as author_login, au.name as author_name');
			$this->db->from($this->mod_table.'_content nc');
			$this->db->join('lang l', "nc.lang = l.short AND nc.id_news = '{$id}'", 'RIGHT');
			$this->db->join('user_url uu', "uu.dest_id = nc.id_news AND uu.type = 'news' AND uu.lang = nc.lang", 'LEFT');
			$this->db->join('admin_users au', "nc.author = au.id", 'LEFT');
			$this->db->order_by('l.position');
			$query = $this->db->get();
				
			foreach($query->result() as $nc_row) {
		
				if($nc_row->main) {
					$main_title = $nc_row->title;
					$main_id_news = $nc_row->id_news;
				} else {
					if(empty($nc_row->title)) $nc_row->title = $main_title;
					if(empty($nc_row->id_news)) $nc_row->id_news = $main_id_news;
				}
		
				$ls = $nc_row->lang;
		
				$nc_row->link = news_url($nc_row, FALSE);
				$nc_row->URI = news_url($nc_row);
					
				$news->lang[$ls] = $nc_row;
			}
		
			return $news;
		}
		return FALSE;
	}

	public function save($news) {
		
		$id = $news->id;
		
		$data = array(
				'id_cat' => '1',
				'id_art' => $this->input->post('id_art'),
				'pub' => $this->input->post('pub')
		);

		$this->db->where('id', $id);
		$this->db->update($this->mod_table, $data);
		
		$changes = 0;
		$lang_data = $this->input->post('lang');
		if(is_array($lang_data)) {
			foreach($lang_data as $lang => $fields) {
				$news_lang_row = ifset($news->lang[$lang]);
				$data = array(
						'title' => $fields['title'],
						'url' => $fields['url'],
						'target' => $fields['target'],
						'admission' => $fields['admission'],
						'content' => $fields['content'],
						'meta_title' => $fields['meta_title'],
						'meta_keywords' => $fields['meta_keywords'],
						'meta_description' => $fields['meta_description'],
						'author' => $this->session->userdata('user_id'),
						'pub' => $fields['pub'],
						'date_add' => $fields['date_add']
				);
		
				if(empty($news_lang_row->id)) {
					//wpis dla danego języka nie istnieje w bazie
					$data['id_news'] = $id;
					$data['lang'] = $lang;
					$this->db->set('date_modified', 'NOW()', FALSE);
					$this->db->insert($this->mod_table.'_content', $data);
		
					$this->user_url->save_url(true, $fields['user_url'], 'news', $id, $lang);
					$changes++;
				} else {
					$this->db->where('id_news', $id);
					$this->db->where('lang', $lang);
					$this->db->set('date_modified', 'NOW()', FALSE);
					$this->db->update($this->mod_table.'_content', $data);
		
					$this->user_url->save_url(true, $fields['user_url'], 'news', $id, $lang);
					$changes++;
						
				}
			}
			if($changes > 0) {
				$this->user_url->update_routes();
			}
		}
		
		$this->db->select('img');
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);
		
		$row = $query->row();
		
		$img_tab = explode('.', $row->img);
		$img = $img_tab[0];
		
		if($this->input->post('del_img') == 1) {
			@unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->img);
			@unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$img_tab[0].'_thumb.'.$img_tab[1]);
		}
		
		if(is_uploaded_file($_FILES['img']['tmp_name'])) {
			$config = null;
			$config['upload_path'] = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
		
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $img;
		
			$this->upload->initialize($config);
		
			if(!$this->upload->do_upload('img')){
				$msg[] = array(
						"Obrazek nie został wgrany poprawnie: ".$this->upload->display_errors('', ''),
						2
				);
			}else {
				$tab = $this->upload->data();
		
				$dir = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
		
				@chmod($tab['full_path'], 0777);
		
				$name = $tab['raw_name'];
		
				$config = null;
				$config['source_image'] = $tab['full_path'];
				$config['new_image'] = $dir.$name.$tab['file_ext'];
		
				$config['create_thumb'] = FALSE;
		
				$config['maintain_ratio'] = TRUE;
				$size = getimagesize($tab['full_path']);
				if($size[0] < $this->img_width){
					$config['width'] = $size[0];
				}else{
					$config['width'] = $this->img_width;
				}
				if($size[1] < $this->img_height){
					$config['height'] = $size[1];
				}else{
					$config['height'] = $this->img_height;
				}
		
				$config['master_dim'] = 'width';
		
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$msg[] = array(
							"Plik <b>$value</b> nie został wgrany poprawnie: ".$this->image_lib->display_errors('', ''),
							2
					);
				}
				$this->image_lib->clear();
		
				$config = null;
				$config['source_image'] = $tab['full_path'];
				$config['new_image'] = $dir.$name.$tab['file_ext'];
				$config['create_thumb'] = TRUE;
		
				$config['maintain_ratio'] = TRUE;
				if($size[0] < $this->thumb_width){
					$config['width'] = $size[0];
				}else{
					$config['width'] = $this->thumb_width;
				}
				if($size[1] < $this->thumb_height){
					$config['height'] = $size[1];
				}else{
					$config['height'] = $this->thumb_height;
				}
		
				$config['master_dim'] = 'width';
		
				$this->image_lib->initialize($config);
		
				if(!$this->image_lib->resize()){
					$msg[] = array(
							"Plik miniaturki <b>$value</b> nie został wgrany poprawnie: ".$this->image_lib->display_errors('', ''),
							2
					);
				}else {
					$data = array('img' => $img.$tab['file_ext']);
		
					$this->db->where('id', $id);
					$this->db->update($this->mod_table, $data);
				}
		
				$this->image_lib->clear();
			}
		}
		
		return TRUE;
	}

	public function crop($id) {
		$data = $this->get_news($id);

		$dir = $data['dir'];
		$img = $data['img'];

		$tab = explode('.', $img);

		$img_save = $tab[0].'_thumb.'.$tab[1];

		$coords['scale'] = $this->input->post('scale');
		$coords['x_axis'] = $this->input->post('x_axis');
		$coords['y_axis'] = $this->input->post('y_axis');
		$coords['width'] = $this->input->post('width');
		$coords['height'] = $this->input->post('height');
		$coords['thumb_width'] = $this->thumb_width;
		$coords['thumb_height'] = $this->thumb_height;

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;

		$this->image_lib->crop_thumb($path, $img, $path_save = null, $img_save, $coords);
	}
}
