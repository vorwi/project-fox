<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends MY_Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function bad_login_count() {
		return $this->session->userdata('login_try');
	}
	
	public function bad_remind_count() {
		return $this->session->userdata('remind_try');
	}
	
	public function lock_login($bl_count) {
		if($bl_count==0) {
			return false;
		}else {
			if($bl_count < config_item('bad_login_limit')) {
				return false;
			}else {
				if(!$this->session->userdata('login_lock')) $this->session->set_userdata('login_lock', time());
				return true;
			}
		}
	}
	
	public function lock_remind($br_count) {
		if($br_count==0) {
			return false;
		}else {
			if($br_count < config_item('bad_remind_limit')) {
				return false;
			}else {
				if(!$this->session->userdata('remind_lock')) $this->session->set_userdata('remind_lock', time());
				return true;
			}
		}
	}
	
	public function lock_login_left() {
		$howlong = config_item('login_lock_time');
		$now = time();
		$start = $this->session->userdata('login_lock');

		if(($now-$start)>=$howlong) {
			return 0;
		}else {
			return $howlong-($now-$start);
		}
	}
	
	public function lock_remind_left() {
		$howlong = config_item('remind_lock_time');
		$now = time();
		$start = $this->session->userdata('remind_lock');
	
		if(($now-$start)>=$howlong) {
			return 0;
		}else {
			return $howlong-($now-$start);
		}
	}
	
	public function remove_login_lock() {
		$this->session->unset_userdata('login_lock');
		$this->session->unset_userdata('login_try');
	}
	
	public function remove_remind_lock() {
		$this->session->unset_userdata('remind_lock');
		$this->session->unset_userdata('remind_try');
	}
	
	public function bad_remind_counter() {
		if($this->session->userdata('remind_try')) {
			$rt = $this->session->userdata('remind_try');
			$rt++;
			$this->session->set_userdata('remind_try',$rt);
		}else{
			$this->session->set_userdata('remind_try',1);
		}
	}
	
	public function check_rem() {
		$this->form_validation->set_rules('rem_email', lang('email'), 'trim|required|valid_email');
		$this->form_validation->set_rules('rem_login', lang('login'), 'trim|required');
		$this->form_validation->set_rules('captcha', lang('kod'), 'trim|contact_captcha_check|required');
		 
		return $this->form_validation->run();
	}
	
	public function remind() {
	
		$hash = random_string('md5');
		 
		$email = $this->input->post('rem_email');
		$login = $this->input->post('rem_login');
		
		$this->db->where('email', $email);
		$this->db->where('login', $login);
		$query = $this->db->update('admin_users', array('hash' => $hash));
	
		if($query && $this->db->affected_rows() == 1){
			$html = lang('Jeżeli chcesz potwierdzić resetowanie hasła, kliknij na poniższy link:').'<br /> ';
			$html .= "<a href=\"http://{$_SERVER['HTTP_HOST']}/admin/confirm/$hash\">http://{$_SERVER['HTTP_HOST']}/admin/confirm/$hash</a><br /><br />".lang("W przeciwnym wypadku zignoruj ten e-mail.");
		
			$body = $this->email->full_html(lang('Resetowanie hasła'), $html);
			$this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
			$this->email->to($email);
	
			$this->email->subject(lang('Resetowanie hasła na:').' '.$_SERVER['HTTP_HOST']);
			$this->email->message($body);
			if($this->email->send()) {
				return true;
			}
		}
		return false;
	}
	
	public function confirmExists($hash=null){
		if($hash===null) {
			return FALSE;
		}
	
		$hash = trim($hash);
	
		$this->db->where('hash',$hash);
		$query = $this->db->get('admin_users');
	
		return $query->num_rows()==0 ? FALSE : TRUE;
	}
	
	public function confirm($hash=null){
		if($hash===null) {
			return FALSE;
		}
		$hash = trim($hash);
		
		$this->db->trans_begin();
		
		$this->db->select('login, email, date_add');
		$this->db->where('hash',$hash);
		$query_email = $this->db->get('admin_users');
		$user = $query_email->row();
		
		$pass = password_generator(null, $user->date_add);
	
		$this->db->set(array(
			'hash' => '',
			'password' => $pass['pass_crypt']
		));
		$this->db->where('hash', $hash);
		$query = $this->db->update('admin_users');
		
		if($query && $this->db->trans_status() === TRUE) {
			$html = lang("Twoje dane do panelu CMS:")."<br /><br />".lang("Adres logowania:")."  http://{$_SERVER['HTTP_HOST']}/admin<br />".lang("login").": {$user->login}<br />".lang("hasło").": {$pass['pass']}<br />";
			
			$body = $this->email->full_html(lang('Nowe Hasło do panelu CMS'), $html);
			$this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
			$this->email->to($user->email);
	
			$this->email->subject(lang('Nowe hasło do panelu CMS').' '.$_SERVER['HTTP_HOST']);
			$this->email->message($body);
			
			if($this->email->send()) {
				$this->db->trans_commit();
				return TRUE;
			}
		}
		$this->db->trans_rollback();
		return FALSE;
	}
}

