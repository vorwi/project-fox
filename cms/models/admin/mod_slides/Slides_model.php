<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Slides_model extends MY_Model {

	public function __construct() {
		parent::__construct('slides');
		$this->_init_module_table(__FILE__, __DIR__);

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
		if(!is_dir($path)) {
			@mkdir($path);
			@chmod($path, 0777);
		}
	}

	public function get_all() {
		$this->db->order_by('position');
		$this->db->order_by('id_art');
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $row) {
				$row->path = $this->_get_art_cat($row->id_art, '');
				$data[] = $row;
			}

			return $data;
		} else
			return FALSE;
	}

	private function _get_art_cat($id_art, $title) {
		$this->db->select('tk1.id, tk1.id_tree, tk1.id_cat, tk2.title');
		$this->db->from('articles as tk1');
		$this->db->join('articles_content as tk2', "tk1.id = tk2.id_art and tk2.lang='".$this->admin->get_main_lang()."'");
		$this->db->where('tk1.id', $id_art);

		$query = $this->db->get();

		$row = $query->row();

		if(!empty($title))
			$title = $row->title.' / '.$title;
		else
			$title = $row->title;

		if($row->id_tree == 0) {
			$this->db->where('id', $row->id_cat);
			$query = $this->db->get('articles_category');

			$row = $query->row();

			$title = $row->name.' / '.$title;
			return $title;
		} else {
			return $this->_get_art_cat($row->id_tree, $title);
		}
	}

	public function get_image($id) {
		$data = array();

		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table.'_img');

		if($query->num_rows() > 0) {
			$data = $query->row();
		} else
			return FALSE;

		$this->db->where('id', $data->id_gal);
		$query = $this->db->get($this->mod_table);
		$row = $query->row();

		$data->dir = $row->dir;
		$data->path = config_item('upload_path').$this->img_dir.$data->dir.'/';

		return (array)$data;
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post))
			return FALSE;

		$this->db->where_in('id_gal', $post);
		$query = $this->db->get($this->mod_table.'_img');

		foreach($query->result() as $row)
			$this->del_img($row->id);

		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		foreach($query->result() as $row) {

			$dir = './'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->dir.'/';

			@delete_files($dir, TRUE);
			@rmdir($dir);
		}

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);

		return TRUE;
	}

	public function add() {
		$this->form_validation->set_rules('title', lang('Nazwa'), 'trim|required');
		$this->form_validation->set_rules('id_art', lang('Artykuł'), 'required');
		$this->form_validation->set_rules('lang');

		return $this->form_validation->run();
	}

	public function insert() {
		$this->db->set('position', 'position+1', FALSE);
		$this->db->update($this->mod_table);

		$dir = time();

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;

		mkdir($path.$dir);
		@chmod($path, 0777);

		if(is_dir($path)) {
			$data = array(
				'id_art' => $this->input->post('id_art'),
				'title' => $this->input->post('title'),
				'lang' => $this->input->post('lang'),
				'dir' => $dir,
				'pub' => 1,
				'position' => 0
			);

			$this->db->insert($this->mod_table, $data);

			return $this->db->affected_rows() == 1;
		} else
			return FALSE;
	}

	public function edit($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $row) {
				if(!empty($row->lang)) {
					$this->db->where('short', $row->lang);
					$query2 = $this->db->get('lang');

					$row->lang = $query2->row();
				}

				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}

	public function save() {
		$data = array();
		$changed = array();

		$post = $this->input->post('check');

		foreach($post as $row) {
			$data = array(
				'title' => htmlspecialchars($this->input->post('title_'.$row)),
				'lang' => $this->input->post('lang_'.$row),
				'id_art' => $this->input->post('id_art_'.$row)
			);
			$this->db->where('id', $row);
			$query = $this->db->update($this->mod_table, $data);

			$changed[] = array('title' => $this->input->post('title_'.$row));
		}

		return $changed;
	}

	public function get_all_images($id) {
		$data = array();

		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);

		$row = $query->row();
		$dir = $row->dir;

		$this->db->where('id_gal', $id);
		$this->db->order_by('position', $id);
		$query = $this->db->get($this->mod_table.'_img');

		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$row->dir = $dir;
				$data[] = $row;
			}

			return $data;
		} else
			return FALSE;
	}

	public function add_img_pl($id) {
		$msg = array();
		$config = null;
		$img = md5(microtime());

		$dir_tmp = './'.config_item('temp').'bg_'.time().'_'.$id.'/';
		@mkdir($dir_tmp);
		@chmod($dir_tmp, 0777);
		if(!is_dir($dir_tmp))
			return FALSE;

		$this->db->select('dir');
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);
		$row = $query->row();
		$gal_dir = $row->dir;

		$config['upload_path'] = $dir_tmp;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $img;

		$this->upload->initialize($config);

		if(is_uploaded_file($_FILES['file']['tmp_name'])) {
			if(!$this->upload->do_upload('file')) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
			} else {

				$tab = $this->upload->data();

				$map_path = $tab['file_path'];

				$map = array($tab['file_name']);
				sort($map);

				if(!empty($map)) {
					if(!$this->save_thumbnails($id, $map, $map_path)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
					}
				}
			}

			return TRUE;
		} else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
		}
	}

	public function save_thumbnails($id, $dir = null, $path = null) {
		$msg = array();

		if($dir == null || $path == null || empty($dir))
			return FALSE;

		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);

		$row = $query->row();
		$dir_gal = $row->dir;

		$i = 0;
		foreach($dir as $value) {
			$file = $path.'/'.$value;

			$tab = pathinfo($file);

			$dir = './'.config_item('site_path').config_item('upload_path').$this->img_dir.$dir_gal.'/';

			$name = md5(microtime());

			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $dir.$name.'.'.$tab['extension'];
			$config['create_thumb'] = FALSE;

			$config['maintain_ratio'] = TRUE;

			$size = getimagesize($file);

			if($size[0] < $this->img_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->img_width;
			if($size[1] < $this->img_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->img_height;

			if($this->master_dim == 'auto') {
				//jeśli zdjęcie jest pionowe, to będzie dopasowane do wysokości; jeśli poziome, to do szerokości
				if($size[0] < $size[1]) {
					$config['master_dim'] = 'height';
				} else {
					$config['master_dim'] = 'width';
				}
			} else {
				$config['master_dim'] = $this->master_dim;
			}

			$this->image_lib->initialize($config);
			$this->image_lib->error_msg = array();
			if(!$this->image_lib->resize())
				$msg[] = array(sprintf(lang("Plik <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')),2);
			$this->image_lib->clear();

			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $dir.$name.'_thumb.'.$tab['extension'];
			$config['create_thumb'] = FALSE;
			//$config['maintain_ratio'] = FALSE;
			$config['maintain_ratio'] = TRUE;

			$size = getimagesize($dir.$name.'.'.$tab['extension']);
			if($size[0] < $this->thumb_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->thumb_width;
			if($size[1] < $this->thumb_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->thumb_height;

			//sprawdzenie, czy zdjęcie trzeba przyciąć z boków, czy z góry
			if(($size[1] * $config['width']) / $size[0] < $config['height']) {
				$config['master_dim'] = 'height';
			} else {
				$config['master_dim'] = 'width';
			}

			$this->image_lib->initialize($config);
			$this->image_lib->error_msg = array();
			if(!$this->image_lib->resize())
				$msg[] = array(sprintf(lang("Plik miniaturki <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')),2);

			$config = null;
			$config['source_image'] = $dir.$name.'_thumb.'.$tab['extension'];
			$size = getimagesize($dir.$name.'_thumb.'.$tab['extension']);
			$config['maintain_ratio'] = FALSE;
			if($size[0] < $this->thumb_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->thumb_width;
			if($size[1] < $this->thumb_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->thumb_height;
			$config['x_axis'] = 0;
			$config['y_axis'] = 0;
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();

			$this->db->select('max(position) as p');
			$this->db->where('id_gal', $id);
			$query = $this->db->get($this->mod_table.'_img');

			$row = $query->row();

			$position = $row->p + 1;

			$data = array(
				'id_gal' => $id,
				'img' => $name.'.'.$tab['extension'],
				'position' => $position,
			);

			$this->db->insert($this->mod_table.'_img', $data);

			@unlink($file);
			$i++;
		}

		delete_files($path, TRUE);
		rmdir($path);

		if(count($msg) > 0)
			$this->session->set_flashdata('msg_files', $msg);

		return TRUE;
	}

	public function del_img($id) {
		$data = array();

		$this->db->select('tk1.img, tk2.dir');
		$this->db->from($this->mod_table.'_img as tk1');
		$this->db->where('tk1.id', $id);
		$this->db->join($this->mod_table.' as tk2', 'tk1.id_gal=tk2.id');
		$query = $this->db->get();

		$row = $query->row();

		$tab = explode('.', $row->img);
		@unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->dir.'/'.$row->img);
		@unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->dir.'/'.$tab[0].'_thumb.'.$tab[1]);
		$this->db->delete('slides_img', array('id' => $id));
		return $this->db->affected_rows() > 0;
	}

	public function img_edit($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table.'_img');

		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}

	public function crop($id) {
		$data = $this->get_image($id);

		$dir = $data['dir'];
		$img = $data['img'];

		$tab = explode('.', $img);

		$img_save = $tab[0].'_thumb.'.$tab[1];

		$coords['x_axis'] = $this->input->post('x_axis');
		$coords['y_axis'] = $this->input->post('y_axis');
		$coords['width'] = $this->input->post('width');
		$coords['height'] = $this->input->post('height');
		$coords['thumb_width'] = $this->thumb_width;
		$coords['thumb_height'] = $this->thumb_height;

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir.$dir.'/';

		$this->image_lib->crop_thumb($path, $img, $path_save = null, $img_save, $coords);
	}

	public function update_img($id) {
		$this->db->where('id', $id);
		$query = $this->db->update($this->mod_table.'_img', array('desc' => $this->input->post('desc')));
		return TRUE;
	}

}
