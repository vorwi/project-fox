<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacts_model extends MY_Model {

	public function __construct() {
		parent::__construct('estatedev_contacts');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all() {

		$this->db->select('tk1.*, tk2.name, tk2.surname, tk2.lang');
		$this->db->select('GROUP_CONCAT(tk4.name SEPARATOR \', \') AS selected_types', false);
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edc');
		$this->db->join($this->mod_table . '_join_types AS tk3', 'tk1.id = tk3.id_edc', 'left');
		$this->db->join('estatedev_types_content AS tk4', 'tk4.id_edt = tk3.id_edt AND tk4.lang = \''.$this->admin->get_main_lang().'\'', 'left');
		$this->db->where('tk2.lang', $this->admin->get_main_lang());
		$this->db->order_by('tk2.surname, tk2.name');
		$this->db->group_by('tk1.id');
		$query = $this->db->get();

		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function get_all_types($idc = null) {

		$this->db->select('tk1.*, tk2.name, tk2.lang');
		$this->db->from('estatedev_types AS tk1');
		$this->db->join('estatedev_types_content AS tk2', 'tk1.id = tk2.id_edt');
		if (is_numeric($idc)) {
			$this->db->select('IF(tk3.id_edc IS NOT NULL, 1, 0) AS selected', FALSE);
			$this->db->join($this->mod_table . '_join_types AS tk3', 'tk3.id_edt = tk1.id AND tk3.id_edc = ' . $idc, 'left');
		}
		$this->db->where('tk2.lang', $this->admin->get_main_lang());
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function exists($id = null, $lang = null) {
		if ($id !== null) {
			$this->db->select('tk1.id');
			$this->db->from($this->mod_table . ' AS tk1');
			$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edc');
			$this->db->where('tk1.id', $id);
			if ($lang !== null) {
				$this->db->where('tk2.lang', $lang);
			}
			$query = $this->db->get();
			
			return $query->num_rows() > 0 ? TRUE : FALSE;
		} else {
			return false;
		}
	}

	public function verify_form() {
		$this->form_validation->set_rules('name', lang('Imię'), 'required|trim');
		$this->form_validation->set_rules('surname', lang('Nazwisko'), 'required|trim');
		$this->form_validation->set_rules('email', lang('E-mail'), 'trim');
		$this->form_validation->set_rules('phone', lang('Numer telefonu'), 'trim');
		$this->form_validation->set_rules('office', lang('Stanowisko'), 'trim');
		$this->form_validation->set_rules('image');

		return $this->form_validation->run();

	}

	public function insert() {

		$data = array(
			'pub' => 1,
			'image' => $this->input->post('image')
		);
		$this->db->set('add_date', 'NOW()', FALSE);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->insert($this->mod_table, $data);

		if ($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();

			$data = array(
				'id_edc' => $id,
				'lang' => $this->admin->get_main_lang(),
				'name' => trim($this->input->post('name')),
				'surname' => trim($this->input->post('surname')),
				'email' => trim($this->input->post('email')),
				'phone' => trim($this->input->post('phone')),
				'office' => trim($this->input->post('office'))
			);
			$this->db->insert($this->mod_table . '_content', $data);

			if ($this->db->affected_rows() == 1 && $this->input->post('type') && is_array($this->input->post('type'))) {
				foreach ($this->input->post('type') as $pt) {
					$data = array(
						'id_edc' => $id,
						'id_edt' => $pt
					);
					$this->db->insert($this->mod_table . '_join_types', $data);
				}
			}
			return $this->db->affected_rows() == 1 ? TRUE : FALSE;

		} else {
			return false;
		}
	}

	public function delete($id) {
		
		$post = !is_numeric($id) ? $this->input->post('check') : $id;
		
		if(empty($post)) return FALSE;
		
		$affected = 0;

		$this->db->where_in('id_edc', $post);
		$this->db->delete($this->mod_table . '_content');
		$affected += $this->db->affected_rows();
				
		$this->db->where_in('id_edc', $post);
		$this->db->delete($this->mod_table . '_join_types');
		$affected += $this->db->affected_rows();
	
		$this->db->where_in('id', $post);
		$this->db->delete($this->mod_table);
		$affected += $this->db->affected_rows();

		return $affected > 0 ? TRUE : FALSE;
	}

	public function edit($id, $lang) {
		if (!$this->exists($id, $lang)) {
			$this->db->where('id_edc', $id);
			$this->db->where('lang', $this->admin->get_main_lang());
			$query = $this->db->get($this->mod_table . '_content');
			$row = $query->row();

			$data = array(
				'id_edc' => $id,
				'lang' => $lang,
				'name' => $row->name,
				'surname' => $row->surname,
				'email' => $row->email,
				'phone' => $row->phone,
				'office' => $row->office
			);
			//$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert($this->mod_table . '_content', $data);
		}

		$this->db->where('id_edc', $id);
		$this->db->where('lang', $lang);
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edc');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$type = $query->row();
			if (!empty($type->lang)) {
				$this->db->where('short', $type->lang);
				$query = $this->db->get('lang');
				$type->lang = $query->row();
			}
			return $type;
		}
		return FALSE;
	}

	public function save($id, $lang) {
		$data = array(
			'pub' => $this->input->post('pub'),
			'image' => $this->input->post('image')
		);
		$this->db->where('id', $id);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->update($this->mod_table, $data);

		$data = array(
			'name' => htmlspecialchars($this->input->post('name')),
			'surname' => htmlspecialchars($this->input->post('surname')),
			'email' => htmlspecialchars($this->input->post('email')),
			'phone' => htmlspecialchars($this->input->post('phone')),
			'office' => htmlspecialchars($this->input->post('office'))
		);
		$this->db->where('id_edc', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table . '_content', $data);
		
		$this->db->where('id_edc', $id);
		$this->db->delete($this->mod_table . '_join_types');

		if ($this->input->post('type') && is_array($this->input->post('type'))) {
			
			foreach ($this->input->post('type') as $pt) {
				$data = array(
					'id_edc' => $id,
					'id_edt' => $pt
				);
				$this->db->insert($this->mod_table . '_join_types', $data);
			}
		}

		return TRUE;
	}

}
