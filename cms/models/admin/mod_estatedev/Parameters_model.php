<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Parameters_model extends MY_Model {

	public function __construct() {
		parent::__construct('estatedev_parameters');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_field_types() {
		$type = $this->db->query('SHOW COLUMNS FROM ' . $this->db->dbprefix($this->mod_table) . ' WHERE Field = \'type\'')->row(0)->Type;
		preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
		$types = explode("','", $matches[1]);
		return $types;
	}

	public function get_all() {
		$data = false;

		$this->db->select('tk1.*, tk2.name, tk2.lang');
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edp');
		$this->db->where('tk2.lang', config_item('main_lang'));
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->result();
		}

		return $data;
	}

	public function get_for_estate($id = null) {
		$data = false;

		$this->db->select('tk1.*, tk2.name, GROUP_CONCAT(tk3.id_edepv SEPARATOR \'|\') AS id_edepv, tk3.text_value', false);
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edp', 'inner');
		$this->db->join($this->mod_table . '_join_estates AS tk3', 'tk3.id_edp = tk1.id AND tk3.id_ede = ' . $id . '', 'left');
		$this->db->where('tk2.lang', config_item('main_lang'));
		$this->db->order_by('tk1.position');
		$this->db->group_by('tk1.id');
		$parameters = $this->db->get();

		if ($parameters->num_rows() > 0) {
			$data = array();
			foreach ($parameters->result() as $param) {
				$param->id_edepv = explode('|', $param->id_edepv);
				$this->db->select('tk1.*, tk2.value', false);
				$this->db->from($this->mod_table . '_values AS tk1');
				$this->db->join($this->mod_table . '_values_content AS tk2', 'tk1.id = tk2.id_edepv', 'inner');
				$this->db->where('tk2.lang', config_item('main_lang'));
				$this->db->where('tk1.id_edep', $param->id);
				$this->db->order_by('tk1.position');
				$values = $this->db->get();

				if ($values->num_rows() > 0) {
					$param->values = $values->result();
				} else {
					$param->values = false;
				}
				$data[] = $param;
			}
		}

		return $data;
	}

	public function exists($id = null, $lang = null) {
		if ($id !== null) {
			$this->db->select('tk1.id');
			$this->db->from($this->mod_table . ' AS tk1');
			$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edp');
			$this->db->where('tk1.id', $id);
			if ($lang !== null) {
				$this->db->where('tk2.lang', $lang);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function insert_validate() {
		$this->form_validation->set_rules('name', lang('Nazwa atrybutu nieruchomości'), 'required');

		if ($this->form_validation->run() === FALSE) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();
		$row = $query->row();
		$position = $row->position + 1;

		$data = array(
			'position' => $position,
			'type' => $this->input->post('type'),
			'searchable' => $this->input->post('searchable'),
			'in_table' => $this->input->post('in_table'),
			'unit' => $this->input->post('unit'),
			'icon' => $this->input->post('icon'),
			'icon_button' => $this->input->post('icon_button'),
			'predefined_parameter' => $this->input->post('predefined_parameter'),
			'presentation' => $this->input->post('presentation'),
			'in_name' => $this->input->post('in_name'),
			'in_name_show' => $this->input->post('in_name_show')
		);
		$this->db->set('add_date', 'NOW()', FALSE);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->insert($this->mod_table, $data);

		if ($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();
			$title = trim($this->input->post('name'));

			$data = array(
				'id_edp' => $id,
				'lang' => config_item('main_lang'),
				'name' => trim($this->input->post('name')),
			);
			$this->db->insert($this->mod_table . '_content', $data);

			if ($this->db->affected_rows() == 1) {

				$this->user_url->save_url(false,$title,'estatedev_parameters',$id,config_item('main_lang'));
				$this->user_url->update_routes();

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function delete($id = null) {
		if ($id !== null) {
			$affected = 0;

			if (is_numeric($id)) {
				$this->db->where('id_edp', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id_edp', $id);
			}
			$this->db->delete($this->mod_table . '_content');
			$affected += $this->db->affected_rows();

			if (is_numeric($id)) {
				$this->db->where('id_edep', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id_edep', $id);
			}
			$values = $this->db->get($this->mod_table . '_values');
			$values_do_delete = array();
			if ($values->num_rows() > 0) {
				foreach ($values->result() as $v) {
					$values_do_delete[] = $v->id;
				}
			}
			if (!empty($values_do_delete)) {
				$this->db->where_in('id_edepv', $values_do_delete);
				$this->db->delete($this->mod_table . '_values_content');
				$affected += $this->db->affected_rows();

				$this->db->where_in('id', $values_do_delete);
				$this->db->delete($this->mod_table . '_values');
				$affected += $this->db->affected_rows();
			}

			if (is_numeric($id)) {
				$this->db->where('id', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id', $id);
			}
			$this->db->delete($this->mod_table);
			$affected += $this->db->affected_rows();

			if (is_numeric($id)) {
				$this->db->where('id_edp', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id_edp', $id);
			}
			$this->db->delete('estatedev_parameters_join_estates');
			$affected += $this->db->affected_rows();
			
			$this->db->where_in('dest_id', $id);
			$this->db->where('type', 'estatedev_parameters');
			$query_uu = $this->db->delete('user_url');
			$this->user_url->update_routes();

			if ($affected > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function edit($id, $lang) {
		if (!$this->exists($id, $lang)) {
			$this->db->where('id_edp', $id);
			$this->db->where('lang', config_item('main_lang'));
			$query = $this->db->get($this->mod_table . '_content');
			$row = $query->row();

			$data = array(
				'id_edp' => $id,
				'lang' => $lang,
				'name' => $row->name,
			);
			//$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert($this->mod_table . '_content', $data);
			
			$this->user_url->save_url(false,$row->name,'estatedev_parameters',$id,$lang);
			$this->user_url->update_routes();
		}

		$this->db->select('tk1.*,tk2.*,uu.url');
		$this->db->from('estatedev_parameters AS tk1');
		$this->db->join('estatedev_parameters_content AS tk2', 'tk1.id = tk2.id_edp');
		$this->db->join('user_url uu','uu.dest_id=tk1.id','left');
		$this->db->where('tk2.id_edp', $id);
		$this->db->where('tk2.lang', $lang);
		$this->db->where('uu.type','estatedev_parameters');
		$this->db->where('uu.lang',$lang);
		
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$type = $query->row();
			if (!empty($type->lang)) {
				$this->db->where('short', $type->lang);
				$query = $this->db->get('lang');
				$type->lang = $query->row();
			}

			$this->db->where('id_edep', $id);
			$this->db->from($this->mod_table . '_values');
			$this->db->order_by('position');
			$values = $this->db->get();
			if ($values->num_rows() > 0) {
				foreach ($values->result() as $value) {
					$this->db->where('id_edepv', $value->id);
					$this->db->from($this->mod_table . '_values_content');
					$values_content = $this->db->get();
					if ($values_content->num_rows() > 0) {
						foreach ($values_content->result() as $value_content) {
							$value->content[$value_content->lang] = $value_content;
						}
					} else {
						$value->content = false;
					}
					$type->values[] = $value;
				}
			} else {
				$type->values = false;
			}

			return $type;
		}
		return FALSE;
	}

	public function save($id, $lang, $all_lang) {
		$data = array(
			'type' => $this->input->post('type'),
			'searchable' => $this->input->post('searchable'),
			'in_table' => $this->input->post('in_table'),
			'unit' => $this->input->post('unit'),
			'icon' => $this->input->post('icon'),
			'icon_button' => $this->input->post('icon_button'),
			'predefined_parameter' => $this->input->post('predefined_parameter'),
			'presentation' => $this->input->post('presentation'),
			'in_name' => $this->input->post('in_name'),
			'in_name_show' => $this->input->post('in_name_show')
		);
		$this->db->where('id', $id);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->update($this->mod_table, $data);

		$data = array(
			'name' => htmlspecialchars($this->input->post('name'))
		);
		$this->db->where('id_edp', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table . '_content', $data);

		$this->db->where('id_edep', $id);
		$this->db->from($this->mod_table . '_values');
		$values = $this->db->get();
		$post_value = $this->input->post('value');
		$post_position = $this->input->post('position');
		if ($values->num_rows() > 0) {
			foreach ($values->result() as $value) {
				if (isset($post_value[$value->id])) {
					if (isset($post_value[$value->id][$lang])) {
						$this->db->where('id_edepv', $value->id);
						$this->db->where('lang', $lang);
						$row = $this->db->get($this->mod_table . '_values_content');
						if ($row->num_rows() > 0) {
							// aktualizacja istniejących tłumaczeń wartości
							$this->db->where('id_edepv', $value->id);
							$this->db->where('lang', $lang);
							$this->db->update($this->mod_table . '_values_content', array('value' => $post_value[$value->id][$lang]));
						} else {
							// wstawienie nowych tłumaczeń wartości
							$data = array(
								'value' => $post_value[$value->id][$lang],
								'id_edepv' => $value->id,
								'lang' => $lang
							);
							$this->db->insert($this->mod_table . '_values_content', $data);
						}
					} else {
						
					}

					$this->db->where('id', $value->id);
					$data = array(
						'position' => $post_position[$value->id]
					);
					$this->db->update($this->mod_table . '_values', $data);
				} else {
					// usunięcie zbędnych tłumaczeń wartości i samych wartości z bazy (są w bazie ale nie ma w post)
					$this->db->where('id_edepv', $value->id);
					$this->db->delete($this->mod_table . '_values_content');
					$this->db->where('id', $value->id);
					$this->db->delete($this->mod_table . '_values');
				}
				unset($post_value[$value->id]);
			}
		}

		if (isset($post_value) && is_array($post_value) && count($post_value) > 0) {
			// wstawienie nowych wartości i ich tłumaczeń
			foreach ($post_value as $val_key => $val) {
				$data = array(
					'id_edep' => $id,
					'position' => $post_position[$val_key]
				);
				$this->db->insert($this->mod_table . '_values', $data);
				$val_id = $this->db->insert_id();

				foreach ($val as $lang_val => $val_content) {
					$data = array(
						'value' => $val_content,
						'id_edepv' => $val_id,
						'lang' => $lang
					);
					$this->db->insert($this->mod_table . '_values_content', $data);

					// dodanie pustej wartości w innych językach
					foreach ($all_lang as $al) {
						if ($al->short !== $lang) {
							$data = array(
								'value' => '',
								'id_edepv' => $val_id,
								'lang' => $al->short
							);
							$this->db->insert($this->mod_table . '_values_content', $data);
						}
					}
				}
			}
		}
		
		$this->user_url->save_url(true,$this->input->post('name'),'estatedev_parameters',$id,$lang);
		$this->user_url->update_routes();

		return TRUE;
	}

}
