<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Investments_model extends MY_Model {

	public function __construct() {
		parent::__construct('estatedev_investments');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all() {
		$data = false;

		$this->db->select('tk1.*, tk2.name, tk2.lang');
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edi');
		$this->db->where('tk2.lang', $this->admin->get_main_lang());
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->result();
		}

		return $data;
	}

	public function get($id, $lang = null) {
		if ($lang === null) {
			$lang = $this->admin->get_main_lang();
		}
		$this->db->where('id_edi', $id);
		$this->db->where('lang', $lang);
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edi');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$item = $query->row();
			if (!empty($item->lang)) {
				$this->db->where('short', $item->lang);
				$query = $this->db->get('lang');
				$item->lang = $query->row();
			}
			return $item;
		}
		return FALSE;
	}

	public function exists($id = null, $lang = null) {
		if ($id !== null) {
			$this->db->select('tk1.id');
			$this->db->from($this->mod_table . ' AS tk1');
			$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edi');
			$this->db->where('tk1.id', $id);
			if ($lang !== null) {
				$this->db->where('tk2.lang', $lang);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function insert_validate() {
		$this->form_validation->set_rules('name', lang('Nazwa'), 'required');

		if ($this->form_validation->run() === FALSE) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();
		$row = $query->row();
		$position = $row->position + 1;

		$data = array(
			'position' => $position,
			'pub' => 1,
			'map_coords' => '(53.7742654, 20.4698676)'
		);
		$this->db->set('add_date', 'NOW()', FALSE);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->insert($this->mod_table, $data);

		if ($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();

			$data = array(
				'id_edi' => $id,
				'lang' => $this->admin->get_main_lang(),
				'name' => trim($this->input->post('name'))
			);
			$this->db->insert($this->mod_table . '_content', $data);

			if ($this->db->affected_rows() == 1) {
				
				$this->user_url->save_url(false,$this->input->post('name'),'estatedev_investments',$id,$this->admin->get_main_lang());
				$this->user_url->update_routes();
				$this->_check_dir($id);
				return $id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function buildings_pub($set = null, $id = null) {
		if ($set !== null && $id !== null) {
			if (is_numeric($id)) {
				$this->db->where('id', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id', $id);
			}
			$this->db->update($this->mod_table . '_buildings', array('pub' => $set));

			if ($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function buildings_save_position() {
		$values = $this->input->post('value');
		if (is_array($values)) {
			foreach ($values as $id => $val) {
				$data = array(
					'position' => $val
				);
				$this->db->where('id', $id);
				$this->db->update($this->mod_table . '_buildings', $data);
			}
		}
		return true;
	}

	public function delete($id = null) {
		if ($id !== null) {
			$affected = 0;

			if (is_numeric($id)) {
				$this->db->where('id', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id', $id);
			}
			$this->db->delete($this->mod_table);
			$affected += $this->db->affected_rows();

			if (is_numeric($id)) {
				$this->db->where('id_edi', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id_edi', $id);
			}
			$this->db->delete($this->mod_table . '_content');
			$affected += $this->db->affected_rows();

			$dir = './' . config_item('site_path') . config_item('upload_path') . $this->gal_dir . '/' . $id . '/';
			if (is_dir($dir)) {
				rrmdir($dir);
			}
			
			$this->db->where_in('dest_id', $id);
			$this->db->where('type', 'estatedev_investments');
			$query_uu = $this->db->delete('user_url');
			$this->user_url->update_routes();

			if ($affected > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function buildings_delete($edib = null) {
		if ($edib !== null) {
			$affected = 0;

			if (is_numeric($edib)) {
				$this->db->where('id', $edib);
			} elseif (is_array($edib)) {
				$this->db->where_in('id', $edib);
			}
			$this->db->delete($this->mod_table . '_buildings');
			$affected += $this->db->affected_rows();

			if ($affected > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function buildings_face_maps_delete($edib = null) {
		if ($edib !== null) {
			$affected = 0;

			$this->db->select('id');
			$this->db->from($this->mod_table . '_face_map');
			if (is_numeric($edib)) {
				$this->db->where('id_edib', $edib);
			} elseif (is_numeric($edib)) {
				$this->db->where('id_edib', $edib);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $m) {
					$this->buildings_delete_sketches_floors($m->id);
				}
			}

			if (is_numeric($edib)) {
				$this->db->where('id_edib', $edib);
			} elseif (is_array($edib)) {
				$this->db->where_in('id_edib', $edib);
			}
			$this->db->delete($this->mod_table . '_face_map');
			$affected += $this->db->affected_rows();

			if ($affected > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function buildings_delete_sketches_floors($id = null, $id_edifm = null) {
		$affected = 0;

		// usunięcie zmapowanych mieszkań
		$this->db->select('id');
		$this->db->from($this->mod_table . '_sketches_floors');
		if (is_numeric($id_edifm)) {
			$this->db->where('id_edifm', $id_edifm);
		} elseif (is_numeric($id)) {
			$this->db->where('id', $id);
		}
		$query = $this->db->get();
		$maps = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $m) {
				$maps[] = $m->id;
			}
		}
		if (count($maps) > 0) {
			$this->db->where_in('id_edisf', $maps);
			$this->db->delete($this->mod_table . '_sketches_floors_maps');
		}

		// usunięcie rzutu piętra
		if (is_numeric($id_edifm)) {
			$this->db->where('id_edifm', $id_edifm);
		} elseif (is_numeric($id)) {
			$this->db->where('id', $id);
		}
		$this->db->delete($this->mod_table . '_sketches_floors');
		$affected += $this->db->affected_rows();

		if ($affected > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_images($id) {
		$data = array();

		$this->_check_dir($id);

		$this->db->where('id_edi', $id);
		$this->db->from($this->mod_table . '_img AS tk1');
		$this->db->order_by('position');
		$images = $this->db->get();

		if ($images->num_rows() > 0) {
			foreach ($images->result() as $image) {
				$image->desc = array();
				$this->db->where('id_edii', $image->id);
				$this->db->from($this->mod_table . '_img_content AS tk1');
				$image_descs = $this->db->get();
				if ($image_descs->num_rows() > 0) {
					foreach ($image_descs->result() as $image_desc) {
						$image->desc[] = $image_desc;
					}
				}
				$data[] = $image;
			}
			return $data;
		}
		return FALSE;
	}

	public function get_image($id) {
		$this->db->where('id', $id);
		$this->db->from($this->mod_table . '_img AS tk1');
		$this->db->order_by('position');
		$images = $this->db->get();

		if ($images->num_rows() > 0) {
			$image = $images->row();
			$image->desc = array();
			$this->db->where('id_edii', $image->id);
			$this->db->from($this->mod_table . '_img_content AS tk1');
			$image_descs = $this->db->get();
			if ($image_descs->num_rows() > 0) {
				foreach ($image_descs->result() as $image_desc) {
					$image->desc[$image_desc->lang] = $image_desc;
				}
			}
			return $image;
		}
		return FALSE;
	}

	public function edit($id, $lang) {
		if (!$this->exists($id, $lang)) {
			$this->db->where('id_edi', $id);
			$this->db->where('lang', $this->admin->get_main_lang());
			$query = $this->db->get($this->mod_table . '_content');
			$row = $query->row();

			$data = array(
				'id_edi' => $id,
				'lang' => $lang,
				'name' => $row->name
			);
			//$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert($this->mod_table . '_content', $data);
			
			$this->user_url->save_url(false,$row->name,'estatedev_investments',$id,$lang);
			$this->user_url->update_routes();
		}

		$this->_check_dir($id);

		$this->db->select('tk1.*,tk2.*,uu.url');
		$this->db->from('estatedev_investments AS tk1');
		$this->db->join('estatedev_investments_content AS tk2', 'tk1.id = tk2.id_edi');
		$this->db->join('user_url uu','uu.dest_id=tk1.id','left');
		$this->db->where('tk2.id_edi', $id);
		$this->db->where('tk2.lang', $lang);
		$this->db->where('uu.type','estatedev_investments');
		$this->db->where('uu.lang',$lang);
		
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$item = $query->row();

			if (!empty($item->lang)) {
				$this->db->where('short', $item->lang);
				$query = $this->db->get('lang');
				$item->lang = $query->row();
			}
			return $item;
		}
		return FALSE;
	}

	public function investments_exists($id) {

		$this->db->where('id_edi', $id);
		$this->db->where('lang', $this->admin->get_main_lang());
		$query = $this->db->get($this->mod_table . '_content');
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return FALSE;
		}
	}

	public function save($id, $lang) {
		$data = array(
			'pub' => $this->input->post('pub'),
			'address' => $this->input->post('address'),
			'map_coords' => $this->input->post('map_coords')
		);
		$this->db->where('id', $id);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->update($this->mod_table, $data);

		$data = array(
			'name' => htmlspecialchars($this->input->post('name')),
			'meta_title' => $this->input->post('meta_title'),
			'meta_description' => $this->input->post('meta_description'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'admission' => $this->input->post('admission'),
			'description' => $this->input->post('description'),
		);
		$this->db->where('id_edi', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table . '_content', $data);

		$this->user_url->save_url(true,$this->input->post('url'),'estatedev_investments',$id,$lang);
		$this->user_url->update_routes();

		return TRUE;
	}

	public function buildings_save($id) {
		$data = array(
			'name' => htmlspecialchars($this->input->post('name')),
			'pub' => $this->input->post('pub'),
			'face_image' => $this->input->post('face_image')
		);
		$this->db->where('id', $id);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->update($this->mod_table . '_buildings', $data);

		return TRUE;
	}

	public function buildings_save_face_maps($id_edib = null) {
		$existing_face_maps = $this->get_buildings_face_maps($id_edib);
		$coords = $this->input->post('coords');
		$floors = $this->input->post('floor');

		if (is_array($coords) && is_array($floors)) {

			// sprawdzenie (aktualizacja/usunięcie) istniejących map pieter
			if (is_array($existing_face_maps)) {
				foreach ($existing_face_maps as $face_map) {
					if (isset($coords[$face_map->id]) && isset($floors[$face_map->id])) {
						$data = array(
							'floor' => $floors[$face_map->id],
							'coords' => $coords[$face_map->id]
						);
						$this->db->where('id', $face_map->id);
						$this->db->update($this->mod_table . '_face_map', $data);

						// zapisanie danych dla tej mapy piętra
						$this->buildings_save_sketches_floors($face_map->id);
					} else {
						// usunięcie mapy piętra (jest w bazie ale brak w post)
						$this->db->where('id', $face_map->id);
						$this->db->delete($this->mod_table . '_face_map');

						// usunięcie danych dla tej mapy piętra
						$this->buildings_delete_sketches_floors(null, $face_map->id);
					}
					unset($coords[$face_map->id]);
					unset($floors[$face_map->id]);
				}
			}

			// dodanie nowych map pięter
			foreach ($coords as $mk => $mc) {
				if (!empty($coords[$mk])) {
					$data = array(
						'id_edib' => $id_edib,
						'floor' => $floors[$mk],
						'coords' => $coords[$mk]
					);
					$this->db->insert($this->mod_table . '_face_map', $data);
					// zapisanie danych dla tej mapy piętra
					$this->buildings_save_sketches_floors($this->db->insert_id(), $mk);
				}
			}
		} else {
			// usunięcie wszelkich danych odnośnie mapowań w bodynku
			$this->buildings_face_maps_delete($id_edib);
		}

		return true;
	}

	public function buildings_save_sketches_floors($id_edifm = null, $post_key = null) {
		if (is_null($post_key)) {
			$post_key = $id_edifm;
		}
		$existing_floors_maps = $this->get_buildings_sketches_floors_only($post_key);
		$floors = $this->input->post('floor_sketch_' . $post_key);

		if (is_array($existing_floors_maps)) {
			foreach ($existing_floors_maps as $floor_map) {
				if (isset($floors[$floor_map->id])) {
					// aktualizacja danych o rzucie piętra
					$floor_sketches = $this->input->post('coords_floor_sketch_' . $post_key . '_' . $floor_map->id);

					$data = array(
						'image' => $floors[$floor_map->id]
					);
					$this->db->where('id', $floor_map->id);
					$this->db->update($this->mod_table . '_sketches_floors', $data);

					if ($floor_sketches) {
						$existing_floors_sketch_maps = $this->get_buildings_sketches_floors_maps_only($floor_map->id);
						// dodanie/aktualizacja istniejących mapowań mieszkań
						foreach ($floor_sketches as $floors_sketch_maps_id => $floors_sketch_maps) {
							if (!empty($floors_sketch_maps)) {
								$exists = false;
								foreach ($existing_floors_sketch_maps as $sk) {
									if ($sk->id == $floors_sketch_maps_id) {
										$exists = true;
										break;
									}
								}
								if ($exists === true) {
									$this->db->where('id', $floors_sketch_maps_id);
									$data = array(
										'coords' => $floors_sketch_maps
									);
									$this->db->update($this->mod_table . '_sketches_floors_maps', $data);
								} else {
									$data = array(
										'id_edisf' => $floor_map->id,
										'coords' => $floors_sketch_maps
									);
									$this->db->insert($this->mod_table . '_sketches_floors_maps', $data);
								}
							}
						}

						// usunięcie zbędnych mapowań mieszkań (są w bazie ale nie ma w post)
						$data_to_delete = array();
						foreach ($existing_floors_sketch_maps as $sk) {
							if (!isset($floor_sketches[$sk->id])) {
								$data_to_delete[] = $sk->id;
							}
						}
						if (count($data_to_delete) > 0) {
							$this->db->where_in('id', $data_to_delete);
							$this->db->delete($this->mod_table . '_sketches_floors_maps');
						}
					}
				} else {
					// usunięcie danych o rzucie piętra (rzut obecny w bazie ale nieobecny w post)
					$this->buildings_delete_sketches_floors($floor_map->id, null);
				}

				unset($floors[$floor_map->id]);
			}
		}

		// dodanie nowych pięter (istnieją w nowych danych ale nie w bazie)
		if (is_array($floors) && count($floors) > 0) {
			foreach ($floors as $floor_key => $floor) {
				if (!empty($floor)) {
					$data = array(
						'image' => $floor,
						'id_edifm' => $id_edifm
					);
					$this->db->insert($this->mod_table . '_sketches_floors', $data);

					$floor_sketch_id = $this->db->insert_id();

					// dodanie nowych mapowań mieszkań do nowego piętra
					$floor_sketches = $this->input->post('coords_floor_sketch_' . $post_key . '_' . $floor_key);
					if ($floor_sketches) {
						foreach ($floor_sketches as $floor_sketches_map) {
							if (!empty($floor_sketches_map)) {
								$data = array(
									'id_edisf' => $floor_sketch_id,
									'coords' => $floor_sketches_map
								);
								$this->db->insert($this->mod_table . '_sketches_floors_maps', $data);
							}
						}
					}
				} else {
					// pominięcie pustych wartości post
				}
			}
		} else {
			// brak nowych danych do zapisania
		}
	}

	public function buildings_exists_sketches_floors($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table . '_buildings');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function save_images($id) {
		$img = md5(microtime());

		$dir_tmp = './' . config_item('temp') . 'estatedev_' . time() . '_' . $id . '/';
		@mkdir($dir_tmp);
		@chmod($dir_tmp, 0777);
		if (!is_dir($dir_tmp)) {
			return FALSE;
		}

		$config = null;
		$config['upload_path'] = $dir_tmp;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $img;

		$this->upload->initialize($config);

		if (is_uploaded_file($_FILES['file']['tmp_name'])) {
			if (!$this->upload->do_upload('file')) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
			} else {
				$tab = $this->upload->data();
				$map_path = $tab['file_path'];
				$map = array($tab['file_name']);
				sort($map);
				if (!empty($map)) {
					if (!$this->save_thumbnails($id, $map, $map_path)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
					}
				}
			}
			return TRUE;
		} else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
		}
	}

	public function save_images_desc($id) {
		$langs = $this->languages;
		$this->db->where('id_edii', $id);
		$this->db->delete($this->mod_table . '_img_content');
		$desc = $this->input->post('desc_value');
		foreach ($langs as $lang) {
			if (!empty($desc[$lang->short])) {
				$data = array(
					'id_edii' => $id,
					'lang' => $lang->short,
					'description' => $desc[$lang->short]
				);
				$this->db->insert($this->mod_table . '_img_content', $data);
			}
		}
		return true;
	}

	public function position_images($id) {
		$post = $this->input->post('position');
		if (empty($post)) {
			return FALSE;
		}

		foreach ($post as $key => $row) {
			$data = array(
				'position' => $row
			);
			$this->db->where('id', $key);
			$this->db->update($this->mod_table . '_img', $data);
		}

		return TRUE;
	}

	public function del_images($id) {
		$this->db->select('tk1.*');
		$this->db->from($this->mod_table . '_img as tk1');
		$this->db->where('tk1.id', $id);
		$query = $this->db->get();

		$row = $query->row();

		$dir = './' . config_item('site_path') . '/' . config_item('upload_path') . '/' . $this->gal_dir . '/' . $row->id_edi . '/';

		$tab = explode('.', $row->image);

		unlink($dir . $row->image);
		unlink($dir . $tab[0] . '_thumb.' . $tab[1]);
		unlink($dir . $tab[0] . '_thumb_list.' . $tab[1]);
		
		$this->db->where('id_edii', $row->id);
		$this->db->delete($this->mod_table . '_img_content');

		$this->db->where('id', $id);
		$this->db->delete($this->mod_table . '_img');

		return TRUE;
	}

	public function save_thumbnails($id, $dir = null, $path = null) {
		$msg = array();

		if ($dir == null)
			return FALSE;
		if ($path == null)
			return FALSE;
		if (empty($dir))
			return FALSE;

		foreach ($dir as $value) {
			$file = $path . '/' . $value;
			$tab = pathinfo($file);

			$dir = './' . config_item('site_path') . config_item('upload_path') . $this->gal_dir . $id . '/';

			$name = md5(microtime());
			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $dir . $name . '.' . $tab['extension'];
			$config['create_thumb'] = FALSE;

			$config['maintain_ratio'] = TRUE;
			$size = getimagesize($file);
			if ($size[0] < $this->gal_img_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->gal_img_width;
			if ($size[1] < $this->gal_img_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->gal_img_height;

			// jeśli zdjęcie jest pionowe, to będzie dopasowane do wysokości; jeśli poziome, to do szerokości
			if ($size[0] < $size[1]) {
				$config['master_dim'] = 'height';
			} else {
				$config['master_dim'] = 'width';
			}

			$config['master_dim'] = 'width';

			$this->image_lib->initialize($config);
			if (!$this->image_lib->resize()) {
				$msg[] = array(sprintf(lang("Plik <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')), 2);
			}
			$this->image_lib->clear();

			$thumb_file = $dir . $name . '_thumb.' . $tab['extension'];
			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $thumb_file;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;

			$size = getimagesize($dir . $name . '.' . $tab['extension']);
			if ($size[0] < $this->gal_thumb_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->gal_thumb_width;
			if ($size[1] < $this->gal_thumb_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->gal_thumb_height;

			// sprawdzenie, czy zdjęcie trzeba przyciąć z boków, czy z góry
			if (($size[1] * $config['width']) / $size[0] < $config['height']) {
				$config['master_dim'] = 'height';
			} else {
				$config['master_dim'] = 'width';
			}

			$this->image_lib->initialize($config);
			if (!$this->image_lib->resize()) {
				$msg[] = array(sprintf(lang("Plik miniaturki <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')), 2);
			}
			$this->image_lib->clear();

			// przycięcie do ustalonych wymiarów miniaturek
			$config = null;
			$config['source_image'] = $thumb_file;
			$size = getimagesize($thumb_file);
			$config['maintain_ratio'] = FALSE;
			if ($size[0] < $this->gal_thumb_width) {
				$config['width'] = $size[0];
			} else {
				$config['width'] = $this->gal_thumb_width;
			}
			if ($size[1] < $this->gal_thumb_height) {
				$config['height'] = $size[1];
			} else {
				$config['height'] = $this->gal_thumb_height;
			}
			$config['x_axis'] = 0;
			$config['y_axis'] = 0;
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();

			$thumb_file = $dir . $name . '_thumb_list.' . $tab['extension'];
			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $thumb_file;
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = TRUE;

			$size = getimagesize($dir . $name . '.' . $tab['extension']);
			if ($size[0] < $this->gal_thumb_list_width)
				$config['width'] = $size[0];
			else
				$config['width'] = $this->gal_thumb_list_width;
			if ($size[1] < $this->gal_thumb_list_height)
				$config['height'] = $size[1];
			else
				$config['height'] = $this->gal_thumb_list_height;

			// sprawdzenie, czy zdjęcie trzeba przyciąć z boków, czy z góry
			if (($size[1] * $config['width']) / $size[0] < $config['height']) {
				$config['master_dim'] = 'height';
			} else {
				$config['master_dim'] = 'width';
			}

			$this->image_lib->initialize($config);
			if (!$this->image_lib->resize()) {
				$msg[] = array(sprintf(lang("Plik miniaturki <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')), 2);
			}
			$this->image_lib->clear();

			// przycięcie do ustalonych wymiarów miniaturek
			$config = null;
			$config['source_image'] = $thumb_file;
			$size = getimagesize($thumb_file);
			$config['maintain_ratio'] = FALSE;
			if ($size[0] < $this->gal_thumb_list_width) {
				$config['width'] = $size[0];
			} else {
				$config['width'] = $this->gal_thumb_list_width;
			}
			if ($size[1] < $this->gal_thumb_list_height) {
				$config['height'] = $size[1];
			} else {
				$config['height'] = $this->gal_thumb_list_height;
			}
			$config['x_axis'] = 0;
			$config['y_axis'] = 0;
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();

			$this->db->select('max(position) as position');
			$this->db->where('id_edi', $id);
			$query = $this->db->get($this->mod_table . '_img');

			$row1 = $query->row();

			$position = $row1->position + 1;

			$data = array(
				'id_edi' => $id,
				'image' => $name . '.' . $tab['extension'],
				'position' => $position
			);
			$this->db->set('add_date', 'NOW()', false);
			$this->db->set('lastmod_date', 'NOW()', false);
			$this->db->insert($this->mod_table . '_img', $data);

			sleep(1);
		}

		delete_files($path, TRUE);
		rmdir($path);

		if (count($msg) > 0) {
			$this->session->set_flashdata('msg_files', $msg);
		}

		return TRUE;
	}

	public function crop($id) {
		$data = $this->get_image($id);

		$tab = explode('.', $data->image);

		$img_save = $tab[0] . '_thumb.' . $tab[1];

		$coords['scale'] = $this->input->post('scale');
		$coords['x_axis'] = $this->input->post('x_axis');
		$coords['y_axis'] = $this->input->post('y_axis');
		$coords['width'] = $this->input->post('width');
		$coords['height'] = $this->input->post('height');
		$coords['thumb_width'] = $this->gal_thumb_width;
		$coords['thumb_height'] = $this->gal_thumb_height;

		$path = './' . config_item('site_path') . config_item('upload_path') . $this->gal_dir . '/' . $this->tab['id'] . '/';

		$this->image_lib->crop_thumb($path, $data->image, $path_save = null, $img_save, $coords);
	}

	public function crop_list($id) {
		$data = $this->get_image($id);

		$tab = explode('.', $data->image);

		$img_save = $tab[0] . '_thumb_list.' . $tab[1];

		$coords['scale'] = $this->input->post('scale');
		$coords['x_axis'] = $this->input->post('x_axis');
		$coords['y_axis'] = $this->input->post('y_axis');
		$coords['width'] = $this->input->post('width');
		$coords['height'] = $this->input->post('height');
		$coords['thumb_width'] = $this->gal_thumb_list_width;
		$coords['thumb_height'] = $this->gal_thumb_list_height;

		$path = './' . config_item('site_path') . config_item('upload_path') . $this->gal_dir . '/' . $this->tab['id'] . '/';

		$this->image_lib->crop_thumb($path, $data->image, $path_save = null, $img_save, $coords);
	}

	public function get_buildings($id_edi = null, $id_edib = null) {
		$this->db->from($this->mod_table . '_buildings');
		if (!empty($id_edi)) {
			$this->db->where('id_edi', $id_edi);
		} elseif (!empty($id_edib)) {
			$this->db->where('id', $id_edib);
		}
		$this->db->order_by('position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			if (!empty($id_edib)) {
				return $query->row();
			} else {
				return $query->result();
			}
		}
		return FALSE;
	}

	public function get_buildings_face_maps($id_edib = null) {
		$this->db->from($this->mod_table . '_face_map');
		$this->db->where('id_edib', $id_edib);
		$this->db->order_by('floor', 'desc');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $map) {
				$map->floors = $this->get_buildings_sketches_floors($map->id);
				$data[] = $map;
			}

			return $data;
		}
		return FALSE;
	}

	public function get_buildings_sketches_floors_maps($id = null, $id_edifm = null) {
		$this->db->select('tk1.image, tk2.coords, tk1.id AS floor_id, tk2.id AS floorsketch_id');
		$this->db->from($this->mod_table . '_sketches_floors AS tk1');
		$this->db->join($this->mod_table . '_sketches_floors_maps AS tk2', 'tk2.id_edisf = tk1.id', 'left');
		if (!is_null($id)) {
			$this->db->where('tk1.id', $id);
		}
		if (!is_null($id_edifm)) {
			$this->db->where('tk1.id_edifm', $id_edifm);
		}
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $r) {
				if (!empty($r->coords)) {
					$data[$r->floor_id] = $r;
				}
			}
			return $data;
		}
		return FALSE;
	}

	public function get_buildings_sketches_floors_only($id_edifm = null) {
		$this->db->select('*');
		$this->db->from($this->mod_table . '_sketches_floors AS tk1');
		$this->db->where('tk1.id_edifm', $id_edifm);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return FALSE;
	}

	public function get_buildings_sketches_floors_maps_only($edisf = null) {
		$this->db->select('*');
		$this->db->from($this->mod_table . '_sketches_floors_maps AS tk1');
		$this->db->where('tk1.id_edisf', $edisf);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
		return FALSE;
	}

	public function get_buildings_sketches_floors($id_edifm = null) {
		$this->db->from($this->mod_table . '_sketches_floors');
		$this->db->where('id_edifm', $id_edifm);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $floor) {
				$floor->sketches = $this->get_buildings_sketches_floors_maps_only($floor->id);
				$data[] = $floor;
			}

			return $data;
		}
		return FALSE;
	}

	public function get_all_maps() {
		$data = array();

		$this->db->select('tk1.id AS investment_id, tk2.name AS investment_name, tk3.id AS building_id, tk3.name AS building_name, tk3.face_image');
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edi');
		$this->db->join($this->mod_table . '_buildings AS tk3', 'tk3.id_edi = tk1.id');
		$this->db->order_by('tk3.position');
		$buildings = $this->db->get();
		if ($buildings->num_rows() > 0) {
			foreach ($buildings->result() as $building) {
				$this->db->select('tk1.id, tk1.floor, tk1.coords');
				$this->db->from($this->mod_table . '_face_map AS tk1');
				$this->db->where('id_edib', $building->building_id);
				$this->db->order_by('floor');
				$faces = $this->db->get();
				
				$building->face_maps = false;
				
				if ($faces->num_rows() > 0) {
					$building->face_maps = array();
					foreach ($faces->result() as $face) {
						$face->floors_sketches = false;
						
						$this->db->select('tk1.id, tk1.image');
						$this->db->from($this->mod_table . '_sketches_floors AS tk1');
						$this->db->where('id_edifm', $face->id);
						$floors_sketches = $this->db->get();
						
						if ($floors_sketches->num_rows() > 0) {
							$face->floors_sketches = array();
							foreach ($floors_sketches->result() as $floor_sketch) {
								$this->db->select('tk1.id, tk1.coords');
								$this->db->from($this->mod_table . '_sketches_floors_maps AS tk1');
								$this->db->where('id_edisf', $floor_sketch->id);
								$floors_sketches_maps = $this->db->get();
								
								$floor_sketch->sketches_floors_maps = $floors_sketches_maps->result();
								
								$face->floors_sketches[] = $floor_sketch;
							}
						}
						
						$building->face_maps[] = $face;
					}
				}

				$data[] = $building;
			}
		}

		return $data;
	}
	
	public function get_estate_map($estate_id = null){
		$this->db->select('tk1.id');
		$this->db->from($this->mod_table . '_sketches_floors_maps AS tk1');
		$this->db->where('id_ede', $estate_id);
		$floors_sketches_maps = $this->db->get();
		if ($floors_sketches_maps->num_rows() > 0) {
			return $floors_sketches_maps->row()->id;
		} else {
			return null;
		}
	}
	
	public function set_estate_map($idedisfm = null, $idede = null){
		$this->db->where('id_ede', $idede);
		$this->db->update($this->mod_table . '_sketches_floors_maps', array('id_ede' => null));
		
		$this->db->where('id', $idedisfm);
		$this->db->update($this->mod_table . '_sketches_floors_maps', array('id_ede' => $idede));
		return true;
	}

	public function add_buildings($id_edi) {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table . '_buildings');
		$this->db->where('id_edi', $id_edi);
		$query = $this->db->get();
		$row = $query->row();
		$position = $row->position + 1;

		$data = array(
			'position' => $position,
			'pub' => 1,
			'name' => $this->input->post('name'),
			'face_image' => $this->input->post('face_image'),
			'id_edi' => $id_edi
		);
		$this->db->set('add_date', 'NOW()', FALSE);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->insert($this->mod_table . '_buildings', $data);

		if ($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();
			return $id;
		} else {
			return false;
		}
	}

	private function _check_dir($id) {
		$dir = './' . config_item('site_path') . config_item('upload_path') . $this->gal_dir . '/' . $id . '/';
		if (!is_dir($dir)) {
			mkdir($dir);
		}
	}

}
