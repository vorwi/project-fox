SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `cms_estatedev_contacts`,
	`cms_estatedev_contacts_content`,
	`cms_estatedev_contacts_join_types`,
	`cms_estatedev_estates`,
	`cms_estatedev_estates_content`,
	`cms_estatedev_estates_img`,
	`cms_estatedev_estates_img_content`,
	`cms_estatedev_investments`,
	`cms_estatedev_investments_buildings`,
	`cms_estatedev_investments_content`,
	`cms_estatedev_investments_face_map`,
	`cms_estatedev_investments_img`,
	`cms_estatedev_investments_img_content`,
	`cms_estatedev_investments_sketches_floors`,
	`cms_estatedev_investments_sketches_floors_maps`,
	`cms_estatedev_parameters`,
	`cms_estatedev_parameters_content`,
	`cms_estatedev_parameters_join_estates`,
	`cms_estatedev_parameters_values`,
	`cms_estatedev_parameters_values_content`,
	`cms_estatedev_types`,
	`cms_estatedev_types_content`;

SET foreign_key_checks = 1;