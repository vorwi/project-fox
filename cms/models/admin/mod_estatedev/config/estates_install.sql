-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 21 Lip 2016, 10:00
-- Wersja serwera: 5.6.20-log
-- Wersja PHP: 5.5.15

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Baza danych: `neocms`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_contacts`
--

DROP TABLE IF EXISTS `cms_estatedev_contacts`;
CREATE TABLE `cms_estatedev_contacts` (
  `id` int(11) NOT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `pub` int(1) NOT NULL,
  `image` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_contacts_content`
--

DROP TABLE IF EXISTS `cms_estatedev_contacts_content`;
CREATE TABLE `cms_estatedev_contacts_content` (
  `id_edc` int(11) NOT NULL,
  `lang` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `surname` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `office` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_contacts_join_types`
--

DROP TABLE IF EXISTS `cms_estatedev_contacts_join_types`;
CREATE TABLE `cms_estatedev_contacts_join_types` (
  `id_edt` int(11) NOT NULL,
  `id_edc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_estates`
--

DROP TABLE IF EXISTS `cms_estatedev_estates`;
CREATE TABLE `cms_estatedev_estates` (
  `id` int(11) NOT NULL,
  `id_edt` int(11) DEFAULT NULL,
  `id_edi` int(11) DEFAULT NULL,
  `id_edib` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `pub` int(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_estates_content`
--

DROP TABLE IF EXISTS `cms_estatedev_estates_content`;
CREATE TABLE `cms_estatedev_estates_content` (
  `id_ede` int(11) NOT NULL,
  `lang` varchar(2) CHARACTER SET utf8 NOT NULL,
  `admission` text CHARACTER SET utf8,
  `description` text CHARACTER SET utf8,
  `meta_title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `meta_keywords` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_estates_img`
--

DROP TABLE IF EXISTS `cms_estatedev_estates_img`;
CREATE TABLE `cms_estatedev_estates_img` (
  `id` int(11) NOT NULL,
  `id_ede` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_estates_img_content`
--

DROP TABLE IF EXISTS `cms_estatedev_estates_img_content`;
CREATE TABLE `cms_estatedev_estates_img_content` (
  `id_edei` int(11) NOT NULL,
  `lang` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments`
--

DROP TABLE IF EXISTS `cms_estatedev_investments`;
CREATE TABLE `cms_estatedev_investments` (
  `id` int(11) NOT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `pub` int(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `address` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `map_coords` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments_buildings`
--

DROP TABLE IF EXISTS `cms_estatedev_investments_buildings`;
CREATE TABLE `cms_estatedev_investments_buildings` (
  `id` int(11) NOT NULL,
  `id_edi` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `face_image` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `pub` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments_content`
--

DROP TABLE IF EXISTS `cms_estatedev_investments_content`;
CREATE TABLE `cms_estatedev_investments_content` (
  `id_edi` int(11) NOT NULL,
  `lang` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL,
  `admission` text COLLATE utf8_polish_ci,
  `description` text COLLATE utf8_polish_ci,
  `meta_title` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `meta_keywords` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `meta_description` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments_face_map`
--

DROP TABLE IF EXISTS `cms_estatedev_investments_face_map`;
CREATE TABLE `cms_estatedev_investments_face_map` (
  `id` int(11) NOT NULL,
  `id_edib` int(11) DEFAULT NULL,
  `coords` text COLLATE utf8_polish_ci,
  `floor` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments_img`
--

DROP TABLE IF EXISTS `cms_estatedev_investments_img`;
CREATE TABLE `cms_estatedev_investments_img` (
  `id` int(11) NOT NULL,
  `id_edi` int(11) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `image` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments_img_content`
--

DROP TABLE IF EXISTS `cms_estatedev_investments_img_content`;
CREATE TABLE `cms_estatedev_investments_img_content` (
  `id_edii` int(11) NOT NULL,
  `lang` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `description` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments_sketches_floors`
--

DROP TABLE IF EXISTS `cms_estatedev_investments_sketches_floors`;
CREATE TABLE `cms_estatedev_investments_sketches_floors` (
  `id` int(11) NOT NULL,
  `id_edifm` int(11) DEFAULT NULL,
  `image` varchar(200) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_investments_sketches_floors_maps`
--

DROP TABLE IF EXISTS `cms_estatedev_investments_sketches_floors_maps`;
CREATE TABLE `cms_estatedev_investments_sketches_floors_maps` (
  `id` int(11) NOT NULL,
  `id_edisf` int(11) DEFAULT NULL,
  `id_ede` int(11) DEFAULT NULL,
  `coords` text COLLATE utf8_polish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_parameters`
--

DROP TABLE IF EXISTS `cms_estatedev_parameters`;
CREATE TABLE `cms_estatedev_parameters` (
  `id` int(11) NOT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `searchable` int(1) DEFAULT NULL,
  `in_table` int(1) DEFAULT NULL,
  `unit` varchar(45) COLLATE utf8_polish_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `icon_button` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `predefined_parameter` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `presentation` int(1) NOT NULL,
  `in_name` int(1) NOT NULL DEFAULT '0',
  `in_name_show` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_parameters_content`
--

DROP TABLE IF EXISTS `cms_estatedev_parameters_content`;
CREATE TABLE `cms_estatedev_parameters_content` (
  `id_edp` int(11) NOT NULL,
  `lang` varchar(2) CHARACTER SET utf8 NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_parameters_join_estates`
--

DROP TABLE IF EXISTS `cms_estatedev_parameters_join_estates`;
CREATE TABLE `cms_estatedev_parameters_join_estates` (
  `id_ede` int(11) NOT NULL,
  `id_edp` int(11) NOT NULL,
  `id_edepv` int(11) NOT NULL,
  `text_value` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_parameters_values`
--

DROP TABLE IF EXISTS `cms_estatedev_parameters_values`;
CREATE TABLE `cms_estatedev_parameters_values` (
  `id` int(11) NOT NULL,
  `id_edep` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_parameters_values_content`
--

DROP TABLE IF EXISTS `cms_estatedev_parameters_values_content`;
CREATE TABLE `cms_estatedev_parameters_values_content` (
  `id_edepv` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_types`
--

DROP TABLE IF EXISTS `cms_estatedev_types`;
CREATE TABLE `cms_estatedev_types` (
  `id` int(11) NOT NULL,
  `add_date` datetime DEFAULT NULL,
  `lastmod_date` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `pub` int(1) DEFAULT NULL,
  `icon_inactive` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `icon_active` varchar(255) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_estatedev_types_content`
--

DROP TABLE IF EXISTS `cms_estatedev_types_content`;
CREATE TABLE `cms_estatedev_types_content` (
  `id_edt` int(11) NOT NULL,
  `lang` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_polish_ci DEFAULT NULL,
  `name_plural` varchar(100) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `cms_estatedev_contacts`
--
ALTER TABLE `cms_estatedev_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_estatedev_contacts_content`
--
ALTER TABLE `cms_estatedev_contacts_content`
  ADD PRIMARY KEY (`id_edc`,`lang`);

--
-- Indexes for table `cms_estatedev_contacts_join_types`
--
ALTER TABLE `cms_estatedev_contacts_join_types`
  ADD PRIMARY KEY (`id_edt`,`id_edc`),
  ADD KEY `fk_cms_estatedev_types_has_cms_estatedev_contacts_cms_estat_idx` (`id_edc`),
  ADD KEY `fk_cms_estatedev_types_has_cms_estatedev_contacts_cms_estat_idx1` (`id_edt`);

--
-- Indexes for table `cms_estatedev_estates`
--
ALTER TABLE `cms_estatedev_estates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `edt_idx` (`id_edt`),
  ADD KEY `edi_idx` (`id_edi`),
  ADD KEY `edib_idx` (`id_edib`);

--
-- Indexes for table `cms_estatedev_estates_content`
--
ALTER TABLE `cms_estatedev_estates_content`
  ADD PRIMARY KEY (`id_ede`,`lang`);

--
-- Indexes for table `cms_estatedev_estates_img`
--
ALTER TABLE `cms_estatedev_estates_img`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ede_idx` (`id_ede`);

--
-- Indexes for table `cms_estatedev_estates_img_content`
--
ALTER TABLE `cms_estatedev_estates_img_content`
  ADD PRIMARY KEY (`id_edei`,`lang`);

--
-- Indexes for table `cms_estatedev_investments`
--
ALTER TABLE `cms_estatedev_investments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_estatedev_investments_buildings`
--
ALTER TABLE `cms_estatedev_investments_buildings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `edib_edi` (`id_edi`);

--
-- Indexes for table `cms_estatedev_investments_content`
--
ALTER TABLE `cms_estatedev_investments_content`
  ADD PRIMARY KEY (`id_edi`,`lang`);

--
-- Indexes for table `cms_estatedev_investments_face_map`
--
ALTER TABLE `cms_estatedev_investments_face_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_estatedev_investments_face_map_FKIndex1` (`id_edib`);

--
-- Indexes for table `cms_estatedev_investments_img`
--
ALTER TABLE `cms_estatedev_investments_img`
  ADD PRIMARY KEY (`id`),
  ADD KEY `edi_idx` (`id_edi`);

--
-- Indexes for table `cms_estatedev_investments_img_content`
--
ALTER TABLE `cms_estatedev_investments_img_content`
  ADD PRIMARY KEY (`id_edii`,`lang`);

--
-- Indexes for table `cms_estatedev_investments_sketches_floors`
--
ALTER TABLE `cms_estatedev_investments_sketches_floors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `edifm_idx` (`id_edifm`);

--
-- Indexes for table `cms_estatedev_investments_sketches_floors_maps`
--
ALTER TABLE `cms_estatedev_investments_sketches_floors_maps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `edisf_idx` (`id_edisf`),
  ADD KEY `ede_idx` (`id_ede`);

--
-- Indexes for table `cms_estatedev_parameters`
--
ALTER TABLE `cms_estatedev_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_estatedev_parameters_position_index` (`position`);

--
-- Indexes for table `cms_estatedev_parameters_content`
--
ALTER TABLE `cms_estatedev_parameters_content`
  ADD PRIMARY KEY (`id_edp`,`lang`);

--
-- Indexes for table `cms_estatedev_parameters_join_estates`
--
ALTER TABLE `cms_estatedev_parameters_join_estates`
  ADD PRIMARY KEY (`id_ede`,`id_edp`,`id_edepv`),
  ADD KEY `edpje_edepv` (`id_edepv`);

--
-- Indexes for table `cms_estatedev_parameters_values`
--
ALTER TABLE `cms_estatedev_parameters_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_edep_idx` (`id_edep`);

--
-- Indexes for table `cms_estatedev_parameters_values_content`
--
ALTER TABLE `cms_estatedev_parameters_values_content`
  ADD PRIMARY KEY (`id_edepv`,`lang`);

--
-- Indexes for table `cms_estatedev_types`
--
ALTER TABLE `cms_estatedev_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_estatedev_types_content`
--
ALTER TABLE `cms_estatedev_types_content`
  ADD PRIMARY KEY (`id_edt`,`lang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_contacts`
--
ALTER TABLE `cms_estatedev_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_estates`
--
ALTER TABLE `cms_estatedev_estates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_estates_img`
--
ALTER TABLE `cms_estatedev_estates_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_investments`
--
ALTER TABLE `cms_estatedev_investments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_investments_buildings`
--
ALTER TABLE `cms_estatedev_investments_buildings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_investments_face_map`
--
ALTER TABLE `cms_estatedev_investments_face_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_investments_img`
--
ALTER TABLE `cms_estatedev_investments_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_investments_sketches_floors`
--
ALTER TABLE `cms_estatedev_investments_sketches_floors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_investments_sketches_floors_maps`
--
ALTER TABLE `cms_estatedev_investments_sketches_floors_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_parameters`
--
ALTER TABLE `cms_estatedev_parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_parameters_values`
--
ALTER TABLE `cms_estatedev_parameters_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `cms_estatedev_types`
--
ALTER TABLE `cms_estatedev_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `cms_estatedev_contacts_content`
--
ALTER TABLE `cms_estatedev_contacts_content`
  ADD CONSTRAINT `edcc_edc` FOREIGN KEY (`id_edc`) REFERENCES `cms_estatedev_contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_contacts_join_types`
--
ALTER TABLE `cms_estatedev_contacts_join_types`
  ADD CONSTRAINT `fk_cms_estatedev_types_join_contacts1` FOREIGN KEY (`id_edt`) REFERENCES `cms_estatedev_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cms_estatedev_types_join_contacts2` FOREIGN KEY (`id_edc`) REFERENCES `cms_estatedev_contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_estates`
--
ALTER TABLE `cms_estatedev_estates`
  ADD CONSTRAINT `edi` FOREIGN KEY (`id_edi`) REFERENCES `cms_estatedev_investments` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `edib` FOREIGN KEY (`id_edib`) REFERENCES `cms_estatedev_investments_buildings` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `edt` FOREIGN KEY (`id_edt`) REFERENCES `cms_estatedev_types` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Ograniczenia dla tabeli `cms_estatedev_estates_content`
--
ALTER TABLE `cms_estatedev_estates_content`
  ADD CONSTRAINT `ede` FOREIGN KEY (`id_ede`) REFERENCES `cms_estatedev_estates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_estates_img_content`
--
ALTER TABLE `cms_estatedev_estates_img_content`
  ADD CONSTRAINT `edei` FOREIGN KEY (`id_edei`) REFERENCES `cms_estatedev_estates_img` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_investments_buildings`
--
ALTER TABLE `cms_estatedev_investments_buildings`
  ADD CONSTRAINT `edib_edi` FOREIGN KEY (`id_edi`) REFERENCES `cms_estatedev_investments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_investments_content`
--
ALTER TABLE `cms_estatedev_investments_content`
  ADD CONSTRAINT `edic_edi` FOREIGN KEY (`id_edi`) REFERENCES `cms_estatedev_investments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_investments_face_map`
--
ALTER TABLE `cms_estatedev_investments_face_map`
  ADD CONSTRAINT `edifm_edib` FOREIGN KEY (`id_edib`) REFERENCES `cms_estatedev_investments_buildings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_investments_img`
--
ALTER TABLE `cms_estatedev_investments_img`
  ADD CONSTRAINT `edim_edi` FOREIGN KEY (`id_edi`) REFERENCES `cms_estatedev_investments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_investments_img_content`
--
ALTER TABLE `cms_estatedev_investments_img_content`
  ADD CONSTRAINT `edimc_edii` FOREIGN KEY (`id_edii`) REFERENCES `cms_estatedev_investments_img` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_investments_sketches_floors`
--
ALTER TABLE `cms_estatedev_investments_sketches_floors`
  ADD CONSTRAINT `edisf_edifm` FOREIGN KEY (`id_edifm`) REFERENCES `cms_estatedev_investments_face_map` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_investments_sketches_floors_maps`
--
ALTER TABLE `cms_estatedev_investments_sketches_floors_maps`
  ADD CONSTRAINT `edisfm_ede` FOREIGN KEY (`id_ede`) REFERENCES `cms_estatedev_estates` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `edisfm_edisf` FOREIGN KEY (`id_edisf`) REFERENCES `cms_estatedev_investments_sketches_floors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_parameters_content`
--
ALTER TABLE `cms_estatedev_parameters_content`
  ADD CONSTRAINT `id_edp` FOREIGN KEY (`id_edp`) REFERENCES `cms_estatedev_parameters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_parameters_values`
--
ALTER TABLE `cms_estatedev_parameters_values`
  ADD CONSTRAINT `id_edep` FOREIGN KEY (`id_edep`) REFERENCES `cms_estatedev_parameters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_parameters_values_content`
--
ALTER TABLE `cms_estatedev_parameters_values_content`
  ADD CONSTRAINT `idedpev` FOREIGN KEY (`id_edepv`) REFERENCES `cms_estatedev_parameters_values` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograniczenia dla tabeli `cms_estatedev_types_content`
--
ALTER TABLE `cms_estatedev_types_content`
  ADD CONSTRAINT `edtc_edt` FOREIGN KEY (`id_edt`) REFERENCES `cms_estatedev_types` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
