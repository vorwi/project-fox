<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Types_model extends MY_Model {
	

	public function __construct() {
		parent::__construct('estatedev_types');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	public function get_all() {
		$data = false;

		$this->db->select('tk1.*, tk2.name, tk2.lang');
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edt');
		$this->db->where('tk2.lang', config_item('main_lang'));
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$data = $query->result();
		}

		return $data;
	}

	public function exists($id = null, $lang = null) {
		if ($id !== null) {
			$this->db->select('tk1.id');
			$this->db->from($this->mod_table . ' AS tk1');
			$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_edt');
			$this->db->where('tk1.id', $id);
			if ($lang !== null) {
				$this->db->where('tk2.lang', $lang);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function insert_validate() {
		$this->form_validation->set_rules('name', 'Nazwa nieruchomości', 'required');

		if ($this->form_validation->run() === FALSE) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();
		$row = $query->row();
		$position = $row->position + 1;

		$data = array(
			'position' => $position,
			'pub' => 1
		);
		$this->db->set('add_date', 'NOW()', FALSE);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->insert($this->mod_table, $data);

		if ($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();
			$title = trim($this->input->post('name'));

			$data = array(
				'id_edt' => $id,
				'lang' => config_item('main_lang'),
				'name' => htmlspecialchars(trim($this->input->post('name'))),
				'name_plural' => htmlspecialchars(trim($this->input->post('name_plural'))),
			);
			$this->db->insert($this->mod_table . '_content', $data);
				
			if ($this->db->affected_rows() == 1) {
				
				$this->user_url->save_url(false,$title,'estatedev_types',$id,config_item('main_lang'));
				$this->user_url->update_routes();
				
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function delete($id = null) {
		if ($id !== null) {
			$affected = 0;

			if (is_numeric($id)) {
				$this->db->where('id', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id', $id);
			}
			$this->db->delete($this->mod_table);
			$affected += $this->db->affected_rows();

			if (is_numeric($id)) {
				$this->db->where('id_edt', $id);
			} elseif (is_array($id)) {
				$this->db->where_in('id_edt', $id);
			}
			$this->db->delete($this->mod_table . '_content');
			$affected += $this->db->affected_rows();
			
			$this->db->where_in('dest_id', $id);
			$this->db->where('type', 'estatedev_types');
			$query_uu = $this->db->delete('user_url');
			$this->user_url->update_routes();

			if ($affected > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function edit($id, $lang) {
		if (!$this->exists($id, $lang)) {
			$this->db->where('id_edt', $id);
			$this->db->where('lang', config_item('main_lang'));
			$query = $this->db->get($this->mod_table . '_content');
			$row = $query->row();

			$data = array(
				'id_edt' => $id,
				'lang' => $lang,
				'name' => $row->name,
				'name_plural' => $row->name_plural
			);
			//$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert($this->mod_table . '_content', $data);
			
			$this->user_url->save_url(false,$row->name,'estatedev_types',$id,$lang);
			$this->user_url->update_routes();
		}

		$this->db->select('tk1.*,tk2.*,uu.url');
		$this->db->from('estatedev_types AS tk1');
		$this->db->join('estatedev_types_content AS tk2', 'tk1.id = tk2.id_edt');
		$this->db->join('user_url uu','uu.dest_id=tk1.id','left');
		$this->db->where('tk2.id_edt', $id);
		$this->db->where('tk2.lang', $lang);
		$this->db->where('uu.type','estatedev_types');
		$this->db->where('uu.lang',$lang);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$type = $query->row();
			if (!empty($type->lang)) {
				$this->db->where('short', $type->lang);
				$query = $this->db->get('lang');
				$type->lang = $query->row();
			}
			return $type;
		}
		return FALSE;
	}

	public function save($id, $lang) {
		$data = array(
			'pub' => $this->input->post('pub'),
			'icon_inactive' => $this->input->post('icon_inactive'),
			'icon_active' => $this->input->post('icon_active')
		);
		$this->db->where('id', $id);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->update($this->mod_table, $data);

		$data = array(
			'name' => htmlspecialchars(trim($this->input->post('name'))),
			'name_plural' => htmlspecialchars(trim($this->input->post('name_plural'))),
		);
		$this->db->where('id_edt', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table.'_content', $data);
		
		$this->user_url->save_url(true,$this->input->post('url'),'estatedev_types',$id,$lang);
		$this->user_url->update_routes();
		
		return TRUE;
	}

}
