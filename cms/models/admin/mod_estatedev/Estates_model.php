<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estates_model extends MY_Model {

	public function __construct() {
		parent::__construct('estatedev_estates');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all() {

		$this->db->select('tk1.*, tk2.admission, tk2.lang, CONCAT(tk3.name, \' - \', tk4.name) AS name', false);
		$this->db->from($this->mod_table . ' AS tk1');
		$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_ede');
		$this->db->join('estatedev_types_content AS tk3', 'tk1.id_edt = tk3.id_edt AND tk3.lang = \'' . $this->admin->get_main_lang() . '\'', 'left');
		$this->db->join('estatedev_investments_content AS tk4', 'tk1.id_edi = tk4.id_edi AND tk4.lang = \'' . $this->admin->get_main_lang() . '\'', 'left');
		$this->db->where('tk2.lang', $this->admin->get_main_lang());
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function exists($id = null, $lang = null) {
		if ($id !== null) {
			$this->db->select('tk1.id');
			$this->db->from($this->mod_table . ' AS tk1');
			$this->db->join($this->mod_table . '_content AS tk2', 'tk1.id = tk2.id_ede');
			$this->db->where('tk1.id', $id);
			if ($lang !== null) {
				$this->db->where('tk2.lang', $lang);
			}
			$query = $this->db->get();
			return $query->num_rows() > 0 ? TRUE : FALSE;
		} else {
			return false;
		}
	}

	public function verify_form($type = 'add') {
		$this->form_validation->set_rules('id_edt', lang('Typ nieruchomości'), 'required|trim');
		$this->form_validation->set_rules('id_edi', lang('Inwestycja'), 'required|trim');
		
		if($type == 'edit') {
			
		}

		return $this->form_validation->run();
	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();
		$row = $query->row();
		$position = $row->position + 1;

		$data = array(
			'position' => $position,
			'id_edt' => $this->input->post('id_edt'),
			'id_edi' => $this->input->post('id_edi'),
			'pub' => 0
		);
		$this->db->set('add_date', 'NOW()', FALSE);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->insert($this->mod_table, $data);

		if ($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();
			$data = array(
				'id_ede' => $id,
				'lang' => $this->admin->get_main_lang(),
				'admission' => '',
				'description' => '',
				'meta_title' => '',
				'meta_keywords' => '',
				'meta_description' => ''
			);
			$this->db->insert($this->mod_table . '_content', $data);
			
			$this->user_url->save_url(false,'estate-'.$id,'estatedev_estates',$id,$this->admin->get_main_lang());
			$this->user_url->update_routes();

			return $id;
		} else {
			return false;
		}
	}

	public function delete($id) {
		
		$post = !is_numeric($id) ? $this->input->post('check') : $id;
		
		if(empty($post)) return FALSE;
		
		$affected = 0;

		// usunięcie wersji językowych
		$this->db->where_in('id_ede', $post);	
		$this->db->delete($this->mod_table . '_content');
		$affected += $this->db->affected_rows();

		// usunięcie parametrów
		$this->db->where_in('id_ede', $post);
		$this->db->delete('estatedev_parameters_join_estates');
		$affected += $this->db->affected_rows();

		// usunięcie nieruchomości
		$this->db->where_in('id', $post);	
		$this->db->delete($this->mod_table);
		$affected += $this->db->affected_rows();
		
		$this->db->where_in('dest_id', $post);
		$this->db->where('type', 'estatedev_estates');
		$query_uu = $this->db->delete('user_url');
		$this->user_url->update_routes();

		return $affected > 0 ? TRUE : FALSE;
	}

	public function edit($id, $lang) {

		if (!$this->exists($id, $lang)) {

			$data = array(
				'id_ede' => $id,
				'lang' => $lang
			);
			$this->db->insert($this->mod_table . '_content', $data);
			
			$this->user_url->save_url(false,'estate-'.$id,'estatedev_estates',$id,$lang);
			$this->user_url->update_routes();
		}

		$this->db->select('tk1.*,tk2.*,uu.url');
		$this->db->from('estatedev_estates AS tk1');
		$this->db->join('estatedev_estates_content AS tk2', 'tk1.id = tk2.id_ede');
		$this->db->join('user_url uu','uu.dest_id=tk1.id','left');
		$this->db->where('tk1.id', $id);
		$this->db->where('tk2.lang', $lang);
		$this->db->where('uu.type','estatedev_estates');
		$this->db->where('uu.lang',$lang);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			$estate = $query->row();
			if (!empty($estate->lang)) {
				$this->db->where('short', $estate->lang);
				$query = $this->db->get('lang');
				$estate->lang = $query->row();
			}
			$estate->parameters = $this->parameters_model->get_for_estate($estate->id);
			
			$estate->edisfm = $this->investments_model->get_estate_map($id);

			return $estate;
		}
		return FALSE;
	}

	public function save($id, $lang) {
		
		$parameters = $this->parameters_model->get_for_estate($id);

		if (is_array($parameters)) {
			$post = $this->input->post('param');
			if ($post) {
				$this->db->where('id_ede', $id);
				$this->db->delete('estatedev_parameters_join_estates');
				foreach ($parameters as $param) {
					if (isset($post[$param->id])) {
						$data = array();
						if ($param->type === 'text' || $param->type === 'file') {
							$data = array(
								'id_ede' => $id,
								'id_edp' => $param->id,
								'id_edepv' => 0,
								'text_value' => $post[$param->id]
							);
							$this->db->insert('estatedev_parameters_join_estates', $data);
						} else if (!empty($post[$param->id])) {
							if (is_array($post[$param->id])) {
								$data = array();
								foreach ($post[$param->id] as $selected_value) {
									$data[] = array(
										'id_ede' => $id,
										'id_edp' => $param->id,
										'id_edepv' => $selected_value,
										'text_value' => null
									);
								}
								$this->db->insert_batch('estatedev_parameters_join_estates', $data);
							} else {
								$data = array(
									'id_ede' => $id,
									'id_edp' => $param->id,
									'id_edepv' => $post[$param->id],
									'text_value' => null
								);
								$this->db->insert('estatedev_parameters_join_estates', $data);
							}
						}
					}
				}
			}
		}
		
		
		$edt = empty($this->input->post('id_edt')) ? null : $this->input->post('id_edt');
		$edi = empty($this->input->post('id_edi')) ? null : $this->input->post('id_edi');
		$edib = empty($this->input->post('id_edib')) ? null : $this->input->post('id_edib');
		$edisfm = empty($this->input->post('edisfm')) ? null : $this->input->post('edisfm');
		
		$data = array(
			'id_edt' => $edt,
			'id_edi' => $edi,
			'id_edib' => $edib,
			'pub' => $this->input->post('pub')
		);
		$this->db->where('id', $id);
		$this->db->set('lastmod_date', 'NOW()', FALSE);
		$this->db->update($this->mod_table, $data);
		
		$url = $this->input->post('url') ? $this->input->post('url') : 'estate-'.$id;

		$data = array(
			'admission' => $this->input->post('admission'),
			'description' => $this->input->post('description'),
			'meta_title' => $this->input->post('meta_title'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'meta_description' => $this->input->post('meta_description')
		);
		$this->db->where('id_ede', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table . '_content', $data);

		$this->investments_model->set_estate_map($edisfm, $id);
		
		$this->user_url->save_url(true,$url,'estatedev_estates',$id,$lang);
		$this->user_url->update_routes();

		return TRUE;
	}

}
