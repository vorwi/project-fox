<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends MY_Model {

	public function __construct(){
		parent::__construct('admin_users');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	public function get_groups_with_users($params = FALSE){
		if(is_array($params)) $params = (object)$params;
		
		$this->db->select('rc.*, r.*');
		$this->db->from('groups as r');
		$this->db->join('groups_content as rc', "r.id = rc.id_g AND rc.lang = '".$_SESSION['admin_lang']."'", 'left');
		$this->db->order_by('r.position');
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $row){
				$params_users = new stdClass();
				$params_users->status = 1;
				if(ENVIRONMENT != 'development'){
					$params_users->main = 0;
				}
				$params_users->group = $row->id;
				if(isset($params->id_user) && !empty($params->id_user)) {
					if(!is_array($params->id_user)){
						$params->id_user = array($params->id_user);
					}
					$params_users->id = $params->id_user;
				}
				$row->options = $this->get_rows($params_users);
				if($row->options !== FALSE){
					$data[$row->id] = $row;
				}
			}
			if(ENVIRONMENT == 'development'){
				$row = new stdClass();
				$row->name = 'Artneo';
				$row->id = 'Artneo';
				
				$params_users = new stdClass();
				$params_users->status = 1;
				$params_users->main = 1;
				$params_users->group = NULL;
				if(isset($params->id_user) && !empty($params->id_user)) {
					if(!is_array($params->id_user)){
						$params->id_user = array($params->id_user);
					}
					$params_users->id = $params->id_user;
				}
				$row->options = $this->get_rows($params_users);
				if($row->options !== FALSE){
					$data[$row->id] = $row;
				}
			}
			return $data;
		}else{
			return FALSE;
		}
	}
	
	public function get_rows($params = FALSE) {
		if(is_array($params)) $params = (object)$params;
		
		$data = array();
		
		$this->db->select('u.*');
		$this->db->from($this->mod_table.' as u');
		if(isset($params->id) && !empty($params->id)) {
			if(!is_array($params->id)){
				$params->id = array($params->id);
			}
			$this->db->where_in('u.id', $params->id);
		}
		if(isset($params->status)) {
			$this->db->where('status', $params->status);
		}
		if(ENVIRONMENT != 'development') {
			$this->db->where('main', 0);
		}elseif(ENVIRONMENT == 'development' && isset($params->main)){
			$this->db->where('main', $params->main);
		}
		
		if(isset($params->group)) {
			$this->db->where('u.id_group', $params->group);
		}
		$this->db->order_by('u.id_group, u.login');
		if(isset($params->offset)) {
			$this->db->limit($params->limit, $params->offset);
		}
		if(isset($params->count)) {
			$query = $this->db->get();
			$return_array = array(
					'count' => $query->num_rows()
			);
			return $return_array;
		} else {
			$query = $this->db->get();
		}
		
		if($query->num_rows()>0){
			$data_key = 'id';
			foreach($query->result() as $row) {
				if(isset($params->data_key) && !empty($params->data_key) && isset($row->{$params->data_key})){
					$data_key = $params->data_key;
				}
				$data[$row->{$data_key}] = $row;
			}
			return $data;
		}else {
			return FALSE;
		}
	}

	public function add() {
		$this->form_validation->set_rules('id_group', lang('Grupa'), 'required');
		$this->form_validation->set_rules('login', lang('Login'), 'trim|required|duplicate_login_check');
		$this->form_validation->set_rules('email', lang('Email'), 'trim|required|valid_email');
		return $this->form_validation->run();
	}

	public function insert() {
		$pass = password_generator();
	 
		$data = array(
			'id_group' => $this->input->post('id_group'),
			'date_add' => $pass['time'],
			'login' => $this->input->post('login'),
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'post_code' => $this->input->post('post_code'),
			'city' => $this->input->post('city'),
			'address' => $this->input->post('address'),
			'password' => $pass['pass_crypt'],
			'main' => 0,
			'status' => 1
		);
		
		$this->db->insert($this->mod_table, $data);
	
		if($this->db->affected_rows()==1) {
			$id = $this->db->insert_id();
			
			if($this->input->post('send_password')==1){
				$html = lang("Twoje dane do panelu CMS:")."<br /><br />".lang("Adres logowania:")."  http://{$_SERVER['HTTP_HOST']}/admin<br />".lang("login").": {$this->input->post('login')}<br />".lang("hasło").": {$pass['pass']}<br />";
				
				$body = $this->email->full_html(lang('Hasło do panelu CMS'), $html);
				$this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], 'no-reply@'.$_SERVER['HTTP_HOST']);
				$this->email->to($this->input->post('email'));
		
				$this->email->subject(lang('Hasło do panelu CMS').' '.$_SERVER['HTTP_HOST']);
				$this->email->message($body);
			
				$this->email->send();
			}

			return $pass['pass'];
		}else {
			return FALSE;
		}
	}
	
	public function edit($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;
		
		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		return $query->num_rows()>0 ? $query->result() : FALSE;
	}
	
	public function save() {
		$data = array();
		$changed = array();

		$post = $this->input->post('check');

		foreach($post as $row) {
			$data = array(
				'id_group' => ifempty($this->input->post('id_group_'.$row)),
				'name' => $this->input->post('name_'.$row),
				'login' => $this->input->post('login_'.$row),
				'email' => $this->input->post('email_'.$row),
				'phone' => $this->input->post('phone_'.$row),
				'post_code' => $this->input->post('post_code_'.$row),
				'city' => $this->input->post('city_'.$row),
				'address' => $this->input->post('address_'.$row),
				'status' => $this->input->post('status_'.$row)
			);
			
			if($this->input->post('pass_reset_'.$row)==1){
				$pass = password_generator();
				$data['date_add']=$pass['time'];
				$data['password']=$pass['pass_crypt'];
			}
			
			$this->db->where('id', $row);
			$query = $this->db->update($this->mod_table, $data);
			$affected_rows = $this->db->affected_rows();
			
			if($this->input->post('pass_reset_'.$row)==1){
				$changed[] = array('login'=>$this->input->post('login_'.$row), 'password'=>$pass['pass']);
				if($affected_rows==1){
					$html = lang("Stare haslo zostało zmienione. Twoje nowe dane do panelu CMS:")."<br /><br />".lang("Adres logowania:")."  http://{$_SERVER['HTTP_HOST']}/admin<br />".lang("login").": {$this->input->post('login_'.$row)}<br />".lang("nowe hasło").": {$pass['pass']}<br />";
					
					$body = $this->email->full_html(lang('Hasło do panelu CMS'), $html);
					$this->email->from('no-reply@'.$_SERVER['HTTP_HOST'], 'no-reply@'.$_SERVER['HTTP_HOST']);
					$this->email->to($this->input->post('email_'.$row));
			
					$this->email->subject(lang('Hasło do panelu CMS').' '.$_SERVER['HTTP_HOST']);
					$this->email->message($body);
				
					$this->email->send();
				}
			}
		}
		return $changed;
	}
	
	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;
		
		if(empty($post)) { return FALSE; }

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);
		
		return TRUE;
	}
	
	public function powers($id) {
		$data = array();
		$post = !is_numeric($id) ? $this->input->post('check') : $id;
		
		$this->db->select('mc.*, m.*');
		$this->db->from('modules as m');
		$this->db->join('modules_content as mc', "m.id = mc.id_m AND mc.lang = '".$_SESSION['admin_lang']."'", 'left');
		$this->db->where('m.main', 0);
		$this->db->where('m.global', 0);
		$this->db->where('m.pub', 1);
		$this->db->order_by('m.mod_dir');
		$this->db->order_by('m.position');
		$query = $this->db->get();
		
		if($query->num_rows()>0) {
			foreach($query->result() as $row) {
				$data['modules'][]= $row;
			}
		}
		
		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);
		
		if($query->num_rows()>0){
			foreach($query->result() as $row) {
				$this->db->where('id_u', $row->id);
				$query1 = $this->db->get('rights');
				$tab = array();
				
				if($query1->num_rows()>0) {
					foreach($query1->result() as $row1) {
						$tab[] = $row1;
					}
				}
				
				$data['users'][] = array('user'=>$row,'rights'=>$tab);
			}
			return $data;
		}else {
			return FALSE;
		}
	}
	
	public function powers_save() {
		$data = array();

		$del = $this->input->post('id');
		$this->db->where_in('id_u', $del);
		$query = $this->db->delete('rights');
		
		$post = $this->input->post('rights');
		$post_cq = $this->input->post('can_query');
		
		if(!empty($post)) {
			foreach($post as $key => $row) {
				$id = $key;
				$tab = $row;
				
				foreach($tab as $row1){
					$data = array(
						'id_u' => $id,
						'id_m' => $row1
						);
						
					$this->db->insert('rights', $data);
				}
			}
		}
		return TRUE;
	}
}
