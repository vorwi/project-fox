<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups_model extends MY_Model {

	public function __construct(){
		parent::__construct('groups');
		$this->_init_module_table(__FILE__, __DIR__);
	}
	
	public function get_rows($params = FALSE) {
		if(is_array($params)) $params = (object)$params;
		
		$this->db->select('rc.*, r.*'); 
		$this->db->from($this->mod_table.' as r');
		$this->db->join($this->mod_table.'_content as rc', "r.id = rc.id_g AND rc.lang = '".$_SESSION['admin_lang']."'", 'left');
		$this->db->order_by('r.position');
		$query = $this->db->get();
	
		if($query->num_rows() > 0) {
			$data = array();
			foreach ($query->result() as $row){
				$data[$row->id] = $row;
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) { return FALSE; }

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);
		return TRUE;
	}

	public function add() {
		$this->form_validation->set_rules('name', lang('Nazwa'), 'trim|required');
		$this->form_validation->set_rules('description', lang('Opis'), 'trim');

		return $this->form_validation->run();
	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();

		$row = $query->row();
		$position = $row->position+1;

		$data = array(
			'position'	=> $position
		);

		$this->db->insert($this->mod_table, $data);

		if($this->db->affected_rows()==1) {
			 $id = $this->db->insert_id();

			 $data = array(
				'id_g' => $id,
			 	'lang' => $this->admin->get_main_lang(),
				'name' => $this->input->post('name'),
			 	'description' => $this->input->post('description')
			);

			if($this->neocms->insert_lang_content($this->mod_table.'_content', $data)){ 
				return TRUE;
			}else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function LangExists($id,$lang){
		$this->db->where('id_g',$id);
		$this->db->where('lang',$lang);
		$query = $this->db->get($this->mod_table.'_content');
		return $query->num_rows()>0;
	}

	public function edit($id,$lang) {
		if(!$this->LangExists($id, $lang)){
			$this->db->where('id_g',$id);
			$this->db->where('lang',$this->admin->get_main_lang());
			$query = $this->db->get($this->mod_table.'_content');
			if($query->num_rows()>0){
				$row = $query->row();
			}else{
				$row = new stdClass();
				$row->name = '';
				$row->description = '';
			}
			
			$data = (array)$row;
			unset($data['id']);
			$data['lang'] = $lang;
			$this->db->insert($this->mod_table.'_content', $data);
			
		}
		
		$this->db->select('rc.*, r.*');
		$this->db->from($this->mod_table.' as r');
		$this->db->join($this->mod_table.'_content as rc', 'r.id = rc.id_g');
		$this->db->where('rc.id_g',$id);
		$this->db->where('rc.lang', $lang);
		$query = $this->db->get();

		if($query->num_rows()>0){
			$row = $query->row();

			if(!empty($row->lang)){
				$row->lang = $this->neocms->get_language($row->lang);
			}

			return $row;
		}else{
			return FALSE;
		}
	}

	public function save($id,$lang) {
		$data = array(
			'name' => $this->input->post('name'),
			'description' => $this->input->post('description')
		);

		$this->db->where('id_g', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table.'_content', $data);

		return TRUE;
	}
	
	public function rights($id) {

		$this->db->where('id_g', $id);
		$query = $this->db->get('group_rights');
	
		return $query->result();
	}
	
	public function power_models() {
		$data = array();
		 
		$this->db->select('mc.*, m.*');
		$this->db->from('modules as m');
		$this->db->join('modules_content as mc', "m.id = mc.id_m AND mc.lang = '".$_SESSION['admin_lang']."'", 'left');
		$this->db->where('m.main', 0);
		$this->db->where('m.global', 0);
		$this->db->where('m.pub', 1);
		$this->db->order_by('m.mod_dir');
		$this->db->order_by('m.position');
		$query = $this->db->get();
	
		return $query->num_rows()>0 ? $query->result() : FALSE;
	}
	
	public function powers_save($id) {
		$this->db->where_in('id_g', $id);
		$query = $this->db->delete('group_rights');
	
		$post = $this->input->post('rights');
		$post_cq = $this->input->post('can_query');
	
		if(!empty($post)) {
			foreach($post as $key => $row) {
				$data = array(
						'id_g' => $id,
						'id_m' => $row
				);
	
				$this->db->insert('group_rights', $data);
			}
		}
		return TRUE;
	}
}
