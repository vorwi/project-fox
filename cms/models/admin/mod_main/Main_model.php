<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends MY_Model {

	public function __construct(){
	
		parent::__construct();
	}
	
	public function get_logins($limit = 8) {
		$table = 'admin_users_tracking';
		if($this->db->table_exists($table)) {
			$this->db->from($table);
			if(!$this->user->main) {
				$this->db->where('id_u', $this->user->id);
			}
			$this->db->order_by('date_try', 'DESC');
			$this->db->limit($limit);
			$query = $this->db->get();
			
			return $query->result();
		}
		return FALSE;
	}
	
	public function get_articles($limit = 8) {
		$table = 'articles_content';
		if($this->db->table_exists($table) && $this->_check_module('articles')) {
			$this->db->select("ac.*, au.name as author_name");
			$this->db->from($table.' as ac');
			$this->db->join('admin_users as au', 'ac.author = au.id');
			$this->db->order_by('date_modified', 'DESC');
			$this->db->limit($limit);
			$query = $this->db->get();
			
			return $query->result();
		}
		return FALSE;
	}
	
	public function get_news($limit = 8) {
		$table = 'news';
		if($this->db->table_exists($table) && $this->_check_module('news')) {
			$this->db->from($table);
			$this->db->order_by('date_modified', 'DESC');
			$this->db->limit($limit);
			$query = $this->db->get();
			
			return $query->result();
		}
		return FALSE;
	}
	
	private function _check_module($name) {
		$this->db->from('modules');
		$this->db->where('mod', $name);
		$this->db->where('pub', 1);
		$query = $this->db->get();
		return $query->num_rows() > 0;
	}
}

