<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backups_model extends MY_Model {

	public function __construct(){
		parent::__construct();
	}
	
	public function get_all() {
		$files = directory_map(config_item('backup_path'));
		rsort($files);
		array_shift($files);
		return $files;
	}
	
	public function restore_backup() {
		$file = config_item('backup_path').$this->input->post('backup_file');
		if(file_exists($file)) {
			$templine = '';
			$lines = explode("\n",gzdecode(file_get_contents($file)));
			$num_errors = 0;
			foreach($lines as $line) {
				if(substr($line, 0, 2) == '--' || $line == '') continue;
				$templine .= $line;
				if(substr(trim($line), -1, 1) == ';') {
					if(!$this->db->simple_query($templine)) $num_errors++;
					$templine = '';
				}				
			}
			return $num_errors==0;
		} 
		else return false;		
	}
}