<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Languages_model extends MY_Model {

	public function __construct() {
		parent::__construct('lang');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_languages() {

		$this->db->order_by('position');
		$query = $this->db->get('lang');

		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function get_lang($code) {

		$this->db->where('short', $code);
		$query = $this->db->get('lang');

		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}

	public function add() {

		$this->form_validation->set_rules('short', 'kod ISO', 'trim|required|alpha');
		$this->form_validation->set_rules('lang', 'nazwa', 'trim|required');
		$this->form_validation->set_rules('dir', 'katalog', 'trim|required');
		$this->form_validation->set_rules('copy_articles');

		return $this->form_validation->run();
	}

	public function insert() {
		
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();
		$position = $query->row()->position + 1;
		
		$code = $this->input->post('short');
		$set = array(
			'short' => $code,
			'lang' => $this->input->post('lang'),
			'dir' => $this->input->post('dir'),
			'pub' => 0,
			'position' => $position
		);
		$this->db->insert($this->mod_table, $set);

		if($this->db->affected_rows() == 1) {

			if($this->input->post('copy_articles') == 1) {

				$this->db->where('lang', $this->admin->get_main_lang());
				$query = $this->db->get('articles_content');

				if($query->num_rows() > 0) {
					foreach($query->result() as $key => $row) {
						$set = array(
							'id_art' => $row->id_art,
							'lang' => $code,
							'title' => $row->title,
							'short_title' => $row->short_title,
							'url' => $row->url,
							'target' => $row->target,
							'admission' => $row->admission,
							'content' => $row->content,
							'meta_title' => $row->meta_title,
							'meta_description' => $row->meta_description,
							'meta_keywords' => $row->meta_keywords,
							'admission_on' => $row->admission_on,
							'author' => $this->session->userdata('user_id'),
							'pub' => 0
						);
						$this->db->insert('articles_content', $set);
						$this->user_url->save_url(false, $row->title, 'articles', $row->id_art, $code);
					}
					$this->user_url->update_routes();
				}
			}

			return TRUE;
		}
		return FALSE;
	}

	public function save($id) {
		$set = array(
			'short' => $this->input->post('short'),
			'lang' => $this->input->post('lang'),
			'dir' => $this->input->post('dir'),
			'pub' => $this->input->post('pub')
		);
		$this->db->where('id', $id);
		$this->db->update($this->mod_table, $set);

		return $this->db->affected_rows() == 1;
	}

	public function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->mod_table);

		return $this->db->affected_rows() == 1;
	}

}
