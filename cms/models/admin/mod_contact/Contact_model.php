<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends MY_Model {

	public function __construct() {
		parent::__construct('contact');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all() {
		$this->db->select('tk1.*, tk2.title');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join('articles_content as tk2', 'tk1.id_art=tk2.id_art and tk2.lang=\''.$this->admin->get_main_lang().'\'');
		$this->db->order_by('tk1.name');
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $row) {

				$row->title = $this->_get_art_cat($row->id_art, '');

				$data[] = $row;
			}

			return $data;
		} else
			return FALSE;
	}

	private function _get_art_cat($id_art, $title) {
		$this->db->select('tk1.id, tk1.id_tree, tk1.id_cat, tk2.title');
		$this->db->from('articles as tk1');
		$this->db->join('articles_content as tk2', "tk1.id = tk2.id_art and tk2.lang='".$this->admin->get_main_lang()."'");
		$this->db->where('tk1.id', $id_art);

		$query = $this->db->get();

		$row = $query->row();

		$title = !empty($title) ? $row->title.' / '.$title : $row->title;

		if($row->id_tree == 0) {
			$this->db->where('id', $row->id_cat);
			$query = $this->db->get('articles_category');

			$row = $query->row();

			$title = $row->name.' / '.$title;
			return $title;
		} else {

			return $this->_get_art_cat($row->id_tree, $title);
		}
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) return FALSE;

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);

		return TRUE;
	}

	public function verify_form($type = 'add') {
		
		if($type == 'add') {
			$this->form_validation->set_rules('id_art', lang('Artykuł'), 'required|trim');
			$this->form_validation->set_rules('email', lang('Adres e-mail'), 'required|valid_email|trim');
			$this->form_validation->set_rules('name', lang('Nazwa wyświetlana'), 'trim');
			$this->form_validation->set_rules('lang');
		} elseif ($type == 'edit') {
			
			$post = $this->input->post('check');
			
			foreach($post as $row) {
				$this->form_validation->set_rules('id_art_'.$row, lang('Artykuł').' #'.$row, 'required|trim');
				$this->form_validation->set_rules('email_'.$row, lang('Adres e-mail').' #'.$row, 'required|valid_email|trim');
				$this->form_validation->set_rules('name_'.$row, lang('Nazwa wyświetlana').' #'.$row, 'trim');
				$this->form_validation->set_rules('lang_'.$row);
			}		
		}
		
		return $this->form_validation->run();
	}

	public function insert() {
		$data = array(
			'id_art' => $this->input->post('id_art'),
			'lang' => $this->input->post('lang'),
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'attach' => ($this->input->post('attach') !== NULL ? $this->input->post('attach') : 0),
			'attach_label' => ($this->input->post('attach_label') !== NULL ? $this->input->post('attach_label') : ''),
			'pub' => 1
		);

		$this->db->insert($this->mod_table, $data);

		return $this->db->affected_rows() == 1;
	}

	public function edit($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;
		
		if(empty($post)) return FALSE;

		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $row) {
				if(!empty($row->lang)) {
					$row->lang = $this->neocms->get_language($row->lang);
				}

				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}

	public function save() {
		$changed = array();

		$post = $this->input->post('check');

		foreach($post as $row) {
			$data = array(
				'id_art' => $this->input->post('id_art_'.$row),
				'name' => $this->input->post('name_'.$row),
				'lang' => $this->input->post('lang_'.$row),
				'email' => $this->input->post('email_'.$row),
				'attach' => ($this->input->post('attach_'.$row) !== NULL ? $this->input->post('attach_'.$row) : 0),
				'attach_label' => ($this->input->post('attach_label_'.$row) !== NULL ? $this->input->post('attach_label_'.$row) : '')
			);

			$this->db->where('id', $row);
			$query = $this->db->update($this->mod_table, $data);

			$changed[] = array('name' => $this->input->post('name_'.$row));
		}

		return $changed;
	}

}
