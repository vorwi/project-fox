<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends MY_Model {

	public function __construct() {		
		parent::__construct('articles_category');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all() {		
		$this->db->order_by('position');
		$query = $this->db->get($this->mod_table);

		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function delete($id) {		
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) return FALSE;

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);

		return TRUE;
	}

	public function verify_form() {
		$this->form_validation->set_rules('name', lang('Nazwa'), 'required|trim');
		return $this->form_validation->run();
	}

	public function insert() {		
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$query = $this->db->get();

		$position = $query->row()->position + 1;

		$data = array(
			'name' => $this->input->post('name'),
			'position' => $position,
		);

		$this->db->insert($this->mod_table, $data);

		return $this->db->affected_rows() == 1 ? TRUE : FALSE;
	}

	public function catExists($id) {		
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);
		return $query->num_rows() > 0;
	}

	public function edit($id) {		
		$post = !is_numeric($id) ? $this->input->post('check') : $id;
		
		if(empty($post)) return FALSE;

		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		return $query->num_rows() > 0 ? $query->result() : FALSE;
	}

	public function save() {		
		$data = array();

		$post = $this->input->post('check');

		foreach($post as $row) {
			
			$data[] = array('id' => $row, 'name' => $this->input->post('name_'.$row), );
		}
		$this->db->update_batch($this->mod_table, $data, 'id');

		return $data;
	}
}
