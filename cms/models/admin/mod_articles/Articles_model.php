<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Articles_model extends MY_Model {

    public function __construct() {
        parent::__construct('articles');
        $this->_init_module_table(__FILE__, __DIR__);
    }

    public function get_all_categories($cat = null) {
        if(is_numeric($cat)) $this->db->where('id', $cat);
        $this->db->order_by('position');
        $query = $this->db->get($this->mod_table.'_category');

        if($query->num_rows() > 0) {
            $data = array();
            foreach($query->result() as $row) {
                $data['categories'][$row->id] = $row;
                $data['categories'][$row->id]->articles = $this->get_all_articles($row->id, 0, TRUE);
            }

            return $data;
        } else
            return FALSE;
    }

    public function get_all_articles($id_cat, $id_tree, $with_children = FALSE) {
        $this->db->select('tk2.*, tk1.*');
        $this->db->from($this->mod_table.' as tk1');
        $this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_art', 'left');
        if(is_array($id_tree)) $this->db->where_in('tk1.id_tree', $id_tree);
        else $this->db->where('tk1.id_tree', $id_tree);
        $this->db->where('tk1.id_cat', $id_cat);
        $this->db->where('tk2.lang', $this->admin->get_main_lang());
        $this->db->order_by('tk1.position');
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            if($with_children) {
                $data = array();
                $parent_ids = array();

                foreach($query->result() as $row) {
                    $parent_ids[] = $row->id;
                    $data[$row->id] = $row;
                }
                $query->free_result();
                $children = $this->get_all_articles($id_cat, $parent_ids, $with_children);

                foreach($children as $row) {
                    if(!isset($data[$row->id_tree]->children))
                        $data[$row->id_tree]->children = array();
                    $data[$row->id_tree]->children[] = $row;
                }

                return $data;
            } else {
                return $query->result();
            }
        }
        return array();
    }

    public function save_positions($items) {
        $rows = array();
        $i = 1;
        foreach($items as $item) {
            if(!isset($item['id']) || (isset($item['id']) && empty($item['id']))) {
                continue;
            }
            $rows[] = array(
                'id' => $item['id'],
                'id_tree' => (empty($item['parent_id']) ? 0 : $item['parent_id']),
                'position' => $i
            );
            $i++;
        }

        if(!empty($rows)) {
            $affected_rows = $this->db->update_batch($this->mod_table, $rows, 'id');
            return $affected_rows;
        } else {
            return 0;
        }

        return false;
    }

    public function delete($id) {
        $post = !is_numeric($id) ? $this->input->post('check') : $id;

        if(empty($post)) return FALSE;

        $this->db->where_in('id', $post);
        $this->db->or_where_in('id_tree', $post);
        $query = $this->db->delete($this->mod_table);

        if($this->db->affected_rows() > 0) {
            $query = $this->db->get($this->mod_table);
            foreach($query->result() as $row) {
                if($row->id_tree != 0) {
                    $this->db->where('id', $row->id_tree);
                    $query1 = $this->db->get($this->mod_table);

                    if($query1->num_rows() == 0) {
                        $this->db->where('id', $row->id);
                        $this->db->or_where('id_tree', $row->id);
                        $query = $this->db->delete($this->mod_table);
                    }
                }
            }
        }

        $this->db->where_in('dest_id', $post);
        $this->db->where('type', 'articles');
        $query_uu = $this->db->delete('user_url');
        $this->user_url->update_routes();

        return TRUE;
    }

    public function verify_form($type = 'add', $id_art = 0) {		
        $this->form_validation->set_rules('id_tree');
        
        if($type == 'add') {
            $this->form_validation->set_rules('title', lang('Nazwa'), 'trim|required|htmlspecialchars');
        }
        
        if($type == 'edit') {
            
            foreach($this->languages as $row) {
                $this->form_validation->set_rules('lang['.$row->short.'][title]', lang('Nazwa').' '.lang_flag($row), 'trim|required|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][short_title]', lang('Skrócony tytuł').' '.lang_flag($row), 'trim|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][url]', lang('Zewnętrzny odnośnik').' '.lang_flag($row), 'trim|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][user_url]', lang('Niestandardowy adres URL').' '.lang_flag($row), 'trim|iso_clear');
                $this->form_validation->set_rules('lang['.$row->short.'][meta_title]', lang('Meta Tytuł (title)').' '.lang_flag($row), 'max_length[60]|trim|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][meta_description]', lang('Meta Opis (description)').' '.lang_flag($row), 'max_length[160]|trim');
                $this->form_validation->set_rules('lang['.$row->short.'][meta_keywords]', lang('Meta Słowa kluczowe (keywords)').' '.lang_flag($row), 'trim');
                $this->form_validation->set_rules('lang['.$row->short.'][og_title]', lang('Open Graph title').' '.lang_flag($row), 'trim|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][og_description]', lang('Open Graph description').' '.lang_flag($row), 'trim|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][og_image]', lang('Open Graph image').' '.lang_flag($row), 'trim|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][og_url]', lang('Open Graph url').' '.lang_flag($row), 'trim|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][admission]');
                $this->form_validation->set_rules('lang['.$row->short.'][pub]');
                $this->form_validation->set_rules('lang['.$row->short.'][target]');
                $this->form_validation->set_rules('lang['.$row->short.'][content]', lang('Treść').' '.lang_flag($row), 'trim');
            }
            
            $this->form_validation->set_rules('id_tree',lang('Pozycja w drzewie'),'required|not['.$id_art.']',array('not'=>lang('Artykuł nie może być swoim własnym rodzicem.')));			
            $this->form_validation->set_rules('pub');
        }
        return $this->form_validation->run();
    }

    public function insert() {
        $this->db->select('max(position) as position');
        $this->db->from($this->mod_table);
        $this->db->where('id_cat', $this->input->post('id_cat'));
        $this->db->where('id_tree', $this->input->post('id_tree'));
        $query = $this->db->get();

        $position = $query->row()->position + 1;

        $data = array(
            'id_cat' => $this->input->post('id_cat'),
            'id_tree' => $this->input->post('id_tree'),
            'position' => $position
        );

        $this->db->insert($this->mod_table, $data);

        if($this->db->affected_rows() == 1) {
            $id = $this->db->insert_id();

            $data = array(
                'id_art' => $id,
                'lang' => $this->admin->get_main_lang(),
                'title' => $this->input->post('title'),
                'author' => $this->session->userdata('user_id'),
                'pub' => 1,
            );
            $this->db->set('date_add', 'NOW()', FALSE);
            $this->db->set('date_modified', 'NOW()', FALSE);
            $this->db->insert($this->mod_table.'_content', $data);

            $s = $this->db->affected_rows() == 1 ? TRUE : FALSE;
            
            if($this->input->post('id_tree')!=0) {
                $this->db->where('dest_id', $this->input->post('id_tree'));
                $this->db->where('lang', $this->admin->get_main_lang());
                $this->db->where('type', 'articles');
                $query = $this->db->get('user_url');
                if($query->num_rows()>0) {
                    $raw_user_url = $query->row()->url.'/'.$this->input->post('title');
                }
                else $raw_user_url = $this->input->post('title');
            }
            else $raw_user_url = $this->input->post('title');

            $this->user_url->save_url(false, $raw_user_url, 'articles', $id, $this->admin->get_main_lang());
            $this->user_url->update_routes();

            return $s === TRUE ? $this->input->post('title') : FALSE;
        }
        else return FALSE;
    }

    public function artExists($id) {
        $this->db->where('id_art', $id);
        $query = $this->db->get($this->mod_table.'_content');
        return $query->num_rows() > 0;
    }

    public function edit($id) {

        //pobranie informacji niezależnych od języka
        $this->db->where('id', $id);
        $query = $this->db->get($this->mod_table);

        if($query->num_rows() > 0) {
            $art = $query->row();
            
            //dodanie pól językowych
            $art->lang = array();
            
            $this->db->select('ac.*, l.short as lang, l.main, uu.url as user_url, au.login as author_login, au.name as author_name');
            $this->db->from($this->mod_table.'_content ac');

            $this->db->join('lang l', "ac.lang = l.short AND ac.id_art = '{$id}'", 'RIGHT');

            $this->db->join('user_url uu', "uu.dest_id = ac.id_art AND uu.type = 'articles' AND uu.lang = ac.lang", 'LEFT');
            $this->db->join('admin_users au', "ac.author = au.id", 'LEFT');
            $this->db->order_by('l.position');
            $query = $this->db->get();
            
            foreach($query->result() as $ac_row) {
                
                if($ac_row->main) {
                    $main_title = $ac_row->title;
                    $main_id_art = $ac_row->id_art;
                } else {
                    if(empty($ac_row->title)) $ac_row->title = $main_title;
                    if(empty($ac_row->id_art)) $ac_row->id_art = $main_id_art;
                }
                
                $ls = $ac_row->lang;
                
                $ac_row->link = article_url($ac_row, FALSE);
                $ac_row->URI = article_url($ac_row);
            
                $art->lang[$ls] = $ac_row;
            }

            return $art;
        }
        return FALSE;

    }

    public function save($article) {
        
        $id = $article->id;
        
        $data = array(
            'id_tree' => $this->input->post('id_tree'),
            'pub' => $this->input->post('pub'),
            'pub_menu' => $this->input->post('pub_menu')
        );
        $this->db->where('id', $id);
        $this->db->update($this->mod_table, $data);

        $changes = 0;
        $lang_data = $this->input->post('lang');
        if(is_array($lang_data)) {
            foreach($lang_data as $lang => $fields) {
                $art_lang_row = ifset($article->lang[$lang]);

                $data = array(
                        'title' 			=> $fields['title'],
                        'short_title' 		=> $fields['short_title'],
                        'meta_title' 		=> $fields['meta_title'],
                        'url' 				=> $fields['url'],
                        'target' 			=> $fields['target'],
                        'meta_description' 	=> $fields['meta_description'],
                        'meta_keywords' 	=> $fields['meta_keywords'],
                        'og_title' 			=> $fields['og_title'],
                        'og_description' 	=> $fields['og_description'],
                        'og_image' 			=> $fields['og_image'],
                        'og_url' 			=> $fields['og_url'],
                        'admission' 		=> $fields['admission'],
                        'content'	 		=> $fields['content'],
                        'author'	 		=> $this->session->userdata('user_id'),
                        'pub'	 			=> $fields['pub'],
                );

                // każda podstrona ma inne pola które podmienia dla strony głównej

                if ($id == 4) {

                    $data['main_header']       = $fields['main_header'];
                    $data['main_subtitle']     = $fields['main_subtitle'];
                    $data['main_link']         = $fields['main_link'];

                } elseif ($id == 5){

                    $data['about_header']      = $fields['about_header'];
                    $data['about_admission']   = $fields['about_admission'];
                    $data['about_content']     = $fields['about_content'];

                } elseif ($id == 6){

                    $data['clients_header']        = $fields['clients_header'];
                    $data['clients_link_label']    = $fields['clients_link_label'];
                    $data['clients_link']          = $fields['clients_link'];

                    // for ($i=1; $i <= 7 ; $i++) { 
                    //     $data['clients_number_'.$i.'_top']    = $fields['clients_number_'.$i.'_top'];
                    //     $data['clients_number_'.$i]           = $fields['clients_number_'.$i];
                    //     $data['clients_number_'.$i.'_bottom'] = $fields['clients_number_'.$i.'_bottom'];
                    // }

                } elseif ($id == 7){

                    $data['offer_header']          = $fields['offer_header'];
                    $data['offer_admission']       = $fields['offer_admission'];
                    $data['offer_content']         = $fields['offer_content'];

                } elseif ($id == 8){


                    $data['collaboration_header']  = $fields['collaboration_header'];
                    // $data['collaboration_content_left'] = $fields['collaboration_content_left'];
                    // $data['collaboration_content_right'] = $fields['collaboration_content_right'];

                } elseif ($id == 9){

                    $data['distinguish_header']     = $fields['distinguish_header'];
                    $data['distinguish_link_label'] = $fields['distinguish_link_label'];
                    $data['distinguish_link']       = $fields['distinguish_link'];

                    // for ($i=1; $i <= 5 ; $i++) { 
                    //     $data['distinguish_icontext_'.$i] = $fields['distinguish_icontext_'.$i];
                    // }

                } elseif ($id == 10){

                    $data['contact_content']    = $fields['contact_content'];
                    $data['contact_img']        = $fields['contact_img'];
                    $data['contact_directions'] = $fields['contact_directions'];

                }
        
                if(empty($art_lang_row->id)) {
                    //wpis dla danego języka nie istnieje w bazie
                    $data['id_art'] = $id;
                    $data['lang'] = $lang;
                    $this->db->set('date_add', 'NOW()', FALSE);
                    $this->db->set('date_modified', 'NOW()', FALSE);
                    $this->db->insert($this->mod_table.'_content', $data);
        
                    $this->user_url->save_url(true, $fields['user_url'], 'articles', $id, $lang);
                    $changes++;
                } else {
                    $this->db->where('id_art', $id);
                    $this->db->where('lang', $lang);
                    $this->db->set('date_modified', 'NOW()', FALSE);
                    $this->db->update($this->mod_table.'_content', $data);
    
                    $this->user_url->save_url(true, $fields['user_url'], 'articles', $id, $lang);
                    $changes++;
                    
                }
            }
            if($changes > 0) {
                $this->user_url->update_routes();
            }
        }

        return TRUE;
    }

    public function get_all_numbers()
    {
        $this->db->select('*');
        $this->db->from('numbers');
        
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function add_number()
    {   
        $data = [
            'top' => $this->input->post('top'),
            'number' => $this->input->post('number'),
            'bottom' => $this->input->post('bottom'),
        ];

        $this->db->insert('numbers', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function save_numbers()
    {
        $numbers = $this->input->post('numbers');

        $this->db->empty_table('numbers'); 

        foreach ($numbers as $number) {
            $data = [
                'top' => $number['top'],
                'number' => $number['number'],
                'bottom' => $number['bottom'],
            ];
    
            $this->db->insert('numbers', $data);
        }

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function delete_number($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('numbers');

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_all_offers()
    {
        $this->db->select('*');
        $this->db->from('offers');
        
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function add_offer()
    {   
        $data = [
            'text' => $this->input->post('text'),
        ];

        $this->db->insert('offers', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function save_offers()
    {
        $offers = $this->input->post('offers');

        $this->db->empty_table('offers'); 

        foreach ($offers as $offer) {
            $data = [
                'text' => $offer['text'],
            ];
    
            $this->db->insert('offers', $data);
        }

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function delete_offer($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('offers');

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_all_skills()
    {
        $this->db->select('*');
        $this->db->from('skills');
        
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function add_skill()
    {   
        $data = [
            'icon' => $this->input->post('icon'),
            'text' => $this->input->post('text'),
        ];

        $this->db->insert('skills', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function save_skills()
    {
        $skills = $this->input->post('skills');

        $this->db->empty_table('skills'); 

        foreach ($skills as $skill) {
            $data = [
                'icon' => $skill['icon'],
                'text' => $skill['text'],
            ];
    
            $this->db->insert('skills', $data);
        }

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function delete_skill($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('skills');

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}
