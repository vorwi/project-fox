<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends MY_Model {

	public function __construct() {
		parent::__construct('gallery');
		$this->_init_module_table(__FILE__, __DIR__);

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
		if(!is_dir($path)) {
			@mkdir($path);
			@chmod($path, 0777);
		}
	}

	public function get_all() {
		$this->db->order_by('position asc, id_art, id_news');
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $row) {
				if($row->id_art != NULL) {
					$row->title = $this->_get_art_cat($row->id_art, '');
				} elseif($row->id_news != NULL) {
					$row->title = $this->_get_news($row->id_news);
				} else {
					$row->title = '';
				}
				$data[] = $row;
			}

			return $data;
		} else
			return FALSE;
	}

	public function get_image($id) {
		$data = array();

		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table.'_img');

		if($query->num_rows() > 0) {
			$row = $query->row();
			$gal = $row->id_gal;
			$img = $row->img;
			$desc = $row->desc;
		} else
			return FALSE;

		$this->db->where('id', $gal);
		$query = $this->db->get($this->mod_table);

		$row = $query->row();
		$dir = $row->dir;

		$path = config_item('upload_path').$this->img_dir.$dir.'/';

		$data = array(
			'id' => $id,
			'dir' => $dir,
			'img' => $img,
			'path' => $path,
			'desc' => $desc,
		);

		return $data;
	}

	private function _get_art_cat($id_art, $title) {
		$this->db->select('tk1.id, tk1.id_tree, tk1.id_cat, tk2.title');
		$this->db->from('articles as tk1');
		$this->db->join('articles_content as tk2', "tk1.id = tk2.id_art and tk2.lang='".$this->admin->get_main_lang()."'");
		$this->db->where('tk1.id', $id_art);

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$row = $query->row();

			if(!empty($title))
				$title = $row->title.' / '.$title;
			else
				$title = '<b>'.$row->title.'</b>';

			if($row->id_tree == 0) {
				$this->db->where('id', $row->id_cat);
				$query = $this->db->get('articles_category');

				$row = $query->row();

				$title = $row->name.' / '.$title;
				return $title;
			} else {
				return $this->_get_art_cat($row->id_tree, $title);
			}
		} else {
			return "[".sprintf(lang('usunięty artykuł nr %s'), $id_art)."]";
		}
	}

	private function _get_news($id_news) {
		$this->db->select('nc.title, ac1.title as art_title, ac2.title as art_parent_title, ac3.title as art_grandparent_title');
		$this->db->from('news as n');
		$this->db->join('news_content as nc', "n.id = nc.id_news and nc.lang='".$this->admin->get_main_lang()."'");
		$this->db->join('articles_content as ac1', "n.id_art = ac1.id_art and ac1.lang='".$this->admin->get_main_lang()."'");
		$this->db->join('articles as a1', 'ac1.id_art = a1.id');
		$this->db->join('articles_content as ac2', "a1.id_tree = ac2.id_art and ac2.lang='".$this->admin->get_main_lang()."'", 'LEFT');
		$this->db->join('articles as a2', 'ac2.id_art = a2.id', 'LEFT');
		$this->db->join('articles_content as ac3', "a2.id_tree = ac3.id_art and ac3.lang='".$this->admin->get_main_lang()."'", 'LEFT');
		$this->db->where('n.id', $id_news);

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			$row = $query->row();

			$path = '';
			if(!empty($row->art_grandparent_title))
				$path .= $row->art_grandparent_title.' / ';
			if(!empty($row->art_parent_title))
				$path .= $row->art_parent_title.' / ';
			$path .= $row->art_title.' / <b>'.$row->title.'</b>';
		} else {
			$path = "[".sprintf(lang('usunięta aktualność nr %s'), $id_news)."]";
		}

		return 'NEWS: '.$path;
	}

	public function get_all_news() {
		$data = array();

		$this->db->select('id, name');
		$this->db->order_by('position');
		$query = $this->db->get('news_category');

		$categories = array();
		if($query->num_rows() > 0) {
			foreach($query->result() as $cat)
				$categories[] = $cat->id;
		} else {
			$categories[] = 1;
			$cat = new stdClass();
		}
		foreach($categories as $cat_id) {
			$this->db->select('n.id, nc.title, ac1.title as art_title, ac2.title as art_parent_title, ac3.title as art_grandparent_title');
			$this->db->from('news as n');
			$this->db->join('news_content as nc', "n.id = nc.id_news and nc.lang='".$this->admin->get_main_lang()."'");
			$this->db->join('articles_content as ac1', "n.id_art = ac1.id_art and ac1.lang='".$this->admin->get_main_lang()."'");
			$this->db->join('articles as a1', 'ac1.id_art = a1.id');
			$this->db->join('articles_content as ac2', "a1.id_tree = ac2.id_art and ac2.lang='".$this->admin->get_main_lang()."'", 'LEFT');
			$this->db->join('articles as a2', 'ac2.id_art = a2.id', 'LEFT');
			$this->db->join('articles_content as ac3', "a2.id_tree = ac3.id_art and ac3.lang='".$this->admin->get_main_lang()."'", 'LEFT');
			$this->db->where('n.id_cat', $cat_id);
			$this->db->order_by('n.id_art');
			$queryn = $this->db->get();

			if($queryn->num_rows() > 0) {
				$cat->news = $queryn->result();
			}

			$data[] = $cat;
		}
		return $data;
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) return FALSE;

		$this->db->where_in('id_gal', $post);
		$query = $this->db->get($this->mod_table.'_img');

		foreach($query->result() as $row) $this->del_img($row->id);

		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		foreach($query->result() as $row) {
			$dir = './'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->dir.'/';

			@delete_files($dir, TRUE);
			@rmdir($dir);
		}

		$this->db->where_in('id', $post);
		$query = $this->db->delete($this->mod_table);

		return TRUE;
	}

	public function add() {
		$this->form_validation->set_rules('id_art', lang('Artykuł'), 'required');
		$this->form_validation->set_rules('id_news', lang('Aktualność'), 'required');
		$this->form_validation->set_rules('name', lang('Nazwa'), 'trim|required');
		$this->form_validation->set_rules('lang');
		$this->form_validation->set_rules('intro');
		$this->form_validation->set_rules('desc');

		return $this->form_validation->run();
	}

	public function insert() {
		$this->db->set('position', 'position+1', FALSE);
		$this->db->update($this->mod_table);

		$dir = time();

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;

		mkdir($path.$dir);
		@chmod($path, 0777);

		$id_art = $this->input->post('id_art');
		$id_news = $this->input->post('id_news');

		if($id_art != 0) {
			$id_news = NULL;
		} elseif($id_news != 0) {
			$id_art = NULL;
		} else {
			$msg = array(lang('Aby dodać galerię należy wybrać artykuł albo aktualność.'),1);
			$this->neocms->forward_msg($msg);
			return FALSE;
		}

		if(is_dir($path)) {
			$data = array(
				'id_art' => $id_art,
				'id_news' => $id_news,
				'lang' => $this->input->post('lang'),
				'dir' => $dir,
				'name' => htmlspecialchars($this->input->post('name')),
				'intro' => $this->input->post('intro'),
				'desc' => $this->input->post('desc'),
				'pub' => 1,
				'position' => 0
			);

			$this->db->insert($this->mod_table, $data);

			return $this->db->affected_rows() == 1;
		} else
			return FALSE;
	}

	public function edit($id) {
		$data = array();
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		$this->db->where_in('id', $post);
		$query = $this->db->get($this->mod_table);

		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				if(!empty($row->lang)) {
					$this->db->where('short', $row->lang);
					$query2 = $this->db->get('lang');

					$row->lang = $query2->row();
				}

				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}

	public function save() {
		$data = array();
		$changed = array();

		$post = $this->input->post('check');

		foreach($post as $row) {
			$data = array(
				'name' => htmlspecialchars($this->input->post('name_'.$row)),
				'intro' => $this->input->post('intro_'.$row),
				'lang' => $this->input->post('lang_'.$row),
				'desc' => $this->input->post('desc_'.$row)
			);

			$id_art = $this->input->post('id_art_'.$row);
			$id_news = $this->input->post('id_news_'.$row);
			if($id_art != 0) {
				$data['id_art'] = $id_art;
				$data['id_news'] = NULL;
			} elseif($id_news != 0) {
				$data['id_art'] = NULL;
				$data['id_news'] = $id_news;
			}

			$this->db->where('id', $row);
			$query = $this->db->update($this->mod_table, $data);

			$changed[] = array('name' => $this->input->post('name_'.$row));
		}

		return $changed;
	}

	public function get_all_images($id) {
		$data = array();

		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);

		$row = $query->row();
		$dir = $row->dir;

		$this->db->where('id_gal', $id);
		$this->db->order_by('position', $id);
		$query = $this->db->get($this->mod_table.'_img');

		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {

				$row->dir = $dir;
				$data[] = $row;
			}

			return $data;
		} else
			return FALSE;
	}

	public function add_img_pl($id) {
		$msg = array();
		$config = null;
		$img = md5(microtime());

		$dir_tmp = './'.config_item('temp').'gallery_'.time().'_'.$id.'/';
		@mkdir($dir_tmp);
		@chmod($dir_tmp, 0777);
		if(!is_dir($dir_tmp))
			return FALSE;

		$this->db->select('dir');
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);
		$row = $query->row();
		$gal_dir = $row->dir;

		$config['upload_path'] = $dir_tmp;
		$config['allowed_types'] = 'gif|jpeg|jpg|png';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $img;

		$this->upload->initialize($config);

		if(is_uploaded_file($_FILES['file']['tmp_name'])) {
			if(!$this->upload->do_upload('file')) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
			} else {
				$tab = $this->upload->data();

				$map_path = $tab['file_path'];

				$map = array($tab['file_name']);
				sort($map);

				if(!empty($map)) {
					if(!$this->save_thumbnails($id, $map, $map_path)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
					}
				}
			}

			return TRUE;
		} else {
			die('{"jsonrpc" : "2.0", "error" : {"code": 500, "message": "File upload failed."}, "id" : "id"}');
		}
	}

	public function save_thumbnails($id, $dir = null, $path = null) {
		$msg = array();

		if($dir == null || $path == null || empty($dir)) return FALSE;

		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table);

		$row = $query->row();
		$dir_gal = $row->dir;

		$i = 0;
		foreach($dir as $value) {
			$file = $path.'/'.$value;

			$tab = pathinfo($file);

			$dir = './'.config_item('site_path').config_item('upload_path').$this->img_dir.$dir_gal.'/';

			$name = md5(microtime());

			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $dir.$name.'.'.$tab['extension'];
			$config['create_thumb'] = FALSE;

			$config['maintain_ratio'] = TRUE;

			$size = getimagesize($file);
			$config['width'] = $size[0] < $this->img_width ? $size[0] : $this->img_width;
			$config['height'] = $size[1] < $this->img_height ? $size[1] : $this->img_height;

			//jeśli zdjęcie jest pionowe, to będzie dopasowane do wysokości; jeśli poziome, to do szerokości
			$config['master_dim'] = $size[0] < $size[1] ? 'height' : 'width';

			$this->image_lib->initialize($config);
			$this->image_lib->error_msg = array();
			if(!$this->image_lib->resize()) {
				$msg[] = array(sprintf(lang("Plik <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')),2);
			}
				
			$this->image_lib->clear();

			$config = null;
			$config['source_image'] = $file;
			$config['new_image'] = $dir.$name.'_thumb.'.$tab['extension'];
			$config['create_thumb'] = FALSE;
			//$config['maintain_ratio'] = FALSE;
			$config['maintain_ratio'] = TRUE;

			$size = getimagesize($dir.$name.'.'.$tab['extension']);
			$config['width'] = $size[0] < $this->thumb_width ? $size[0] : $this->thumb_width;
			$config['height'] = $size[1] < $this->thumb_height ? $size[1] : $this->thumb_height;

			//sprawdzenie, czy zdjęcie trzeba przyciąć z boków, czy z góry
			$config['master_dim'] = ($size[1] * $config['width']) / $size[0] < $config['height'] ? 'height' : 'width';

			$this->image_lib->initialize($config);
			$this->image_lib->error_msg = array();
			if(!$this->image_lib->resize())
				$msg[] = array(sprintf(lang("Plik miniaturki <b>%s</b> nie został wgrany poprawnie: %s"), $value, $this->image_lib->display_errors('', '')),2);

			$config = null;
			$config['source_image'] = $dir.$name.'_thumb.'.$tab['extension'];
			$size = getimagesize($dir.$name.'_thumb.'.$tab['extension']);
			$config['maintain_ratio'] = FALSE;
			$config['width'] = $size[0] < $this->thumb_width ? $size[0] : $this->thumb_width;
			$config['height'] = $size[1] < $this->thumb_height ? $size[1] : $this->thumb_height;
			$config['x_axis'] = 0;
			$config['y_axis'] = 0;
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();

			$this->db->select('max(position) as p');
			$this->db->where('id_gal', $id);
			$query = $this->db->get($this->mod_table.'_img');

			$row = $query->row();

			$position = $row->p + 1;

			$data = array(
				'id_gal' => $id,
				'img' => $name.'.'.$tab['extension'],
				'position' => $position
			);

			$this->db->insert($this->mod_table.'_img', $data);

			@unlink($file);
			$i++;
		}

		delete_files($path, TRUE);
		rmdir($path);

		if(count($msg) > 0)
			$this->session->set_flashdata('msg_files', $msg);

		return TRUE;
	}

	public function del_img($id) {
		$data = array();

		$this->db->select('tk1.img, tk2.dir');
		$this->db->from($this->mod_table.'_img as tk1');
		$this->db->where('tk1.id', $id);
		$this->db->join($this->mod_table.' as tk2', 'tk1.id_gal=tk2.id');
		$query = $this->db->get();

		$row = $query->row();

		$tab = explode('.', $row->img);
		@unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->dir.'/'.$row->img);
		@unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->dir.'/'.$tab[0].'_thumb.'.$tab[1]);
		$this->db->delete($this->mod_table.'_img', array('id' => $id));
		return $this->db->affected_rows() > 0;
	}

	public function img_edit($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->mod_table.'_img');
		return $query->num_rows() > 0 ? $query->row() : FALSE;
	}

	public function crop($id) {
		$data = $this->get_image($id);

		$dir = $data['dir'];
		$img = $data['img'];

		$tab = explode('.', $img);

		$img_save = $tab[0].'_thumb.'.$tab[1];

		$coords['scale'] = $this->input->post('scale');
		$coords['x_axis'] = $this->input->post('x_axis');
		$coords['y_axis'] = $this->input->post('y_axis');
		$coords['width'] = $this->input->post('width');
		$coords['height'] = $this->input->post('height');
		$coords['thumb_width'] = $this->thumb_width;
		$coords['thumb_height'] = $this->thumb_height;

		$path = './'.config_item('site_path').config_item('upload_path').$this->img_dir.$dir.'/';

		$this->image_lib->crop_thumb($path, $img, $path_save = null, $img_save, $coords);
	}

	public function update_img($id) {
		$this->db->where('id', $id);
		$query = $this->db->update($this->mod_table.'_img', array('desc' => $this->input->post('desc')));
		return TRUE;
	}

}
