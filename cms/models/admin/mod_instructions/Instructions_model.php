<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Instructions_model extends MY_Model {

	public function __construct() {
		parent::__construct('instructions');
		$this->_init_module_table(__FILE__, __DIR__);
	}

	public function get_all($id_tree = 0, $with_children = TRUE) {
		$this->db->select('tk2.*, tk1.*');
		$this->db->from($this->mod_table.' as tk1');
		$this->db->join($this->mod_table.'_content as tk2', 'tk1.id = tk2.id_art', 'left');
		if(is_array($id_tree)) $this->db->where_in('tk1.id_tree', $id_tree);
		else $this->db->where('tk1.id_tree', $id_tree);
		$this->db->where('tk2.lang', $this->admin->get_main_lang());
		$this->db->order_by('tk1.position');
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			if($with_children) {
				$data = array();
				$parent_ids = array();

				foreach($query->result() as $row) {
					$parent_ids[] = $row->id;
					$data[$row->id] = $row;
				}
				$query->free_result();
				$children = $this->get_all($parent_ids, $with_children);

				foreach($children as $row) {
					if(!isset($data[$row->id_tree]->children))
						$data[$row->id_tree]->children = array();
					$data[$row->id_tree]->children[] = $row;
				}

				return $data;
			} else {
				return $query->result();
			}
		}
		return array();
	}

	public function save_positions($items) {
		$rows = array();
		$i = 1;
		foreach($items as $item) {
			if(!isset($item['id']) || (isset($item['id']) && empty($item['id']))) {
				continue;
			}
			$rows[] = array(
				'id' => $item['id'],
				'id_tree' => (empty($item['parent_id']) ? 0 : $item['parent_id']),
				'position' => $i
			);
			$i++;
		}

		if(!empty($rows)) {
			$affected_rows = $this->db->update_batch($this->mod_table, $rows, 'id');
			return $affected_rows;
		} else {
			return 0;
		}

		return false;
	}

	public function delete($id) {
		$post = !is_numeric($id) ? $this->input->post('check') : $id;

		if(empty($post)) return FALSE;

		$this->db->where_in('id', $post);
		$this->db->or_where_in('id_tree', $post);
		$query = $this->db->delete($this->mod_table);

		if($this->db->affected_rows() > 0) {
			$query = $this->db->get($this->mod_table);
			foreach($query->result() as $row) {
				if($row->id_tree != 0) {
					$this->db->where('id', $row->id_tree);
					$query1 = $this->db->get($this->mod_table);

					if($query1->num_rows() == 0) {
						$this->db->where('id', $row->id);
						$this->db->or_where('id_tree', $row->id);
						$query = $this->db->delete($this->mod_table);
					}
				}
			}
		}

		return TRUE;
	}

	public function verify_form($type = 'add', $id_art = 0) {		
		$this->form_validation->set_rules('id_tree');
		$this->form_validation->set_rules('title', lang('Tytuł'), 'required|trim|htmlspecialchars');
		
		if($type == 'edit') {
			$this->form_validation->set_rules('id_tree',lang('Pozycja w drzewie'),'required|not['.$id_art.']',array('not'=>lang('Artykuł nie mo�e by� swoim w�asnym rodzicem.')));
			$this->form_validation->set_rules('pub_all');
			$this->form_validation->set_rules('pub');
			$this->form_validation->set_rules('art_content', 'required|trim');
		}
		return $this->form_validation->run();
	}

	public function insert() {
		$this->db->select('max(position) as position');
		$this->db->from($this->mod_table);
		$this->db->where('id_tree', $this->input->post('id_tree'));
		$query = $this->db->get();

		$position = $query->row()->position + 1;

		$data = array(
			'id_tree' => $this->input->post('id_tree'),
			'position' => $position
		);

		$this->db->insert($this->mod_table, $data);

		if($this->db->affected_rows() == 1) {
			$id = $this->db->insert_id();

			$data = array(
				'id_art' => $id,
				'lang' => $this->admin->get_main_lang(),
				'title' => $this->input->post('title'),
				'author' => $this->session->userdata('user_id'),
				'pub' => 1,
			);
			$this->db->set('date_add', 'NOW()', FALSE);
			$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert($this->mod_table.'_content', $data);

			return $this->db->affected_rows() == 1 ? $this->input->post('title') : FALSE;
		}
		else return FALSE;
	}

	public function artExists($id) {
		$this->db->where('id_art', $id);
		$query = $this->db->get($this->mod_table.'_content');
		return $query->num_rows() > 0;
	}

	public function artLangExists($id, $lang) {
		$this->db->where('id_art', $id);
		$this->db->where('lang', $lang);
		$query = $this->db->get($this->mod_table.'_content');
		return $query->num_rows() > 0;
	}

	public function edit($id, $lang) {
		if(!$this->artLangExists($id, $lang)) {
			$this->db->where('id_art', $id);
			$this->db->where('lang', $this->admin->get_main_lang());
			$query = $this->db->get($this->mod_table.'_content');
			$row = $query->row();

			$data = array(
				'id_art' => $id,
				'title' => $row->title,
				'content' => $row->content,
				'lang' => $lang,
				'pub' => 1,
			);
			$this->db->set('date_modified', 'NOW()', FALSE);
			$this->db->insert($this->mod_table.'_content', $data);
		}

		$this->db->where('id_art', $id);
		$this->db->where('lang', $lang);
		$query = $this->db->get($this->mod_table.'_content');

		if($query->num_rows() > 0) {
			$art = $query->row();

			$this->db->select('id_tree,pub');
			$this->db->from($this->mod_table);
			$this->db->where('id', $id);
			$query = $this->db->get();
			$art_stub = $query->row();
			$art->id_tree = $art_stub->id_tree;
			$art->pub_all = $art_stub->pub;

			if(is_numeric($art->author)) {
				$this->db->select('login, name');
				$this->db->where('id', $art->author);
				$query = $this->db->get('admin_users');

				$art->author = $query->row();
			}

			if(!empty($art->lang)) {
				$art->lang = $this->neocms->get_language($art->lang);
			}

			return $art;
		}
		return FALSE;

	}

	public function save($id, $lang) {
		$data = array(
			'id_tree' => $this->input->post('id_tree'),
			'pub' => $this->input->post('pub_all')
		);
		$this->db->where('id', $id);
		$this->db->update($this->mod_table, $data);

		$data = array(
			'title' => $this->input->post('title'),
			'content' => $this->input->post('art_content'),
			'author' => $this->session->userdata('user_id'),
			'pub' => $this->input->post('pub')
		);

		$this->db->set('date_modified', 'NOW()', FALSE);
		$this->db->where('id_art', $id);
		$this->db->where('lang', $lang);
		$this->db->update($this->mod_table.'_content', $data);

		return TRUE;
	}

}
