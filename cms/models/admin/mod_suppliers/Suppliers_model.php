<? if(!defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers_model extends MY_Model {

    public function __construct() {
        parent::__construct('suppliers');
        $this->_init_module_table(__FILE__, __DIR__);

        $path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
        if(!is_dir($path)) {
            @mkdir($path);
            @chmod($path, 0777);
        }
    }

    public function get_rows($id = null, $lang = null) {
        if (!isset($lang)){
            $lang = $this->admin->get_main_lang();
        }
        $this->db->select('tk1.*, tk2.*, tk1.pub, tk1.id');
        $this->db->from($this->mod_table.' tk1');
        $this->db->from($this->mod_table.'_content tk2');
        $this->db->where('tk1.id = tk2.id_supplier');
        $this->db->where('tk2.lang', $lang);
        if (isset($id)){
            $this->db->where('tk1.id', $id);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0){
            if (isset($id) && $query->num_rows() === 1){
                return $query->row();
            }
            return $query->result();
        }

        return FALSE;
    }

    public function delete($id) {

        $post = !is_numeric($id) ? $this->input->post('check') : $id;

        if(empty($post)) return FALSE;

        $this->db->where_in('id', $post);
        $query = $this->db->get($this->mod_table);

        foreach($query->result() as $row) {

            $tab = explode('.', $row->img);
            $path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;

            @unlink($path.$tab[0].'_thumb.'.$tab[1]);
            @unlink($path.$tab[0].'.'.$tab[1]);

        }

        $this->db->where_in('id', $post);
        $query = $this->db->delete($this->mod_table);

        $this->db->where_in('dest_id', $post);
        $this->db->where('type', 'news');
        $query_uu = $this->db->delete('user_url');
        $this->user_url->update_routes();

        return TRUE;
    }
    
    public function verify_form($type = 'add', $id_art = 0) {
        $this->form_validation->set_rules('id_art');
        
        if($type == 'add') {
            $this->form_validation->set_rules('name', lang('Nazwa'), 'trim|required|htmlspecialchars');
        }
    
        if($type == 'edit') {
            
            foreach($this->languages as $row) {
                $this->form_validation->set_rules('name', lang('Nazwa'), 'trim|required|htmlspecialchars');
                $this->form_validation->set_rules('lang['.$row->short.'][offer]', lang('Oferta').' '.lang_flag($row), 'trim|required|htmlspecialchars');
            }

            $this->form_validation->set_rules('pub');
        }
        return $this->form_validation->run();
    }

    public function insert() {
        $img = uniqid(time(), TRUE);

        $data = array(
            'name' => $this->input->post('name'),
            'img' => $img,
            'pub' => 1
        );
        $this->db->insert($this->mod_table, $data);
        
        if($this->db->affected_rows() == 1) {
            $id = $this->db->insert_id();
            
            $data = array(
                    'id_supplier' => $id,
                    'lang' => $this->admin->get_main_lang(),
                    'author' => $this->session->userdata('user_id'),
                    'pub' => 1,
            );
            $this->db->set('date_add', 'NOW()', FALSE);
            $this->db->set('date_modified', 'NOW()', FALSE);
            $this->db->insert($this->mod_table.'_content', $data);
            
            // $this->user_url->save_url(false, $this->input->post('title'), 'news', $id, $this->admin->get_main_lang());
            // $this->user_url->update_routes();
            
            return $id;
        } else {
            return FALSE;
        }
    }

    public function rowExists($id) {

        $this->db->where('id_supplier', $id);
        $query = $this->db->get($this->mod_table.'_content');
        return $query->num_rows() > 0;
    }
    
    public function newsLangExists($id, $lang) {
        $this->db->where('id_news', $id);
        $this->db->where('lang', $lang);
        $query = $this->db->get($this->mod_table.'_content');
        return $query->num_rows() > 0;
    }

    public function edit($id) {
        
        //pobranie informacji niezależnych od języka
        $this->db->where('id', $id);
        $query = $this->db->get($this->mod_table);
        
        if($query->num_rows() > 0) {
            $row = $query->row();
                
            //dodanie pól językowych
            $row->lang = array();
                
            $this->db->select('content.*, l.short as lang, l.main, au.login as author_login, au.name as author_name');
            $this->db->from($this->mod_table.'_content content');
            $this->db->join('lang l', "content.lang = l.short AND content.id_supplier = '{$id}'", 'RIGHT');
            // $this->db->join('user_url uu', "uu.dest_id = content.id_news AND uu.type = 'news' AND uu.lang = content.lang", 'LEFT');
            $this->db->join('admin_users au', "content.author = au.id", 'LEFT');
            $this->db->order_by('l.position');
            $query = $this->db->get();
                
            foreach($query->result() as $content) {
        
                // if($content->main) {
                //     $main_title = $content->title;
                //     $main_id_news = $content->id_news;
                // } else {
                //     if(empty($content->title)) $content->title = $main_title;
                //     if(empty($content->id_news)) $content->id_news = $main_id_news;
                // }
        
                $ls = $content->lang;
        
                // $content->link = news_url($content, FALSE);
                // $content->URI = news_url($content);
                    
                $row->lang[$ls] = $content;
            }
        
            return $row;
        }
        return FALSE;
    }

    public function save($row) {
        
        $id = $row->id;
        
        $data = array(
                'name' => $this->input->post('name'),
                'pub' => $this->input->post('pub'),
                'project' => $this->input->post('project'),
        );

        $this->db->where('id', $id);
        $this->db->update($this->mod_table, $data);
        
        $changes = 0;
        $lang_data = $this->input->post('lang');
        if(is_array($lang_data)) {
            foreach($lang_data as $lang => $fields) {
                $row_lang_row = ifset($row->lang[$lang]);
                $data = array(
                        'offer' => $fields['offer'],
                		'url' => $fields['url'],
                );
        
                if(empty($row_lang_row->id)) {
                    //wpis dla danego języka nie istnieje w bazie
                    $data['id_supplier'] = $id;
                    $data['lang'] = $lang;
                    $this->db->set('date_modified', 'NOW()', FALSE);
                    $this->db->insert($this->mod_table.'_content', $data);
        
                    $this->user_url->save_url(true, $fields['user_url'], 'news', $id, $lang);
                    $changes++;
                } else {
                    $this->db->where('id_supplier', $id);
                    $this->db->where('lang', $lang);
                    $this->db->set('date_modified', 'NOW()', FALSE);
                    $this->db->update($this->mod_table.'_content', $data);
        
                    $this->user_url->save_url(true, $fields['user_url'], 'news', $id, $lang);
                    $changes++;
                        
                }
            }
            if($changes > 0) {
                $this->user_url->update_routes();
            }
        }
        
        $this->db->select('img');
        $this->db->where('id', $id);
        $query = $this->db->get($this->mod_table);
        
        $row = $query->row();
        
        $img_tab = explode('.', $row->img);
        $img = $img_tab[0];
        
        if($this->input->post('del_img') == 1) {
            @unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$row->img);
            @unlink('./'.config_item('site_path').config_item('upload_path').$this->img_dir.$img_tab[0].'_thumb.'.$img_tab[1]);
        }
        
        if(is_uploaded_file($_FILES['img']['tmp_name'])) {
            $config = null;
            $config['upload_path'] = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
        
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['overwrite'] = TRUE;
            $config['file_name'] = $img;
        
            $this->upload->initialize($config);
        
            if(!$this->upload->do_upload('img')){
                $msg[] = array(
                        "Obrazek nie został wgrany poprawnie: ".$this->upload->display_errors('', ''),
                        2
                );
            }else {
                $tab = $this->upload->data();
        
                $dir = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
        
                @chmod($tab['full_path'], 0777);
        
                $name = $tab['raw_name'];
        
                $config = null;
                $config['source_image'] = $tab['full_path'];
                $config['new_image'] = $dir.$name.$tab['file_ext'];
        
                $config['create_thumb'] = FALSE;
        
                $config['maintain_ratio'] = TRUE;
                $size = getimagesize($tab['full_path']);
                if($size[0] < $this->img_width){
                    $config['width'] = $size[0];
                }else{
                    $config['width'] = $this->img_width;
                }
                if($size[1] < $this->img_height){
                    $config['height'] = $size[1];
                }else{
                    $config['height'] = $this->img_height;
                }
        
                $config['master_dim'] = 'width';
        
                $this->image_lib->initialize($config);
        
                if(!$this->image_lib->resize()){
                    $msg[] = array(
                            "Plik <b>$value</b> nie został wgrany poprawnie: ".$this->image_lib->display_errors('', ''),
                            2
                    );
                }
                $this->image_lib->clear();
        
                $config = null;
                $config['source_image'] = $tab['full_path'];
                $config['new_image'] = $dir.$name.$tab['file_ext'];
                $config['create_thumb'] = TRUE;
        
                $config['maintain_ratio'] = TRUE;
                if($size[0] < $this->thumb_width){
                    $config['width'] = $size[0];
                }else{
                    $config['width'] = $this->thumb_width;
                }
                if($size[1] < $this->thumb_height){
                    $config['height'] = $size[1];
                }else{
                    $config['height'] = $this->thumb_height;
                }
        
                $config['master_dim'] = 'width';
        
                $this->image_lib->initialize($config);
        
                if(!$this->image_lib->resize()){
                    $msg[] = array(
                            "Plik miniaturki <b>$value</b> nie został wgrany poprawnie: ".$this->image_lib->display_errors('', ''),
                            2
                    );
                }else {
                    $data = array('img' => $img.$tab['file_ext']);
        
                    $this->db->where('id', $id);
                    $this->db->update($this->mod_table, $data);
                }
        
                $this->image_lib->clear();
            }
        }
        
        return TRUE;
    }

    public function crop($id) {
        $data = $this->get_rows($id);
        $data = (array)$data;

        $dir = './'.config_item('site_path').config_item('upload_path').$this->img_dir;
        $img = $data['img'];

        $tab = explode('.', $img);

        $img_save = $tab[0].'_thumb.'.$tab[1];

        $coords['scale'] = $this->input->post('scale');
        $coords['x_axis'] = $this->input->post('x_axis');
        $coords['y_axis'] = $this->input->post('y_axis');
        $coords['width'] = $this->input->post('width');
        $coords['height'] = $this->input->post('height');
        $coords['thumb_width'] = $this->thumb_width;
        $coords['thumb_height'] = $this->thumb_height;

        $path = './'.config_item('site_path').config_item('upload_path').$this->img_dir;

        $this->image_lib->crop_thumb($path, $img, $path_save = null, $img_save, $coords);
    }
}
