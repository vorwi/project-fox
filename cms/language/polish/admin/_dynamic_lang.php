<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

//Dodaj dostawcę
$lang['80e0a75eb19021225faf726baed9182e'] = 'Dodaj dostawcę';
//Nie znaleziono żadnego dostawcy.
$lang['cde64b8d8a88784a1a65c98ccfff6c97'] = 'Nie znaleziono żadnego dostawcy.';
//Błąd 404
$lang['dcdd56a5d4240e6bcbee5387f2c51d79'] = 'Błąd 404';
//Strona nie została odnaleziona
$lang['553fbc06af2a6c26c19bf17ef83f5bee'] = 'Strona nie została odnaleziona';
//Strona <b>%s</b> nie istnieje lub jest niedostępna.
$lang['e766cfb10ab64b45fad1fb041767837f'] = 'Strona <b>%s</b> nie istnieje lub jest niedostępna.';
//Ścieżka błędu
$lang['4a8494fb3719163bd25a58b4b0bbc83a'] = 'Ścieżka błędu';
//Wszelkie prawa zatrzeżone
$lang['159dde366ff9756d2e99087bd8434e6c'] = 'Wszelkie prawa zatrzeżone';
//Pokaż/ukryj menu
$lang['25a46ece5b470feb270763f4b9a5e628'] = 'Pokaż/ukryj menu';
//Menadżer plików
$lang['237b070ce5a8758e9cb3cb50f7cd7b06'] = 'Menadżer plików';
//Zarządzaj plikami na serwerze
$lang['6189aaf8a169c820b516a60a015a594f'] = 'Zarządzaj plikami na serwerze';
//Przejdź do strony
$lang['d393663027b2a75bc24791deb40983b5'] = 'Przejdź do strony';
//Mój profil
$lang['44cb64d196743ff912b397decd6408a2'] = 'Mój profil';
//Aktualizacja danych konta
$lang['dc993a0381e582e6ffaa8bf89e9871a0'] = 'Aktualizacja danych konta';
//Zmiana hasła
$lang['52a0e409f748daf10e312c4bf9081b3e'] = 'Zmiana hasła';
//Ustaw nowe hasło do konta
$lang['a2073fbefe7fa7ec1714b4336ccae6d6'] = 'Ustaw nowe hasło do konta';
//Menu główne
$lang['e561df155755a3cf2b92314169570fa2'] = 'Menu główne';
//Wpis został dodany.
$lang['26da96173d95a61a1a93ec7e2eb0f1e8'] = 'Wpis został dodany.';
//: edycja
$lang['79ad3fd83c180cce147b8351b4c82fd2'] = ': edycja';
//Edytuj wersję: 
$lang['cfe502ff629bb6559692b236488ebb18'] = 'Edytuj wersję: ';
//Oferta
$lang['7547c77f8947f2968f65036347e6f2fe'] = 'Oferta';
//Logotyp
$lang['7dfc8f0b910d36bb547fba121b402ed6'] = 'Logotyp';
//Aktualność nie istnieje.
$lang['6d618bf1489154add140a9bbcab13d5d'] = 'Aktualność nie istnieje.';
//Data dodania
$lang['0a5a84180a5a788e9ce3556c86fdf4b1'] = 'Data dodania';
//Czy na pewno chcesz usunąć?
$lang['0d49952b295cd51d1d6f5abe356d9be9'] = 'Czy na pewno chcesz usunąć?';
//Czy na pewno chcesz usunąć zaznaczone wpisy?
$lang['39fbbbcf526b1746c39f1a1ef510a3db'] = 'Czy na pewno chcesz usunąć zaznaczone wpisy?';
//Wpis został zaktualizowany
$lang['f068613f4953a6fbbfe2435d68ba861c'] = 'Wpis został zaktualizowany';
//usuń zdjęcie
$lang['a48f70b4143ee31c527b14eba4d7c2f1'] = 'usuń zdjęcie';
//Plik pdf z projektem
$lang['f1373c0479bd57b9b6fe2515b844ff67'] = 'Plik pdf z projektem';
//Dodaj artykuł
$lang['603a1331f26f09613668602212cc0190'] = 'Dodaj artykuł';
//główna pozycja
$lang['debe72995dc45c9fe3997b1ea2bd8491'] = 'główna pozycja';
//Zapisz zmiany
$lang['2dadd6a07cccb1a253dd1cb88b5e938e'] = 'Zapisz zmiany';
//Rozwiń wszystkie
$lang['421bf532abfcd4a8d4ac666dff845a52'] = 'Rozwiń wszystkie';
//Zwiń wszystkie
$lang['89c1a0303e8d359bc69501f5316a97b3'] = 'Zwiń wszystkie';
//Zaznacz wszystkie
$lang['3ef72982d06ab40240c630dee8a6bff5'] = 'Zaznacz wszystkie';
//Prowadzi do
$lang['11c367e9453712d006e6a835153de11a'] = 'Prowadzi do';
//Dane podstawowe
$lang['42987f1d5f9baea7ab80efd32860e9cb'] = 'Dane podstawowe';
//Skrócony tytuł
$lang['b43a29b8c7c04722fe9ba4768b52b599'] = 'Skrócony tytuł';
//Skrócony tytuł będzie widoczny tylko w menu, natomiast dłuższa wersja jako nagłówek strony. Pole opcjonalnie.
$lang['a14f181c278ab7f0cbadabcf6dabcc21'] = 'Skrócony tytuł będzie widoczny tylko w menu, natomiast dłuższa wersja jako nagłówek strony. Pole opcjonalnie.';
//Zewnętrzny odnośnik
$lang['d78ac2033f0d196af4424c695d9d3987'] = 'Zewnętrzny odnośnik';
//Link do którego ma kierować podstrona z menu, zamiast wyświetlania jej treści (np.: http://artneo.pl). Pole opcjonalnie.
$lang['a2e5956668a73f9adfe1c835a233ecaa'] = 'Link do którego ma kierować podstrona z menu, zamiast wyświetlania jej treści (np.: http://artneo.pl). Pole opcjonalnie.';
//Niestandardowy adres URL
$lang['331e7b45af7db2065895569a65d6b7b2'] = 'Niestandardowy adres URL';
//Adres, pod którym będzie dostępna ta podstrona. Podawać bez domeny na początku. Pole opcjonalnie.
$lang['0c5ab099819fcdcc79be4768f0291cef'] = 'Adres, pod którym będzie dostępna ta podstrona. Podawać bez domeny na początku. Pole opcjonalnie.';
//Meta-tagi (SEO)
$lang['f46c44950ae173988045372c21e39400'] = 'Meta-tagi (SEO)';
//Tytuł (title)
$lang['1e0521d71c9a2dd3458a9498b327276f'] = 'Tytuł (title)';
//Opis (description)
$lang['07e59757293aa2dad28c2a0365aa11dc'] = 'Opis (description)';
//Słowa kluczowe (keywords)
$lang['8234b1c16748e4798df8f26cd167c884'] = 'Słowa kluczowe (keywords)';
//Meta tagi Open Graph
$lang['6e8bfe17b9ab07fd4f03490c3d10d058'] = 'Meta tagi Open Graph';
//Pozycja w drzewie
$lang['52caf8ddc7ab742dd3b8b55e71033776'] = 'Pozycja w drzewie';
//Data modyfikacji
$lang['e1fdd71441faf7a7beb7ff709ceda4b1'] = 'Data modyfikacji';
//cały artykuł
$lang['2d851be36914845c22ee61f59f0d302e'] = 'cały artykuł';
//języki
$lang['afb1bdead20ee1c426cce97727544e1c'] = 'języki';
//Link do podstrony
$lang['6a1f4aec23ede22343411d4c41ba61af'] = 'Link do podstrony';
//Link do używania w panelu
$lang['ca5a355bc952cc60c1c87c1e9203af3c'] = 'Link do używania w panelu';
//Sposób otwierania
$lang['3b3dce3481135048ac63d9039dc3128f'] = 'Sposób otwierania';
//nowe okno
$lang['14c2e7125a38d0753705476f28dc9918'] = 'nowe okno';
//Artykuł nie istnieje
$lang['bf2bc4350f22413b15105780ffb8f662'] = 'Artykuł nie istnieje';
//Meta Tytuł (title)
$lang['57f0a4c4eb13f971e997730183d1eb15'] = 'Meta Tytuł (title)';
//Meta Opis (description)
$lang['65445389cde820431a072be0f0f33c24'] = 'Meta Opis (description)';
//Meta Słowa kluczowe (keywords)
$lang['29969617b2cedf038f728b1ac2335c29'] = 'Meta Słowa kluczowe (keywords)';
//Open Graph title
$lang['cb2c29640ee9e79d212aaefc7e16ccb7'] = 'Open Graph title';
//Open Graph description
$lang['b73bf95ed801ce7cec10ef570dcb2f3d'] = 'Open Graph description';
//Open Graph image
$lang['26ce0c375885fd9324607724d5bb89f1'] = 'Open Graph image';
//Open Graph url
$lang['5ad149ce10594597b1b5f342471bb642'] = 'Open Graph url';
//Artykuł nie może być swoim własnym rodzicem.
$lang['85b6e17182a674d5ec350e74fd157046'] = 'Artykuł nie może być swoim własnym rodzicem.';
//Artykuł został zaktualizowany
$lang['0cf90a03ff63e3214ec59804c4bf2706'] = 'Artykuł został zaktualizowany';
//Zmiany nie zostały zapisane.
$lang['0a24b8fd83637ba5716293e6d38951e2'] = 'Zmiany nie zostały zapisane.';
//Zaznaczone wpisy zostały usunięte.
$lang['47c76c5f6336b497a8dfe960f2f18149'] = 'Zaznaczone wpisy zostały usunięte.';
//Zmiany zostały zapisane.
$lang['2fcb2d53a9df3929748417cd4001a252'] = 'Zmiany zostały zapisane.';
//Zaloguj się
$lang['a61d196e6576a0a91ac6aab9034d5535'] = 'Zaloguj się';
//Logowanie do systemu zarządzania treścią
$lang['f4ce1f22f067256b8c73c8fc150d9206'] = 'Logowanie do systemu zarządzania treścią';
//Przejdź do resetowania hasła
$lang['b77cff1f8c448419e6a1fadb5afe7fe0'] = 'Przejdź do resetowania hasła';
//Nie pamiętam hasła...
$lang['0fcab7c5faa78721839cecedfbc0322e'] = 'Nie pamiętam hasła...';
//Login lub hasło nie są poprawne.
$lang['fe241da8cd1ffdc8047c440a423276cb'] = 'Login lub hasło nie są poprawne.';
//próba logowania:
$lang['821451f47bb51bf3a9d68487f119bfa6'] = 'próba logowania:';
//Znajdujesz się w panelu administracyjnym strony 
$lang['c3d6252d045c0da73c16664f71bfca2e'] = 'Znajdujesz się w panelu administracyjnym strony ';
//W razie problemów z obsługą, prosimy o kontakt z naszym Biurem Obsługi Klienta: 
$lang['86e9b9a3afbec40d1a4b61a73bc90d04'] = 'W razie problemów z obsługą, prosimy o kontakt z naszym Biurem Obsługi Klienta: ';
//Ostatnie logowania
$lang['a8549875ac1c25011c9406597434550d'] = 'Ostatnie logowania';
//Zmiany w podstronach
$lang['d49893edf622260a073273111a51b482'] = 'Zmiany w podstronach';
//Nagłówek
$lang['558ad18f93bff88701c754d41557ece9'] = 'Nagłówek';
//Podtytuł
$lang['df11fccab518078c26c1a1182f6478d5'] = 'Podtytuł';
//Liczba nr.
$lang['c166afc18875b454645afbc3cb96ba0c'] = 'Liczba nr.';
//Tekst ponad liczbą
$lang['9cd4bea96c8cb413c79f4f1f35f58c92'] = 'Tekst ponad liczbą';
//Liczba
$lang['e66530071dcd9334bd12376027356c0f'] = 'Liczba';
//Tekst pod liczbą
$lang['b7f49fcf62e2e13937aee8897cc85180'] = 'Tekst pod liczbą';
//Tekst obok linku
$lang['113bfe288fb5d9bec301dfcf14ea51c1'] = 'Tekst obok linku';
//Treść po lewej stronie
$lang['af1dee424be3a6efb4345d6be5b66a9c'] = 'Treść po lewej stronie';
//Treść po prawej stronie
$lang['f039507a3fd3ee069d17a11172497b69'] = 'Treść po prawej stronie';
//Tekst ponad ikoną nr.
$lang['4a11b4e379a154a4a40929cf86e6b86b'] = 'Tekst ponad ikoną nr.';
//Wskazówki - dojazd
$lang['19c60f0601288fee2893d12b04d270f7'] = 'Wskazówki - dojazd';
//Dodaj formularz
$lang['24ec66effdd1abfa0feacc83068de537'] = 'Dodaj formularz';
//Wyświetlanie na podstronie
$lang['692cc513770a25940e55474565f2b8e7'] = 'Wyświetlanie na podstronie';
//Adres e-mail
$lang['79f061725d6a76d4e58500746e8a2b71'] = 'Adres e-mail';
//Adres na który będą wysyłane wiadomości z formularza.
$lang['1d91dcf66eacd6192ea53c3f5e785387'] = 'Adres na który będą wysyłane wiadomości z formularza.';
//Nazwa wyświetlana
$lang['884606c3f6bfeb19aac54aa3b3d86034'] = 'Nazwa wyświetlana';
//Tekst wprowadzony w tym polu, będzie wyświetlany zamiast powyższego e-maila. Pole opcjonalnie.
$lang['422e3e8b3fc9417806edfab7eb034805'] = 'Tekst wprowadzony w tym polu, będzie wyświetlany zamiast powyższego e-maila. Pole opcjonalnie.';
//Artykuł został dodany.
$lang['bcf76dc87f3f9c58fb57797978a57829'] = 'Artykuł został dodany.';
//Adres URL &#039;O nas&#039; był już przypisany do innego elementu. Spróbuj dobrać inny ciąg znaków.
$lang['fab45e0926b5b619ae662141f2b8756f'] = 'Adres URL &#039;O nas&#039; był już przypisany do innego elementu. Spróbuj dobrać inny ciąg znaków.';
//Wybierz język:
$lang['700019e1cd5438cfbc791bec4463fd21'] = 'Wybierz język:';
//Dodaj kategorię
$lang['ccb382eff45e6f94c806f5b715606315'] = 'Dodaj kategorię';
//Dodaj ustawienie
$lang['f411686812de21b10446d1bd945d01a1'] = 'Dodaj ustawienie';
//Wartość wstępna
$lang['d6400658f1ae532a72cff72abd90434b'] = 'Wartość wstępna';
//Zależne od języka?
$lang['91d85ece1ee5e9b4277dff6bb463d809'] = 'Zależne od języka?';
//Czy na pewno wysłać formularz z błędami?
$lang['b6189ceb380f5ae9d275733e89a455e4'] = 'Czy na pewno wysłać formularz z błędami?';
//Nazwa może składać się tylko ze znaków alfanumerycznych.
$lang['7dc5c888fe2278e3192ffa604ec7d648'] = 'Nazwa może składać się tylko ze znaków alfanumerycznych.';
//Czy na pewno chcesz usunąć kategorię ustawień?
$lang['f54d7eda2c8b385fab5d3269c16a281d'] = 'Czy na pewno chcesz usunąć kategorię ustawień?';
//Spowoduje to też usunięcie wszystkich ustawień przypisanych do tej kategorii.
$lang['be5c9151ee452dc3450a6ed7612845a1'] = 'Spowoduje to też usunięcie wszystkich ustawień przypisanych do tej kategorii.';
//Usuń kategorię
$lang['2ebe479e2554b50783fb2628426d3ee4'] = 'Usuń kategorię';
//Zapisz kolejność
$lang['262453099b6dee4a5f888754cf6e25d2'] = 'Zapisz kolejność';
//Ustawienia zostały zapisane.
$lang['24283db5a991bda08b906681dc3a4527'] = 'Ustawienia zostały zapisane.';
//Zmiany zostały zapisane
$lang['f4d7ff25e16fea4c7bf20d0324b8941f'] = 'Zmiany zostały zapisane';
//Dodaj liczbę
$lang['920459c22c93f04cab585da6dfd2482b'] = 'Dodaj liczbę';
//Akcja
$lang['c499c56c6e3a10bb9354260d4bf08a5b'] = 'Akcja';
//Edytuj liczby
$lang['4d1927ff123302b281ef94179a710099'] = 'Edytuj liczby';
//Tekst
$lang['016bd7e1bf6f7b6c84afcdf3e047948f'] = 'Tekst';
//Wypunktowanie w sekcji
$lang['259d00273c37f61237f6eac0ba363cb3'] = 'Wypunktowanie w sekcji';
//Edytuj tekst
$lang['de003f85ab9ee0e3c00bc17225604128'] = 'Edytuj tekst';
//Edytuj wypunktowanie
$lang['0f6757ab14feea2c7c3caba4bb7d6a44'] = 'Edytuj wypunktowanie';
//Ikony w sekcji
$lang['2ff0148f7ad8543c90f3f30a2297ee9a'] = 'Ikony w sekcji';
//Pozycje zostały zapisane pomyślnie
$lang['7ce8f143fe2c049d31f1cbcba1724a74'] = 'Pozycje zostały zapisane pomyślnie';
//Dodaj język
$lang['564d26e5a9ad5feeb30f1d1ffc28bd92'] = 'Dodaj język';
//Nazwa języka
$lang['0cc961b178bb2cb041bf42c2d3f79fd0'] = 'Nazwa języka';
//Kod ISO 3166-1 alpha-2
$lang['ac97e3dd2dd17aad2e5d3f2ecc32f6e7'] = 'Kod ISO 3166-1 alpha-2';
//Nazwa katalogu w 
$lang['a259b71b931401f33613eb75a0288138'] = 'Nazwa katalogu w ';
//Stworzyć kopię wszystkich artykułów z języka głównego?
$lang['7df27d267f9e92263ddaad40dd308fa4'] = 'Stworzyć kopię wszystkich artykułów z języka głównego?';
//Edytuj język
$lang['8c872a64552a64b72dbea68e8465a1ed'] = 'Edytuj język';
//Edytuj tłumaczenia
$lang['3882ee0b02ceb976aa623b15a8225e87'] = 'Edytuj tłumaczenia';
//Czy na pewno chcesz usunąć zaznaczone języki?
$lang['88ff99f0f0754d1dacb1e1cd8f03ed00'] = 'Czy na pewno chcesz usunąć zaznaczone języki?';
//email
$lang['0c83f57c786a0b4a39efab23731c7ebc'] = 'email';
//kod
$lang['7a9e2fba2f949c98c0dadbbad7ae09a1'] = 'kod';
//Przypomnienie hasła
$lang['8ccaa56aa4ca04affdc6b6ba7bff17b9'] = 'Przypomnienie hasła';
//Resetowanie hasła
$lang['36b612ce47c1953004e27a178aad4d8f'] = 'Resetowanie hasła';
//Wpisz poniżej swój adres e-mail, który jest podany w ustawieniach konta. Zostanie na niego wysłana instrukcja aktywowania nowego hasła.
$lang['c9566bf712af1dc6d8a94fcb6ca58026'] = 'Wpisz poniżej swój adres e-mail, który jest podany w ustawieniach konta. Zostanie na niego wysłana instrukcja aktywowania nowego hasła.';
//Przepisz kod z obrazka
$lang['4f94b41f5309c8aa0d961fd29f3f6761'] = 'Przepisz kod z obrazka';
//Przejdź do logowania
$lang['f3534107b94987bd11065a4bd72941d3'] = 'Przejdź do logowania';
//Formularze: edycja
$lang['87cb3e9be7cb8763d015f62add434ddc'] = 'Formularze: edycja';
//Zaznaczone formularze zostały zaktualizowane.
$lang['3b0604d58a6698fa0784e8b0592e5d2a'] = 'Zaznaczone formularze zostały zaktualizowane.';
//w menu
$lang['cde4de6b8b700515652db21184ed5f69'] = 'w menu';
//Nie znaleziono formularza lub nie został zaznaczony żaden formularz.
$lang['6504bd5272d587e361096fd0387ab615'] = 'Nie znaleziono formularza lub nie został zaznaczony żaden formularz.';
//Kadrowanie zdjęcia
$lang['01e7d601ab14374f69e819f063920cf3'] = 'Kadrowanie zdjęcia';
//Najpierw zaznacz obszar zdjęcia do wykadrowania.
$lang['4d29f3fb130ce30f563b4942637282ae'] = 'Najpierw zaznacz obszar zdjęcia do wykadrowania.';
//Dodaj wypunktowanie
$lang['4cc7e4f532f979e3c234ad78fab42064'] = 'Dodaj wypunktowanie';
//Dodaj użytkownika
$lang['9804cb948bfb955a04be8079a7bb1590'] = 'Dodaj użytkownika';
//Imię i nazwisko
$lang['31ab24b3be4b720bc47777f21ab1ffc9'] = 'Imię i nazwisko';
//Dane kontaktowe
$lang['bce53da0d51e032af0faa2cf070e7b00'] = 'Dane kontaktowe';
//Dane adresowe
$lang['43ed664346b3c43702985495eff0c9a9'] = 'Dane adresowe';
//Ulica i nr budynku
$lang['d2638013c5bc8f4fe455b70c395bf334'] = 'Ulica i nr budynku';
//Kod pocztowy
$lang['3f13587bff86fff2fce52173436902b3'] = 'Kod pocztowy';
//Wysłanie hasła
$lang['a57b206834c2c3ad7fcef44665ad58ca'] = 'Wysłanie hasła';
//Wyślij hasło na podany adres email użytkownika
$lang['1010ea225b41481b4bdce9e0016f81bd'] = 'Wyślij hasło na podany adres email użytkownika';
//Liczba wyników spełniających kryteria:
$lang['d14508079904c795ea8b551a7b840c6a'] = 'Liczba wyników spełniających kryteria:';
//Użytkownik został dodany. Hasło <b>%s</b> zostało wysłane na podany email.
$lang['4d79be20f7cc5339121d43eb17495295'] = 'Użytkownik został dodany. Hasło <b>%s</b> zostało wysłane na podany email.';
//Nie znaleziono wpisów wymagających zapisanie pozycji
$lang['ca4e0898fdb9c9ea8101f592137963aa'] = 'Nie znaleziono wpisów wymagających zapisanie pozycji';
//: uprawnienia
$lang['8b227f5ac758da2e763925b2d62b9ac3'] = ': uprawnienia';
//Ustawienie zostało dodane.
$lang['38d878e4e743829a96fdf489bc7f4d6b'] = 'Ustawienie zostało dodane.';
//Kolejność została zaktualizowana.
$lang['6b9cc011b29434c9aaaad381462973e0'] = 'Kolejność została zaktualizowana.';
//Adres WWW
$lang['238008a28309e8ba2fff517fb9130686'] = 'Adres WWW';
//Dodaj moduł
$lang['028f62fa7d6997d599d5c7f17d2e9094'] = 'Dodaj moduł';
//Etykieta wyświetla się nad pozycją modułu w menu i służy do wizualnego grupowania. Pole opcjonalnie.
$lang['e8c3cbc3676051ee26e9cbeb18f30505'] = 'Etykieta wyświetla się nad pozycją modułu w menu i służy do wizualnego grupowania. Pole opcjonalnie.';
//Parametry modułu
$lang['6f3b06aeb76db3cc9477e1edd209ce3c'] = 'Parametry modułu';
//Wyświetlaj w menu
$lang['57bc822a3b8db1e486afd10524bbc567'] = 'Wyświetlaj w menu';
//Tylko dla Artneo
$lang['0353a412614ffa6734843886432d1bc4'] = 'Tylko dla Artneo';
//Usuń z dostępu globalnego
$lang['e85ac8325b999ef05f9e4ed8a4fea01b'] = 'Usuń z dostępu globalnego';
//Usuń z domyślnych
$lang['8cbc3beece41125b7e9119da4291c86f'] = 'Usuń z domyślnych';
//Ustaw jako widoczne tylko dla Artneo
$lang['311c087aaa21e7b4c2642336b52506a0'] = 'Ustaw jako widoczne tylko dla Artneo';
//Ustaw dostęp gloabalny
$lang['724311896fe3d65f40b23484cb5994ae'] = 'Ustaw dostęp gloabalny';
//Ustaw jako domyślny moduł
$lang['ee7621ebd0a2ca576a608b5d53aaf5a7'] = 'Ustaw jako domyślny moduł';
//Nie wyświetlaj w menu
$lang['9178efbc9bb6574918fe77d144ac8ca2'] = 'Nie wyświetlaj w menu';
//Ustaw jako widoczne dla wszystkich
$lang['f03451eadec0ab37d6e35be7680e06da'] = 'Ustaw jako widoczne dla wszystkich';
//Ustaw dostęp globalny
$lang['f09dca2b7b5a90d9ef1fba36cb9e0fe6'] = 'Ustaw dostęp globalny';
