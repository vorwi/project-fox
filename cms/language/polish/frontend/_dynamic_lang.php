<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

//Błąd 404
$lang['dcdd56a5d4240e6bcbee5387f2c51d79'] = 'Błąd 404';
//Strona nie została odnaleziona
$lang['553fbc06af2a6c26c19bf17ef83f5bee'] = 'Strona nie została odnaleziona';
//Strona <b>%s</b> nie istnieje lub jest niedostępna.
$lang['e766cfb10ab64b45fad1fb041767837f'] = 'Strona <b>%s</b> nie istnieje lub jest niedostępna.';
//Ścieżka błędu
$lang['4a8494fb3719163bd25a58b4b0bbc83a'] = 'Ścieżka błędu';
//Polityka Plików Cookies
$lang['e5badde0bbf8f7b0463a9df0d441c6f9'] = 'Polityka Plików Cookies';
//Polityka plików cookies
$lang['ac69a1a272afe643aae3c806bfbf8854'] = 'Polityka plików cookies';
//Proszę nie zmieniać wartości tego pola
$lang['ce8ab69f4bc7b358ca1d59ae351caa5c'] = 'Proszę nie zmieniać wartości tego pola';
//Proszę nie zaznaczać tego pola
$lang['c04f45a1f1c8b2b8ab22974398441e08'] = 'Proszę nie zaznaczać tego pola';
//E-mail
$lang['1e884e3078d9978e216a027ecd57fb34'] = 'E-mail';
//Do
$lang['0567953871b1bf589b797d9b178d5a94'] = 'Do';
//Twój adres e-mail
$lang['9362da1a3c86b2aac9e795f74a50bcd1'] = 'Twój adres e-mail';
//Treść wiadomości
$lang['33451acd52c91fd8b51063d3bbcee3b0'] = 'Treść wiadomości';
//Wyślij
$lang['aeba5b89f8ef9eb1e44f6ae6ec7385e9'] = 'Wyślij';
//email
$lang['0c83f57c786a0b4a39efab23731c7ebc'] = 'email';
//Imie
$lang['2a4bdb82e1b258d4dd1e599589ab8466'] = 'Imie';
//Nazwisko
$lang['f37c4446e4a882972cd3a80053dd795f'] = 'Nazwisko';
//Numer telefonu
$lang['5919d72ce2550e803b4e1257f4192be5'] = 'Numer telefonu';
//Wiadomość z formularza kontaktowego
$lang['566efcd07f1b765fe189c5cc25b7a89e'] = 'Wiadomość z formularza kontaktowego';
//Dziękujemy. Email został wysłany.
$lang['94c18bb385a8e82c582bbb834cd74352'] = 'Dziękujemy. Email został wysłany.';
//Aktualności
$lang['d5451106815d85514c5e060736690442'] = 'Aktualności';
