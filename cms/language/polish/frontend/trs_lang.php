<? defined('BASEPATH') OR exit('No direct script access allowed');

//contact_view
$lang['cf_adressee'] = "Do:";
$lang['cf_email'] = "Twój e-mail";
$lang['cf_title'] = "Tytuł";
$lang['cf_content'] = "Treść wiadomości:";
$lang['cf_rewrite_code'] = "Przepisz kod z obrazka:";
$lang['cf_enter_code'] = "Tutaj wpisz kod...";
$lang['cf_send'] = "Wyślij";
$lang['cff_email'] = "Email";
$lang['cff_code'] = "Kod";
$lang['cff_title'] = "Tytuł";
$lang['cff_content'] = "Treść wiadomości";

//news_view
$lang['nw_read_more'] = 'czytaj więcej &gt;';

$lang['menu_open'] = 'Otwórz<br />menu';
$lang['menu_close'] = 'Zamknij<br />menu';

$lang['newsletter_unsubcribe'] = 'Jeżeli nie chcesz otrzymywać naszych wiadomości kliknij w poniższy link lub skopiuj go do przeglądarki';