<? defined('BASEPATH') OR exit('No direct script access allowed');

$lang["form_validation_contact_captcha_check"] = 'Przepisany kod nie jest poprawny.';
$lang["form_validation_not"] = 'Pole {field} jest wymagane.';