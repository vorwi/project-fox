<? defined('BASEPATH') OR exit('No direct script access allowed');

//contact_view
$lang['cf_adressee'] = "To:";
$lang['cf_email'] = " Your e-mail";
$lang['cf_title'] = " Title";
$lang['cf_content'] = "Message:";
$lang['cf_rewrite_code'] = "Rewrite code from the picture:";
$lang['cf_enter_code'] = "Enter code here...";
$lang['cf_send'] = "Send";
$lang['cff_email'] = "Email";
$lang['cff_code'] = "Code";
$lang['cff_title'] = "Title";
$lang['cff_content'] = "Message";

//news_view
$lang['nw_read_more'] = 'read more &gt;';

$lang['menu_open'] = 'Open<br />menu';
$lang['menu_close'] = 'Close<br />menu';

$lang['newsletter_unsubcribe'] = 'If you do not want to receive our messages, click on the link below or copy it to your browser';