<? defined('BASEPATH') OR exit('No direct script access allowed');

$lang["form_validation_contact_captcha_check"] = 'Rewritten captcha code is not valid.';
$lang["form_validation_not"] = 'The {field} field is required.';