<? defined('BASEPATH') OR exit('No direct script access allowed');

$lang["form_validation_duplicate_login_check"] = 'The given username is already used.';