<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$this->config->load('url_prefix');

$conf = (object)$this->config->config;

//$route['default_controller'] = "frontend/intro"; //jeżeli ma być intro
$route['default_controller'] = "frontend/articles/index";
$route['404_override'] = 'error_404';
$route['translate_uri_dashes'] = FALSE;

// aw_cms admin
$route['admin'] 				= "admin/mod_main/main";
$route['admin/login'] 			= "admin/mod_login/login";
$route['admin/remind'] 			= "admin/mod_login/login/remind";
$route['admin/logout'] 			= "admin/mod_login/login/logout";
$route['admin/instrukcja'] 		= "admin/mod_instruction/instruction";
$route['admin/confirm/(:any)'] 	= "admin/mod_login/login/confirm/hash/$1";
$route['admin/lang/(:any)'] 	= "";
 
//aw_cms frontend
$route['lang/(:any)'] 			= "frontend/articles";
$route['sitemap.xml']			= "frontend/sitemap/index";
$route['robots.txt']            = "frontend/sitemap/robots";
$route['polityka-plikow-cookies'] = "frontend/articles/cookie_info";
$route['newsletter-send-queue'] = "frontend/newsletter/send_from_queue";

/*
$route['sitemap']					= "frontend/sitemap/sitemappage";
$route['kontakt'] 					= "frontend/contact/index";
$route[$conf->px_gallery] 			= "frontend/gallery/index";
$route[$conf->px_gallery.'/(:num)'] 	= "frontend/gallery/index";
$route[$conf->px_newsletter] 		= "frontend/newsletter/index";

*/
$route[$conf->px_search] 			= "frontend/search/index";
$route[$conf->px_news] 				= "frontend/news/index";
$route[$conf->px_news.'/(:num)'] 	= "frontend/news/index";
$route[$conf->px_news.'/(:num)/(:any)'] 		= "frontend/news/single/$1";

//aw_cms dynamic
$route[$conf->px_articles_pag.'/(:num)']		= "frontend/articles/index/id/$1";
$route[$conf->px_articles_pag.'/(:num)/(:num)']	= "frontend/articles/index/id/$1/page/$2";
$route[$conf->px_articles.'/(:num)/(:any)']		= "frontend/articles/index/id/$1/sufix/$2";
$route[$conf->px_gallery.'/(:num)/(:any)']		= "frontend/gallery/single/$1";
$route['content/(:any)'] 						= "frontend/$1";
$route[$conf->px_search.'/(:any)'] 					= "frontend/search/index/$1";

/*$route['lang/(:any)'] 				= "";
$route[$conf->px_newsletter.'/(:any)/(:any)/(:num)/(:num).html'] 	= "frontend/newsletter/newsletter_new/d/$1/m/$2/y/$3/nr/$4";
$route[$conf->px_newsletter.'/(:any)'] 							= "frontend/newsletter/$1";
*/

/*$route[$conf->px_products_category.'/(:num)'] 			= "frontend/products/category/$1";
$route[$conf->px_products_category.'/(:num)/(:any)'] 			= "frontend/products/category/$1";

$route[$conf->px_product.'/(:num)'] 			= "frontend/products/index/$1";
$route[$conf->px_product.'/(:num)/(:any)'] 			= "frontend/products/index/$1";*/

$route['coordinates'] = 'frontend/articles/coordinates';

if(file_exists(APPPATH . "cache/routes.php")){ include_once APPPATH . "cache/routes.php"; }

/* End of file routes.php */
/* Location: ./application/config/routes.php */