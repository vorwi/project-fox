<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// typy parametrów
$config['field_types'] = array();
$config['field_types'][] = (object) array(
	'field_name' => 'Lista rozwijalna (select)',
	'fielt_type' => 'select'
);
$config['field_types'][] = (object) array(
	'field_name' => 'Lista wielokrotnego wyboru (checkbox)',
	'fielt_type' => 'checkbox'
);
$config['field_types'][] = (object) array(
	'field_name' => 'Lista jednokrotnego wyboru (radio)',
	'fielt_type' => 'radio'
);
$config['field_types'][] = (object) array(
	'field_name' => 'Pole tekstowe',
	'fielt_type' => 'text'
);
$config['field_types'][] = (object) array(
	'field_name' => 'Plik',
	'fielt_type' => 'file'
);
$config['field_types'][] = (object) array(
	'field_name' => 'Predefiniowana wartość',
	'fielt_type' => 'predefined_parameter'
);

// predefiniowane parametry
// wartość "parameter_method" definiuje jednocześnie nazwę używanych (w sumie trzech) metod modelu:
// 1. metoda pobierająca wartości do wyszukiwarki "get_values_[parameter_method]"
// 2. metoda dodająca do zapytania filtrowanie według wartości parametru "filter_values_[parameter_method]"
// 3. metoda dodająca do nieruchomości informację o parametrze "view_values_[parameter_method]"
$config['predefined_parameters'] = array();
$config['predefined_parameters'][] = (object) array(
	'parameter_name' => 'Nazwa inwestycji',
	'parameter_method' => 'investment_name'
);
$config['predefined_parameters'][] = (object) array(
	'parameter_name' => 'Piętro',
	'parameter_method' => 'estate_floor'
);
$config['predefined_parameters'][] = (object) array(
	'parameter_name' => 'Budynek',
	'parameter_method' => 'building'
);