<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['px_articles'] = 'page';
$config['px_articles_pag'] = 'page';
$config['px_news'] = 'news';
$config['px_gallery'] = 'gallery';
$config['px_newsletter'] = 'newsletter';
$config['px_search'] = 'szukaj';

$config['px_products_category'] = 'category';
$config['px_product'] = 'product';