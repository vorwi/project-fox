<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['base_path'] 			= './';
$config['site_path'] 			= '';
$config['upload_path'] 			= 'userfiles/';
$config['temp'] 				= 'temp/';
$config['settings_files_path'] 	= $config['upload_path'].'settings/';
// admin_profiler: Enable profilers for controller classes in admin
$config['profiler_a'] 			= (ENVIRONMENT == 'development');
$config['profiler_f'] 			= (ENVIRONMENT == 'development');
$config['log_files_for'] 		= 3;				// how many months should log files be kept
$config['sql_log'] 				= 3;				// 0 = none, 1 = delete, 2 = 1 + update, 3 = 2 + insert
$config['sql_log_days'] 		= 30;				// how many days should SQL logs be kept
//które tabele pominąć przy zapisywaniu logów SQL
$config['sql_log_ignore_tables'] = array('admin_users_tracking', 'sessions', 'modules', 'sql_log', 'captcha');

$config['template_a'] 			= '';				// default admin template
$config['template_f'] 			= 'default/';		// default frontend template
$config['gfx_a'] 				= 'gfx/admin/';		// gfx admin path
$config['gfx_c'] 				= 'gfx/common/';	// gfx common path
$config['gfx_f'] 				= 'gfx/frontend/';	// gfx admin path
$config['external_path'] 		= 'external/';		// external scripts path
$config['admin_tracking'] 		= TRUE;				// TRUE = tracking admins browser
$config['backup_path'] 			= FCPATH.'backup/';	// path of backup files storage
$config['backup_login'] 		= $_SERVER['SERVER_ADDR'] == '127.0.0.1' ? FALSE : TRUE; // backup database after correct login
$config['bad_login_limit'] 		= 3;				// limit login attempts
$config['login_lock_time'] 		= 5;				// lock login after login_limit_try for % seconds
$config['bad_remind_limit'] 	= 3;				// limit remind attempts
$config['remind_lock_time'] 	= 5;				// lock remind after remind_limit_try for % seconds
$config['artneo_password'] 		= TRUE;

$config['error_delimiters'] 	= array ('start' => '','end' => '<br />');

$config['main_account_protect'] = TRUE;
$config['password_salt'] 		= 'xjm^#5kgf)6';

$config['main_lang'] 			= 'pl';
$config['main_admin_lang'] 			= 'pl';
$config['main_art']		 		= 1;
$config['main_menu'] 			= 1;
$config['main_id'] 				= 1;
$config['mobile_site_enabled'] 	= FALSE;
$config['breadcrumbs_enabled'] 	= FALSE;

$min = (ENVIRONMENT == 'development' ? '' : '.min');
$min = '.min';

//główne pliki css frontendu
$config['frontend_header_css'] = array(
	array('file' => 'https://cdn.plyr.io/3.3.21/plyr.css'),
	array('file' => 'https://fonts.googleapis.com/icon?family=Material+Icons'),
	array('file' => 'css/style'.$min.'.css'),
	array('file' => $config['gfx_f'].'css/cms.css')
);
//główne pliki js frontendu
$config['frontend_header_js']  = array(
	// array('file' => 'js/scripts.js', 'position' => 'first'),
	//array('file' => 'js/frontend'.$min.'.js')
);
//pliki js przed </body>
$config['footer_js']  = array(
	//array('file' => 'js/scripts.js', 'position' => 'first'),
	array('file' => 'js/app'.$min.'.js')
);

//główne pliki css panelu administracyjnego
$config['admin_header_css'] = array(
	array('file' => $config['external_path'].'bootstrap/css/bootstrap.min.css'),
	array('file' => $config['external_path'].'AdminLTE/css/AdminLTE.min.css'),
	array('file' => $config['external_path'].'jquery-ui/jquery-ui.min.css'),
	array('file' => $config['external_path'].'font-awesome/css/font-awesome.min.css'),
	array('file' => $config['external_path'].'lightGallery/css/lightgallery.min.css'),
	array('file' => $config['external_path'].'toastr/toastr.css'),
	array('file' => $config['gfx_a'].'css/selectize/selectize.bootstrap3.css'),
	array('file' => $config['gfx_a'].'css/neocms.css', 'position' => 'last')
);
//główne pliki js panelu administracyjnego
$config['admin_header_js']  = array(
	array('file' => $config['gfx_a'].'js/scripts.js', 'position' => 'first'),
	array('file' => $config['external_path'].'bootstrap/js/bootstrap.min.js'),
	array('file' => $config['external_path'].'AdminLTE/js/app.js'),
	array('file' => $config['external_path'].'lightGallery/js/lightgallery.min.js'),
	array('file' => $config['external_path'].'lightGallery/js/lg-thumbnail.min.js'),
	array('file' => $config['external_path'].'toastr/toastr.min.js'),
	array('file' => $config['gfx_a'].'js/jquery.isloading.min.js'),
	array('file' => $config['gfx_a'].'js/jquery.slimscroll.min.js'),
	array('file' => $config['gfx_a'].'js/selectize/selectize.min.js'),
	array('file' => $config['gfx_a'].'js/neocms.js', 'position' => 'last')
);

//główne pliki css elfinder
$config['elfinder_header_css'] = array(
	array('file' => $config['external_path'].'jquery-ui/jquery-ui.min.css'),
	array('file' => $config['external_path'].'elfinder/css/elfinder.min.css'),
	array('file' => $config['external_path'].'elfinder/css/theme.css')
);
//główne pliki js elfinder
$config['elfinder_header_js']  = array(
	array('file' => $config['external_path'].'jquery/jquery.min.js'),
	array('file' => $config['external_path'].'jquery-ui/jquery-ui.min.js'),
	array('file' => $config['external_path'].'elfinder/js/elfinder.min.js'),
	array('file' => $config['external_path'].'elfinder/js/i18n/elfinder.pl.js')
);

//$config['dm_to_lang'] = array('domena.en'=>'en','domena.pl'=>'pl');
$config['dm_to_lang'] = array();